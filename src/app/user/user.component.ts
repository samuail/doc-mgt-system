import { Component, OnInit, OnDestroy, ViewChild } from "@angular/core";
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from "@angular/animations";
import { first } from "rxjs/operators";
import { Subscription } from "rxjs";
import { MatPaginator } from "@angular/material/paginator";
import { MatSnackBar } from "@angular/material/snack-bar";
import { MatSort } from "@angular/material/sort";
import { MatTableDataSource, MatTable } from "@angular/material/table";
import { Router } from "@angular/router";

import { Message } from "src/app/_models/_helpers/message";

import { User } from "src/app/_models/user";
import { UserService } from "src/app/_services/user.service";

import { Role } from "src/app/_models/role";
import { RoleService } from "src/app/_services/role.service";

import { Menu } from "src/app/_models/menu";
import { MenuItem } from "src/app/_models/menu-item";
import { MenuService } from "src/app/_services/menu.service";

import { Department } from "src/app/_models/department";
import { DepartmentItem } from "src/app/_models/department-item";
import { DepartmentService } from "src/app/_services/department.service";

import { UserDepartmentRole } from "src/app/_models/user-department-role";

import { AuthenticationService } from "src/app/_services/authentication.service";
import { InfoDialogService } from "src/app/_services/info-dialog.service";

@Component({
  selector: "app-user",
  templateUrl: "./user.component.html",
  styleUrls: ["./user.component.css"],
  animations: [
    trigger("detailExpand", [
      state(
        "collapsed",
        style({ height: "0px", minHeight: "0", display: "none" })
      ),
      state("expanded", style({ height: "*" })),
      transition(
        "expanded <=> collapsed",
        animate("225ms cubic-bezier(0.4, 0.0, 0.2, 1)")
      ),
    ]),
  ],
})
export class UserComponent implements OnInit, OnDestroy {
  subManager = new Subscription();

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatTable, { static: true }) table: MatTable<any>;

  title: string;
  message: Message = new Message();

  users: User[] = [];
  user: User = new User();

  menuItems: MenuItem[] = [];
  menus: Menu[] = [];
  menu: Menu = new Menu();

  parentMenus: Menu[] = [];
  allMenus: Menu[] = [];

  userRoles: Role[] = [];
  roles: Role[] = [];
  role: Role = new Role();

  department: Department = new Department();
  departments: Department[] = [];
  departmentItems: DepartmentItem[] = [];

  userDepartmentRole: UserDepartmentRole = new UserDepartmentRole();

  dataSource = new MatTableDataSource<User>();
  displayedColumns = ["firstName", "lastName", "email", "operations"];
  expandedUser: User | null;

  loading = false;
  resultsLength = 0;

  menuActions: string[] = [];

  canAdd: boolean = false;
  canEdit: boolean = false;
  canDelete: boolean = false;
  canAssignRole: boolean = false;
  canAssignMenus: boolean = false;
  canMenuActions: boolean = false;
  canChangeStatus: boolean = false;
  canAssignDepartments: boolean = false;

  constructor(
    private router: Router,
    public snackBar: MatSnackBar,
    private userService: UserService,
    private authenticationService: AuthenticationService,
    private menuService: MenuService,
    private roleService: RoleService,
    private departmentService: DepartmentService,
    public infoDialogService: InfoDialogService
  ) {
    this.menuActions = this.menuService.getMenuActions("Users");
    for (var action of this.menuActions) {
      if (action == "Add") {
        this.canAdd = true;
      } else if (action == "Edit") {
        this.canEdit = true;
      } else if (action == "Delete") {
        this.canDelete = true;
      } else if (action == "Assign Roles") {
        this.canAssignRole = true;
      } else if (action == "Assign Menus") {
        this.canAssignMenus = true;
      } else if (action == "Menu Actions") {
        this.canMenuActions = true;
      } else if (action == "Change Status") {
        this.canChangeStatus = true;
      } else if (action == "Assign Departments") {
        this.canAssignDepartments = true;
      }
    }
  }

  ngOnInit(): void {
    this.getAll();
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngOnDestroy() {
    this.subManager.unsubscribe();
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  getAll(): void {
    this.loading = true;
    let getAll = this.userService
      .getAll()
      .pipe(first())
      .subscribe(
        (response) => {
          if (response.success) {
            for (var value of response.data) {
              this.user = {
                id: value["id"],
                firstName: value["first_name"],
                lastName: value["last_name"],
                userName: value["email"],
                password: "",
                active: value["active"],
                token: "",
              };
              this.users.push(this.user);
            }
            this.loading = false;
            this.resultsLength = response.total;
            this.dataSource.data = this.users;
          } else {
            this.message.title = "Users";
            this.message.type = "error";
            this.message.content = response.errors;
            const infoDialogRef = this.infoDialogService.openInfoDialog(
              this.message
            );
            infoDialogRef.afterClosed().subscribe(() => {
              if (response.errors[0] == "Signature has expired") {
                this.authenticationService.logout_offline();
                this.router.navigate(["/login"]);
              }
            });
          }
        },
        (error) => {
          this.loading = false;
          this.infoDialogService.openInfoDialog(error);
        }
      );
    this.subManager.add(getAll);
  }

  addUser() {
    this.title = "Add User";
    const dialogRef = this.userService.openUserDialog(this.title, new User());
    const sub = dialogRef.componentInstance.onUserSave.subscribe((data) => {
      if (data) {
        data.loading = true;
        let create = this.userService
          .create(data.form.value)
          .pipe(first())
          .subscribe(
            (response) => {
              if (response.success) {
                this.message.title = this.title;
                this.message.content = [response.message];
                this.message.type = "success";
                const infoDialogRef = this.infoDialogService.openInfoDialog(
                  this.message
                );
                infoDialogRef.afterClosed().subscribe(() => {
                  this.user = {
                    id: response.data["id"],
                    firstName: response.data["first_name"],
                    lastName: response.data["last_name"],
                    userName: response.data["email"],
                    password: "",
                    active: response.data["active"],
                    token: "",
                  };
                  this.users.push(this.user);
                  this.resultsLength = this.resultsLength + 1;
                  this.dataSource = new MatTableDataSource(this.users);
                  this.dataSource.paginator = this.paginator;
                  this.dataSource.sort = this.sort;
                  this.table.renderRows();
                  data.close();
                });
              } else {
                this.message.title = this.title;
                this.message.content = [];
                this.message.type = "error";
                for (var value of response.errors) {
                  this.message.content.push(value);
                }
                const infoDialogRef = this.infoDialogService.openInfoDialog(
                  this.message
                );
                infoDialogRef.afterClosed().subscribe(() => {
                  data.loading = false;
                });
              }
            },
            (error) => {
              data.loading = false;
              this.infoDialogService.openInfoDialog(error);
            }
          );
        this.subManager.add(create);
      }
    });

    dialogRef.afterClosed().subscribe(() => {
      sub.unsubscribe();
    });
  }

  editUser(user) {
    this.title = "Edit User";
    const dialogRef = this.userService.openUserDialog(this.title, user);
    const sub = dialogRef.componentInstance.onUserSave.subscribe((data) => {
      if (data) {
        data.loading = true;
        let update = this.userService
          .update(user.id, data.form.value)
          .pipe(first())
          .subscribe(
            (response) => {
              if (response.success) {
                this.message.title = this.title;
                this.message.content = [response.message];
                this.message.type = "success";
                const infoDialogRef = this.infoDialogService.openInfoDialog(
                  this.message
                );
                infoDialogRef.afterClosed().subscribe(() => {
                  this.users = this.users.filter((item) => item.id !== user.id);
                  this.user = {
                    id: response.data["id"],
                    firstName: response.data["first_name"],
                    lastName: response.data["last_name"],
                    userName: response.data["email"],
                    password: "",
                    active: response.data["active"],
                    token: "",
                  };
                  this.users.push(this.user);
                  this.dataSource = new MatTableDataSource(this.users);
                  this.dataSource.paginator = this.paginator;
                  this.dataSource.sort = this.sort;
                  this.table.renderRows();
                  data.close();
                });
              } else {
                this.message.title = this.title;
                this.message.content = [];
                this.message.type = "error";
                for (var value of response.errors) {
                  this.message.content.push(value);
                  // this.message.content += "_";
                }
                const infoDialogRef = this.infoDialogService.openInfoDialog(
                  this.message
                );
                infoDialogRef.afterClosed().subscribe(() => {
                  data.loading = false;
                });
              }
            },
            (error) => {
              data.loading = false;
              this.infoDialogService.openInfoDialog(error);
            }
          );
        this.subManager.add(update);
      }
    });

    dialogRef.afterClosed().subscribe(() => {
      sub.unsubscribe();
    });
  }

  defineUserStatus(user) {
    this.title = "De/Activate User";
    const dialogRef = this.userService.openUserDialog(this.title, user);
    const sub = dialogRef.componentInstance.onUserSave.subscribe((data) => {
      if (data) {
        data.loading = true;
        let changeUserStatus = this.userService
          .changeUserStatus(user.id, data.form.value.status)
          .pipe(first())
          .subscribe(
            (response) => {
              if (response.success) {
                this.message.title = this.title;
                this.message.content = [response.message];
                this.message.type = "success";
                const infoDialogRef = this.infoDialogService.openInfoDialog(
                  this.message
                );
                infoDialogRef.afterClosed().subscribe(() => {
                  this.users = this.users.filter((item) => item.id !== user.id);
                  this.user = {
                    id: user.id,
                    firstName: user.firstName,
                    lastName: user.lastName,
                    userName: user.userName,
                    password: "",
                    active: !user.active,
                    token: "",
                  };
                  this.users.push(this.user);
                  this.dataSource = new MatTableDataSource(this.users);
                  this.dataSource.paginator = this.paginator;
                  this.dataSource.sort = this.sort;
                  this.table.renderRows();
                  data.close();
                });
              }
            },
            (error) => {
              data.loading = false;
              this.infoDialogService.openInfoDialog(error);
            }
          );
        this.subManager.add(changeUserStatus);
      }
    });

    dialogRef.afterClosed().subscribe(() => {
      sub.unsubscribe();
    });
  }

  deleteUser(user) {
    let snackBarRef = this.snackBar.open(`Deleting user #${user.firstName}`);
  }

  defineUserMenus(user) {
    this.menus = [];
    this.menuItems = [];
    this.parentMenus = [];
    this.allMenus = [];
    let getUserMenus = this.userService
      .getUserMenus(user.id)
      .pipe(first())
      .subscribe(
        (response) => {
          if (response.success) {
            let getAllMenu = this.menuService
              .getAll()
              .pipe(first())
              .subscribe(
                (responseAll) => {
                  if (responseAll.success) {
                    for (var value of responseAll.data) {
                      this.menu = {
                        id: value["id"],
                        text: value["text"],
                        icon_cls: value["icon_cls"],
                        class_name: value["class_name"],
                        location: value["location"],
                        parent_id: value["parent_id"],
                        parent_name: null,
                      };
                      this.menus.push(this.menu);
                    }
                    this.parentMenus = this.menus.filter(
                      (item) => item.parent_id === null
                    );
                    for (var menu of this.parentMenus) {
                      this.allMenus.push(menu);
                      for (var child of this.menus.filter(
                        (item) => item.parent_id === menu["id"]
                      )) {
                        this.allMenus.push(child);
                      }
                    }
                    this.menuItems = response.data;
                    const dialogRef = this.menuService.openMenuDialog(
                      "Assign Menus",
                      this.menuItems,
                      this.menus,
                      []
                    );
                    this.assignUserMenus(dialogRef, this.allMenus, user);
                  } else {
                    this.message.title = "Assign Menus";
                    this.message.type = "error";
                    this.message.content = response.errors;
                    const infoDialogRef = this.infoDialogService.openInfoDialog(
                      this.message
                    );
                    infoDialogRef.afterClosed().subscribe(() => {
                      if (response.errors[0] == "Signature has expired") {
                        this.authenticationService.logout_offline();
                        this.router.navigate(["/login"]);
                      }
                    });
                  }
                },
                (error) => {
                  this.infoDialogService.openInfoDialog(error);
                }
              );
            this.subManager.add(getAllMenu);
          } else {
            this.message.title = "Assign Menus";
            this.message.type = "error";
            this.message.content = response.errors;
            const infoDialogRef = this.infoDialogService.openInfoDialog(
              this.message
            );
            infoDialogRef.afterClosed().subscribe(() => {
              if (response.errors[0] == "Signature has expired") {
                this.authenticationService.logout_offline();
                this.router.navigate(["/login"]);
              }
            });
          }
        },
        (error) => {
          this.infoDialogService.openInfoDialog(error);
        }
      );
    this.subManager.add(getUserMenus);
  }

  assignUserMenus(dialogRef, allMenus, user) {
    this.title = "Assign User Menu";
    const sub = dialogRef.componentInstance.onMenuSave.subscribe((data) => {
      if (data) {
        const selectedMenuIds = data.menuForm.value.allMenus
          .map((v, i) => (v ? allMenus[i].id : null))
          .filter((v) => v !== null);

        data.loading = true;
        let assignUserMenus = this.userService
          .assignUserMenus(user.id, selectedMenuIds)
          .pipe(first())
          .subscribe(
            (response) => {
              if (response.success) {
                this.message.title = this.title;
                this.message.content = [response.message];
                this.message.type = "success";
                const infoDialogRef = this.infoDialogService.openInfoDialog(
                  this.message
                );
                infoDialogRef.afterClosed().subscribe(() => {
                  this.table.renderRows();
                  data.close();
                });
              } else {
                this.message.title = this.title;
                this.message.type = "error";
                this.message.content = response.errors;
                const infoDialogRef = this.infoDialogService.openInfoDialog(
                  this.message
                );
                infoDialogRef.afterClosed().subscribe(() => {
                  if (response.errors[0] == "Signature has expired") {
                    data.close();
                    this.authenticationService.logout_offline();
                    this.router.navigate(["/login"]);
                  }
                });
              }
            },
            (error) => {
              data.loading = false;
              this.infoDialogService.openInfoDialog(error);
            }
          );
        this.subManager.add(assignUserMenus);
      }
    });

    dialogRef.afterClosed().subscribe(() => {
      sub.unsubscribe();
    });
  }

  defineUserMenuActions(user) {
    let menu_names = [];
    this.title = "Menu Actions";
    this.menuItems = [];
    let getUserMenus = this.userService
      .getUserMenus(user.id)
      .pipe(first())
      .subscribe(
        (response) => {
          if (response.success) {
            this.parentMenus = response.data.filter(
              (item) => item.parent_id === null
            );

            for (var menu of response.data) {
              for (var child of menu.children) {
                menu_names.push(child.displayName);
              }
            }

            let getMenuActions = this.userService
              .getMenuActions(menu_names)
              .pipe(first())
              .subscribe(
                (responseAct) => {
                  if (responseAct.success) {
                    let getAssignedMenuActions = this.userService
                      .getAssignedMenuActions(user.id)
                      .pipe(first())
                      .subscribe(
                        (responseAssigned) => {
                          if (responseAssigned.success) {
                            let allMenuActions = this.collapseTree(
                              responseAct.data
                            );
                            let assignedMenuActions = this.collapseTree(
                              responseAssigned.data
                            );
                            const dialogRef = this.userService.openUserMenuActionDialog(
                              this.title,
                              allMenuActions,
                              assignedMenuActions
                            );
                            this.assignUserMenuActions(
                              dialogRef,
                              allMenuActions,
                              user
                            );
                          } else {
                            this.message.title = this.title;
                            this.message.type = "error";
                            this.message.content = responseAct.errors;
                            const infoDialogRef = this.infoDialogService.openInfoDialog(
                              this.message
                            );
                            infoDialogRef.afterClosed().subscribe(() => {
                              if (
                                responseAct.errors[0] == "Signature has expired"
                              ) {
                                this.authenticationService.logout_offline();
                                this.router.navigate(["/login"]);
                              }
                            });
                          }
                        },
                        (error) => {
                          this.infoDialogService.openInfoDialog(error);
                        }
                      );
                    this.subManager.add(getAssignedMenuActions);
                  } else {
                    this.message.title = this.title;
                    this.message.type = "error";
                    this.message.content = responseAct.errors;
                    const infoDialogRef = this.infoDialogService.openInfoDialog(
                      this.message
                    );
                    infoDialogRef.afterClosed().subscribe(() => {
                      if (responseAct.errors[0] == "Signature has expired") {
                        this.authenticationService.logout_offline();
                        this.router.navigate(["/login"]);
                      }
                    });
                  }
                },
                (error) => {
                  this.infoDialogService.openInfoDialog(error);
                }
              );
            this.subManager.add(getMenuActions);
          } else {
            this.message.title = this.title;
            this.message.type = "error";
            this.message.content = response.errors;
            const infoDialogRef = this.infoDialogService.openInfoDialog(
              this.message
            );
            infoDialogRef.afterClosed().subscribe(() => {
              if (response.errors[0] == "Signature has expired") {
                this.authenticationService.logout_offline();
                this.router.navigate(["/login"]);
              }
            });
          }
        },
        (error) => {
          this.infoDialogService.openInfoDialog(error);
        }
      );
    this.subManager.add(getUserMenus);
  }

  collapseTree(tree) {
    let menu_action: any;
    let menu_actions: any[] = [];
    for (var parent of tree) {
      menu_action = {
        id: parent["parent_id"],
        text: parent["parent_text"],
        type: "Parent",
      };
      menu_actions.push(menu_action);
      for (var menu of parent["menus"]) {
        menu_action = {
          id: menu["menu_id"],
          text: menu["menu_text"],
          type: "Menu",
        };
        menu_actions.push(menu_action);
        for (var action of menu["actions"]) {
          menu_action = {
            menu_action_id: action["menu_action_id"],
            action_id: action["action_id"],
            text: action["action_name"],
            active: action["active"],
            type: "Action",
          };
          menu_actions.push(menu_action);
        }
      }
    }
    return menu_actions;
  }

  assignUserMenuActions(dialogRef, allMenuActions, user) {
    this.title = "Assign Menu Actions";
    const sub = dialogRef.componentInstance.onMenuActionSave.subscribe(
      (data) => {
        if (data) {
          const selectedMenuActionIds = data.menuActionForm.value.allMenuActions
            .map((v, i) =>
              v && allMenuActions[i]["type"] == "Action"
                ? allMenuActions[i].menu_action_id
                : null
            )
            .filter((v) => v !== null);
          data.loading = true;
          let assignUserMenuActions = this.userService
            .assignUserMenuActions(user.id, selectedMenuActionIds)
            .pipe(first())
            .subscribe(
              (response) => {
                if (response.success) {
                  this.message.title = this.title;
                  this.message.content = [response.message];
                  this.message.type = "success";
                  const infoDialogRef = this.infoDialogService.openInfoDialog(
                    this.message
                  );
                  infoDialogRef.afterClosed().subscribe(() => {
                    data.close();
                  });
                } else {
                  this.message.title = this.title;
                  this.message.type = "error";
                  this.message.content = response.errors;
                  const infoDialogRef = this.infoDialogService.openInfoDialog(
                    this.message
                  );
                  infoDialogRef.afterClosed().subscribe(() => {
                    if (response.errors[0] == "Signature has expired") {
                      data.close();
                      this.authenticationService.logout_offline();
                      this.router.navigate(["/login"]);
                    }
                  });
                }
              },
              (error) => {
                data.loading = false;
                this.infoDialogService.openInfoDialog(error);
              }
            );
          this.subManager.add(assignUserMenuActions);
        }
      }
    );
  }

  defineUserRole(user) {
    this.roles = [];
    this.userRoles = [];
    let getUserRoles = this.userService
      .getUserRoles(user.id)
      .pipe(first())
      .subscribe(
        (response) => {
          if (response.success) {
            let getAllRoles = this.roleService
              .getAll()
              .pipe(first())
              .subscribe(
                (responseAll) => {
                  if (responseAll.success) {
                    for (var value of responseAll.data) {
                      this.role = {
                        id: value["id"],
                        name: value["name"],
                      };
                      this.roles.push(this.role);
                    }
                    for (var value of response.data) {
                      this.role = {
                        id: value["id"],
                        name: value["name"],
                      };
                      this.userRoles.push(this.role);
                    }

                    const dialogRef = this.roleService.openRoleDialog(
                      "Assign Roles",
                      this.userRoles,
                      this.roles,
                      []
                    );
                    this.assignUserRoles(dialogRef, user, this.roles);
                  }
                },
                (error) => {
                  this.infoDialogService.openInfoDialog(error);
                }
              );
            this.subManager.add(getAllRoles);
          }
        },
        (error) => {
          this.infoDialogService.openInfoDialog(error);
        }
      );
    this.subManager.add(getUserRoles);
  }

  assignUserRoles(dialogRef, user, roles) {
    this.title = "Assign User Roles";
    const sub = dialogRef.componentInstance.onRoleSave.subscribe((data) => {
      const selectedRoleIds = [data.roleForm.value.allRoles];

      data.loading = true;

      let assignUserRoles = this.userService
        .assignUserRoles(user.id, selectedRoleIds)
        .pipe(first())
        .subscribe(
          (response) => {
            if (response.success) {
              this.message.title = this.title;
              this.message.content = [response.message];
              this.message.type = "success";
              const infoDialogRef = this.infoDialogService.openInfoDialog(
                this.message
              );
              infoDialogRef.afterClosed().subscribe(() => {
                this.table.renderRows();
                data.close();
              });
            }
          },
          (error) => {
            data.loading = false;
            this.infoDialogService.openInfoDialog(error);
          }
        );
      this.subManager.add(assignUserRoles);
    });

    dialogRef.afterClosed().subscribe(() => {
      sub.unsubscribe();
    });
  }

  defineUserDepartments(user) {
    this.roles = [];
    this.departments = [];
    let getUserDepartments = this.userService
      .getUserDepartments(user.id)
      .pipe(first())
      .subscribe(
        (response) => {
          if (response.success) {
            this.userDepartmentRole = response.data;
            let getAllRoles = this.roleService
              .getAll()
              .pipe(first())
              .subscribe(
                (roleResponse) => {
                  if (roleResponse.success) {
                    for (var value of roleResponse.data) {
                      this.role = {
                        id: value["id"],
                        name: value["name"],
                      };
                      this.roles.push(this.role);
                    }
                  } else {
                  }
                },
                (error) => {
                  this.infoDialogService.openInfoDialog(error);
                }
              );
            this.subManager.add(getAllRoles);
            let getAllDepartments = this.departmentService
              .getAllForTree()
              .pipe(first())
              .subscribe(
                (departmentResponse) => {
                  if (departmentResponse.success) {
                    this.departmentItems = departmentResponse.data;
                    const dialogRef = this.userService.openUserDepartmentRoleDialog(
                      "User Departments",
                      this.userDepartmentRole,
                      this.departmentItems,
                      this.roles
                    );
                    this.assignUserDepartmentRoles(
                      dialogRef,
                      this.userDepartmentRole,
                      user
                    );
                  } else {
                    if (response.errors[0] == "Signature has expired") {
                      this.authenticationService.logout_offline();
                      this.router.navigate(["/login"]);
                    }
                  }
                },
                (error) => {
                  this.infoDialogService.openInfoDialog(error);
                }
              );
            this.subManager.add(getAllDepartments);            
          } else {
          }
        },
        (error) => {}
      );
    this.subManager.add(getUserDepartments);
  }

  assignUserDepartmentRoles(dialogRef, userDepartment, user) {
    this.title = "User Department Role";
    const sub = dialogRef.componentInstance.onUserDepartmentRoleSave.subscribe(
      (data) => {
        data.loading = true;
        let departmentId = data.userDepartmentForm.value.office;
        let roleId = data.userDepartmentForm.value.role;
        if (userDepartment) {
          let updateUserDepartmentRoles = this.userService
            .updateUserDepartmentRoles(
              userDepartment.id,
              user.id,
              departmentId,
              roleId
            )
            .pipe(first())
            .subscribe(
              (response) => {
                if (response.success) {
                  this.message.title = this.title;
                  this.message.content = [response.message];
                  this.message.type = "success";
                  const infoDialogRef = this.infoDialogService.openInfoDialog(
                    this.message
                  );
                  infoDialogRef.afterClosed().subscribe(() => {
                    data.close();
                  });
                } else {
                  this.message.title = this.title;
                  this.message.content = response.errors;
                  this.message.type = "error";
                  const infoDialogRef = this.infoDialogService.openInfoDialog(
                    this.message
                  );
                  infoDialogRef.afterClosed().subscribe(() => {
                    data.loading = false;
                  });
                }
              },
              (error) => {
                data.loading = false;
                this.infoDialogService.openInfoDialog(error);
              }
            );
          this.subManager.add(updateUserDepartmentRoles);
        } else {
          let assignUserDepartmentRoles = this.userService
            .assignUserDepartmentRoles(user.id, departmentId, roleId)
            .pipe(first())
            .subscribe(
              (response) => {
                if (response.success) {
                  this.message.title = this.title;
                  this.message.content = [response.message];
                  this.message.type = "success";
                  const infoDialogRef = this.infoDialogService.openInfoDialog(
                    this.message
                  );
                  infoDialogRef.afterClosed().subscribe(() => {
                    data.close();
                  });
                }
              },
              (error) => {
                data.loading = false;
                this.infoDialogService.openInfoDialog(error);
              }
            );
          this.subManager.add(assignUserDepartmentRoles);
        }
      }
    );
    dialogRef.afterClosed().subscribe(() => {
      sub.unsubscribe();
    });
  }
}
