import { Component, OnInit, Inject, EventEmitter } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";

import {
  FormGroup,
  FormBuilder,
  FormControl,
  FormArray,
  Validators,
  ValidatorFn,
} from "@angular/forms";

@Component({
  selector: "app-user-menu-action-dialog",
  templateUrl: "./user-menu-action-dialog.component.html",
  styleUrls: ["./user-menu-action-dialog.component.css"],
})
export class UserMenuActionDialogComponent implements OnInit {
  loading = false;
  menuActionForm: FormGroup;
  title: string;

  allMenuActions: any[] = [];
  assigned_menu_actions: any[] = [];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<UserMenuActionDialogComponent>
  ) {
    this.allMenuActions = [];
    this.assigned_menu_actions = [];
    this.title = data.title;
    this.allMenuActions = data.menu_actions;
    this.assigned_menu_actions = data.assigned_menu_actions;
  }

  ngOnInit() {
    const menuActionControls = this.allMenuActions.map((c) =>
      this.assigned_menu_actions.find((ob) => {
        if (c.type == "Action") {
          return ob.menu_action_id === c.menu_action_id;
        } else {
          return ob.text === c.text;
        }
      })
        ? new FormControl(true)
        : new FormControl(false)
    );

    this.menuActionForm = this.fb.group({
      allMenuActions: new FormArray(
        menuActionControls,
        this.minSelectedCheckboxes(1)
      ),
    });
  }

  minSelectedCheckboxes(min = 1) {
    const validator: ValidatorFn = (formArray: FormArray) => {
      const totalSelected = formArray.controls
        .map((control) => control.value)
        .reduce((prev, next) => (next ? prev + next : prev), 0);

      return totalSelected >= min ? null : { required: true };
    };

    return validator;
  }

  onMenuActionSave = new EventEmitter();

  save() {
    if (this.menuActionForm.invalid) {
      return;
    }
    this.onMenuActionSave.emit(this);
  }

  close() {
    this.dialogRef.close(false);
  }
}
