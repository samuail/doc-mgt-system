import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserMenuActionDialogComponent } from './user-menu-action-dialog.component';

describe('UserMenuActionDialogComponent', () => {
  let component: UserMenuActionDialogComponent;
  let fixture: ComponentFixture<UserMenuActionDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserMenuActionDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserMenuActionDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
