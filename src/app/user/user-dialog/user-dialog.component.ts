import { Component, OnInit, Inject, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

import { User } from '../../_models/user';
import { MustMatch } from '../../_helpers/must-match.validator';


@Component({
  selector: 'app-user-dialog',
  templateUrl: './user-dialog.component.html',
  styleUrls: ['./user-dialog.component.css']
})
export class UserDialogComponent implements OnInit {

  loading = false;
  private formSubmitAttempt: boolean;
  form: FormGroup;
  title: string;

  user: User = new User();

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, 
  			  private fb: FormBuilder,
  			  private dialogRef: MatDialogRef<UserDialogComponent>) {
 	this.title = data.title;
 	this.user = data.user;
  }

  ngOnInit() {
    if(this.title == 'Add User') {
    	this.form = this.fb.group({
    		firstName: ['', Validators.required],
      	lastName: ['', Validators.required],
      	userName:['', [Validators.required]],
    		password: ['', Validators.required],
    		passwordConfirmation: ['', Validators.required]
    	},{
        	validator: MustMatch('password', 'passwordConfirmation')
    	});    
    } else if(this.title == 'Edit User'){
    	this.form = this.fb.group({
    		firstName: [this.user.firstName, Validators.required],
      	lastName: [this.user.lastName, Validators.required],
      	userName:[this.user.userName, [Validators.required]]
    	});  
    } else if(this.title == 'De/Activate User'){
      status = String(this.user.active);
      this.form = this.fb.group({
        status: [status, Validators.required]
      });  
    } else {
      this.form = this.fb.group({
        oldPassword:['', [Validators.required]],
        password: ['', Validators.required],
        passwordConfirmation: ['', Validators.required]
      },{
          validator: MustMatch('password', 'passwordConfirmation')
      });
    }
  }

  isFieldInvalid(field: string) {
    return (
      (!this.form.get(field).valid && this.form.get(field).touched) ||
      (this.form.get(field).untouched && this.formSubmitAttempt)
    );
  }

  onUserSave = new EventEmitter();

  save() {
    if (this.form.invalid) {
          return;
    }
    this.onUserSave.emit(this);
  }

  close() {
	 this.dialogRef.close();
  }

}
