import { Component, OnInit, Inject, EventEmitter } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
import { first } from "rxjs/operators";
import { Subscription } from "rxjs";
import { Router } from "@angular/router";

import { Department } from "src/app/_models/department";
import { Role } from "src/app/_models/role";

import { UserDepartmentRole } from "src/app/_models/user-department-role";
import { DepartmentItem } from "src/app/_models/department-item";
import { Message } from "src/app/_models/_helpers/message";

import { AuthenticationService } from "src/app/_services/authentication.service";
import { InfoDialogService } from "src/app/_services/info-dialog.service";
import { DepartmentService } from "src/app/_services/department.service";

@Component({
  selector: "app-user-department-dialog",
  templateUrl: "./user-department-dialog.component.html",
  styleUrls: ["./user-department-dialog.component.css"],
})
export class UserDepartmentDialogComponent implements OnInit {
  loading = false;
  loadingDeptTree = false;
  userDepartmentForm: FormGroup;
  title: string;
  message: Message = new Message();

  subManager = new Subscription();

  userDepartmentRole: UserDepartmentRole = new UserDepartmentRole();  
  roles: Role[] = [];

  assignedRoles: Role[] = [];

  department: Department = new Department();
  departments: Department[] = [];
  subDepartment: Department = new Department();
  subDepartments: Department[] = [];

  departmentItems: DepartmentItem[] = [];
  collegeDepartmentItems: DepartmentItem[] = [];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<UserDepartmentDialogComponent>,
    private router: Router,
    private authenticationService: AuthenticationService,
    public infoDialogService: InfoDialogService,
    private departmentService: DepartmentService
  ) {
    this.title = data.title;
    this.userDepartmentRole = data.userDepartmentRole;
    this.departmentItems = data.departmentItems;
    this.roles = data.roles;
  }

  ngOnInit() {
    if (this.userDepartmentRole) {
      let office_id = this.userDepartmentRole.department_user_role.department_id;
      let role_id = this.userDepartmentRole.department_user_role.user_role_id;
      this.title = "Edit " + this.title;

      var centerCollege_id;
      var department_id;

      let colleges = this.departmentItems;
        for (var college of colleges) {
          for (var value of college["children"]){
            for (var value2 of value["children"]){
              if (value2.id == office_id) {
                centerCollege_id = college.id;
                department_id = value.id;
              }
            }            
          }          
        }

      this.onCollegeSelect(centerCollege_id);
      this.onDepartmentSelect(department_id);
      this.onSubDepartmentSelect(office_id);

      this.userDepartmentForm = this.fb.group({
        centerCollege:[centerCollege_id, Validators.required],
        department: [department_id, Validators.required],
        office: [office_id, Validators.required],
        role: [role_id, Validators.required],
      });
    } else {
      this.title = "Add " + this.title;
      this.userDepartmentForm = this.fb.group({
        centerCollege:["", Validators.required],        
        department: ["", Validators.required],
        office: ["", Validators.required],
        role: ["", Validators.required],
      });
    }
  }

  onUserDepartmentRoleSave = new EventEmitter();

  save() {
    if (this.userDepartmentForm.invalid) {
      return;
    }
    this.onUserDepartmentRoleSave.emit(this);
  }

  close() {
    this.dialogRef.close();
  }

  initDepartmentTree() {
    this.loadingDeptTree = true;
    let getAllforTree = this.departmentService
      .getAllForTree()
      .pipe(first())
      .subscribe(
        (response) => {
          if (response.success) {            
            this.departmentItems = response.data;
            this.loadingDeptTree = false;            
          } else {
            if (response.errors[0] == "Signature has expired") {
              this.authenticationService.logout_offline();
              this.router.navigate(["/login"]);
            }
          }
        },
        (error) => {
          this.infoDialogService.openInfoDialog(error);
        }
      );
    this.subManager.add(getAllforTree);
  }

  isCenter: boolean = false;
  isCollege: boolean = false;
  isOutgoingInternal: boolean = true;

  onCollegeSelect(deptId) {
    this.departments = [];
    this.collegeDepartmentItems = [];
    if (this.departmentItems != null) {
      let college = this.departmentItems.filter( (dept) => dept.id == deptId )[0];
      if (college["description"] == "Main Campus"){
        this.isCenter = true;
        this.isCollege = false;
        this.isOutgoingInternal = false;
      } else {
        this.isCenter = false;
        this.isCollege = true;
        this.isOutgoingInternal = false;
      }
      this.collegeDepartmentItems = college["children"];
      for (var value of this.collegeDepartmentItems) {
        this.department = {
          id: value["id"],
          code: value["code"],
          description: value["description"],
          department_type: value["department_type"],
        };
        this.departments.push(this.department);
      }
    }    
  }

  onDepartmentSelect(deptId) {
    this.subDepartments = [];
    if (this.collegeDepartmentItems.length > 0) {
      let department = this.collegeDepartmentItems.filter( (dept) => dept.id == deptId )[0]["children"];
      for (var value of department) {
        this.subDepartment = {
          id: value["id"],
          code: value["code"],
          description: value["description"],
          department_type: value["department_type"],
        };
        this.subDepartments.push(this.subDepartment);
      }
    }
  }

  assignRole (values) {
    let role: Role;
    let roles: Role [] = [];
    for (var value of values) {
      role = {
        id: value["id"],
        name: value["name"],
      };
      roles.push(role);
    }
    return roles;
  }

  onSubDepartmentSelect(deptId) {
    let getAssignedRoleForDepartment = this.departmentService
      .getAssignedRoleForDepartment(deptId)
      .pipe(first())
      .subscribe(
        (response) => {
          if (response.success) {
            this.assignedRoles = this.assignRole(response.data);
            this.assignedRoles.sort((a,b) => a.name.localeCompare(b.name));
          } else {
            this.message.title = this.title;
            this.message.type = "error";
            this.message.content = response.errors;
            const infoDialogRef = this.infoDialogService.openInfoDialog(
              this.message
            );
            infoDialogRef.afterClosed().subscribe(() => {
              if (response.errors[0] == "Signature has expired") {
                this.authenticationService.logout_offline();
                this.router.navigate(["/login"]);
              }
            });
          }
        },
        (error) => {
          this.infoDialogService.openInfoDialog(error);
        }
      );
    this.subManager.add(getAssignedRoleForDepartment);
  }
}
