import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserDepartmentDialogComponent } from './user-department-dialog.component';

describe('UserDepartmentDialogComponent', () => {
  let component: UserDepartmentDialogComponent;
  let fixture: ComponentFixture<UserDepartmentDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserDepartmentDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserDepartmentDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
