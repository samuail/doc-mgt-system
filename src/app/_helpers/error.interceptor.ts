import { Injectable } from "@angular/core";
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
} from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { catchError } from "rxjs/operators";

import { AuthenticationService } from "src/app/_services/authentication.service";

import { Message } from "src/app/_models/_helpers/message";

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  message: Message = new Message();

  constructor(private authenticationService: AuthenticationService) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    this.message.type = "error";
    return next.handle(request).pipe(
      catchError((err) => {
        if (err.status === 401) {
          this.authenticationService.logout();
          this.message.content = [err.error.errors];
          this.message.title = err.statusText;
        } else if (err.status === 500) {
          if (err.error != null) {
            if (err.error.message === "Signature has expired") {
              this.authenticationService.logout_offline();
              location.href = "";
              this.message.title = "User Session";
              this.message.content = [
                "Your session has expired, Please Log in again!",
              ];
            } else {
              this.message.title = "Internal Server Error";
              this.message.content = [err.message];
            }
          } else {
            this.message.title = "Internal Server Error";
            this.message.content = [err.message];
          }
        } else if (err.status === 404) {
          this.message.title = "Server Error";
          if (err.error.errors == undefined) {
            this.message.content = [err.message];
          } else {
            this.message.content = [err.error.errors];
          }
        } else {
          this.message.title = "Server Error";
          if (err.status === 0) {
            this.message.content = [
              "Your Application APi Server is not running. Please contact your Administrator!",
            ];
          } else {
            this.message.content = [err.statusText];
          }
        }

        return throwError(this.message);
      })
    );
  }
}
