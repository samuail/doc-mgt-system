import { Pipe, PipeTransform } from '@angular/core';

declare var $: any;

@Pipe({
    name: 'convertDate'
})

export class ConvertDatePipe implements PipeTransform {

  transform(date: string): string {
    var letter_dateGC = date;
    var jdGC = $.calendars.instance('gregorian').newDate( 
                parseInt(letter_dateGC.substr(0,4), 10), 
                parseInt(letter_dateGC.substr(5,2), 10), 
                parseInt(letter_dateGC.substr(8,2), 10)).toJD();

    var dateEC = $.calendars.instance('ethiopian').fromJD(jdGC);
    
    var day = dateEC["_day"] < 10 ? "0" + dateEC["_day"] : dateEC["_day"];
    var month = dateEC["_month"] < 10 ? "0" + dateEC["_month"] : dateEC["_month"];
    
    var letter_dateEC = day + "-" + month + "-" + dateEC["_year"];

    return letter_dateEC;
  }
  
}
