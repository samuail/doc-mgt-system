import { Pipe, PipeTransform } from '@angular/core';

declare var $: any;

@Pipe({
    name: 'truncateString'
})

export class TruncateStringPipe implements PipeTransform {

  transform(str: string): string {
    return (str.length > 40) ? str.substr(0, 39) : str;
  }
  
}
