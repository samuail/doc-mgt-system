import { Component, OnInit, Input, OnDestroy } from "@angular/core";

import { SingleDataSet, Label, Color, MultiDataSet } from 'ng2-charts';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { Subscription } from "rxjs";
import { first } from "rxjs/operators";
import { Router } from "@angular/router";

import { Message } from "src/app/_models/_helpers/message";
import { User } from "src/app/_models/user";
import { DepartmentItem } from "src/app/_models/department-item";

import { DataService } from "../../_core/data.service";
import { DashboardService } from "src/app/_services/dashboard/dashboard.service";
import { AuthenticationService } from "src/app/_services/authentication.service";
import { InfoDialogService } from "src/app/_services/info-dialog.service";
import { DepartmentService } from "src/app/_services/department.service";

@Component({
  selector: "app-user-dashboard",
  templateUrl: "./user-dashboard.component.html",
  styleUrls: ["./user-dashboard.component.css"],
})
export class UserDashboardComponent implements OnInit, OnDestroy {
  subManager = new Subscription();

  loading: boolean = false;
  loadingUserStatus: boolean = false;
  loadingDeptTree: boolean = false;
  message: Message = new Message();

  currentUser: User;
  departmentItems: DepartmentItem[] = [];

  dashboardData: any;
  constructor(public dataService: DataService, private translate: TranslateService,
    private dashboardService: DashboardService,
    private authenticationService: AuthenticationService,
    private router: Router,
    public infoDialogService: InfoDialogService,
    private departmentService: DepartmentService) {
      this.authenticationService.currentUser.subscribe(
        (x) => (this.currentUser = x)
      );
    }
  
  checkNumeric(value) {
    return !isNaN(parseFloat(value)) && !isNaN(value - 0);
  }

  ngOnInit() {
    this.initData();

    this.translate.get('crMgt.userDashboard.status.new').subscribe((res: string) => {
      this.usersChartLabels.push(res); 
      this.statusChartLabels.push(res);     
    });

    this.translate.get('crMgt.userDashboard.status.accepted').subscribe((res: string) => {
      this.usersChartLabels.push(res); 
      this.statusChartLabels.push(res);     
    });

    this.translate.get('crMgt.userDashboard.status.processing').subscribe((res: string) => {
      this.usersChartLabels.push(res); 
      this.statusChartLabels.push(res);     
    });

    this.translate.get('crMgt.userDashboard.status.archived').subscribe((res: string) => {
      this.usersChartLabels.push(res); 
      this.statusChartLabels.push(res);     
    });

    this.translate.get('crMgt.userDashboard.status.rejected').subscribe((res: string) => {
      this.usersChartLabels.push(res); 
      this.statusChartLabels.push(res);     
    });

    this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
      this.translate.get('crMgt.userDashboard.status.new').subscribe((res: string) => {
        this.usersChartLabels[0] = res;
        this.statusChartLabels[0] = res;        
        this.centerOfficeStatusData[0]["label"] = res;
        this.collegeOfficeStatusData[0]["label"] = res;
      });

      this.translate.get('crMgt.userDashboard.status.accepted').subscribe((res: string) => {
        this.usersChartLabels[1] = res;
        this.statusChartLabels[1] = res;
        this.centerOfficeStatusData[1]["label"] = res;
        this.collegeOfficeStatusData[1]["label"] = res;
        this.usersStatusData[0]["label"] = res;
      });

      this.translate.get('crMgt.userDashboard.status.processing').subscribe((res: string) => {
        this.usersChartLabels[2] = res;
        this.statusChartLabels[2] = res;
        this.centerOfficeStatusData[2]["label"] = res;
        this.collegeOfficeStatusData[2]["label"] = res;
      });

      this.translate.get('crMgt.userDashboard.status.archived').subscribe((res: string) => {
        this.usersChartLabels[3] = res;
        this.statusChartLabels[3] = res;
        this.centerOfficeStatusData[3]["label"] = res;
        this.collegeOfficeStatusData[3]["label"] = res;
      });

      this.translate.get('crMgt.userDashboard.status.rejected').subscribe((res: string) => {
        this.usersChartLabels[4] = res;
        this.statusChartLabels[4] = res;
        this.centerOfficeStatusData[4]["label"] = res;
        this.collegeOfficeStatusData[4]["label"] = res;
        this.usersStatusData[2]["label"] = res;
      });

      this.translate.get('crMgt.userDashboard.status.assigned').subscribe((res: string) => {        
        this.usersStatusData[1]["label"] = res;
      });
     
      this.translate.get('crMgt.userDashboard.incomingCenter').subscribe((res: string) => {
        this.correspondenceByMonthChartData[0]["label"] = res;
      });

      this.translate.get('crMgt.userDashboard.incomingCollege').subscribe((res: string) => {
        this.correspondenceByMonthChartData[1]["label"] = res;
      });

      this.translate.get('crMgt.userDashboard.incomingExternal').subscribe((res: string) => {
        this.correspondenceByMonthChartData[2]["label"] = res;
      });

      this.translate.get('crMgt.userDashboard.outgoing').subscribe((res: string) => {
        this.correspondenceByMonthChartData[3]["label"] = res;
      });
    });
  }

  ngOnDestroy() {
    this.subManager.unsubscribe();
  }

  initData() {
    this.initDepartmentTree();
    this.loading = true;
    let getDashboardData = this.dashboardService
      .getAll()
      .pipe(first())
      .subscribe(
        (response) => {
          if (response.success) {
            this.loading = false;
            this.dashboardData =  response.data;            
            let newIncoming = this.dashboardData.incoming_centers.reduce((n,x) => n + (x.status==="New"), 0);
            let acceptIncoming = this.dashboardData.incoming_centers.reduce((n,x) => n + (x.status==="Accepted"), 0);
            let processIncoming = this.dashboardData.incoming_centers.reduce((n,x) => n + (x.status==="Processing"), 0);
            let archivedIncoming = this.dashboardData.incoming_centers.reduce((n,x) => n + (x.status==="Archived"), 0);
            let rejectedIncoming = this.dashboardData.incoming_centers.reduce((n,x) => n + (x.status==="Rejected"), 0);

            let newIncomingCollege = this.dashboardData.incoming_colleges.reduce((n,x) => n + (x.status==="New"), 0);
            let acceptIncomingCollege = this.dashboardData.incoming_colleges.reduce((n,x) => n + (x.status==="Accepted"), 0);
            let processIncomingCollege = this.dashboardData.incoming_colleges.reduce((n,x) => n + (x.status==="Processing"), 0);
            let archivedIncomingCollege = this.dashboardData.incoming_colleges.reduce((n,x) => n + (x.status==="Archived"), 0);
            let rejectedIncomingCollege = this.dashboardData.incoming_colleges.reduce((n,x) => n + (x.status==="Rejected"), 0);
            
            let newIncomingExternal = this.dashboardData.incoming_externals.reduce((n,x) => n + (x.status==="New"), 0);
            let acceptIncomingExternal = this.dashboardData.incoming_externals.reduce((n,x) => n + (x.status==="Accepted"), 0);
            let processIncomingExternal = this.dashboardData.incoming_externals.reduce((n,x) => n + (x.status==="Processing"), 0);
            let archivedIncomingExternal = this.dashboardData.incoming_externals.reduce((n,x) => n + (x.status==="Archived"), 0);
            let rejectedIncomingExternal = this.dashboardData.incoming_externals.reduce((n,x) => n + (x.status==="Rejected"), 0);

            let newOutgoingInternal = this.dashboardData.outgoing_internals.reduce((n,x) => n + (x.status==="New"), 0);
            let acceptOutgoingInternal = this.dashboardData.outgoing_internals.reduce((n,x) => n + (x.status==="Accepted"), 0);
            let processOutgoingInternal = this.dashboardData.outgoing_internals.reduce((n,x) => n + (x.status==="Processing"), 0);
            let archivedOutgoingInternal = this.dashboardData.outgoing_internals.reduce((n,x) => n + (x.status==="Archived"), 0);
            let rejectedOutgoingInternal = this.dashboardData.outgoing_internals.reduce((n,x) => n + (x.status==="Rejected"), 0);

            let newOutgoingExternal = this.dashboardData.outgoing_externals.reduce((n,x) => n + (x.status==="New"), 0);
            let acceptOutgoingExternal = this.dashboardData.outgoing_externals.reduce((n,x) => n + (x.status==="Accepted"), 0);
            let processOutgoingExternal = this.dashboardData.outgoing_externals.reduce((n,x) => n + (x.status==="Processing"), 0);
            let archivedOutgoingExternal = this.dashboardData.outgoing_externals.reduce((n,x) => n + (x.status==="Archived"), 0);
            let rejectedOutgoingExternal = this.dashboardData.outgoing_externals.reduce((n,x) => n + (x.status==="Rejected"), 0);

            let centerResult = this.dashboardData.incoming_centers.reduce((h, obj) => Object.assign(h, { [obj.source.id]:( h[obj.source.id] || [] ).concat(obj) }), {});
            let centerDataNew = [];
            let centerDataAccepted = [];
            let centerDataProcessing = [];
            let centerDataArchived = [];
            let centerDataRejected = [];
            let centerSortedResult = Object.entries(centerResult).sort((a, b) => (a.values.length < b.values.length ? 1 : -1));
            Object.entries(centerSortedResult).forEach(
              ([key, value]) => {
                this.centerOfficeStatusLabels.push(value[1][0]["source"]["code"]);                
                centerDataNew.push(centerSortedResult[key][1].reduce((n,x) => n + (x.status==="New"), 0));
                centerDataAccepted.push(centerSortedResult[key][1].reduce((n,x) => n + (x.status==="Accepted"), 0));
                centerDataProcessing.push(centerSortedResult[key][1].reduce((n,x) => n + (x.status==="Processing"), 0));
                centerDataArchived.push(centerSortedResult[key][1].reduce((n,x) => n + (x.status==="Archived"), 0));
                centerDataRejected.push(centerSortedResult[key][1].reduce((n,x) => n + (x.status==="Rejected"), 0));
              }
            ); 
            let newLabel = "";
            this.translate.get('crMgt.userDashboard.status.new').subscribe((res: string) => {
              newLabel = res;
            });
            let acceptedLabel = "";
            this.translate.get('crMgt.userDashboard.status.accepted').subscribe((res: string) => {
              acceptedLabel = res;
            });
            let processingLabel = "";
            this.translate.get('crMgt.userDashboard.status.processing').subscribe((res: string) => {
              processingLabel = res;
            });
            let archivedLabel = "";
            this.translate.get('crMgt.userDashboard.status.archived').subscribe((res: string) => {
              archivedLabel = res;
            });
            let rejectedLabel = "";
            this.translate.get('crMgt.userDashboard.status.rejected').subscribe((res: string) => {
              rejectedLabel = res;
            });
            this.centerOfficeStatusData.push(
              { data: centerDataNew, label: newLabel, stack: 'a', backgroundColor: 'rgba(54, 162, 235)', hoverBackgroundColor: "154360" },
              { data: centerDataAccepted, label: acceptedLabel, stack: 'a', backgroundColor: '#27AE60', hoverBackgroundColor: "rgb(20, 90, 50)" },
              { data: centerDataProcessing, label: processingLabel, stack: 'a', backgroundColor: 'rgba(255, 206, 86, 0.8)', hoverBackgroundColor: "#F39C12" },
              { data: centerDataArchived, label: archivedLabel, stack: 'a', backgroundColor: '#4D5360', hoverBackgroundColor: "#17202A" },
              { data: centerDataRejected, label: rejectedLabel, stack: 'a', backgroundColor: 'rgba(255, 99, 132)', hoverBackgroundColor: "rgba(100, 30, 22)" }
            ); 
            
            let collegeResult = this.dashboardData.incoming_colleges.reduce((h, obj) => Object.assign(h, { [obj.source.id]:( h[obj.source.id] || [] ).concat(obj) }), {})            
            let collegeDataNew = [];
            let collegeDataAccepted = [];
            let collegeDataProcessing = [];
            let collegeDataArchived = [];
            let collegeDataRejected = [];
            let collegeSortedResult = Object.entries(collegeResult).sort((a, b) => (a.values.length < b.values.length ? 1 : -1));
            Object.entries(collegeSortedResult).forEach(
              ([key, value]) => {
                this.collegeOfficeStatusLabels.push(value[1][0]["source"]["code"]);
                collegeDataNew.push(collegeSortedResult[key][1].reduce((n,x) => n + (x.status==="New"), 0));
                collegeDataAccepted.push(collegeSortedResult[key][1].reduce((n,x) => n + (x.status==="Accepted"), 0));
                collegeDataProcessing.push(collegeSortedResult[key][1].reduce((n,x) => n + (x.status==="Processing"), 0));
                collegeDataArchived.push(collegeSortedResult[key][1].reduce((n,x) => n + (x.status==="Archived"), 0));
                collegeDataRejected.push(collegeSortedResult[key][1].reduce((n,x) => n + (x.status==="Rejected"), 0));
              }
            ); 
            
            this.collegeOfficeStatusData.push(
              { data: collegeDataNew, label: newLabel, stack: 'a', backgroundColor: 'rgba(54, 162, 235)', hoverBackgroundColor: "154360" },
              { data: collegeDataAccepted, label: acceptedLabel, stack: 'a', backgroundColor: '#27AE60', hoverBackgroundColor: "rgb(20, 90, 50)" },
              { data: collegeDataProcessing, label: processingLabel, stack: 'a', backgroundColor: 'rgba(255, 206, 86, 0.8)', hoverBackgroundColor: "#F39C12" },
              { data: collegeDataArchived, label: archivedLabel, stack: 'a', backgroundColor: '#4D5360', hoverBackgroundColor: "#17202A" },
              { data: collegeDataRejected, label: rejectedLabel, stack: 'a', backgroundColor: 'rgba(255, 99, 132)', hoverBackgroundColor: "rgba(100, 30, 22)" }
            );

            let officeOutgoingResult = this.dashboardData.outgoing_internals.reduce((h, obj) => Object.assign(h, { [obj.source.id]:( h[obj.source.id] || [] ).concat(obj) }), {})            
            let officeOutgoingDataNew = [];
            let officeOutgoingDataAccepted = [];
            let officeOutgoingDataProcessing = [];
            let officeOutgoingDataArchived = [];
            let officeOutgoingDataRejected = [];
            let officeOutgoingSortedResult = Object.entries(officeOutgoingResult).sort((a, b) => (a.values.length < b.values.length ? 1 : -1));
            Object.entries(officeOutgoingSortedResult).forEach(
              ([key, value]) => {
                this.officeOutgoingStatusLabels.push(value[1][0]["destination"]["code"]);
                officeOutgoingDataNew.push(officeOutgoingSortedResult[key][1].reduce((n,x) => n + (x.status==="New"), 0));
                officeOutgoingDataAccepted.push(officeOutgoingSortedResult[key][1].reduce((n,x) => n + (x.status==="Accepted"), 0));
                officeOutgoingDataProcessing.push(officeOutgoingSortedResult[key][1].reduce((n,x) => n + (x.status==="Processing"), 0));
                officeOutgoingDataArchived.push(officeOutgoingSortedResult[key][1].reduce((n,x) => n + (x.status==="Archived"), 0));
                officeOutgoingDataRejected.push(officeOutgoingSortedResult[key][1].reduce((n,x) => n + (x.status==="Rejected"), 0));
              }
            );

            this.officeOutgoingStatusData.push(
              { data: officeOutgoingDataNew, label: newLabel, stack: 'a', backgroundColor: 'rgba(54, 162, 235)', hoverBackgroundColor: "154360" },
              { data: officeOutgoingDataAccepted, label: acceptedLabel, stack: 'a', backgroundColor: '#27AE60', hoverBackgroundColor: "rgb(20, 90, 50)" },
              { data: officeOutgoingDataProcessing, label: processingLabel, stack: 'a', backgroundColor: 'rgba(255, 206, 86, 0.8)', hoverBackgroundColor: "#F39C12" },
              { data: officeOutgoingDataArchived, label: archivedLabel, stack: 'a', backgroundColor: '#4D5360', hoverBackgroundColor: "#17202A" },
              { data: officeOutgoingDataRejected, label: rejectedLabel, stack: 'a', backgroundColor: 'rgba(255, 99, 132)', hoverBackgroundColor: "rgba(100, 30, 22)" }
            );
            
            this.incomingStatusChartData.push(
                { 
                  label: 'Center', 
                  data: [newIncoming, acceptIncoming, processIncoming, archivedIncoming, rejectedIncoming]
                },
                { 
                  label: 'College', 
                  data: [newIncomingCollege, acceptIncomingCollege, processIncomingCollege, archivedIncomingCollege, rejectedIncomingCollege]
                },
                { 
                  label: 'External', 
                  data: [newIncomingExternal, acceptIncomingExternal, processIncomingExternal, archivedIncomingExternal, rejectedIncomingExternal]
                }
            );
            this.incomingStatusChartData.forEach(ds => {
              ds.backgroundColor = ['rgba(54, 162, 235)',
                "#27AE60",
                'rgba(255, 206, 86, 0.8)',      
                "#4D5360",
                'rgba(255, 99, 132)'];              
            });

            this.outgoingStatusChartData.push(
              {
                label: 'Internal',
                data: [newOutgoingInternal, acceptOutgoingInternal, processOutgoingInternal, archivedOutgoingInternal, rejectedOutgoingInternal]
              },
              {
                label: 'External',
                data: [newOutgoingExternal, acceptOutgoingExternal, processOutgoingExternal, archivedOutgoingExternal, rejectedOutgoingExternal]
              }
            );
            this.outgoingStatusChartData.forEach(ds => {
              ds.backgroundColor = ['rgba(54, 162, 235)',
                "#27AE60",
                'rgba(255, 206, 86, 0.8)',      
                "#4D5360",
                'rgba(255, 99, 132)'];
            });

            let centerArchived = this.dashboardData.incoming_centers.filter((item) => item.status == "Archived");
            let collegeArchived = this.dashboardData.incoming_colleges.filter((item) => item.status == "Archived");
            let externalArchived = this.dashboardData.incoming_externals.filter((item) => item.status == "Archived");
            
            let incomingArchived = centerArchived.concat(collegeArchived.concat(externalArchived)).sort((a, b) => (a.archived_date < b.archived_date ? 1 : -1));
            let incomingArchivedDate = incomingArchived.reduce((h, obj) => Object.assign(h, { [obj.archived_date.substring(0,7)]:( h[obj.archived_date.substring(0,7)] || [] ).concat(obj) }), {});

            let sumMonth: number = 0;
            let sumMonthData = [];

            let count = 1;

            Object.entries(incomingArchivedDate).forEach(
              ([key, value]) => {                
                if (count <= 6) {
                  this.amountPaidChartLabels.splice(0, 0, this.determineMonthText(key.substring(5,7)));
                  Object.entries(value).forEach(
                    ([key2, value2]) => {
                      let amount = value2.amount;
                      sumMonth = sumMonth + (this.checkNumeric(amount) ? parseFloat(value2.amount) : 0);
                    }
                  );                  
                  sumMonthData.splice(0, 0, sumMonth.toFixed(2));
                  sumMonth = 0;
                  count++;
                }                
              }
            );
            this.amountPaidChartData.push({data: sumMonthData});

            let centerGroupByReceivedDate = this.dashboardData.incoming_centers.reduce((h, obj) => Object.assign(h, { [obj.received_date.substring(0,7)]:( h[obj.received_date.substring(0,7)] || [] ).concat(obj) }), {});
            let collegeGroupByReceivedDate = this.dashboardData.incoming_colleges.reduce((h, obj) => Object.assign(h, { [obj.received_date.substring(0,7)]:( h[obj.received_date.substring(0,7)] || [] ).concat(obj) }), {});
            let externalGroupByReceivedDate = this.dashboardData.incoming_externals.reduce((h, obj) => Object.assign(h, { [obj.received_date.substring(0,7)]:( h[obj.received_date.substring(0,7)] || [] ).concat(obj) }), {});
            let outgoingGroupByReceivedDate = this.dashboardData.outgoing_internals.concat(this.dashboardData.outgoing_externals).reduce((h, obj) => Object.assign(h, { [obj.received_date.substring(0,7)]:( h[obj.received_date.substring(0,7)] || [] ).concat(obj) }), {});
            let centerReceivedDateData = [];
            let collegeReceivedDateData = [];
            let externalReceivedDateData = [];
            let outgoingReceivedDateData = [];
            Object.entries(centerGroupByReceivedDate).forEach(
              ([key, value]) => {
                centerReceivedDateData.push({ type: "Center", month: key, count: centerGroupByReceivedDate[key].length });
              }
            );

            Object.entries(collegeGroupByReceivedDate).forEach(
              ([key, value]) => {
                collegeReceivedDateData.push({ type: "College", month: key, count: collegeGroupByReceivedDate[key].length });
              }
            );

            Object.entries(externalGroupByReceivedDate).forEach(
              ([key, value]) => {
                externalReceivedDateData.push({ type: "External", month: key, count: externalGroupByReceivedDate[key].length });
              }
            );

            Object.entries(outgoingGroupByReceivedDate).forEach(
              ([key, value]) => {
                outgoingReceivedDateData.push({ type: "Outgoing", month: key, count: outgoingGroupByReceivedDate[key].length });
              }
            );
            let result = centerReceivedDateData.concat(collegeReceivedDateData.concat(externalReceivedDateData.concat(outgoingReceivedDateData)));
            let resultSorted = result.sort((a, b) => (a.month < b.month ? 1 : -1));
            let resultGrouped = resultSorted.reduce((h, obj) => Object.assign(h, { [obj.month]:( h[obj.month] || [] ).concat(obj) }), {});
            let centerArray = [];
            let collegeArray = [];
            let externalArray = [];
            let outgoingArray = [];

            let countGrouping = 1;

            Object.entries(resultGrouped).forEach(
              ([key, value]) => {
                if (countGrouping <= 6){  
                  this.correspondenceByMonthChartLabels.splice(0, 0, this.determineMonthText(key.substring(5,7)));
                  let center = resultGrouped[key].filter((item) => item.type == "Center");
                  centerArray.splice(0, 0,center.length > 0 ? center[0]["count"] : 0);
                  let college = resultGrouped[key].filter((item) => item.type == "College");
                  collegeArray.splice(0, 0,college.length > 0 ? college[0]["count"] : 0);
                  let external = resultGrouped[key].filter((item) => item.type == "External");
                  externalArray.splice(0, 0,external.length > 0 ? external[0]["count"] : 0);
                  let outgoing = resultGrouped[key].filter((item) => item.type == "Outgoing");
                  outgoingArray.splice(0, 0,outgoing.length > 0 ? outgoing[0]["count"] : 0);
                  countGrouping++;
                }
              }
            );

            let centerLabel = "";
            this.translate.get('crMgt.userDashboard.incomingCenter').subscribe((res: string) => {
              centerLabel = res;
            });

            let collegeLabel = "";
            this.translate.get('crMgt.userDashboard.incomingCollege').subscribe((res: string) => {
              collegeLabel = res;
            });

            let externalLabel = "";
            this.translate.get('crMgt.userDashboard.incomingExternal').subscribe((res: string) => {
              externalLabel = res;
            });

            let outgoingLabel = "";
            this.translate.get('crMgt.userDashboard.outgoing').subscribe((res: string) => {
              outgoingLabel = res;
            });

            this.correspondenceByMonthChartData.push( 
              { data: centerArray, label: centerLabel, backgroundColor: '#2196F3', hoverBackgroundColor: "154360" }, 
              { data: collegeArray, label: collegeLabel, backgroundColor: '#28B463', hoverBackgroundColor: "154360" }, 
              { data: externalArray, label: externalLabel, backgroundColor: '#F44336', hoverBackgroundColor: "154360" }, 
              { data: outgoingArray, label: outgoingLabel, backgroundColor: '#4A235A', hoverBackgroundColor: "154360" });
          } else {
            this.message.title = "Dashboard";
            this.message.type = "error";
            this.message.content = response.errors;
            const infoDialogRef = this.infoDialogService.openInfoDialog(
              this.message
            );
            infoDialogRef.afterClosed().subscribe(() => {
              if (response.errors[0] == "Signature has expired") {
                this.authenticationService.logout_offline();
                this.router.navigate(["/login"]);
              }
            });
          }
        },
        (error) => {
          this.infoDialogService.openInfoDialog(error);
        }
      );
    
    this.loadingUserStatus = true;
    let getUserStatus = this.dashboardService
      .getUserStatus()
      .pipe(first())
      .subscribe(
        (response) => {
          if (response.success){
            this.loadingUserStatus = false;
            let assignedCount = 0;
            let acceptedCount = 0;
            let rejectedCount = 0;
            let currentUserData = [];
            let acceptedCountArray = [];
            let assignedCountArray = [];
            let rejectedCountArray = [];
            
            let userStatusData = response.data.reduce((h, obj) => Object.assign(h, { [obj.to_user.id]:( h[obj.to_user.id] || [] ).concat(obj) }), {});
            Object.entries(userStatusData).forEach(
            ([key, value]) => {        
              let full_name = userStatusData[key][0]["to_user"]["first_name"] + " " + userStatusData[key][0]["to_user"]["last_name"];
              let userStatusData2 = userStatusData[key].reduce((h, obj) => Object.assign(h, { [obj.correspondence_type]:( h[obj.correspondence_type] || [] ).concat(obj) }), {});
              Object.entries(userStatusData2).forEach(
                ([key2, value2]) => {
                  let userStatusData3 = userStatusData2[key2].reduce((h, obj) => Object.assign(h, { [obj.correspondence.id]:( h[obj.correspondence.id] || [] ).concat(obj) }), {});
                  Object.entries(userStatusData3).forEach(
                    ([key3, value3]) => {
                      if (userStatusData3[key3].length == 1) {
                        userStatusData3[key3][0]["status"] == "Assigned" ? assignedCount++ : userStatusData3[key3][0]["status"] == "Accepted" ? acceptedCount++ : rejectedCount++;
                      } else {
                        let sorted = userStatusData3[key3].sort((a, b) => (a.order > b.order) ? -1 : 1 );
                        sorted[0]["status"] == "Assigned" ? assignedCount++ : sorted[0]["status"] == "Accepted" ? acceptedCount++ : rejectedCount++;
                      }
                    }
                  );
                }
              );                
              this.usersStatusLabels.push(full_name);
              acceptedCountArray.push(acceptedCount);
              assignedCountArray.push(assignedCount);
              rejectedCountArray.push(rejectedCount);
              assignedCount = 0;
              acceptedCount = 0;
              rejectedCount = 0;
                              
              if (userStatusData[key][0]["to_user"]["id"] == this.currentUser.id) {
                currentUserData = userStatusData[key];                
              }
            });
            let acceptedLabel = "";
            this.translate.get('crMgt.userDashboard.status.accepted').subscribe((res: string) => {
              acceptedLabel = res;
            });

            let assignedLabel = "";
            this.translate.get('crMgt.userDashboard.status.assigned').subscribe((res: string) => {
              assignedLabel = res;
            });

            let rejectedLabel = "";
            this.translate.get('crMgt.userDashboard.status.rejected').subscribe((res: string) => {
              rejectedLabel = res;
            });
            this.usersStatusData.push(
                { data: acceptedCountArray, label: acceptedLabel, stack: 'a', backgroundColor: '#27AE60', hoverBackgroundColor: "rgb(20, 90, 50)" },
                { data: assignedCountArray, label: assignedLabel, stack: 'a', backgroundColor: '#BDC3C7', hoverBackgroundColor: "rgb(98, 101, 103)" },
                { data: rejectedCountArray, label: rejectedLabel, stack: 'a', backgroundColor: 'rgba(255, 99, 132)', hoverBackgroundColor: "rgba(100, 30, 22)" }
            );

            let currentUserAssigned = 0;
            let currentUserAccepted = 0;
            let currentUserRejected = 0;

            let currentUserData2 = currentUserData.reduce((h, obj) => Object.assign(h, { [obj.correspondence_type]:( h[obj.correspondence_type] || [] ).concat(obj) }), {});
            Object.entries(currentUserData2).forEach(
              ([key2, value2]) => {
                let userStatusData3 = currentUserData2[key2].reduce((h, obj) => Object.assign(h, { [obj.correspondence.id]:( h[obj.correspondence.id] || [] ).concat(obj) }), {});
                Object.entries(userStatusData3).forEach(
                  ([key3, value3]) => {
                    if (userStatusData3[key3].length == 1) {
                      userStatusData3[key3][0]["status"] == "Assigned" ? currentUserAssigned++ : userStatusData3[key3][0]["status"] == "Accepted" ? currentUserAccepted++ : currentUserRejected++;
                    } else {
                      let sorted = userStatusData3[key3].sort((a, b) => (a.order > b.order) ? -1 : 1 );
                      sorted[0]["status"] == "Assigned" ? currentUserAssigned++ : sorted[0]["status"] == "Accepted" ? currentUserAccepted++ : currentUserRejected++;
                    }
                  }
                );
              }
            );

            let total = currentUserAssigned + currentUserAccepted + currentUserRejected;
            this.valueAssigned = (currentUserAssigned * 100) / total;
            this.valueAccepted = (currentUserAccepted * 100) / total;
            this.valueRejected = (currentUserRejected * 100) / total;

            this.assignedStrokeDashoffset = 100 - this.valueAssigned;
            this.acceptedStrokeDashoffset = 100 - this.valueAccepted;
            this.rejectedStrokeDashoffset = 100 - this.valueRejected;
          } else {
            this.message.title = "Dashboard";
            this.message.type = "error";
            this.message.content = response.errors;
            const infoDialogRef = this.infoDialogService.openInfoDialog(
              this.message
            );
            infoDialogRef.afterClosed().subscribe(() => {
              if (response.errors[0] == "Signature has expired") {
                this.authenticationService.logout_offline();
                this.router.navigate(["/login"]);
              }
            });
          }
        },
        (error) => {
          this.infoDialogService.openInfoDialog(error);          
        }
      );

    this.subManager.add(getDashboardData);
    this.subManager.add(getUserStatus);
  }

  determineMonthText(month) {
    let monthText = "";
    switch (month) {
      case '01':
        monthText = "January";
        break;
      case '02':
        monthText = "February";
        break; 
      case '03':
        monthText = "March";
        break;
      case '04':
        monthText = "April";
        break;
      case '05':
        monthText = "May";
        break;
      case '06':
        monthText = "June";
        break;
      case '07':
        monthText = "July";
        break;
      case '08':
        monthText = "August";
        break;
      case '09':
        monthText = "September";
        break;
      case '10':
        monthText = "October";
        break;
      case '11':
        monthText = "November";
        break;
      default:
        monthText = "December";
        break;      
    }
    return monthText;
  }

  initDepartmentTree() {
    this.loadingDeptTree = true;
    let getAllforTree = this.departmentService
      .getAllForTree()
      .pipe(first())
      .subscribe(
        (response) => {
          if (response.success) {    
            this.loadingDeptTree = false;
            this.departmentItems = response.data;            
          } else {
            this.message.title = "Get Department Tree";
            this.message.type = "error";
            this.message.content = response.errors;
            const infoDialogRef = this.infoDialogService.openInfoDialog(
              this.message
            );
            infoDialogRef.afterClosed().subscribe(() => {
              if (response.errors[0] == "Signature has expired") {
                this.authenticationService.logout_offline();
                this.router.navigate(["/login"]);
              }
            });
          }
        },
        (error) => {
          this.infoDialogService.openInfoDialog(error);
        }
      );
    this.subManager.add(getAllforTree);
  }

  returnDepartmentTree(dept_code){  
    var departments = this.departmentItems;
    for (var college of departments) {
      for (var department of college["children"]) {
        for (var office of department["children"]) {
          if (office.code == dept_code) {
            return college["code"] + " / " + department["code"] + " / " + office["description"];
          }
        }
      }
    }
  }

  get acctType(): string {
    return this.dataService.acctType;
  }

  // Documents
  public documentsChartLabels: string[] = [
    "Incoming",
    "Outcoming",
    "Office Memo",
  ];
  public documentsChartData: number[] = [150, 200, 50];
  public documentsChartType: string = "doughnut";
  public documentsChartColors: Array<any> = [
    {
      hoverBorderColor: [
        "rgba(0, 0, 0, 0.1)",
        "rgba(0, 0, 0, 0.1)",
        "rgba(0, 0, 0, 0.1)",
      ],
      hoverBorderWidth: 0,
      backgroundColor: ["#4D5360", "#46BFBD", "#27AE60"],
      hoverBackgroundColor: ["#616774", "#5AD3D1", "#52BE80"],
    },
  ];

  public documentsChartOptions: any = {
    responsive: true,
  };

  // Document Origins
  public documentOriginsChartLabels: string[] = ["Internal", "External"];
  public documentOriginsChartData: number[] = [50, 150];
  public documentOriginsChartType: string = "doughnut";
  public documentOriginsChartColors: Array<any> = [
    {
      hoverBorderColor: ["rgba(0, 0, 0, 0.1)", "rgba(0, 0, 0, 0.1)"],
      hoverBorderWidth: 0,
      backgroundColor: ["#46BFBD", "#27AE60"],
      hoverBackgroundColor: ["#5AD3D1", "#52BE80"],
    },
  ];

  public documentOriginsChartOptions: any = {
    responsive: true,
  };

  // Work Units
  public workUnitsChartLabels: string[] = [
    "Director",
    "Team Leaders",
    "Others",
  ];
  public workUnitsChartData: number[] = [1, 8, 90];
  public workUnitsChartType: string = "doughnut";
  public workUnitsChartColors: Array<any> = [
    {
      hoverBorderColor: [
        "rgba(0, 0, 0, 0.1)",
        "rgba(0, 0, 0, 0.1)",
        "rgba(0, 0, 0, 0.1)",
      ],
      hoverBorderWidth: 0,
      backgroundColor: ["#46BFBD", "#27AE60", "#4D5360"],
      hoverBackgroundColor: ["#5AD3D1", "#52BE80", "#616774"],
    },
  ];

  public workUnitsChartOptions: any = {
    responsive: true,
  };

  //correspondence

  public barChartData: ChartDataSets [] = [
		{ data: [65, 59, 80, 81, 56, 55, 40], label: 'Center' },
		{ data: [28, 48, 40, 19, 86, 27, 90], label: 'College' },
    { data: [30, 65, 50, 60, 67, 25, 30], label: 'External'},
    { data: [30, 65, 50, 60, 67, 25, 30], label: 'Outgoing'}
  ];
  
  public barChartOptions: ChartOptions = {
		responsive: true,
		tooltips: {
			titleFontFamily: 'titillium web, sans-serif',
			titleMarginBottom: 10,
			bodyFontFamily: 'titillium web, sans-serif'
		},
		legend: {
			labels: {
				fontFamily: 'titillium web,sans-serif'
			}
		},
		scales:{
			xAxes:[{
				ticks: {					
					fontFamily: 'titillium web,sans-serif'
				}
			}],
			yAxes: [{
				ticks: {
					fontFamily: 'titillium web,sans-serif'
				}
			}]
		}
	};
	public barChartLabels: Label[] = ['Sept', 'Oct', 'Nov', 'Dec', 'Jan', 'Feb', 'Mar'];
	public barChartType: ChartType = 'bar';
	public barChartLegend = true;
  public barChartPlugins = [];

  public correspondenceByMonthChartData: ChartDataSets [] = [];
  
  public correspondenceByMonthChartOptions: ChartOptions = {
		responsive: true,
		tooltips: {
			titleFontFamily: 'titillium web, sans-serif',
			titleMarginBottom: 10,
			bodyFontFamily: 'titillium web, sans-serif'
		},
		legend: {
			labels: {
				fontFamily: 'titillium web,sans-serif'
			}
		},
		scales:{
			xAxes:[{
				ticks: {					
					fontFamily: 'titillium web,sans-serif'
				}
			}],
			yAxes: [{
				ticks: {
					fontFamily: 'titillium web,sans-serif'
				}
			}]
		}
	};
	public correspondenceByMonthChartLabels: Label[] = [];
	public correspondenceByMonthChartType: ChartType = 'bar';
	public correspondenceByMonthChartLegend = true;
  public correspondenceByMonthChartPlugins = [];
  
  public centerOfficeStatusData: ChartDataSets [] = [];
  public centerOfficeStatusLabels: Label[] = [];
  public centerOfficeStatusOptions: ChartOptions = {
    responsive: true,
    maintainAspectRatio: false,
    tooltips: {
			titleFontFamily: 'titillium web, sans-serif',
			titleMarginBottom: 10,
			bodyFontFamily: 'titillium web, sans-serif',
      callbacks: {
        title: (context) => {          
          var title = '';
          if (!this.loadingDeptTree) {  
            title = this.returnDepartmentTree(context[0].label);            
          }
          return title;
        }
      }
		},
		legend: {
			labels: {
				fontFamily: 'titillium web,sans-serif'
			}
		},
    scales:{
			xAxes:[{
				ticks: {					
					fontFamily: 'titillium web,sans-serif'
				}
			}],
			yAxes: [{
				ticks: {
					fontFamily: 'titillium web,sans-serif'
				}
			}]
		}
  };
  public centerOfficeStatusLegend = true;
  public centerOfficeStatusType: ChartType = 'horizontalBar';  

  public collegeOfficeStatusData: ChartDataSets [] = [];
  public collegeOfficeStatusLabels: Label[] = [];
  public collegeOfficeStatusOptions: ChartOptions = {
    responsive: true,
    maintainAspectRatio: false,
    tooltips: {
			titleFontFamily: 'titillium web, sans-serif',
			titleMarginBottom: 10,
			bodyFontFamily: 'titillium web, sans-serif',
      callbacks: {
        title: (context) => {          
          var title = '';
          if (!this.loadingDeptTree) {  
            title = this.returnDepartmentTree(context[0].label);            
          }
          return title;
        }
      }
		},
		legend: {
			labels: {
				fontFamily: 'titillium web,sans-serif'
			}
		},
    scales:{
			xAxes:[{
				ticks: {					
					fontFamily: 'titillium web,sans-serif'
				}
			}],
			yAxes: [{
				ticks: {
					fontFamily: 'titillium web,sans-serif'
				}
			}]
		}
  };
  public collegeOfficeStatusLegend = true;
  public collegeOfficeStatusType: ChartType = 'horizontalBar';

  public officeOutgoingStatusData: ChartDataSets [] = [];
  public officeOutgoingStatusLabels: Label[] = [];
  public officeOutgoingStatusOptions: ChartOptions = {
    responsive: true,
    maintainAspectRatio: false,
    tooltips: {
			titleFontFamily: 'titillium web, sans-serif',
			titleMarginBottom: 10,
			bodyFontFamily: 'titillium web, sans-serif',
      callbacks: {
        title: (context) => {          
          var title = '';
          if (!this.loadingDeptTree) {  
            title = this.returnDepartmentTree(context[0].label);            
          }
          return title;
        }
      }
		},
		legend: {
			labels: {
				fontFamily: 'titillium web,sans-serif'
			}
		},
    scales:{
			xAxes:[{
				ticks: {					
					fontFamily: 'titillium web,sans-serif'
				}
			}],
			yAxes: [{
				ticks: {
					fontFamily: 'titillium web,sans-serif'
				}
			}]
		}
  };
  public officeOutgoingStatusLegend = true;
  public officeOutgoingStatusType: ChartType = 'horizontalBar';

  public usersStatusData: ChartDataSets [] = [];
  public usersStatusLabels: Label[] = [];
  public usersStatusOptions: ChartOptions = {
    responsive: true,
    maintainAspectRatio: false,
    tooltips: {
			titleFontFamily: 'titillium web, sans-serif',
			titleMarginBottom: 10,
			bodyFontFamily: 'titillium web, sans-serif'
		},
		legend: {
			labels: {
				fontFamily: 'titillium web,sans-serif'
			}
		},
    scales:{
			xAxes:[{
				ticks: {					
					fontFamily: 'titillium web,sans-serif'
				}
			}],
			yAxes: [{
				ticks: {
					fontFamily: 'titillium web,sans-serif'
				}
			}]
		}
  };
  public usersStatusLegend = true;
  public usersStatusType: ChartType = 'horizontalBar';

  //correspondence with status  
  public usersChartLabels: string [] = [];
  public usersChartData: ChartDataSets[] = [
    {
      label: 'Internal',
      data: [3, 10, 20, 50, 5]
    },
    {
      label: 'External',
      data: [2, 10, 20, 50, 5]
    }
  ];
  public usersChartType:string = 'doughnut';
  public usersChartColors: Color[] = [
    {
      backgroundColor: [
        'rgba(54, 162, 235, 0.6)',
        'rgba(0, 148, 97, 0.6)',
        'rgba(255, 206, 86, 0.6)',      
        'rgba(0, 0, 0, 0.6)',
        'rgba(255, 99, 132, 0.6)',
      ]
    }
  ];

	public usersChartOptions: ChartOptions = {
		responsive: true,		
		tooltips: {
			bodyFontFamily: 'titillium web, sans-serif'
		},
		legend: {
			position: 'left',
			display: true,
			labels: {
				fontFamily: "titillium web,sans-serif"
			}
		}
  };

  // events
  public usersChartClicked(e: any): void {
    // console.log(e);
  }

  public usersChartHovered(e: any): void {
    // console.log(e);
  }

  centerHover: boolean = false;
  collegeHover: boolean = false;
  externalHover: boolean = false;

  internalOutgoingHover: boolean = false;
  externalOutgoingHover: boolean = false;

  public incomingStatusChartHovered(e: any): void {
    let hoverLabel = e.active[0]["_model"]["label"];
    switch (hoverLabel) {
      case 'Center':
        this.centerHover = true;
        this.collegeHover = false;
        this.externalHover = false;
        break;
      case 'College':
        this.centerHover = false;
        this.collegeHover = true;
        this.externalHover = false;
        break; 
      case 'External':
        this.centerHover = false;
        this.collegeHover = false;
        this.externalHover = true;
        break;    
      default:
        this.centerHover = false
        this.collegeHover = false;
        this.externalHover = false;
        break;
    }
  }

  public incomingStatusChartMuseOut(e: any): void {
    this.centerHover = false;
    this.collegeHover = false;
    this.externalHover = false;
  }

  public outgoingStatusChartHovered(e: any): void {
    let hoverLabel = e.active[0]["_model"]["label"];
    switch (hoverLabel) {
      case 'Internal':
        this.internalOutgoingHover = true;
        this.externalOutgoingHover = false;        
        break;
      case 'External':
        this.internalOutgoingHover = false;
        this.externalOutgoingHover = true;
        break;       
      default:
        this.internalOutgoingHover = false;
        this.externalOutgoingHover = false;
        break;
    }
  }

  public outgoingStatusChartMuseOut(e: any): void {
    this.internalOutgoingHover = false;
    this.externalOutgoingHover = false;
  }

  public incomingStatusChartData: ChartDataSets[] = [];
  
  public outgoingStatusChartData: ChartDataSets[] = [];
  
  public statusChartLabels: string [] = [];
  public statusChartType:string = 'doughnut';
  public statusChartOptions: ChartOptions = {
    responsive: true
  };
  
  amountPaidChartData: ChartDataSets[] = [];
    // { data: [85, 72, 78, 75, 77, 75], label: 'Crude oil prices' },
  // ];

  amountPaidChartLabels: Label[] = [];
  // ['January', 'February', 'March', 'April', 'May', 'June'];

  amountPaidChartOptions = {
    responsive: true,
    // maintainAspectRatio: false,
    tooltips: {
			titleFontFamily: 'titillium web, sans-serif',
			titleMarginBottom: 10,
			bodyFontFamily: 'titillium web, sans-serif',
      callbacks: {
        label: function(tooltipItem, data) {
          var value = data.datasets[0].data[tooltipItem.index];
          if(parseInt(value) >= 1000){
              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ' Birr';
          } else {
              return value + ' Birr';
          }
        }
      }
		},
		legend: {
			labels: {
				fontFamily: 'titillium web,sans-serif'
			}
		},
    scales:{
			xAxes:[{
				ticks: {					
					fontFamily: 'titillium web,sans-serif'
				}
			}],
			yAxes: [{
				ticks: {
					fontFamily: 'titillium web,sans-serif',
          callback: function(value, index, values) {
            return value.toLocaleString();
          }
				}
			}]
		}
  };

  amountPaidChartColors: Color[] = [
    {
      borderColor: 'black',
      backgroundColor: 'rgba(255,255,0,0.28)',
    },
  ];

  amountPaidChartLegend = false;
  amountPaidChartPlugins = [];
  amountPaidChartType = 'line';

  ////////////////////////
  // PolarArea
  	public polarAreaChartLabels: Label[] = ['Agent Sales', 'Self Sales', 'Corporate Sales'];
  	public polarAreaChartData: SingleDataSet = [100, 40, 120];
  	public polarAreaLegend = true;

    public polarAreaChartType: ChartType = 'polarArea';
    
  ///////////////////
  @Input() value: number = 82;
	@Input() value2: number = 50;
  @Input() value3: number = 50;

  @Input() valueAssigned: number = 0;
  @Input() valueAccepted: number = 0;
  @Input() valueRejected: number = 0;
  
  public circumference: number = 2 * Math.PI * 20;
	public strokeDashoffset: number = 20;
	public strokeDashoffset2: number = 55;
	public strokeDashoffset3: number = 40;
	public strokeDashoffset4: number = 30;

  public assignedStrokeDashoffset: number = 0;
  public acceptedStrokeDashoffset: number = 0;
  public rejectedStrokeDashoffset: number = 0;
}
