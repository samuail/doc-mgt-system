import { Component, OnInit, OnDestroy, Input } from "@angular/core";
import { Router } from "@angular/router";
import { LangChangeEvent, TranslateService } from "@ngx-translate/core";
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { Color, Label } from 'ng2-charts';
import { Subscription } from "rxjs";
import { first } from "rxjs/operators";

import { Message } from "src/app/_models/_helpers/message";
import { DepartmentItem } from "src/app/_models/department-item";
import { User } from "src/app/_models/user";

import { AuthenticationService } from "src/app/_services/authentication.service";
import { DashboardService } from "src/app/_services/dashboard/dashboard.service";
import { DepartmentService } from "src/app/_services/department.service";
import { InfoDialogService } from "src/app/_services/info-dialog.service";

@Component({
  selector: 'app-user-dashboard-new',
  templateUrl: './user-dashboard-new.component.html',
  styleUrls: ['./user-dashboard-new.component.css']
})
export class UserDashboardNewComponent implements OnInit, OnDestroy {

  subManager = new Subscription();
  loadingDeptTree: boolean = false;
  message: Message = new Message();

  currentUser: User;

  departmentItems: DepartmentItem[] = [];

  constructor(private router: Router,
        private translate: TranslateService,
        private dashboardService: DashboardService,
        private departmentService: DepartmentService,
        private infoDialogService: InfoDialogService,
        private authenticationService: AuthenticationService) {
    this.authenticationService.currentUser.subscribe(
      (x) => (this.currentUser = x)
    );
  }

  // Correspondence By Month Bar and Line Chart
  public correspondenceByMonthBChartData: ChartDataSets [] = [];  
  public correspondenceByMonthLChartData: ChartDataSets[] = [];
  
  public correspondenceByMonthChartOptions: ChartOptions = {
		responsive: true,
		tooltips: {
			titleFontFamily: 'titillium web, sans-serif',
			titleMarginBottom: 10,
			bodyFontFamily: 'titillium web, sans-serif'
		},
		legend: {
      position: 'left',
			labels: {
				fontFamily: 'titillium web,sans-serif'
			}
		},
		scales:{
			xAxes:[{
				ticks: {					
					fontFamily: 'titillium web,sans-serif'
				}
			}],
			yAxes: [{
				ticks: {
					fontFamily: 'titillium web,sans-serif'
				}
			}]
		}
	};
	public correspondenceByMonthChartLabels: Label[] = [];
	public correspondenceByMonthChartType: ChartType = 'bar';
	public correspondenceByMonthChartLegend = true;
  public correspondenceByMonthChartPlugins = [];

  lineChartOptions: ChartOptions = {
    responsive: true,
    tooltips: {
			titleFontFamily: 'titillium web, sans-serif',
			titleMarginBottom: 10,
			bodyFontFamily: 'titillium web, sans-serif',
      mode: 'index',
      callbacks: {
        labelColor: function(context, chart) {
          return {
              borderColor: chart.config.data.datasets[context.datasetIndex].borderColor.toString(),
              backgroundColor: chart.config.data.datasets[context.datasetIndex].borderColor.toString(),
          };
        },
      }
		},
		legend: {
      display: false,
			labels: {
				fontFamily: 'titillium web,sans-serif'
			}
		},
		scales:{
			xAxes:[{
				ticks: {					
					fontFamily: 'titillium web,sans-serif'
				}
			}],
			yAxes: [{
				ticks: {
					fontFamily: 'titillium web,sans-serif'
				}
			}]
		}
  };

  lineChartLegend = true;
  lineChartPlugins = [];
  lineChartType = 'line';
  public lineChartColors;
  

  // Correspondence Monthly Paid Line Chart
  amountPaidChartData: ChartDataSets[] = [];
  amountPaidChartLabels: Label[] = [];
  amountPaidChartOptions = {
    responsive: true,
    tooltips: {
			titleFontFamily: 'titillium web, sans-serif',
			titleMarginBottom: 10,
			bodyFontFamily: 'titillium web, sans-serif',
      callbacks: {
        label: function(tooltipItem, data) {
          var value = data.datasets[0].data[tooltipItem.index];
          if(parseInt(value) >= 1000){
              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ' Birr';
          } else {
              return value + ' Birr';
          }
        }
      }
		},
		legend: {
			labels: {
				fontFamily: 'titillium web,sans-serif'
			}
		},
    scales:{
			xAxes:[{
				ticks: {					
					fontFamily: 'titillium web,sans-serif'
				}
			}],
			yAxes: [{
				ticks: {
					fontFamily: 'titillium web,sans-serif',
          callback: function(value, index, values) {
            return value.toLocaleString();
          }
				}
			}]
		}
  };

  amountPaidChartColors: Color[] = [
    {
      borderColor: 'black',
      backgroundColor: 'rgba(255,255,0,0.28)',
    },
  ];

  amountPaidChartLegend = false;
  amountPaidChartPlugins = [];
  amountPaidChartType = 'line';

  // Incoming and Outgoing Status Doughnut Chart
  public centerStatusChartData: ChartDataSets[] = [];
  public collegeStatusChartData: ChartDataSets[] = [];
  public externalStatusChartData: ChartDataSets[] = [];
  public internalOutStatusChartData: ChartDataSets[] = [];
  public externalOutStatusChartData: ChartDataSets[] = [];
  public manuscriptStatusChartData: ChartDataSets[] = [];
  
  public statusChartLabels: Label[] = [];
  public outStatusChartLabels: Label[] = [];
  public manuscriptStatusChartLabels: Label[] = [];
  public statusChartType: ChartType = 'doughnut';
  public statusChartOptions: ChartOptions = {
    responsive: true,
    tooltips: {
			titleFontFamily: 'titillium web, sans-serif',
			titleMarginBottom: 10,
			bodyFontFamily: 'titillium web, sans-serif'
		},
  };

  // Office Status Details Horizontal Bar Chart
  public centerOfficeStatusData: ChartDataSets [] = [];
  public centerOfficeStatusLabels: Label[] = [];
  public centerOfficeStatusOptions: ChartOptions = {
    responsive: true,
    maintainAspectRatio: false,
    tooltips: {
			titleFontFamily: 'titillium web, sans-serif',
			titleMarginBottom: 10,
			bodyFontFamily: 'titillium web, sans-serif',
      callbacks: {
        title: (context) => {          
          var title = '';
          if (!this.loadingDeptTree) {  
            title = this.returnDepartmentTree(context[0].label);            
          }
          return title;
        }
      }
		},
		legend: {
			labels: {
				fontFamily: 'titillium web,sans-serif'
			}
		},
    scales:{
			xAxes:[{
				ticks: {					
					fontFamily: 'titillium web,sans-serif'
				}
			}],
			yAxes: [{
				ticks: {
					fontFamily: 'titillium web,sans-serif'
				}
			}]
		}
  };
  public centerOfficeStatusLegend = true;
  public centerOfficeStatusType: ChartType = 'horizontalBar';

  public collegeOfficeStatusData: ChartDataSets [] = [];
  public collegeOfficeStatusLabels: Label[] = [];
  public collegeOfficeStatusOptions: ChartOptions = {
    responsive: true,
    maintainAspectRatio: false,
    tooltips: {
			titleFontFamily: 'titillium web, sans-serif',
			titleMarginBottom: 10,
			bodyFontFamily: 'titillium web, sans-serif',
      callbacks: {
        title: (context) => {          
          var title = '';
          if (!this.loadingDeptTree) {  
            title = this.returnDepartmentTree(context[0].label);            
          }
          return title;
        }
      }
		},
		legend: {
			labels: {
				fontFamily: 'titillium web,sans-serif'
			}
		},
    scales:{
			xAxes:[{
				ticks: {					
					fontFamily: 'titillium web,sans-serif'
				}
			}],
			yAxes: [{
				ticks: {
					fontFamily: 'titillium web,sans-serif'
				}
			}]
		}
  };
  public collegeOfficeStatusLegend = true;
  public collegeOfficeStatusType: ChartType = 'horizontalBar';

  public officeOutgoingStatusData: ChartDataSets [] = [];
  public officeOutgoingStatusLabels: Label[] = [];
  public officeOutgoingStatusOptions: ChartOptions = {
    responsive: true,
    maintainAspectRatio: false,
    tooltips: {
			titleFontFamily: 'titillium web, sans-serif',
			titleMarginBottom: 10,
			bodyFontFamily: 'titillium web, sans-serif',
      callbacks: {
        title: (context) => {          
          var title = '';
          if (!this.loadingDeptTree) {  
            title = this.returnDepartmentTree(context[0].label);            
          }
          return title;
        }
      }
		},
		legend: {
			labels: {
				fontFamily: 'titillium web,sans-serif'
			}
		},
    scales:{
			xAxes:[{
				ticks: {					
					fontFamily: 'titillium web,sans-serif'
				}
			}],
			yAxes: [{
				ticks: {
					fontFamily: 'titillium web,sans-serif'
				}
			}]
		}
  };
  public officeOutgoingStatusLegend = true;
  public officeOutgoingStatusType: ChartType = 'horizontalBar';

  // Department User Status Details Horizontal Bar Chart
  public usersStatusData: ChartDataSets [] = [];
  public usersStatusLabels: Label[] = [];
  public usersStatusOptions: ChartOptions = {
    responsive: true,
    maintainAspectRatio: false,
    tooltips: {
			titleFontFamily: 'titillium web, sans-serif',
			titleMarginBottom: 10,
			bodyFontFamily: 'titillium web, sans-serif'
		},
		legend: {
			labels: {
				fontFamily: 'titillium web,sans-serif'
			}
		},
    scales:{
			xAxes:[{
				ticks: {					
					fontFamily: 'titillium web,sans-serif'
				}
			}],
			yAxes: [{
				ticks: {
					fontFamily: 'titillium web,sans-serif'
				}
			}]
		}
  };
  public usersStatusLegend = true;
  public usersStatusType: ChartType = 'horizontalBar';

  ngOnInit(): void {
    this.initDepartmentTree();
    this.loadCorrespondenceByType();
    this.loadManuscriptByStage();
    this.loadCorrespondenceByMonth();
    this.loadCorrespondenceMonthlyPayment();
    this.loadCorrespondenceByStatus();
    this.loadCenterOfficeByStatus();
    this.loadCollegeOfficeByStatus();
    this.loadOfficeOutgoingByStatus();
    this.loadUserByStatus();

    this.translate.get('crMgt.userDashboard.status.new').subscribe((res: string) => {      
      this.newLabel = res;
      this.outStatusChartLabels.push(res);
    });

    this.translate.get('crMgt.userDashboard.status.sent').subscribe((res: string) => {
      this.sentLabel = res;
      this.statusChartLabels.push(res);
    });

    this.translate.get('crMgt.userDashboard.status.sentOut').subscribe((res: string) => {
      this.sentOutLabel = res;
      this.outStatusChartLabels.push(res);
    });

    this.translate.get('crMgt.userDashboard.status.accepted').subscribe((res: string) => {
      this.acceptedLabel = res;
      this.statusChartLabels.push(res);
      this.manuscriptStatusChartLabels.push(res);
    });

    this.translate.get('crMgt.userDashboard.status.stage1').subscribe((res: string) => {
      this.manuscriptStatusChartLabels.push(res);
    });
    this.translate.get('crMgt.userDashboard.status.stage2').subscribe((res: string) => {
      this.manuscriptStatusChartLabels.push(res);
    });
    this.translate.get('crMgt.userDashboard.status.stage3').subscribe((res: string) => {
      this.manuscriptStatusChartLabels.push(res);
    });
    this.translate.get('crMgt.userDashboard.status.stage4').subscribe((res: string) => {
      this.manuscriptStatusChartLabels.push(res);
    });
    this.translate.get('crMgt.userDashboard.status.stage5').subscribe((res: string) => {
      this.manuscriptStatusChartLabels.push(res);
    });
    this.translate.get('crMgt.userDashboard.status.stage6').subscribe((res: string) => {
      this.manuscriptStatusChartLabels.push(res);
    });

    this.translate.get('crMgt.userDashboard.status.acceptedOut').subscribe((res: string) => {
      this.outStatusChartLabels.push(res);
      this.acceptedOutLabel = res;
    });

    this.translate.get('crMgt.userDashboard.status.processing').subscribe((res: string) => {
      this.processingLabel = res;
      this.statusChartLabels.push(res);
      this.outStatusChartLabels.push(res);
    });

    this.translate.get('crMgt.userDashboard.status.archived').subscribe((res: string) => {
      this.archivedLabel = res;
      this.statusChartLabels.push(res);
      this.outStatusChartLabels.push(res);
      this.manuscriptStatusChartLabels.push(res);
    });

    this.translate.get('crMgt.userDashboard.status.rejected').subscribe((res: string) => {
      this.rejectedLabel = res;
      this.statusChartLabels.push(res);
      this.outStatusChartLabels.push(res);
      this.manuscriptStatusChartLabels.push(res);
    });
    
    this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
      this.translate.get('crMgt.userDashboard.incomingCenter').subscribe((res: string) => {
        this.correspondenceByMonthBChartData[0]["label"] = res;
        this.correspondenceByMonthLChartData[0]["label"] = res;
      });

      this.translate.get('crMgt.userDashboard.incomingCollege').subscribe((res: string) => {
        this.correspondenceByMonthBChartData[1]["label"] = res;
        this.correspondenceByMonthLChartData[1]["label"] = res;
      });

      this.translate.get('crMgt.userDashboard.incomingExternal').subscribe((res: string) => {
        this.correspondenceByMonthBChartData[2]["label"] = res;
        this.correspondenceByMonthLChartData[2]["label"] = res;
      });

      this.translate.get('crMgt.userDashboard.outgoing').subscribe((res: string) => {
        this.correspondenceByMonthBChartData[3]["label"] = res;
        this.correspondenceByMonthLChartData[3]["label"] = res;
        this.correspondenceByMonthLChartData[4]["label"] = res;
      });

      this.translate.get('crMgt.manuscript').subscribe((res: string) => {
        this.correspondenceByMonthBChartData[4]["label"] = res;
        this.correspondenceByMonthLChartData[5]["label"] = res;
      });

      this.translate.get('crMgt.userDashboard.status.new').subscribe((res: string) => {        
        this.outStatusChartLabels[0] = res;
        this.newLabel = res;
      });

      this.translate.get('crMgt.userDashboard.status.sent').subscribe((res: string) => {
        this.statusChartLabels[0] = res;
        this.sentLabel = res;
      });

      this.translate.get('crMgt.userDashboard.status.sentOut').subscribe((res: string) => {
        this.outStatusChartLabels[1] = res;   
        this.sentOutLabel = res;
      });

      this.translate.get('crMgt.userDashboard.status.accepted').subscribe((res: string) => {
        this.statusChartLabels[1] = res;
        this.manuscriptStatusChartLabels[0] = res;
      });

      this.translate.get('crMgt.userDashboard.status.stage1').subscribe((res: string) => {
        this.manuscriptStatusChartLabels[1] = res;
      });
      this.translate.get('crMgt.userDashboard.status.stage2').subscribe((res: string) => {
        this.manuscriptStatusChartLabels[2] = res;
      });
      this.translate.get('crMgt.userDashboard.status.stage3').subscribe((res: string) => {
        this.manuscriptStatusChartLabels[3] = res;
      });
      this.translate.get('crMgt.userDashboard.status.stage4').subscribe((res: string) => {
        this.manuscriptStatusChartLabels[4] = res;
      });
      this.translate.get('crMgt.userDashboard.status.stage5').subscribe((res: string) => {
        this.manuscriptStatusChartLabels[5] = res;
      });
      this.translate.get('crMgt.userDashboard.status.stage6').subscribe((res: string) => {
        this.manuscriptStatusChartLabels[6] = res;
      });

      this.translate.get('crMgt.userDashboard.status.acceptedOut').subscribe((res: string) => {
        this.outStatusChartLabels[2] = res;       
      });

      this.translate.get('crMgt.userDashboard.status.processing').subscribe((res: string) => {
        this.statusChartLabels[2] = res;
        this.outStatusChartLabels[3] = res;
      });

      this.translate.get('crMgt.userDashboard.status.archived').subscribe((res: string) => {
        this.statusChartLabels[3] = res;
        this.outStatusChartLabels[4] = res;
        this.manuscriptStatusChartLabels[7] = res;
      });

      this.translate.get('crMgt.userDashboard.status.rejected').subscribe((res: string) => {
        this.statusChartLabels[4] = res;
        this.outStatusChartLabels[5] = res;
        this.manuscriptStatusChartLabels[8] = res;
      });
      
    });
  }

  ngOnDestroy() {
    this.subManager.unsubscribe();
  }

  checkNumeric(value) {
    return !isNaN(parseFloat(value)) && !isNaN(value - 0);
  }

  initDepartmentTree() {
    this.loadingDeptTree = true;
    let getAllforTree = this.departmentService
      .getAllForTree()
      .pipe(first())
      .subscribe(
        (response) => {
          if (response.success) {    
            this.loadingDeptTree = false;
            this.departmentItems = response.data;            
          } else {
            this.message.title = "Department Tree";
            this.message.type = "error";
            this.message.content = response.errors;
            this.errorDialog(this.message, response);
          }
        },
        (error) => {
          this.errorDialog(error, null);
        }
      );
    this.subManager.add(getAllforTree);
  }

  returnDepartmentTree(dept_code){  
    var departments = this.departmentItems;
    for (var college of departments) {
      for (var department of college["children"]) {
        for (var office of department["children"]) {
          if (office.code == dept_code) {
            return college["code"] + " / " + department["code"] + " / " + office["description"];
          }
        }
      }
    }
  }

  loadingCByType: boolean = false;
  loadingCByMonth: boolean = false;
  loadingCByStatus: boolean = false;
  loadingCMonthlyPayment: boolean = false;
  loadingCenterByStatus: boolean = false;
  loadingCollegeByStatus: boolean = false;
  loadingOutgoingByStatus: boolean = false;
  loadingUserStatus: boolean = false;
  loadingMByStage: boolean = false;
  loadingMByMonth: boolean = false;
  errorDialogOpened: boolean = false;

  errorDialog(message, response) {
    let infoDialogRef = null;
    if (!this.errorDialogOpened) {
      if (response != null) {
        if (response.errors[0] == "Signature has expired") {
          this.errorDialogOpened = true;
          infoDialogRef = this.infoDialogService.openInfoDialog(
            message
          );
        } else {
          infoDialogRef = this.infoDialogService.openInfoDialog(
            message
          );
        }
      } else {
        infoDialogRef = this.infoDialogService.openInfoDialog(
          message
        );
      }
      infoDialogRef.afterClosed().subscribe(() => {
        if (response != null) {
          if (response.errors[0] == "Signature has expired") {
            this.authenticationService.logout_offline();
            this.router.navigate(["/login"]);
          }
        }
      });
    }     
  }

  centerTotal: number = 0;
  collegeTotal: number = 0;
  externalTotal: number = 0;
  internalOutTotal: number = 0;
  externalOutTotal: number = 0; 

  loadCorrespondenceByType() {
    this.loadingCByType = true;
    let loadCorrespondenceByType = this.dashboardService
        .getCorrespondenceByType()
        .pipe(first())
        .subscribe(
          (response) => {
            if (response.success) {    
            this.loadingCByType = false;
            this.centerTotal = response.data[0]['center'];
            this.collegeTotal = response.data[1]['college'];
            this.externalTotal = response.data[2]['external'];
            this.internalOutTotal = response.data[3]['internal'];
            this.externalOutTotal = response.data[4]['external_out'];
          } else {
            this.message.title = "Correspondence By Type";
            this.message.type = "error";
            this.message.content = response.errors;
            this.errorDialog(this.message, response);
          }
          },
          (error) => {
            this.errorDialog(error, null);
          }
        );
    this.subManager.add(loadCorrespondenceByType);
  }

  centerSixTotal = 0;
  collegeSixTotal = 0;
  externalSixTotal = 0;
  outgoingSixTotal = 0;
  manuscriptSixTotal = 0;

  loadCorrespondenceByMonth() {
    this.loadingCByMonth = true;
    let loadCorrespondenceByMonth = this.dashboardService
        .getCorrespondenceByMonth()
        .pipe(first())
        .subscribe(
          (response) => {
            if (response.success) {             
            let centerArray = [];
            let collegeArray = [];
            let externalArray = [];
            let outgoingArray = [];
            let internalOutgoingArray = [];
            let externalOutgoingArray = [];
            for (var data of response.data) {
              this.correspondenceByMonthChartLabels.push(data['month']);
              centerArray.push(data['center']);
              collegeArray.push(data['college']);
              externalArray.push(data['external']);
              outgoingArray.push(data['outgoing']);
              internalOutgoingArray.push(data['internal']);
              externalOutgoingArray.push(data['external_out']);
              this.centerSixTotal += data['center'];
              this.collegeSixTotal += data['college'];
              this.externalSixTotal += data['external'];
              this.outgoingSixTotal += data['outgoing'];
            }

            let centerLabel = "";
            this.translate.get('crMgt.userDashboard.incomingCenter').subscribe((res: string) => {
              centerLabel = res;
            });

            let collegeLabel = "";
            this.translate.get('crMgt.userDashboard.incomingCollege').subscribe((res: string) => {
              collegeLabel = res;
            });

            let externalLabel = "";
            this.translate.get('crMgt.userDashboard.incomingExternal').subscribe((res: string) => {
              externalLabel = res;
            });

            let outgoingLabel = "";
            this.translate.get('crMgt.userDashboard.outgoing').subscribe((res: string) => {
              outgoingLabel = res;
            });
            this.correspondenceByMonthBChartData.push( 
              { data: centerArray, label: centerLabel, backgroundColor: '#2196F3', hoverBackgroundColor: "154360" }, 
              { data: collegeArray, label: collegeLabel, backgroundColor: '#27AE60', hoverBackgroundColor: "#52BE80" }, 
              { data: externalArray, label: externalLabel, backgroundColor: '#E74C3C', hoverBackgroundColor: "#EC7063" }, 
              { data: outgoingArray, label: outgoingLabel, backgroundColor: '#8E44AD', hoverBackgroundColor: "#A569BD" });
            this.correspondenceByMonthLChartData.push( 
              { data: centerArray, label: centerLabel, borderColor: '#3498DB', fill: false }, 
              { data: collegeArray, label: collegeLabel, borderColor: '#27AE60', fill: false },
              { data: externalArray, label: externalLabel, borderColor: '#E74C3C', fill: false }, 
              { data: internalOutgoingArray, label: outgoingLabel, borderColor: '#8E44AD', fill: false },
              { data: externalOutgoingArray, label: outgoingLabel, borderColor: '#A569BD', fill: false });
            this.loadingCByMonth = false;
            this.loadManuscriptByMonth();
          } else {
            this.message.title = "Correspondence By Month";
            this.message.type = "error";
            this.message.content = response.errors;
            this.errorDialog(this.message, response);
          }
          },
          (error) => {
            this.errorDialog(error, null);
          }
        );
    this.subManager.add(loadCorrespondenceByMonth);
  }

  findByStatus(array, status) {
    let count = 0;
    for (var arr of array) {
      if (arr["status"] == status) {
        count = arr["count"];
      }
    }
    return count;
  }

  loadCorrespondenceByStatus() {
    this.loadingCByStatus = true;
    let loadCorrespondenceByStatus = this.dashboardService
        .getCorrespondenceByStatus()
        .pipe(first())
        .subscribe(
          (response) => {
            if (response.success) {             
              let centerArray = response.data[0]['center'];
              let collegeArray = response.data[1]['college'];
              let externalArray = response.data[2]['external'];
              let internalOutgoingArray = response.data[3]['internal'];
              let externalOutgoingArray = response.data[4]['external_out'];
              let centerData = [];
              let collegeData = [];
              let externalData = [];
              let internalData = [];
              let externalOutData = [];
              
              centerData.push(this.findByStatus(centerArray, "Sent"), 
                              this.findByStatus(centerArray, "Accepted"),
                              this.findByStatus(centerArray, "Processing"),
                              this.findByStatus(centerArray, "Archived"),
                              this.findByStatus(centerArray, "Rejected"));
              collegeData.push(this.findByStatus(collegeArray, "Sent"),
                              this.findByStatus(collegeArray, "Accepted"),
                              this.findByStatus(collegeArray, "Processing"),
                              this.findByStatus(collegeArray, "Archived"),
                              this.findByStatus(collegeArray, "Rejected"));
              externalData.push(this.findByStatus(externalArray, "Sent"),
                              this.findByStatus(externalArray, "Accepted"),
                              this.findByStatus(externalArray, "Processing"),
                              this.findByStatus(externalArray, "Archived"),
                              this.findByStatus(externalArray, "Rejected"));
              internalData.push(this.findByStatus(internalOutgoingArray, "New"),
                                this.findByStatus(internalOutgoingArray, "Sent"),
                                this.findByStatus(internalOutgoingArray, "Accepted"),
                                this.findByStatus(internalOutgoingArray, "Processing"),
                                this.findByStatus(internalOutgoingArray, "Archived"),
                                this.findByStatus(internalOutgoingArray, "Rejected"));
              externalOutData.push(this.findByStatus(externalOutgoingArray, "New"),
                                  this.findByStatus(externalOutgoingArray, "Sent"),
                                  this.findByStatus(externalOutgoingArray, "Accepted"),
                                  this.findByStatus(externalOutgoingArray, "Processing"),
                                  this.findByStatus(externalOutgoingArray, "Archived"),
                                  this.findByStatus(externalOutgoingArray, "Rejected"));
              
              this.centerStatusChartData.push(
                {
                  data: centerData
                }
              );
              this.centerStatusChartData.forEach(ds => {
                ds.backgroundColor = ['rgba(54, 162, 235)',
                  "#27AE60",
                  'rgba(255, 206, 86, 0.8)',      
                  "#4D5360",
                  'rgba(255, 99, 132)'];              
              });

              this.collegeStatusChartData.push(
                {
                  data: collegeData
                }
              );
              this.collegeStatusChartData.forEach(ds => {
                ds.backgroundColor = ['rgba(54, 162, 235)',
                  "#27AE60",
                  'rgba(255, 206, 86, 0.8)',      
                  "#4D5360",
                  'rgba(255, 99, 132)'];              
              });

              this.externalStatusChartData.push(
                {
                  data: externalData
                }
              );
              this.externalStatusChartData.forEach(ds => {
                ds.backgroundColor = ['rgba(54, 162, 235)',
                  "#27AE60",
                  'rgba(255, 206, 86, 0.8)',      
                  "#4D5360",
                  'rgba(255, 99, 132)'];              
              });

              this.internalOutStatusChartData.push(
                {
                  data: internalData
                }
              );
              this.internalOutStatusChartData.forEach(ds => {
                ds.backgroundColor = ['#85C1E9',
                  'rgba(54, 162, 235)',
                  "#27AE60",
                  'rgba(255, 206, 86, 0.8)',      
                  "#4D5360",
                  'rgba(255, 99, 132)'];              
              });

              this.externalOutStatusChartData.push(
                {
                  data: externalOutData
                }
              );
              this.externalOutStatusChartData.forEach(ds => {
                ds.backgroundColor = ['#85C1E9',
                  'rgba(54, 162, 235)',
                  "#27AE60",
                  'rgba(255, 206, 86, 0.8)',      
                  "#4D5360",
                  'rgba(255, 99, 132)'];              
              });

              this.loadingCByStatus = false;
            } else {
              this.message.title = "Correspondence By Status";
              this.message.type = "error";
              this.message.content = response.errors;
              this.errorDialog(this.message, response);
            }
          },
          (error) => {
            this.errorDialog(error, null);
          }
        );
    this.subManager.add(loadCorrespondenceByStatus);
  }

  loadCorrespondenceMonthlyPayment() {
    this.loadingCMonthlyPayment = true;
    let loadCorrespondenceMonthlyPayment = this.dashboardService
        .getCorrespondenceMonthlyPayment()
        .pipe(first())
        .subscribe(
          (response) => {
            if (response.success) {             
              let monthlyPaidAmount = [];
              for (var data of response.data) {
                this.amountPaidChartLabels.push(data['month']);
                monthlyPaidAmount.push(data['amount']);
              }
              this.amountPaidChartData.push( 
                { data: monthlyPaidAmount });
              this.loadingCMonthlyPayment = false;
            } else {
              this.message.title = "Correspondence By Month";
              this.message.type = "error";
              this.message.content = response.errors;
              this.errorDialog(this.message, response);
            }
          },
          (error) => {
            this.errorDialog(error, null);
          }
        );
    this.subManager.add(loadCorrespondenceMonthlyPayment);
  }

  newLabel = "";
  sentLabel = "";
  sentOutLabel = "";
  acceptedLabel = "";
  acceptedOutLabel = "";
  processingLabel = "";
  archivedLabel = "";
  rejectedLabel = "";

  loadCenterOfficeByStatus() {
    this.loadingCenterByStatus = true;
    let loadCenterOfficeByStatus = this.dashboardService
        .getCenterOfficeByStatus()
        .pipe(first())
        .subscribe(
          (response) => {
            if (response.success) {
              let centerSentData = [];
              let centerAcceptedData = [];
              let centerProcessingData = [];
              let centerArchivedData = [];
              let centerRejectedData = [];
              for (var value of response.data) {
                this.centerOfficeStatusLabels.push(value['department']['code']);
                centerSentData.push(this.findByStatus(value['by_status'], 'Sent'));
                centerAcceptedData.push(this.findByStatus(value['by_status'], 'Accepted'));
                centerProcessingData.push(this.findByStatus(value['by_status'], 'Processing'));
                centerArchivedData.push(this.findByStatus(value['by_status'], 'Archived'));
                centerRejectedData.push(this.findByStatus(value['by_status'], 'Rejected'));
              }
              this.centerOfficeStatusData.push(
                { data: centerSentData, label: this.sentLabel, stack: 'a', backgroundColor: 'rgba(54, 162, 235)', hoverBackgroundColor: "154360" },
                { data: centerAcceptedData, label: this.acceptedLabel, stack: 'a', backgroundColor: '#27AE60', hoverBackgroundColor: "rgb(20, 90, 50)" },
                { data: centerProcessingData, label: this.processingLabel, stack: 'a', backgroundColor: 'rgba(255, 206, 86, 0.8)', hoverBackgroundColor: "#F39C12" },
                { data: centerArchivedData, label: this.archivedLabel, stack: 'a', backgroundColor: '#4D5360', hoverBackgroundColor: "#17202A" },
                { data: centerRejectedData, label: this.rejectedLabel, stack: 'a', backgroundColor: 'rgba(255, 99, 132)', hoverBackgroundColor: "rgba(100, 30, 22)" }
              );
              this.loadingCenterByStatus = false;
            } else {
              this.message.title = "Correspondence from Center";
              this.message.type = "error";
              this.message.content = response.errors;
              this.errorDialog(this.message, response);
            }
          },
          (error) => {
            this.errorDialog(error, null);
          }
        );
    this.subManager.add(loadCenterOfficeByStatus);
  }

  loadCollegeOfficeByStatus() {
    this.loadingCollegeByStatus = true;
    let loadCollegeOfficeByStatus = this.dashboardService
        .getCollegeOfficeByStatus()
        .pipe(first())
        .subscribe(
          (response) => {
            if (response.success) {
              let collegeSentData = [];
              let collegeAcceptedData = [];
              let collegeProcessingData = [];
              let collegeArchivedData = [];
              let collegeRejectedData = [];
              for (var value of response.data) {
                this.collegeOfficeStatusLabels.push(value['department']['code']);
                collegeSentData.push(this.findByStatus(value['by_status'], 'Sent'));
                collegeAcceptedData.push(this.findByStatus(value['by_status'], 'Accepted'));
                collegeProcessingData.push(this.findByStatus(value['by_status'], 'Processing'));
                collegeArchivedData.push(this.findByStatus(value['by_status'], 'Archived'));
                collegeRejectedData.push(this.findByStatus(value['by_status'], 'Rejected'));
              }
              this.collegeOfficeStatusData.push(
                { data: collegeSentData, label: this.sentLabel, stack: 'a', backgroundColor: 'rgba(54, 162, 235)', hoverBackgroundColor: "154360" },
                { data: collegeAcceptedData, label: this.acceptedLabel, stack: 'a', backgroundColor: '#27AE60', hoverBackgroundColor: "rgb(20, 90, 50)" },
                { data: collegeProcessingData, label: this.processingLabel, stack: 'a', backgroundColor: 'rgba(255, 206, 86, 0.8)', hoverBackgroundColor: "#F39C12" },
                { data: collegeArchivedData, label: this.archivedLabel, stack: 'a', backgroundColor: '#4D5360', hoverBackgroundColor: "#17202A" },
                { data: collegeRejectedData, label: this.rejectedLabel, stack: 'a', backgroundColor: 'rgba(255, 99, 132)', hoverBackgroundColor: "rgba(100, 30, 22)" }
              );
              this.loadingCollegeByStatus = false;
            } else {
              this.message.title = "Correspondence from Colleges";
              this.message.type = "error";
              this.message.content = response.errors;
              this.errorDialog(this.message, response);
            }
          },
          (error) => {
            this.errorDialog(error, null);
          }
        );
    this.subManager.add(loadCollegeOfficeByStatus);
  }

  loadOfficeOutgoingByStatus() {
    this.loadingOutgoingByStatus = true;
    let loadOfficeOutgoingByStatus = this.dashboardService
        .getOfficeOutgoingByStatus()
        .pipe(first())
        .subscribe(
          (response) => {
            if (response.success) {
              let outgoingNewData = [];
              let outgoingSentData = [];
              let outgoingAcceptedData = [];
              let outgoingProcessingData = [];
              let outgoingArchivedData = [];
              let outgoingRejectedData = [];
              for (var value of response.data) {
                this.officeOutgoingStatusLabels.push(value['department']['code']);
                outgoingNewData.push(this.findByStatus(value['by_status'], 'New'));
                outgoingSentData.push(this.findByStatus(value['by_status'], 'Sent'));
                outgoingAcceptedData.push(this.findByStatus(value['by_status'], 'Accepted'));
                outgoingProcessingData.push(this.findByStatus(value['by_status'], 'Processing'));
                outgoingArchivedData.push(this.findByStatus(value['by_status'], 'Archived'));
                outgoingRejectedData.push(this.findByStatus(value['by_status'], 'Rejected'));
              }
              this.officeOutgoingStatusData.push(
                { data: outgoingNewData, label: this.newLabel, stack: 'a', backgroundColor: '#5DADE2', hoverBackgroundColor: "#85C1E9" },
                { data: outgoingSentData, label: this.sentOutLabel, stack: 'a', backgroundColor: 'rgba(54, 162, 235)', hoverBackgroundColor: "154360" },
                { data: outgoingAcceptedData, label: this.acceptedOutLabel, stack: 'a', backgroundColor: '#27AE60', hoverBackgroundColor: "rgb(20, 90, 50)" },
                { data: outgoingProcessingData, label: this.processingLabel, stack: 'a', backgroundColor: 'rgba(255, 206, 86, 0.8)', hoverBackgroundColor: "#F39C12" },
                { data: outgoingArchivedData, label: this.archivedLabel, stack: 'a', backgroundColor: '#4D5360', hoverBackgroundColor: "#17202A" },
                { data: outgoingRejectedData, label: this.rejectedLabel, stack: 'a', backgroundColor: 'rgba(255, 99, 132)', hoverBackgroundColor: "rgba(100, 30, 22)" }
              );
              this.loadingOutgoingByStatus = false;
            } else {
              this.message.title = "Outgoing Correspondence";
              this.message.type = "error";
              this.message.content = response.errors;
              this.errorDialog(this.message, response);
            }
          },
          (error) => {
            this.errorDialog(error, null);
          }
        );
    this.subManager.add(loadOfficeOutgoingByStatus);
  }

  @Input() valueAssigned: number = 0;
  @Input() valueAccepted: number = 0;
  @Input() valueRejected: number = 0;
  @Input() valueForwarded: number = 0;
  
  public circumference: number = 2 * Math.PI * 20;
	public strokeDashoffset: number = 20;

  public assignedStrokeDashoffset: number = 0;
  public acceptedStrokeDashoffset: number = 0;
  public rejectedStrokeDashoffset: number = 0;
  public forwardedStrokeDashoffset: number = 0;

  loadUserByStatus() {
    this.loadingUserStatus = true;
    let loadUserByStatus = this.dashboardService
        .getUserStatus()
        .pipe(first())
        .subscribe(
          (response) => {
            if (response.success) {
              let currentUserData = [];
              let forwardedCountArray = [];
              let acceptedCountArray = [];
              let assignedCountArray = [];
              let rejectedCountArray = [];
              for (var value of response.data) {
                let fullName = value['user']['first_name'] + ' ' + value['user']['last_name'];
                this.usersStatusLabels.push(fullName);
                forwardedCountArray.push(value['from_user']);
                acceptedCountArray.push(this.findByStatus(value['to_user'], "Accepted"));
                assignedCountArray.push(this.findByStatus(value['to_user'], 'Assigned'));
                rejectedCountArray.push(this.findByStatus(value['to_user'], 'Rejected'));

                if (value['user']["id"] == this.currentUser.id) {
                  currentUserData = value;                
                }
              }
              let acceptedLabel = "";
              this.translate.get('crMgt.userDashboard.status.accepted').subscribe((res: string) => {
                acceptedLabel = res;
              });

              let assignedLabel = "";
              this.translate.get('crMgt.userDashboard.status.assigned').subscribe((res: string) => {
                assignedLabel = res;
              });

              let rejectedLabel = "";
              this.translate.get('crMgt.userDashboard.status.rejected').subscribe((res: string) => {
                rejectedLabel = res;
              });

              let forwardedLabel = "";
              this.translate.get('crMgt.userDashboard.status.forwarded').subscribe((res: string) => {
                forwardedLabel = res;
              });

              this.usersStatusData.push(
                { data: assignedCountArray, label: assignedLabel, stack: 'a', backgroundColor: '#BDC3C7', hoverBackgroundColor: "rgb(98, 101, 103)" },
                { data: acceptedCountArray, label: acceptedLabel, stack: 'a', backgroundColor: '#27AE60', hoverBackgroundColor: "rgb(20, 90, 50)" },                  
                { data: rejectedCountArray, label: rejectedLabel, stack: 'a', backgroundColor: 'rgba(255, 99, 132)', hoverBackgroundColor: "rgba(100, 30, 22)" },
                { data: forwardedCountArray, label: forwardedLabel, stack: 'a', backgroundColor: '#A569BD', hoverBackgroundColor: "rgb(98, 101, 103)" }
              );

              if ("user" in currentUserData) {
                let currentUserAssigned = this.findByStatus(currentUserData['to_user'], "Accepted");
                let currentUserAccepted = this.findByStatus(currentUserData['to_user'], 'Assigned');
                let currentUserRejected = this.findByStatus(currentUserData['to_user'], 'Rejected');
                let currentUserForwarded = currentUserData['from_user'];

                let total = currentUserAssigned + currentUserAccepted + currentUserRejected;
                this.valueAssigned = (currentUserAssigned * 100) / total;
                this.valueAccepted = (currentUserAccepted * 100) / total;
                this.valueRejected = (currentUserRejected * 100) / total;
                this.valueForwarded = (currentUserForwarded * 100) / total;

                this.assignedStrokeDashoffset = 100 - this.valueAssigned;
                this.acceptedStrokeDashoffset = 100 - this.valueAccepted;
                this.rejectedStrokeDashoffset = 100 - this.valueRejected;
                this.forwardedStrokeDashoffset = 100 - this.valueForwarded;
              }
              this.loadingUserStatus = false;
              this.loadManuscriptUserByStatus();
            } else {
              this.message.title = "Department Users";
              this.message.type = "error";
              this.message.content = response.errors;
              this.errorDialog(this.message, response);
            }
          },
          (error) => {
            this.errorDialog(error, null);
          }
        );
    this.subManager.add(loadUserByStatus);
  }

  manuscriptTotal: number = 0;
  stage1Total: number = 0;
  stage2Total: number = 0;
  stage3Total: number = 0;
  stage4Total: number = 0; 
  stage5Total: number = 0; 
  stage6Total: number = 0; 

  loadManuscriptByStage() {
    this.loadingMByStage = true;
    let loadManuscriptByStage = this.dashboardService
        .getManuscriptByStage()
        .pipe(first())
        .subscribe(
          (response) => {
            if (response.success) {    
            this.loadingMByStage = false;
            let new_manuscript = response.data[1]["New"];
            let archived = response.data[8]["Archived"];
            let rejected = response.data[9]["Rejected"];
            this.manuscriptTotal = response.data[0]['manuscript'];
            this.stage1Total = response.data[2]['stage_1'];
            this.stage2Total = response.data[3]['stage_2'];
            this.stage3Total = response.data[4]['stage_3'];
            this.stage4Total = response.data[5]['stage_4'];
            this.stage5Total = response.data[6]['stage_5'];
            this.stage6Total = response.data[7]['stage_6'];

            let manuscriptData = [];

            manuscriptData.push(new_manuscript, this.stage1Total, this.stage2Total, 
                                this.stage3Total, this.stage4Total, this.stage5Total, 
                                this.stage6Total, archived, rejected);

            this.manuscriptStatusChartData.push(
                {
                  data: manuscriptData
                }
            );
            this.manuscriptStatusChartData.forEach(ds => {
              ds.backgroundColor = ['rgba(54, 162, 235)',
                "#48C9B0",
                "#1ABC9C",
                "#17A589",
                "#148F77",      
                "#117864",
                "#0E6251",
                "#4D5360",
                'rgba(255, 99, 132)'];              
            });

          } else {
            this.message.title = "Manuscript By Stage";
            this.message.type = "error";
            this.message.content = response.errors;
            this.errorDialog(this.message, response);
          }
          },
          (error) => {
            this.errorDialog(error, null);
          }
        );
    this.subManager.add(loadManuscriptByStage);
  }

  loadManuscriptByMonth() {
    this.loadingMByMonth = true;
    let loadManuscriptByMonth = this.dashboardService
        .getManuscriptByMonth()
        .pipe(first())
        .subscribe(
          (response) => {
            if (response.success) {             
              let manuscriptArray = [];
              for (var data of response.data) {
                manuscriptArray.push(data['manuscript']);
                this.manuscriptSixTotal += data['manuscript'];
              }

              let manuscriptLabel = "";
              this.translate.get('crMgt.manuscript').subscribe((res: string) => {
                manuscriptLabel = res;
              });

              if (this.manuscriptSixTotal > 0) {
                this.correspondenceByMonthBChartData.push( 
                  { data: manuscriptArray, label: manuscriptLabel, backgroundColor: '#C0392B', hoverBackgroundColor: "#CD6155" });
                this.correspondenceByMonthLChartData.push( 
                  { data: manuscriptArray, label: manuscriptLabel, borderColor: '#C0392B', fill: false });
              }
              this.loadingMByMonth = false;
            } else {
              this.message.title = "Manuscript By Month";
              this.message.type = "error";
              this.message.content = response.errors;
              this.errorDialog(this.message, response);
            }
          },
          (error) => {
            this.errorDialog(error, null);
          }
        );
    this.subManager.add(loadManuscriptByMonth);
  }

  loadManuscriptUserByStatus() {
    this.loadingUserStatus = true;
    let loadManuscriptUserByStatus = this.dashboardService
        .getManuscriptUsersStatus()
        .pipe(first())
        .subscribe(
          (response) => {
            if (response.success) {
              if (response.data.length > 0) {
                let totalUsers = this.usersStatusLabels.length;
                let assignedCountArray = new Array(totalUsers).fill(0);
                let forwardedCountArray = new Array(totalUsers).fill(0);
                for (var value of response.data) {
                  let fullName = value['user']['first_name'] + ' ' + value['user']['last_name'];
                  let index = this.usersStatusLabels.indexOf(fullName);
                  if (index == -1) {
                    this.usersStatusLabels.push(fullName);
                    assignedCountArray.push(this.findByStatus(value['to_user'], 'Assigned'));
                    forwardedCountArray.push(value['from_user']);
                    totalUsers ++;
                  } else {
                    assignedCountArray.splice(index, 1, this.findByStatus(value['to_user'], 'Assigned'));
                    forwardedCountArray.splice(index, 1, value['from_user']);
                  }
                }
                
                let assignedLabel = "";
                this.translate.get('crMgt.userDashboard.status.assigned2').subscribe((res: string) => {
                  assignedLabel = res;
                });

                let forwardedLabel = "";
                this.translate.get('crMgt.userDashboard.status.forwarded1').subscribe((res: string) => {
                  forwardedLabel = res;
                });

                this.usersStatusData.push(
                  { data: assignedCountArray, label: assignedLabel, stack: 'b', backgroundColor: '#E74C3C', hoverBackgroundColor: "#EC7063" },
                  { data: forwardedCountArray, label: forwardedLabel, stack: 'b', backgroundColor: '#E67E22', hoverBackgroundColor: "#F0B27A" }
                );
              }
              this.loadingUserStatus = false;
            } else {
              this.message.title = "Department Users";
              this.message.type = "error";
              this.message.content = response.errors;
              this.errorDialog(this.message, response);
            }
          },
          (error) => {
            this.errorDialog(error, null);
          }
        );
    this.subManager.add(loadManuscriptUserByStatus);
  }

}
