import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from "@angular/common/http";
import { MatDialog, MatDialogConfig } from "@angular/material/dialog";

import { environment } from 'src/environments/environment';

import { MethodResponse } from "src/app/_core/method-response";
import { Royalty } from "src/app/_models/royalty";

import { RoyaltyDialogComponent } from "src/app/author/author-royalty-dialog/royalty-dialog/royalty-dialog.component" ;

@Injectable({
  providedIn: 'root'
})
export class AuthorRoyaltyService {

  apiUrl: string = environment.apiURL;

  httpHeaders = new HttpHeaders({
    "Content-Type": "application/json",
  });

  constructor(private http: HttpClient, public dialog: MatDialog) { }

  assignRoyaltyValue(data) {
    let royalty: Royalty = new Royalty();
    royalty = {
      id: data["id"],
      manuscript: data["manuscript"],
      author: data["author"],
      amount: data["amount"],
      referenceNo: data["reference_no"],
      remark: data["remark"],
      paymentDate: data["payment_date"],
      keyInBy: data["key_in_by"]
    }
    return royalty; 
  }

  getAllRoyalty(author) {
    return this.http.get<MethodResponse>(
      `${this.apiUrl}/crmgt/business_settings/author/royalty?author_id=${author.id}`,
      {
        headers: this.httpHeaders,
      }
    );
  }

  openRoyaltyDialog(title, author, royalty?) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = {
      title: title,
      author: author,
      royalty: royalty
    };
    
    dialogConfig.width = "350px";
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    const dialogRef = this.dialog.open(
      RoyaltyDialogComponent,
      dialogConfig
    );

    return dialogRef;
  }

  openRoyaltyPreviewDialog(title, royalty) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = {
      title: title,
      royalty: royalty
    };
    
    dialogConfig.width = "350px";
    // dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    const dialogRef = this.dialog.open(
      RoyaltyDialogComponent,
      dialogConfig
    );

    return dialogRef;
  }

  create(data, author) {
    var formData = data.royaltyForm.value;
    return this.http.post<MethodResponse>(
      `${this.apiUrl}/crmgt/business_settings/author/royalty`,
      {
        royalty: {
          manuscript_id: formData.manuscript,
          author_id: author.id,
          reference_no: formData.referenceNo,
          amount: formData.amount,
          remark: formData.remark
        }
      },
      {
        headers: this.httpHeaders,
      }
    );
  }

  update(data, author, royalty) {
    var formData = data.royaltyForm.value;
    return this.http.put<MethodResponse>(
      `${this.apiUrl}/crmgt/business_settings/author/royalty`,
      {
        id: royalty.id,
        royalty: {
          manuscript_id: formData.manuscript,
          author_id: author.id,
          reference_no: formData.referenceNo,
          amount: formData.amount,
          remark: formData.remark
        }
      },
      {
        headers: this.httpHeaders,
      }
    );
  }
}
