import { Injectable } from "@angular/core";
import { MatDialog, MatDialogConfig } from "@angular/material/dialog";
import { HttpClient, HttpHeaders } from "@angular/common/http";

import { environment } from 'src/environments/environment';

import { MethodResponse } from "src/app/_core/method-response";
import { DepartmentDialogComponent } from "src/app/department/_dialogs/department-dialog/department-dialog.component";

@Injectable({
  providedIn: "root",
})
export class DepartmentService {

  apiUrl: string = environment.apiURL;

  httpHeaders = new HttpHeaders({
    "Content-Type": "application/json",
  });

  constructor(private http: HttpClient, public dialog: MatDialog) {}

  getAll(type) {
    return this.http.get<MethodResponse>(
      `${this.apiUrl}/crmgt/business_settings/department?type=${type}`,
      {
        headers: this.httpHeaders,
      }
    );
  }

  getAllByType(type) {
    return this.http.get<MethodResponse>(
      `${this.apiUrl}/crmgt/business_settings/department_by_type?department_type=${type}`,
      {
        headers: this.httpHeaders,
      }
    );
  }

  getAllForTree() {
    return this.http.get<MethodResponse>(
      `${this.apiUrl}/crmgt/business_settings/department_tree`,
      {
        headers: this.httpHeaders,
      }
    );
  }

  openDepartmentDialog(title, allDepartments, node, department, allRoles?, assignedRoles?, departmentHeadRoleId?) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = {
      title: title,
      allDepartments: allDepartments,
      node: node,
      department: department,
      allRoles: allRoles,
      assignedRoles: assignedRoles,
      departmentHeadRoleId: departmentHeadRoleId
    };

    dialogConfig.width = "400px";

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    const dialogRef = this.dialog.open(DepartmentDialogComponent, dialogConfig);

    return dialogRef;
  }

  create(type, code, description, parent) {
    return this.http.post<MethodResponse>(
      `${this.apiUrl}/crmgt/business_settings/department`,
      {
        department: {
          code: code,
          description: description,
          department_type_id: type,
          parent_id: parent,
        },
      },
      {
        headers: this.httpHeaders,
      }
    );
  }

  getAssignedRoleForDepartment(departmentId) {
    return this.http.get<MethodResponse>(
      `${this.apiUrl}/crmgt/business_settings/department/get_roles?id=${departmentId}`,
      {
        headers: this.httpHeaders,
      }
    );
  }

  assignRoles(department, selectedRoleIds) {
    return this.http.post<MethodResponse>(
      `${this.apiUrl}/crmgt/business_settings/department/assign_roles?id=${department.id}`,
      {
        role_ids: selectedRoleIds
      },
      {
        headers: this.httpHeaders,
      }
    );
  }

  getDepartmentHead(department) {
    return this.http.get<MethodResponse>(
      `${this.apiUrl}/crmgt/business_settings/department/get_heads?id=${department.id}`,
      {
        headers: this.httpHeaders,
      }
    );
  }

  assignDepartmentHead(department, selectedRoleIds) {
    return this.http.post<MethodResponse>(
      `${this.apiUrl}/crmgt/business_settings/department/assign_heads?id=${department.id}`,
      {
        role_ids: selectedRoleIds
      },
      {
        headers: this.httpHeaders,
      }
    );
  }
}
