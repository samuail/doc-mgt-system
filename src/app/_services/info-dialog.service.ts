import { Injectable } from "@angular/core";
import { MatDialog, MatDialogConfig } from "@angular/material/dialog";

import { InfoDialogComponent } from "../_core/info-dialog/info-dialog.component";

@Injectable({
  providedIn: "root",
})
export class InfoDialogService {
  constructor(public dialog: MatDialog) {}

  openInfoDialog(message) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = {
      title: message.title,
      message: message.content,
      type: message.type,
    };

    // if (!this.dialog.openDialogs || !this.dialog.openDialogs.length) return;
    // if (this.dialog.openDialogs.length > 1) return;

    const dialogRef = this.dialog.open(InfoDialogComponent, dialogConfig);

    // const dialogExist = this.dialog.getDialogById('message-pop-up');

    //     if (!dialogExist) {
    //       this.dialog.open(MessagePopUpComponent, {
    //         id: 'message-pop-up',
    //         data: // some data
    //       });
    //     }

    return dialogRef;
  }
}
