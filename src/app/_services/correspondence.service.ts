import { Injectable } from "@angular/core";
import { MatDialog, MatDialogConfig } from "@angular/material/dialog";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";

import { environment } from 'src/environments/environment';

import { MethodResponse } from "../_core/method-response";

import { InternalCorrespondence } from "src/app/_models/internal-correspondence";
import { ExternalIncomingCorrespondence } from "src/app/_models/external-incoming-correspondence";
import { ExternalOutgoingCorrespondence } from "src/app/_models/external-outgoing-correspondence";

import { DateFormatterService } from "src/app/_services/date-formatter.service";

import { CorrespondenceDialogComponent } from "src/app/correspondence/_dialogs/correspondence-dialog/correspondence-dialog.component";
import { DocUploadsComponent } from "src/app/correspondence/_dialogs/doc-uploads/doc-uploads.component";
import { DocPreviewerComponent } from "src/app/correspondence/_dialogs/doc-previewer/doc-previewer.component";
import { DocCommentComponent } from "src/app/correspondence/_dialogs/doc-comment/doc-comment.component";
import { AssignCorrespondenceComponent } from "src/app/correspondence/_dialogs/assign-correspondence/assign-correspondence.component";
import { DocCcComponent } from "src/app/correspondence/_dialogs/doc-cc/doc-cc.component";
import { DocArchiverComponent } from "src/app/correspondence/_dialogs/doc-archiver/doc-archiver.component";

@Injectable({
  providedIn: "root",
})
export class CorrespondenceService {

  apiUrl: string = environment.apiURL;

  httpHeaders = new HttpHeaders({
    "Content-Type": "application/json",
  });

  constructor(private http: HttpClient, public dialog: MatDialog, 
              private dateFormatterService: DateFormatterService) {}

  getInternal(dataSize = "all", requestType = "incoming", departmentType = "center", 
        filter = '', sortOrder = 'dsc', pageNumber = 0, 
        pageSize = 10) {
          let url = "";
          if (dataSize == "all") {
            url = `${this.apiUrl}/crmgt/correspondence/internal`;
          } else if (dataSize == "assigned") {
            url = `${this.apiUrl}/crmgt/correspondence/internal/assigned`;
          } else if (dataSize == "archived") {
            url = `${this.apiUrl}/crmgt/correspondence/internal/archived`;
          }
          return this.http.get<MethodResponse>(url, {
            params: new HttpParams()
                .set('requestType', requestType)
                .set('departmentType', departmentType)
                .set('filter', filter)
                .set('sortOrder', sortOrder)
                .set('pageNumber', pageNumber.toString())
                .set('pageSize', pageSize.toString())
        }).pipe(
            map(response => response)
        );
  }

  getExternal(dataSize = "all", requestType = "incoming", 
        filter = '', sortOrder = 'dsc', pageNumber = 0, 
        pageSize = 10) {
          let url = "";
          if (dataSize == "all") {
            url = `${this.apiUrl}/crmgt/correspondence/external`;
          } else if (dataSize == "assigned") {
            url = `${this.apiUrl}/crmgt/correspondence/external/assigned`;
          } else if (dataSize == "archived") {
            url = `${this.apiUrl}/crmgt/correspondence/external/archived`;
          }
          return this.http.get<MethodResponse>(url, {
            params: new HttpParams()
                .set('requestType', requestType)
                .set('filter', filter)
                .set('sortOrder', sortOrder)
                .set('pageNumber', pageNumber.toString())
                .set('pageSize', pageSize.toString())
        }).pipe(
            map(response => response)
        );
  }

  assignCorrespondenceValue(value) {
    let internalCorrespondence: InternalCorrespondence = new InternalCorrespondence();
        
    var letter_dateEC = this.dateFormatterService.convertDateToEC(value["letter_date"]);
    
    internalCorrespondence = {
      id: value["id"],
      referenceNo: value["reference_no"],
      letterDate: value["letter_date"],
      letterDateEC: letter_dateEC,
      subject: value["subject"],
      keyWord: value["search_keywords"],
      source: value["source"],
      destination: value["destination"],
      receivedDate: value["received_date"],
      sentDate: value["received_date"],
      status: value["status"],
      keyInBy: value["key_in_by"],
      hasCc: value["has_cc"],
      types: value["types"],
      correspondenceCarbonCopies: value["correspondence_ccs"],
      docReferenceNo: value["doc_reference_no"],
      amount: value["amount"],
      remark: value["remark"],
      noDocuments: value["no_documents"],
      archivedDate: value["archived_date"]
    };
    return internalCorrespondence;
  }

  assignExternalIncomingCorrespondenceValue(value) {
    let incomingExternalCorrespondence: ExternalIncomingCorrespondence = new ExternalIncomingCorrespondence();

    var letter_dateEC = this.dateFormatterService.convertDateToEC(value["letter_date"]);

    incomingExternalCorrespondence = {
      id: value["id"],
      referenceNo: value["reference_no"],
      letterDate: value["letter_date"],
      letterDateEC: letter_dateEC,
      subject: value["subject"],
      keyWord: value["search_keywords"],
      source: value["source"],
      destination: value["destination"],
      receivedDate: value["received_date"],
      status: value["status"],
      keyInBy: value["key_in_by"],
      hasCc: value["has_cc"],
      types: value["types"],
      correspondenceCarbonCopies: value["correspondence_ccs"],
      docReferenceNo: value["doc_reference_no"],
      amount: value["amount"],
      remark: value["remark"],
      noDocuments: value["no_documents"],
      archivedDate: value["archived_date"]
    };
    return incomingExternalCorrespondence;    
  }

  assignExternalOutgoingCorrespondenceValue(value) {
    let externalOutgoingCorrespondence: ExternalOutgoingCorrespondence = new ExternalOutgoingCorrespondence();

    var letter_dateEC = this.dateFormatterService.convertDateToEC(value["letter_date"]);

    externalOutgoingCorrespondence = {
      id: value["id"],
      referenceNo: value["reference_no"],
      letterDate: value["letter_date"],
      letterDateEC: letter_dateEC,
      subject: value["subject"],
      keyWord: value["search_keywords"],
      source: value["source"],
      destination: value["destination"],
      sentDate: value["received_date"],
      status: value["status"],
      keyInBy: value["key_in_by"],
      hasCc: value["has_cc"],
      types: value["types"],
      correspondenceCarbonCopies: value["correspondence_ccs"]
    };
    return externalOutgoingCorrespondence;
  }

  getInternalAll(request_type, department_type?) {
    return this.http.get<MethodResponse>(
      `${this.apiUrl}/crmgt/correspondence/internal?request_type=${request_type}&department_type=${department_type}`,
      {
        headers: this.httpHeaders,
      }
    );
  }

  getInternalAssigned(department_type) {
    return this.http.get<MethodResponse>(
      `${this.apiUrl}/crmgt/correspondence/internal/assigned?department_type=${department_type}`,
      {
        headers: this.httpHeaders,
      }
    );
  }

  getExternalAll(request_type) {
    return this.http.get<MethodResponse>(
      `${this.apiUrl}/crmgt/correspondence/external?request_type=${request_type}`,
      {
        headers: this.httpHeaders,
      }
    );
  }

  getExternalAssigned() {
    return this.http.get<MethodResponse>(
      `${this.apiUrl}/crmgt/correspondence/external/assigned`,
      {
        headers: this.httpHeaders,
      }
    );
  }

  getAllComments(correspondenceId) {
    return this.http.get<MethodResponse>(`${this.apiUrl}/crmgt/auth/users`, {
      headers: this.httpHeaders,
    });
  }

  openCorrespondenceDialog(type, title, departmentItems, correspondence?) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = {
      type: type,
      title: title,
      departmentItems: departmentItems,
      correspondence: correspondence,
    };
    if (title != "Upload Document") {
      dialogConfig.width = "600px";
    } else {
      dialogConfig.width = "400px";
    }
    if (title == "Edit Correspondence") { 
      dialogConfig.disableClose = false;
    } else {
      dialogConfig.disableClose = true;
    }
    dialogConfig.autoFocus = true;

    const dialogRef = this.dialog.open(
      CorrespondenceDialogComponent,
      dialogConfig
    );

    return dialogRef;
  }

  openCorrespondenceCcDialog(canAdd, canEdit, canDelete, type, title, correspondence, departmentItems, direction?){
    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = {
      canAdd: canAdd, 
      canEdit: canEdit, 
      canDelete: canDelete,
      direction: direction,
      type: type,
      title: title,
      correspondence: correspondence,
      departmentItems: departmentItems
    };

    dialogConfig.panelClass = "doc-comment-dialog-container";
    dialogConfig.autoFocus = true;

    const dialogRef = this.dialog.open(DocCcComponent, dialogConfig);

    return dialogRef;
  }

  openDocImagesDialog(uploadActions, type, title, correspondence, direction?) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = {
      uploadActions: uploadActions,
      direction: direction,
      type: type,
      title: title,
      correspondence: correspondence
    };

    dialogConfig.panelClass = "doc-comment-dialog-container";
    dialogConfig.autoFocus = true;

    const dialogRef = this.dialog.open(DocUploadsComponent, dialogConfig);

    return dialogRef;
  }

  openDocCommentsDialog(commentActions, type, title, correspondence, direction?) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = {
      commentActions: commentActions,
      type: type,
      title: title,
      correspondence: correspondence,
      direction: direction
    };

    dialogConfig.panelClass = "doc-comment-dialog-container";
    dialogConfig.autoFocus = true;

    const dialogRef = this.dialog.open(DocCommentComponent, dialogConfig);

    return dialogRef;
  }

  openDocPreviewerDialog(previewActions, type, title, correspondence, direction?) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = {
      previewActions: previewActions,
      type: type,
      title: title,
      correspondence: correspondence,
      direction: direction
    };

    dialogConfig.panelClass = "doc-previewer-dialog-container";
    dialogConfig.autoFocus = true;

    const dialogRef = this.dialog.open(DocPreviewerComponent, dialogConfig);

    return dialogRef;
  }

  openDocArchiverDialog(type, title, correspondence, direction?) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = {
      type: type,
      title: title,
      correspondence: correspondence,
      direction: direction
    };

    dialogConfig.width = "400px";
    dialogConfig.autoFocus = true;

    const dialogRef = this.dialog.open(DocArchiverComponent, dialogConfig);

    return dialogRef;
  }

  openAssignCorrespondenceDialog(routeActions, type, title, correspondence, departmentItems, direction?) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = {
      routeActions: routeActions,
      type: type,
      title: title,
      correspondence: correspondence,
      departmentItems: departmentItems,
      direction: direction
    };

    dialogConfig.panelClass = "doc-comment-dialog-container";
    dialogConfig.autoFocus = true;

    const dialogRef = this.dialog.open(
      AssignCorrespondenceComponent,
      dialogConfig
    );

    return dialogRef;
  }  

  createInternal(data, request_type) {
    var formData = data.form.value;
    var source_id;
    var destination_id;
    var letter_date;
    if (request_type == "incoming"){
      source_id = formData.from;
      destination_id = null;
    } else {
      source_id = null;
      destination_id = formData.from;
    }

    if (data.isGCCalendar) { 
      letter_date = formData.letterDate;
    } else{
      letter_date = this.dateFormatterService.convertDateToGC(formData.letterDateEt);
    }

    return this.http.post<MethodResponse>(
      `${this.apiUrl}/crmgt/correspondence/internal`,
      {
        request_type: request_type,
        internal_correspondence: {
          reference_no: formData.referenceNo,
          letter_date: letter_date,
          subject: formData.subject,
          search_keywords: formData.keyWord.split(", "),
          source_id: source_id,
          destination_id: destination_id,
          has_cc: formData.hasCc != "" ? true : false
        },
      },
      {
        headers: this.httpHeaders,
      }
    );
  }

  updateInternal(correspondence, data, request_type) {
    var formData = data.form.value;
    var source_id;
    var destination_id;
    var letter_date;
    if (request_type == "incoming"){
      source_id = formData.from;
      destination_id = correspondence.destination.id;
    } else {
      source_id = correspondence.source.id;
      destination_id = formData.from;
    }

    if (data.isGCCalendar) { 
      letter_date = formData.letterDate;
    } else{
      letter_date = this.dateFormatterService.convertDateToGC(formData.letterDateEt);
    }

    return this.http.put<MethodResponse>(
      `${this.apiUrl}/crmgt/correspondence/internal`,
      {
        id: correspondence.id,
        internal_correspondence: {
          reference_no: formData.referenceNo,
          letter_date: letter_date,
          subject: formData.subject,
          search_keywords:
            typeof formData.keyWord === "string"
              ? formData.keyWord.split(",")
              : formData.keyWord,
          source_id: source_id,
          destination_id: destination_id,
          has_cc: formData.hasCc != "" ? true : false
        },
      },
      {
        headers: this.httpHeaders,
      }
    );
  }

  updateInternalStatus(correspondence, status) {    
    return this.http.put<MethodResponse>(
      `${this.apiUrl}/crmgt/correspondence/internal/update_status`,
      {
        id: correspondence.id,
        status: status
      },
      {
        headers: this.httpHeaders,
      }
    );
  }  

  createExternal(data, request_type) {
    var formData = data.form.value;
    var source_id;
    var destination_id;
    var letter_date;
    if (request_type == "incoming"){
      source_id = formData.from;
      destination_id = null;
    } else {
      source_id = null;
      destination_id = formData.from;
    }

    if (data.isGCCalendar) { 
      letter_date = formData.letterDate;
    } else{
      letter_date = this.dateFormatterService.convertDateToGC(formData.letterDateEt);
    }

    return this.http.post<MethodResponse>(
      `${this.apiUrl}/crmgt/correspondence/external`,
      {
        request_type: request_type,
        external_correspondence: {
          reference_no: formData.referenceNo,
          letter_date: letter_date,
          subject: formData.subject,
          search_keywords: formData.keyWord.split(", "),
          source_id: source_id,
          destination_id: destination_id,
          has_cc:  formData.hasCc != "" ? true : false
        }
      },
      {
        headers: this.httpHeaders,
      }
    );
  }

  updateExternal(correspondence, data, request_type) {
    var formData = data.form.value;
    var source_id;
    var destination_id;
    var letter_date;
    if (request_type == "incoming"){
      source_id = formData.from;
      destination_id = correspondence.destination.id;
    } else {
      source_id = correspondence.source.id;
      destination_id = formData.from;
    }

    if (data.isGCCalendar) { 
      letter_date = formData.letterDate;
    } else{
      letter_date = this.dateFormatterService.convertDateToGC(formData.letterDateEt);
    }

    return this.http.put<MethodResponse>(
      `${this.apiUrl}/crmgt/correspondence/external`,
      {
        request_type: request_type,
        id: correspondence.id,
        external_correspondence: {
          reference_no: formData.referenceNo,
          letter_date: letter_date,
          subject: formData.subject,
          search_keywords:
            typeof formData.keyWord === "string"
              ? formData.keyWord.split(",")
              : formData.keyWord,
          source_id: source_id,
          destination_id: destination_id,
          has_cc:  formData.hasCc != "" ? true : false
        }        
      },
      {
        headers: this.httpHeaders,
      }
    );
  }

  updateExternalStatus(correspondence, status) {    
    return this.http.put<MethodResponse>(
      `${this.apiUrl}/crmgt/correspondence/external/update_status`,
      {
        request_type: "incoming",
        id: correspondence.id,
        status: status
      },
      {
        headers: this.httpHeaders,
      }
    );
  }

  createExternalIncoming(formData) {
    return this.http.post<MethodResponse>(
      `${this.apiUrl}/crmgt/correspondence/external`,
      {
        request_type: "incoming",
        external_correspondence: {
          reference_no: formData.referenceNo,
          letter_date: formData.letterDate,
          subject: formData.subject,
          search_keywords: formData.keyWord.split(", "),
          source_id: formData.from,
          destination_id: null,
        },
      },
      {
        headers: this.httpHeaders,
      }
    );
  }

  updateExternalIncoming(correspondence_id, formData) {
    return this.http.put<MethodResponse>(
      `${this.apiUrl}/crmgt/correspondence/external`,
      {
        request_type: "incoming",
        id: correspondence_id,
        external_correspondence: {
          reference_no: formData.referenceNo,
          letter_date: formData.letterDate,
          subject: formData.subject,
          search_keywords:
            typeof formData.keyWord === "string"
              ? formData.keyWord.split(", ")
              : formData.keyWord,
          source_id: formData.from,
        },
      },
      {
        headers: this.httpHeaders,
      }
    );
  }

  createExternalOutgoing(formData) {
    return this.http.post<MethodResponse>(
      `${this.apiUrl}/crmgt/correspondence/external`,
      {
        request_type: "outgoing",
        external_correspondence: {
          reference_no: formData.referenceNo,
          letter_date: formData.letterDate,
          subject: formData.subject,
          search_keywords: formData.keyWord.split(", "),
          source_id: null,
          destination_id: formData.from,
        },
      },
      {
        headers: this.httpHeaders,
      }
    );
  }

  archiveInternal(formData, correspondence) {
    let docReferenceNo = formData.docReferenceNo;
    let amount = formData.amount;
    let noOfDocuments = formData.noDocuments;
    let remark = formData.remark;
    let status = "Archived";
    return this.http.put<MethodResponse>(
      `${this.apiUrl}/crmgt/correspondence/internal/archive`,
      {
        id: correspondence.id,
        status: status,
        doc_reference_no: docReferenceNo,
        no_documents: noOfDocuments,
        amount: amount,
        remark: remark
      },
      {
        headers: this.httpHeaders,
      }
    );
  }

  getInternalArchived() {
    return this.http.get<MethodResponse>(
      `${this.apiUrl}/crmgt/correspondence/internal/archived`,
      {
        headers: this.httpHeaders,
      }
    );    
  }

  archiveExternal(formData, correspondence) {
    let docReferenceNo = formData.docReferenceNo;
    let amount = formData.amount;
    let noOfDocuments = formData.noDocuments;
    let remark = formData.remark;    
    let status = "Archived";
    return this.http.put<MethodResponse>(
      `${this.apiUrl}/crmgt/correspondence/external/archive`,
      {
        id: correspondence.id,
        status: status,
        doc_reference_no: docReferenceNo,
        no_documents: noOfDocuments,
        amount: amount,
        remark: remark,
        request_type: "incoming"
      },
      {
        headers: this.httpHeaders,
      }
    );
  }

  getExternalArchived() {
    return this.http.get<MethodResponse>(
      `${this.apiUrl}/crmgt/correspondence/external/archived`,
      {
        headers: this.httpHeaders,
      }
    );    
  }
}
