import { Injectable } from '@angular/core';
import { MatDialog, MatDialogConfig } from "@angular/material/dialog";
import { HttpClient, HttpHeaders } from "@angular/common/http";

import { environment } from 'src/environments/environment';

import { MethodResponse } from "../_core/method-response";

import { ManuscriptAuthorDialogComponent } from "src/app/correspondence/incomings/manuscript/_dialog/manuscript-author/manuscript-author-dialog/manuscript-author-dialog.component";

@Injectable({
  providedIn: 'root'
})
export class ManuscriptAuthorService {
  apiUrl: string = environment.apiURL;

  httpHeaders = new HttpHeaders({
    "Content-Type": "application/json",
  });

  constructor(private http: HttpClient, public dialog: MatDialog) {}


  openAuthorDialog(title, authors, manuscript_authors, manuscript_author?) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = {
      title: title,
      authors: authors,
      manuscript_authors: manuscript_authors,
      manuscript_author: manuscript_author
    };

    if (title == 'Preview Author') {
      dialogConfig.width = "400px";
      dialogConfig.disableClose = false;
    } else {
      dialogConfig.width = "300px";
      dialogConfig.disableClose = true;
    }
    dialogConfig.autoFocus = true;

    const dialogRef = this.dialog.open(ManuscriptAuthorDialogComponent, dialogConfig);

    return dialogRef;
  }

  getAll(manuscript) {
    return this.http.get<MethodResponse>(
      `${this.apiUrl}/crmgt/manuscript/authors?manuscript_id=${manuscript.id}`,
      {
        headers: this.httpHeaders,
      }
    );
  }

  create(data, manuscript) {
    var formData = data.authorForm.value;
    return this.http.post<MethodResponse>(
      `${this.apiUrl}/crmgt/manuscript/authors`,
      {
        manuscript_author: {
          manuscript_id: manuscript.id,
          author_id: formData.authors,
        },
      },
      {
        headers: this.httpHeaders,
      }
    );
  }

  update(data, manuscript_author) {
    var formData = data.authorForm.value;
    return this.http.put<MethodResponse>(
      `${this.apiUrl}/crmgt/manuscript/authors`,
      {
        id: manuscript_author.id,
        manuscript_author: {
          author_id: formData.authors,
        },
      },
      {
        headers: this.httpHeaders,
      }
    );
  }
}
