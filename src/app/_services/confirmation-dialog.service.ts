import { Injectable } from '@angular/core';
import { MatDialog, MatDialogConfig } from "@angular/material/dialog";

import { ConfirmationDialogComponent } from "src/app/_core/confirmation-dialog/confirmation-dialog.component";

@Injectable({
  providedIn: 'root'
})
export class ConfirmationDialogService {

  constructor(public dialog: MatDialog) {}

  openConfirmationDialog(message) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = {      
      message: message
    };

    dialogConfig.width = "400px";
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, dialogConfig);

    return dialogRef;
  }
}
