import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { MatDialog, MatDialogConfig } from "@angular/material/dialog";

import { environment } from 'src/environments/environment';

import { MethodResponse } from 'src/app/_core/method-response';

import { AssignCorrespondenceDialogComponent } from "src/app/correspondence/_dialogs/assign-correspondence/assign-correspondence-dialog/assign-correspondence-dialog.component";

@Injectable({
  providedIn: "root",
})
export class CorrespondenceAssignmentService {

  apiUrl: string = environment.apiURL;

  httpHeaders = new HttpHeaders({
    "Content-Type": "application/json",
  });
  
  constructor(private http: HttpClient, public dialog: MatDialog) {}

  openAssignmentDialog(title, department_id, departmentItems, assignment?, assignedUserDepartment?, type?) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = {
      title: title,
      assignment: assignment,
      department_id: department_id,
      departmentItems: departmentItems,
      assignedUserDepartment: assignedUserDepartment,
      type: type
    };

    dialogConfig.width = "400px";
    // dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    const dialogRef = this.dialog.open(
      AssignCorrespondenceDialogComponent,
      dialogConfig
    );

    return dialogRef;
  }

  openAssignmentPreviewDialog(title, correspondenceAssignments) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = {
      title: title,
      assignments: correspondenceAssignments
    };

    dialogConfig.width = "400px";
    // dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    const dialogRef = this.dialog.open(
      AssignCorrespondenceDialogComponent,
      dialogConfig
    );

    return dialogRef;
  }

  openCurrentAssignmentPreviewDialog(title, assignment) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = {
      title: title,
      assignment: assignment
    };

    dialogConfig.width = "400px";
    // dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    const dialogRef = this.dialog.open(
      AssignCorrespondenceDialogComponent,
      dialogConfig
    );

    return dialogRef;
  }

  getAll(correspondence_id, direction, request_type?) {
    var url: string;
    if (direction == "internal") {
      url = `${this.apiUrl}crmgt/correspondence/internal/assignment?correspondence_id=${correspondence_id}`
    } else {
      url = `${this.apiUrl}crmgt/correspondence/external/assignment?correspondence_id=${correspondence_id}&request_type=${request_type}`
    }
    return this.http.get<MethodResponse>(url,
      {
        headers: this.httpHeaders,
      }
    );
  }

  get_url(direction, request_type) {
    var url: string;
    if (direction == "internal") {
      url = `${this.apiUrl}crmgt/correspondence/internal/assignment`
    } else {
      url = `${this.apiUrl}crmgt/correspondence/external/assignment?request_type=${request_type}`
    }
    return url;
  }

  create(to_user_id, correspondence_id, direction, request_type?) {
    return this.http.post<MethodResponse>(this.get_url(direction, request_type),
      {
        correspondence_assignment: {
          to_user_id: to_user_id,
          correspondence_id: correspondence_id          
        }
      },
      {
        headers: this.httpHeaders,
      }
    );
  }

  assignToOtherUser(to_user_id, assignment, direction, request_type?) {
    return this.http.put<MethodResponse>(this.get_url(direction, request_type),
      {
        id: assignment.id,
        correspondence_assignment: {
          to_user_id: to_user_id          
        }
      },
      {
        headers: this.httpHeaders,
      }
    );
  }

  receiveAssignment(status, receivedDate, remark, assignment, direction, request_type?) {
    return this.http.put<MethodResponse>(this.get_url(direction, request_type),
      {
        id: assignment.id,
        correspondence_assignment: {
          status: status,
          received_date: receivedDate,
          rejection_remark: remark
        }
      },
      {
        headers: this.httpHeaders,
      }
    );
  }
}
