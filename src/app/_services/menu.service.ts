import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { MatDialog, MatDialogConfig } from "@angular/material/dialog";

import { environment } from 'src/environments/environment';

import { MethodResponse } from "../_core/method-response";
import { MenuDialogComponent } from "../menu/menu-dialog/menu-dialog.component";

import { AuthenticationService } from "src/app/_services/authentication.service";

@Injectable({
  providedIn: "root",
})
export class MenuService {

  apiUrl: string = environment.apiURL;

  httpHeaders = new HttpHeaders({
    "Content-Type": "application/json",
  });

  app_module_id: number = Number(localStorage.getItem("App_module"));

  menuActions: any[] = [];

  constructor(
    private http: HttpClient,
    public dialog: MatDialog,
    private authenticationService: AuthenticationService
  ) {}

  getAll() {
    return this.http.get<MethodResponse>(`${this.apiUrl}/crmgt/auth/menus`, {
      headers: this.httpHeaders,
    });
  }

  create(roleName: string) {
    return this.http.post<MethodResponse>(`${this.apiUrl}/crmgt/auth/menus`, {
      name: roleName,
      application_module_id: this.app_module_id,
    });
  }

  update(menu, newData) {
    if (newData.parentMenus == 0) {
      newData.parentMenus = null;
    }
    return this.http.put<MethodResponse>(
      `${this.apiUrl}/crmgt/auth/menus/${menu.id}`,
      {
        menu: {
          text: newData.menuName,
          icon_cls: newData.icon,
          class_name: menu.class_name,
          location: menu.location,
          parent_id: newData.parentMenus,
          application_module_id: this.app_module_id,
        },
      },
      {
        headers: this.httpHeaders,
      }
    );
  }

  getMenuUsers(menuId) {
    return this.http.get<MethodResponse>(
      `${this.apiUrl}/crmgt/auth/menus/${menuId}/users`,
      {
        headers: this.httpHeaders,
      }
    );
  }

  openMenuDialog(title, menu, menus, users) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = {
      title: title,
      menu: menu,
      menus: menus,
      users: users,
    };

    dialogConfig.width = "400px";
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    const dialogRef = this.dialog.open(MenuDialogComponent, dialogConfig);

    return dialogRef;
  }

  getMenuActions(menu_item): string[] {
    var actions: string[] = [];
    this.authenticationService.menuActions.subscribe(
      (x) => (this.menuActions = x)
    );
    for (var menuAction of this.menuActions) {
      for (var menu of menuAction["menus"]) {
        if (menu["menu_text"] === menu_item) {
          for (var action of menu["actions"]) {
            actions.push(action["action_name"]);
          }
        }
      }
    }
    return actions;
  }
}
