import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { MatDialog, MatDialogConfig } from "@angular/material/dialog";

import { environment } from 'src/environments/environment';

import { MethodResponse } from "src/app/_core/method-response";
import { OrganizationDialogComponent } from "src/app/organization/organization-dialog/organization-dialog.component";

@Injectable({
  providedIn: "root",
})
export class OrganizationService {

  apiUrl: string = environment.apiURL;

  httpHeaders = new HttpHeaders({
    "Content-Type": "application/json",
  });

  constructor(private http: HttpClient, public dialog: MatDialog) {}

  getAll() {
    return this.http.get<MethodResponse>(
      `${this.apiUrl}/crmgt/business_settings/organization`,
      {
        headers: this.httpHeaders,
      }
    );
  }

  openOrganizationDialog(title, organization) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = {
      title: title,
      organization: organization,
    };
    dialogConfig.width = "400px";
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    const dialogRef = this.dialog.open(
      OrganizationDialogComponent,
      dialogConfig
    );

    return dialogRef;
  }

  create(code, description, organization_type_id) {
    return this.http.post<MethodResponse>(
      `${this.apiUrl}/crmgt/business_settings/organization`,
      {
        organization: {
          code: code,
          description: description,
          organization_type_id: organization_type_id,
        },
      },
      {
        headers: this.httpHeaders,
      }
    );
  }

  update(organizationId, code, description, organization_type_id) {
    return this.http.put<MethodResponse>(
      `${this.apiUrl}/crmgt/business_settings/organization`,
      {
        id: organizationId,
        organization: {
          code: code,
          description: description,
          organization_type_id: organization_type_id,
        },
      },
      {
        headers: this.httpHeaders,
      }
    );
  }
}
