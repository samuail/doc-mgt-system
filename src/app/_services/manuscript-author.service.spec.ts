import { TestBed } from '@angular/core/testing';

import { ManuscriptAuthorService } from './manuscript-author.service';

describe('ManuscriptAuthorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ManuscriptAuthorService = TestBed.get(ManuscriptAuthorService);
    expect(service).toBeTruthy();
  });
});
