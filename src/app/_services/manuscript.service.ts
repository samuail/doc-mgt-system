import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { MatDialog, MatDialogConfig } from "@angular/material/dialog";
import { map } from "rxjs/operators";

import { environment } from 'src/environments/environment';

import { MethodResponse } from "src/app/_core/method-response";

import { DateFormatterService } from "src/app/_services/date-formatter.service";

import { Manuscript } from 'src/app/_models/manuscript';

import { ManuscriptDialogComponent } from "src/app/correspondence/incomings/manuscript/_dialog/manuscript-dialog/manuscript-dialog.component";
import { ManuscriptUploaderComponent } from "src/app/correspondence/incomings/manuscript/_dialog/manuscript-uploader/manuscript-uploader.component";
import { DocPreviewerComponent } from "src/app/correspondence/_dialogs/doc-previewer/doc-previewer.component";
import { ManuscriptAuthorComponent } from "src/app/correspondence/incomings/manuscript/_dialog/manuscript-author/manuscript-author.component";
import { ManuscriptRoutingComponent } from "src/app/correspondence/incomings/manuscript/_dialog/manuscript-routing/manuscript-routing.component";
import { ManuscriptCommentComponent } from "src/app/correspondence/incomings/manuscript/_dialog/manuscript-comment/manuscript-comment.component";
import { ManuscriptArchiverComponent } from "src/app/correspondence/incomings/manuscript/_dialog/manuscript-archiver/manuscript-archiver.component";

@Injectable({
  providedIn: 'root'
})
export class ManuscriptService {

  apiUrl: string = environment.apiURL;

  httpHeaders = new HttpHeaders({
    "Content-Type": "application/json",
  });

  constructor(private http: HttpClient, public dialog: MatDialog,
              private dateFormatterService: DateFormatterService) {}
  

  openManuscriptDialog(type, title, authors, nextNumber?, manuscript?) {    
    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = {
      type: type,
      title: title,
      authors: authors,
      nextNumber: nextNumber,
      manuscript: manuscript
    };
    
    dialogConfig.width = "350px";
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    const dialogRef = this.dialog.open(
      ManuscriptDialogComponent,
      dialogConfig
    );

    return dialogRef;
  }

  openAuthorsDialog(authorActions, title, manuscript) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = {
      authorActions: authorActions,
      title: title,
      manuscript: manuscript
    };

    dialogConfig.panelClass = "doc-comment-dialog-container";
    dialogConfig.autoFocus = true;

    const dialogRef = this.dialog.open(ManuscriptAuthorComponent, dialogConfig);

    return dialogRef;
  }

  openManuscriptUploaderDialog(uploadActions, title, manuscript) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = {
      uploadActions: uploadActions,
      title: title,
      manuscript: manuscript
    };

    dialogConfig.panelClass = "doc-comment-dialog-container";
    dialogConfig.autoFocus = true;

    const dialogRef = this.dialog.open(ManuscriptUploaderComponent, dialogConfig);

    return dialogRef;
  }

  openManuscriptPreviewerDialog(previewActions, title, manuscript) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = {
      previewActions: previewActions,
      title: title,
      manuscript: manuscript,
    };

    dialogConfig.panelClass = "doc-previewer-dialog-container";
    dialogConfig.autoFocus = true;

    const dialogRef = this.dialog.open(DocPreviewerComponent, dialogConfig);

    return dialogRef;
  }

  openAssignManuscriptDialog(routeActions, title, manuscript) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = {
      routeActions: routeActions,
      title: title,
      manuscript: manuscript
    };

    dialogConfig.panelClass = "doc-comment-dialog-container";
    dialogConfig.autoFocus = true;

    const dialogRef = this.dialog.open(
      ManuscriptRoutingComponent,
      dialogConfig
    );

    return dialogRef;
  }

  openManuscriptCommentsDialog(commentActions, title, manuscript) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = {
      commentActions: commentActions,
      title: title,
      manuscript: manuscript
    };

    dialogConfig.panelClass = "doc-comment-dialog-container";
    dialogConfig.autoFocus = true;

    const dialogRef = this.dialog.open(
      ManuscriptCommentComponent,
      dialogConfig
    );

    return dialogRef;
  }

  openDocArchiverDialog(title, manuscript) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = {
      title: title,
      manuscript: manuscript
    };

    dialogConfig.width = "400px";
    dialogConfig.autoFocus = true;

    const dialogRef = this.dialog.open(
      ManuscriptArchiverComponent,
      dialogConfig
    );

    return dialogRef;
  }

  getAll() {
    return this.http.get<MethodResponse>(
      `${this.apiUrl}/crmgt/manuscript`,
      {
        headers: this.httpHeaders,
      }
    );    
  }

  assignManuscriptValue(data) {
    let incomingManuscript: Manuscript = new Manuscript();

    var receivedDateEC = this.dateFormatterService.convertDateToEC(data["received_date"]);

    incomingManuscript = {
      id: data["id"],
      referenceNo: data["reference_no"].substring(6, 10),
      referenceNoFull: data["reference_no"],
      receivedDate: data["received_date"],
      receivedDateEC: receivedDateEC,
      destination: data["destination"],
      title: data["title"],
      keyWord: data["search_keywords"],
      status: data["status"],
      keyInBy: data["key_in_by"],
      authors: data["authors"],
      currentStage: data["current_stage"],
      manuscriptArchive: data["manuscript_archive"]
    }

    return incomingManuscript;
  }

  getManuscript(dataSize = "all", filter = '', sortOrder = 'dsc', pageNumber = 0, 
        pageSize = 10) {
          let url = "";
          if (dataSize == "all") {
            url = `${this.apiUrl}/crmgt/manuscript`;
          } else if (dataSize == "assigned") {
            url = `${this.apiUrl}/crmgt/manuscript/assigned`;
          } else if (dataSize == "archived") {
            url = `${this.apiUrl}/crmgt/manuscript/archived`;
          }
          return this.http.get<MethodResponse>(url, {
            params: new HttpParams()                
                .set('filter', filter)
                .set('sortOrder', sortOrder)
                .set('pageNumber', pageNumber.toString())
                .set('pageSize', pageSize.toString())
        }).pipe(
            map(response => response)
        );
  }

  getAllManuscriptsByAuthor(author) {
    return this.http.get<MethodResponse>(
      `${this.apiUrl}/crmgt/manuscript/author/manuscripts?author_id=${author.id}`,
      {
        headers: this.httpHeaders,
      }
    );
  }

  getNextNumber() {
    return this.http.get<MethodResponse>(
      `${this.apiUrl}/crmgt/manuscript/get_next_no`,
      {
        headers: this.httpHeaders,
      }
    ); 
  }

  create(data) {
    var formData = data.manuscriptForm.value;
    var today: number = new Date().getFullYear();
    var referenceNo = 'AAUPM-' + formData.referenceNo + '-' + today;

    return this.http.post<MethodResponse>(
      `${this.apiUrl}/crmgt/manuscript`,
      {        
        manuscript: {
          reference_no: referenceNo,
          title: formData.title,
          search_keywords: formData.keyWord.split(", "),
        }
      },
      {
        headers: this.httpHeaders,
      }
    );
  }

  update(manuscript, data) {
    var formData = data.manuscriptForm.value;

    return this.http.put<MethodResponse>(
      `${this.apiUrl}/crmgt/manuscript`,
      {        
        id: manuscript.id,
        manuscript: {
          title: formData.title,
          search_keywords: formData.keyWord.split(", "),
        }
      },
      {
        headers: this.httpHeaders,
      }
    );
  }

  updateStatus(manuscript, status) {
    return this.http.put<MethodResponse>(
      `${this.apiUrl}/crmgt/manuscript/update_status`,
      {
        id: manuscript.id,
        status: status
      },
      {
        headers: this.httpHeaders,
      }
    );
  }

  archive(manuscript, formData) {    
    let noPages = formData.noPages;
    let noCopies = formData.noCopies;
    let hasArchiveAtt = formData.hasArchiveAtt;
    let remark = formData.remark;
    let status = "Archived";
    return this.http.put<MethodResponse>(
      `${this.apiUrl}/crmgt/manuscript/archive`,
      {        
        id: manuscript.id,
        status: status,
        no_pages: noPages,
        no_copies: noCopies,
        remark: remark,
        has_archive_att: hasArchiveAtt
      },
      {
        headers: this.httpHeaders,
      }
    );
  }
}
