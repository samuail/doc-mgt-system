import { TestBed } from '@angular/core/testing';

import { ExternalAssessorService } from './external-assessor.service';

describe('ExternalAssessorService', () => {
  let service: ExternalAssessorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ExternalAssessorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
