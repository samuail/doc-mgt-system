import { Injectable } from '@angular/core';
import { MatDialog, MatDialogConfig } from "@angular/material/dialog";

import { AuthorRoyaltyDialogComponent } from "src/app/author/author-royalty-dialog/author-royalty-dialog.component";

@Injectable({
  providedIn: 'root'
})
export class RoyaltyService {

  constructor(public dialog: MatDialog) { }

  openRoyaltyPaymentDialog(title, author) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = {
      title: title,
      author: author
    };
    
    dialogConfig.panelClass = "doc-comment-dialog-container";
    dialogConfig.autoFocus = true;

    const dialogRef = this.dialog.open(
      AuthorRoyaltyDialogComponent,
      dialogConfig
    );

    return dialogRef;
  }
}
