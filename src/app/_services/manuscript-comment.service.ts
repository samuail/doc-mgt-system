import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { MatDialog, MatDialogConfig } from "@angular/material/dialog";

import { environment } from 'src/environments/environment';
import { MethodResponse } from 'src/app/_core/method-response';
import { ManuscriptComment } from '../_models/manuscript-comment';

import { ManuscriptCommentDialogComponent } from "src/app/correspondence/incomings/manuscript/_dialog/manuscript-comment/manuscript-comment-dialog/manuscript-comment-dialog.component";

@Injectable({
  providedIn: 'root'
})
export class ManuscriptCommentService {

  apiUrl: string = environment.apiURL;

  httpHeaders = new HttpHeaders({
    "Content-Type": "application/json",
  });

  constructor(public dialog: MatDialog,
      private http: HttpClient) { }

  assignManuscriptCommentValue(value) {
    let manuscriptComment: ManuscriptComment = new ManuscriptComment();
    manuscriptComment = {
      id: value["id"],
      commentedBy: value["commented_by"],
      manuscript: value["manuscript"],
      content: value["content"],
      commentDate: value["comment_date"],
    }
    return manuscriptComment;
  }

  openCommentDialog(title, comment?) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = {
      title: title,
      comment: comment,
    };

    dialogConfig.width = "400px";
    dialogConfig.autoFocus = true;

    if (title == "Add Comment") {
      dialogConfig.disableClose = true;
    } else {
      dialogConfig.disableClose = false;
    }

    const dialogRef = this.dialog.open(ManuscriptCommentDialogComponent, dialogConfig);

    return dialogRef;
  }

  getAll(manuscript) {
    return this.http.get<MethodResponse>(
      `${this.apiUrl}crmgt/manuscript/comments?manuscript_id=${manuscript.id}`,
      {
        headers: this.httpHeaders,
      }
    );
  }

  create(content, manuscript_id) {
    return this.http.post<MethodResponse>(
      `${this.apiUrl}crmgt/manuscript/comments`,
      {
        manuscript_comment: {
          manuscript_id: manuscript_id,
          content: content
        }
      },
      {
        headers: this.httpHeaders,
      }
    );
  }

  update(content, comment) {
    return this.http.put<MethodResponse>(
      `${this.apiUrl}crmgt/manuscript/comments`,
      {
        id: comment.id,
        manuscript_comment: {
          content: content
        }
      },
      {
        headers: this.httpHeaders,
      }
    );
  }

}
