import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { MatDialog, MatDialogConfig } from "@angular/material/dialog";

import { environment } from 'src/environments/environment';

import { MethodResponse } from "src/app/_core/method-response";

import { AssessorDialogComponent } from 'src/app/external-assessor/_dialog/assessor-dialog/assessor-dialog.component';
import { ExternalAssessor } from 'src/app/_models/external-assessor';

@Injectable({
  providedIn: 'root'
})
export class ExternalAssessorService {
  
  apiUrl: string = environment.apiURL;

  httpHeaders = new HttpHeaders({
    "Content-Type": "application/json",
  });

  constructor(private http: HttpClient, public dialog: MatDialog) { }

  openAssessorDialog(title, assessor?) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = {
      title: title,
      assessor: assessor
    };
    
    dialogConfig.panelClass = "assessor-dialog-container";
    dialogConfig.autoFocus = true;

    if (title == "Add Assessor") {
      dialogConfig.disableClose = true;
    } else {
      dialogConfig.disableClose = false;
    }

    const dialogRef = this.dialog.open(
      AssessorDialogComponent,
      dialogConfig
    );

    return dialogRef;
  }

  assignAssessorValue(data) {
    let externalAssessor: ExternalAssessor = new ExternalAssessor();
    externalAssessor = {
      id: data["id"],
      title: data["title"],
      firstName: data["first_name"],
      fatherName: data["father_name"],
      grandFatherName: data["grand_father_name"],
      email: data["email"],
      telephone: data["telephone"],
      fullName: data["first_name"] + " " + data["father_name"]  + " " + data["grand_father_name"],
      areaOfExpertise: data["area_of_expertise"],
      institution: data["institution"],
      bankName: data["bank_name"],
      bankAcc: data["bank_acc"],
      tin: data["tin"]
      
    }
    return externalAssessor; 
  }

  getAll() {
    return this.http.get<MethodResponse>(
      `${this.apiUrl}/crmgt/business_settings/external_assessor`,
      {
        headers: this.httpHeaders,
      }
    );
  }

  create(data) {
    var formData = data.assessorForm.value;
    return this.http.post<MethodResponse>(
      `${this.apiUrl}/crmgt/business_settings/external_assessor`,
      {
        external_assessor: {
          title: formData.title,
          first_name: formData.firstName,
          father_name: formData.lastName,
          grand_father_name: formData.dFatherName,
          email: formData.email,
          telephone: formData.telephone,
          area_of_expertise: formData.areaOfExpertise,
          institution: formData.institutions,
          bank_name: formData.bName,
          bank_acc: formData.bAcc,
          tin: formData.tin
        }
      },
      {
        headers: this.httpHeaders,
      }
    );
  }

  update(data, assessor) {
    var formData = data.assessorForm.value;
    return this.http.put<MethodResponse>(
      `${this.apiUrl}/crmgt/business_settings/external_assessor`,
      {
        id: assessor.id,
        external_assessor: {
          title: formData.title,
          first_name: formData.firstName,
          father_name: formData.lastName,
          grand_father_name: formData.dFatherName,
          email: formData.email,
          telephone: formData.telephone,
          area_of_expertise: formData.areaOfExpertise,
          institution: formData.institutions,
          bank_name: formData.bName,
          bank_acc: formData.bAcc,
          tin: formData.tin
        }
      },
      {
        headers: this.httpHeaders,
      }
    );
  }
}
