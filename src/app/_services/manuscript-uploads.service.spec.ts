import { TestBed } from '@angular/core/testing';

import { ManuscriptUploadsService } from './manuscript-uploads.service';

describe('ManuscriptUploadsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ManuscriptUploadsService = TestBed.get(ManuscriptUploadsService);
    expect(service).toBeTruthy();
  });
});
