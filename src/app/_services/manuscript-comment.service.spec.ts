import { TestBed } from '@angular/core/testing';

import { ManuscriptCommentService } from './manuscript-comment.service';

describe('ManuscriptCommentService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ManuscriptCommentService = TestBed.get(ManuscriptCommentService);
    expect(service).toBeTruthy();
  });
});
