import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { MatDialog, MatDialogConfig } from "@angular/material/dialog";

import { environment } from 'src/environments/environment';

import { ImgPreviewerDialogComponent } from "src/app/correspondence/_dialogs/doc-uploads/img-previewer-dialog/img-previewer-dialog.component";

@Injectable({
  providedIn: 'root'
})
export class UploadService {
  
  apiUrl: string = environment.apiURL;

  constructor(private http: HttpClient, public dialog: MatDialog) {}

  openDocPreviewerDialog(image, title?) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = {
      image: image,
      title: title
    };

    dialogConfig.panelClass = "img-previewer-dialog-container";
    dialogConfig.autoFocus = true;

    const dialogRef = this.dialog.open(
      ImgPreviewerDialogComponent,
      dialogConfig
    );

    return dialogRef;
  }

  upload(formData: FormData, direction) {
    var url: string;
    if (direction == "internal") {
      url = `${this.apiUrl}crmgt/correspondence/internal/upload`;
    } else {
      url = `${this.apiUrl}crmgt/correspondence/external/upload`;
    }
    return this.http.post<any>(url, formData, {  
      reportProgress: true,  
      observe: 'events'  
    });
  }

  uploadManuscript(formData: FormData) {
    return this.http.post<any>(`${this.apiUrl}crmgt/manuscript/upload`, formData, {  
      reportProgress: true,  
      observe: 'events'  
    });
  }

  uploadManuscriptArchive(formData: FormData) {
    return this.http.post<any>(`${this.apiUrl}crmgt/manuscript/upload_archives`, formData, {  
      reportProgress: true,  
      observe: 'events'  
    });
  }

  change_upload(formData: FormData, direction) {
    var url: string;
    if (direction == "internal") {
      url = `${this.apiUrl}crmgt/correspondence/internal/change_uploads`;
    } else {
      url = `${this.apiUrl}crmgt/correspondence/external/change_uploads`;
    }
    return this.http.post<any>(url, formData, {  
      reportProgress: true,  
      observe: 'events'  
    });
  }

  changeManuscriptUpload(formData) {
    return this.http.post<any>(`${this.apiUrl}crmgt/manuscript/change_uploads`, formData, {  
      reportProgress: true,  
      observe: 'events'  
    });
  }

  changeManuscriptArchive(formData) {
    return this.http.post<any>(`${this.apiUrl}crmgt/manuscript/change_upload_archives`, formData, {  
      reportProgress: true,  
      observe: 'events'  
    });
  }
}
