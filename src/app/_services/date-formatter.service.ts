import { Injectable } from '@angular/core';

declare var $: any;

@Injectable({
  providedIn: 'root'
})
export class DateFormatterService {

  constructor() { }

  convertDateToGC(date) {
    var letter_dateEC = date;
    var jdEC = $.calendars.instance('ethiopian').newDate( 
                parseInt(letter_dateEC.substr(6,4), 10), 
                parseInt(letter_dateEC.substr(3,2), 10), 
                parseInt(letter_dateEC.substr(0,2), 10)).toJD();

    var dateGC = $.calendars.instance('gregorian').fromJD(jdEC);
    var letter_dateGC = dateGC["_year"] + "-" + dateGC["_month"] + "-" + dateGC["_day"];

    return letter_dateGC;
  }

  convertDateToEC(date) {
    var letter_dateGC = date;
    var jdGC = $.calendars.instance('gregorian').newDate( 
                parseInt(letter_dateGC.substr(0,4), 10), 
                parseInt(letter_dateGC.substr(5,2), 10), 
                parseInt(letter_dateGC.substr(8,2), 10)).toJD();

    var dateEC = $.calendars.instance('ethiopian').fromJD(jdGC);
    
    var day = dateEC["_day"] < 10 ? "0" + dateEC["_day"] : dateEC["_day"];
    var month = dateEC["_month"] < 10 ? "0" + dateEC["_month"] : dateEC["_month"];
    
    var letter_dateEC = day + "-" + month + "-" + dateEC["_year"];

    return letter_dateEC;
  }
}
