import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { MatDialog, MatDialogConfig } from "@angular/material/dialog";

import { environment } from 'src/environments/environment';

import { UploadsDialogComponent } from "src/app/correspondence/_dialogs/doc-uploads/uploads-dialog/uploads-dialog.component";
import { ImgPreviewerDialogComponent } from "src/app/correspondence/_dialogs/doc-uploads/img-previewer-dialog/img-previewer-dialog.component";

@Injectable({
  providedIn: 'root'
})
export class ManuscriptUploadsService {
  apiUrl: string = environment.apiURL;

  httpHeaders = new HttpHeaders({
    "Content-Type": "application/json",
  });

  constructor(private http: HttpClient,public dialog: MatDialog) { }

  openUploadsDialog(title, manuscript, image?) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = {
      title: title,
      manuscript: manuscript,
      image: image,
    };

    dialogConfig.width = "400px";
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    const dialogRef = this.dialog.open(UploadsDialogComponent, dialogConfig);

    return dialogRef;
  }

  openDocPreviewerDialog(title, document) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = {
      title: title,
      image: document,
    };

    dialogConfig.panelClass = "img-previewer-dialog-container";
    dialogConfig.autoFocus = true;

    const dialogRef = this.dialog.open(
      ImgPreviewerDialogComponent,
      dialogConfig
    );

    return dialogRef;
  }

  getUploaded(manuscript, title) { 
    let url = ""
    if (title == "Manuscript Preview") {
      url = `${this.apiUrl}/crmgt/manuscript/get_documents?id=${manuscript.id}`;
    } else if(title == "Manuscript Archive Preview") {
      url = `${this.apiUrl}/crmgt/manuscript/get_archive_att?id=${manuscript.id}`; 
    }
    return this.http.get<any>(url, 
      {
        headers: this.httpHeaders,
      }
    );
  }

}
