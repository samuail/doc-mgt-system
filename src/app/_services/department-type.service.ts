import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { MatDialog, MatDialogConfig } from "@angular/material/dialog";

import { environment } from 'src/environments/environment';

import { MethodResponse } from "src/app/_core/method-response";
import { DepartmentTypeDialogComponent } from "src/app/department-type/_dialogs/department-type-dialog/department-type-dialog.component";

@Injectable({
  providedIn: "root",
})
export class DepartmentTypeService {

  apiUrl: string = environment.apiURL;

  httpHeaders = new HttpHeaders({
    "Content-Type": "application/json",
  });

  constructor(private http: HttpClient, public dialog: MatDialog) {}

  getAll() {
    return this.http.get<MethodResponse>(
      `${this.apiUrl}/crmgt/business_settings/department_type`,
      {
        headers: this.httpHeaders,
      }
    );
  }

  openDepartmentTypeDialog(title, departmentType) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = {
      title: title,
      departmentType: departmentType,
    };
    dialogConfig.width = "400px";
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    const dialogRef = this.dialog.open(
      DepartmentTypeDialogComponent,
      dialogConfig
    );

    return dialogRef;
  }

  create(code, name) {
    return this.http.post<MethodResponse>(
      `${this.apiUrl}/crmgt/business_settings/department_type`,
      {
        lookup: {
          code: code,
          name: name,
        },
      },
      {
        headers: this.httpHeaders,
      }
    );
  }
  update(departmentTypeId, code, name) {
    return this.http.put<MethodResponse>(
      `${this.apiUrl}/crmgt/business_settings/department_type`,
      {
        id: departmentTypeId,
        lookup: {
          code: code,
          name: name,
        },
      },
      {
        headers: this.httpHeaders,
      }
    );
  }
}
