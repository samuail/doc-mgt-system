import { TestBed } from '@angular/core/testing';

import { CorrespondenceAssignmentService } from './correspondence-assignment.service';

describe('CorrespondenceAssignmentService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CorrespondenceAssignmentService = TestBed.get(CorrespondenceAssignmentService);
    expect(service).toBeTruthy();
  });
});
