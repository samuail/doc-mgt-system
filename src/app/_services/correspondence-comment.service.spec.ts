import { TestBed } from '@angular/core/testing';

import { CorrespondenceCommentService } from './correspondence-comment.service';

describe('CorrespondenceCommentService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CorrespondenceCommentService = TestBed.get(CorrespondenceCommentService);
    expect(service).toBeTruthy();
  });
});
