import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { MatDialog, MatDialogConfig } from "@angular/material/dialog";

import { environment } from 'src/environments/environment';

import { MethodResponse } from "src/app/_core/method-response";

import { UploadsDialogComponent } from "src/app/correspondence/_dialogs/doc-uploads/uploads-dialog/uploads-dialog.component";
import { ImgPreviewerDialogComponent } from "src/app/correspondence/_dialogs/doc-uploads/img-previewer-dialog/img-previewer-dialog.component";

@Injectable({
  providedIn: "root",
})
export class CorrespondenceUploadsService {

  apiUrl: string = environment.apiURL;

  httpHeaders = new HttpHeaders({
    "Content-Type": "application/json",
  });

  constructor(private http: HttpClient, public dialog: MatDialog) {}

  openUploadsDialog(title, correspondence, type, direction, request_type?, image?) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = {
      title: title,
      correspondence: correspondence,
      type: type,
      direction: direction,
      request_type: request_type,
      image: image,
    };

    dialogConfig.width = "400px";
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    const dialogRef = this.dialog.open(UploadsDialogComponent, dialogConfig);

    return dialogRef;
  }

  openDocPreviewerDialog(title, image) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = {
      title: title,
      image: image,
    };

    dialogConfig.panelClass = "img-previewer-dialog-container";
    dialogConfig.autoFocus = true;

    const dialogRef = this.dialog.open(
      ImgPreviewerDialogComponent,
      dialogConfig
    );

    return dialogRef;
  }

  getUploaded(correspondence, direction, request_type?) {
    var url: string;
    if (direction == "internal") {
      url = `${this.apiUrl}crmgt/correspondence/internal/get_images?id=${correspondence.id}`
    } else {
      url = `${this.apiUrl}crmgt/correspondence/external/get_images?id=${correspondence.id}&request_type=${request_type}`
    }
    
    return this.http.get<any>(url, 
      {
        headers: this.httpHeaders,
      }
    );
  }

  upload(correspondence, type, file) {
    return this.http.post<MethodResponse>(
      `${this.apiUrl}crmgt/correspondence/internal/upload`,
      {
        id: correspondence.id,
        image: {
          type: type,
          files: file
        }
      },
      {
        headers: this.httpHeaders,
      }
    );    
  }
}
