import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatDialog, MatDialogConfig } from "@angular/material/dialog";

import { ManuscriptRoutingDialogComponent } from 'src/app/correspondence/incomings/manuscript/_dialog/manuscript-routing/manuscript-routing-dialog/manuscript-routing-dialog.component';
import { MethodResponse } from 'src/app/_core/method-response';
import { environment } from 'src/environments/environment';
import { ManuscriptAssignment } from 'src/app/_models/manuscript-assignment';

@Injectable({
  providedIn: 'root'
})
export class ManuscriptRoutingService {

  apiUrl: string = environment.apiURL;

  httpHeaders = new HttpHeaders({
    "Content-Type": "application/json",
  });

  constructor(public dialog: MatDialog,
      private http: HttpClient) { }

  openAddAssignmentDialog(title, manuscript, assignment?, userRoleId?) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = {
      title: title,
      manuscript: manuscript,
      manuscript_assignment: assignment,
      user_role_id: userRoleId
    };

    dialogConfig.width = "350px";    
    dialogConfig.autoFocus = true;
    if (title == "Add Assignment") {
      dialogConfig.disableClose = true;
    } else {
      dialogConfig.disableClose = false;
    }

    const dialogRef = this.dialog.open(
      ManuscriptRoutingDialogComponent,
      dialogConfig
    );

    return dialogRef;
  }

  openAssignmentPreviewDialog(title, manuscriptAssignments) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = {
      title: title,
      manuscript_assignments: manuscriptAssignments
    };

    dialogConfig.width = "350px";
    dialogConfig.autoFocus = true;

    const dialogRef = this.dialog.open(
      ManuscriptRoutingDialogComponent,
      dialogConfig
    );

    return dialogRef;
  }

  assignManuscriptAssignmentValue(value) {
    let manuscriptAssignment: ManuscriptAssignment = new ManuscriptAssignment();
    let assigneeType = value["assignee_type"] == 'Sate::Auth::User' ? "internal" : "external";
    manuscriptAssignment = {
          id: value["id"],
          stage: value["stage"],
          assigneeType: assigneeType,
          fromUser: value["from_user"],
          assignee: value["assignee"],
          manuscript: value["manuscript"],          
          assignedDate: value["assigned_date"],
          receivedDate: value["received_date"],
          status: value["status"]
        };
    return manuscriptAssignment;
  }

  getAll(manuscript) {
    return this.http.get<MethodResponse>(
      `${this.apiUrl}crmgt/manuscript/assignments?manuscript_id=${manuscript.id}`,
      {
        headers: this.httpHeaders,
      }
    );
  }

  create(data, manuscript) {
    var formData = data.routingForm.value;
    var toAssignee;
    var toAssigneeType;  
    if (formData.stages == "Incorporate Comments") {
      toAssignee = formData.assignedTo;
      toAssigneeType = "Author"

    } else {
      if (formData.type == '' || formData.type == "internal") {
        toAssignee = formData.assignedTo;
        toAssigneeType = "User"
      } else {
        toAssignee = formData.assignedTo;
        toAssigneeType = "External Assessor"
      }
    }
    return this.http.post<MethodResponse>(
      `${this.apiUrl}crmgt/manuscript/assignments`,
      {
        assignee_type: toAssigneeType,
        manuscript_assignment: {
          stage: formData.stages,
          assignee_id: toAssignee,
          manuscript_id: manuscript.id
        }
      },
      {
        headers: this.httpHeaders,
      }
    );    
  }

  update(data, assignment, manuscript) {
    var formData = data.routingForm.value;
    var toAssignee;
    var toAssigneeType;  
    if (formData.stages == "Incorporate Comments") {
      toAssignee = formData.assignedTo;
      toAssigneeType = "Author"

    } else {
      if (formData.type == '' || formData.type == "internal") {
        toAssignee = formData.assignedTo;
        toAssigneeType = "User"
      } else {
        toAssignee = formData.assignedTo;
        toAssigneeType = "External Assessor"
      }
    }
    return this.http.put<MethodResponse>(
      `${this.apiUrl}crmgt/manuscript/assignments`,
      {
        id: assignment.id,
        assignee_type: toAssigneeType,
        manuscript_assignment: {
          stage: formData.stages,
          assignee_id: toAssignee,
          manuscript_id: manuscript.id
        }
      },
      {
        headers: this.httpHeaders,
      }
    );    
  }
}
