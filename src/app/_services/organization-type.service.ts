import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { MatDialog, MatDialogConfig } from "@angular/material/dialog";

import { environment } from 'src/environments/environment';

import { MethodResponse } from "src/app/_core/method-response";
import { OrganizationTypeDialogComponent } from "src/app/organization-type/organization-type-dialog/organization-type-dialog.component";

@Injectable({
  providedIn: "root",
})
export class OrganizationTypeService {

  apiUrl: string = environment.apiURL;

  httpHeaders = new HttpHeaders({
    "Content-Type": "application/json",
  });

  constructor(private http: HttpClient, public dialog: MatDialog) {}

  getAll() {
    return this.http.get<MethodResponse>(
      `${this.apiUrl}/crmgt/business_settings/organization_type`,
      {
        headers: this.httpHeaders,
      }
    );
  }

  openOrganizationTypeDialog(title, organizationType) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = {
      title: title,
      organizationType: organizationType,
    };
    dialogConfig.width = "400px";
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    const dialogRef = this.dialog.open(
      OrganizationTypeDialogComponent,
      dialogConfig
    );

    return dialogRef;
  }

  create(code, name) {
    return this.http.post<MethodResponse>(
      `${this.apiUrl}/crmgt/business_settings/organization_type`,
      {
        lookup: {
          code: code,
          name: name,
        },
      },
      {
        headers: this.httpHeaders,
      }
    );
  }

  update(organizationTypeId, code, name) {
    return this.http.put<MethodResponse>(
      `${this.apiUrl}/crmgt/business_settings/organization_type`,
      {
        id: organizationTypeId,
        lookup: {
          code: code,
          name: name,
        },
      },
      {
        headers: this.httpHeaders,
      }
    );
  }
}
