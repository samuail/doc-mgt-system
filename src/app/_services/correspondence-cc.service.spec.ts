import { TestBed } from '@angular/core/testing';

import { CorrespondenceCcService } from './correspondence-cc.service';

describe('CorrespondenceCcService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CorrespondenceCcService = TestBed.get(CorrespondenceCcService);
    expect(service).toBeTruthy();
  });
});
