import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { MatDialog, MatDialogConfig } from "@angular/material/dialog";

import { environment } from 'src/environments/environment';

import { MethodResponse } from "src/app/_core/method-response";

import { CcDialogComponent } from "src/app/correspondence/_dialogs/doc-cc/cc-dialog/cc-dialog.component";

@Injectable({
  providedIn: 'root'
})
export class CorrespondenceCcService {

  apiUrl: string = environment.apiURL;

  httpHeaders = new HttpHeaders({
    "Content-Type": "application/json",
  });

  constructor(private http: HttpClient, public dialog: MatDialog) { }

  getAll(correspondence_id, direction, request_type?) {
    var url: string;
    if (direction == "internal") {
      url = `${this.apiUrl}crmgt/correspondence/internal/ccs?correspondence_id=${correspondence_id}`
    } else {
      url = `${this.apiUrl}crmgt/correspondence/external/ccs?correspondence_id=${correspondence_id}&direction=${direction}&request_type=${request_type}`
    }
    return this.http.get<MethodResponse>(url,
      {
        headers: this.httpHeaders,
      }
    );
  }

  get_url(direction, request_type) {
    var url: string;
    if (direction == "internal") {
      url = `${this.apiUrl}crmgt/correspondence/internal/ccs`
    } else {
      url = `${this.apiUrl}crmgt/correspondence/external/ccs?direction=${direction}&request_type=${request_type}`
    }
    return url;
  }
  
  create(formData, correspondence_id, direction, request_type) {
    return this.http.post<MethodResponse>(this.get_url(direction, request_type),
      {
        correspondence_carbon_copy: {
          correspondence_id: correspondence_id,
          destination_id: formData.from
        }
      },
      {
        headers: this.httpHeaders,
      }
    );
  }

  update(formData, correspondenceCc_id, direction, request_type) {
    return this.http.put<MethodResponse>(this.get_url(direction, request_type),
      {
        id: correspondenceCc_id,
        correspondence_carbon_copy: {          
          destination_id: formData.from
        }
      },
      {
        headers: this.httpHeaders,
      }
    );
  }

  openCcDialog(title, departmentItems, correspondenceCc?) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = {
      title: title,
      departmentItems: departmentItems,
      correspondenceCc: correspondenceCc,
    };

    dialogConfig.width = "400px";
    dialogConfig.autoFocus = true;

    const dialogRef = this.dialog.open(CcDialogComponent, dialogConfig);

    return dialogRef;
  }
}
