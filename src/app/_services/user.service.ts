import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { MatDialog, MatDialogConfig } from "@angular/material/dialog";

import { environment } from 'src/environments/environment';

import { MethodResponse } from "../_core/method-response";

import { UserDialogComponent } from "../user/user-dialog/user-dialog.component";
import { UserDepartmentDialogComponent } from "src/app/user/user-department-dialog/user-department-dialog.component";
import { UserMenuActionDialogComponent } from "src/app/user/user-menu-action-dialog/user-menu-action-dialog.component";
import { AuthenticationService } from "./authentication.service";

import { DepartmentRole } from "src/app/_models/department-role";

@Injectable({
  providedIn: "root",
})
export class UserService {

  apiUrl: string = environment.apiURL;

  userDepartment: DepartmentRole;

  httpHeaders = new HttpHeaders({
    "Content-Type": "application/json",
  });

  app_module_id: number = Number(localStorage.getItem("App_module"));

  constructor(private http: HttpClient, public dialog: MatDialog, 
    private authenticationService: AuthenticationService) {}

  getCurrentUserDepartments() {
    this.authenticationService.userDepartment.subscribe(
      (x) => { this.userDepartment = x; }
    );
    return this.userDepartment;
  }

  getAll() {
    return this.http.get<MethodResponse>(`${this.apiUrl}/crmgt/auth/users`, {
      headers: this.httpHeaders,
    });
  }

  getUserRoles(userId) {
    return this.http.get<MethodResponse>(
      `${this.apiUrl}/crmgt/auth/users/${userId}/user_roles`,
      {
        headers: this.httpHeaders,
      }
    );
  }

  getUserMenus(userId) {
    return this.http.get<MethodResponse>(
      `${this.apiUrl}/crmgt/auth/users/${userId}/menus`,
      {
        headers: this.httpHeaders,
      }
    );
  }

  getMenuActions(menu_names) {
    let params = new HttpParams();

    menu_names.forEach((menuName: string) => {
      params = params.append(`menu_names[]`, menuName);
    });
    return this.http.get<MethodResponse>(`${this.apiUrl}/crmgt/auth/menu/actions`, {
      params: params,
      headers: this.httpHeaders,
    });
  }

  getAssignedMenuActions(userId) {
    return this.http.get<MethodResponse>(
      `${this.apiUrl}/crmgt/auth/user/menu/actions?user_id=${userId}`,
      {
        headers: this.httpHeaders,
      }
    );
  }

  assignUserMenuActions(userId, menuActionIds) {
    return this.http.post<MethodResponse>(
      `${this.apiUrl}/crmgt/auth/user/menu/actions`,
      {
        user_id: userId,
        menu_action_ids: menuActionIds,
      },
      {
        headers: this.httpHeaders,
      }
    );
  }

  getUserDepartments(userId) {
    return this.http.get<MethodResponse>(
      `${this.apiUrl}/crmgt/auth/user/${userId}/department_role`,
      {
        headers: this.httpHeaders,
      }
    );
  }

  getAllUsersWithinDepartment(deptId) { 
    return this.http.get<MethodResponse>(
      `${this.apiUrl}/crmgt/business_settings/department/${deptId}/users`,
      {
        headers: this.httpHeaders,
      }
    );
  }

  getAllUsersWithTheRole(deptId, roleId){
    return this.http.get<MethodResponse>(
      `${this.apiUrl}/crmgt/business_settings/department/users?department_id=${deptId}&role_id=${roleId}`,
      {
        headers: this.httpHeaders,
      }
    );
  }

  getUserAssignedDocs(users) {
    let userIds = [];
    for (var user of users) {
      userIds.push(user.id);
    }
    return this.http.get<MethodResponse>(
      `${this.apiUrl}/crmgt/auth/user/${userIds}/assigned_document`,
      {
        headers: this.httpHeaders,
      }
    );
  }

  assignUserMenus(userId, menuIds) {
    return this.http.post<MethodResponse>(
      `${this.apiUrl}/crmgt/auth/users/${userId}/menus`,
      {
        menuIds: menuIds,
      },
      {
        headers: this.httpHeaders,
      }
    );
  }

  assignUserRoles(userId, roleIds) {
    return this.http.post<MethodResponse>(
      `${this.apiUrl}/crmgt/auth/users/${userId}/user_roles`,
      {
        userRoleIds: roleIds,
      },
      {
        headers: this.httpHeaders,
      }
    );
  }

  assignUserDepartmentRoles(userId, departmentId, roleId) {
    return this.http.post<MethodResponse>(
      `${this.apiUrl}/crmgt/auth/user/department_role`,
      {
        user_department_role: {
          user_id: userId,
          department_id: departmentId,
          user_role_id: roleId,
        },
      },
      {
        headers: this.httpHeaders,
      }
    );
  }

  updateUserDepartmentRoles(userDepartmentId, userId, departmentId, roleId) {
    return this.http.put<MethodResponse>(
      `${this.apiUrl}/crmgt/auth/user/department_role`,
      {
        id: userDepartmentId,
        user_department_role: {
          user_id: userId,
          department_id: departmentId,
          user_role_id: roleId,
        },
      },
      {
        headers: this.httpHeaders,
      }
    );
  }

  create(data) {
    return this.http.post<MethodResponse>(
      `${this.apiUrl}/crmgt/auth/users`,
      {
        user: {
          first_name: data.firstName,
          last_name: data.lastName,
          email: data.userName,
          password: data.password,
          application_module_id: this.app_module_id,
        },
      },
      {
        headers: this.httpHeaders,
      }
    );
  }

  update(userId: number, data) {
    return this.http.put<MethodResponse>(
      `${this.apiUrl}/crmgt/auth/users/${userId}`,
      {
        first_name: data.firstName,
        last_name: data.lastName,
        email: data.userName,
        password: data.password,
        application_module_id: this.app_module_id,
      },
      {
        headers: this.httpHeaders,
      }
    );
  }

  changeUserStatus(userId, status) {
    return this.http.post<MethodResponse>(
      `${this.apiUrl}/crmgt/auth/users/${userId}/change_status`,
      {
        status: status,
      },
      {
        headers: this.httpHeaders,
      }
    );
  }

  changePassword(userId, data) {
    return this.http.post<MethodResponse>(
      `${this.apiUrl}/crmgt/auth/user/change_password`,
      {
        id: userId,
        password: data.password,
      },
      {
        headers: this.httpHeaders,
      }
    );
  }

  openUserDialog(title, user) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = {
      title: title,
      user: user,
    };

    dialogConfig.width = "400px";
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    const dialogRef = this.dialog.open(UserDialogComponent, dialogConfig);

    return dialogRef;
  }

  openUserDepartmentRoleDialog(title, userDepartmentRole, departmentItems, roles) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = {
      title: title,
      userDepartmentRole: userDepartmentRole,
      departmentItems: departmentItems,
      roles: roles,
    };

    dialogConfig.width = "400px";
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    const dialogRef = this.dialog.open(
      UserDepartmentDialogComponent,
      dialogConfig
    );

    return dialogRef;
  }

  openUserMenuActionDialog(title, menu_actions, assigned_menu_actions) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = {
      title: title,
      menu_actions: menu_actions,
      assigned_menu_actions: assigned_menu_actions,
    };

    dialogConfig.width = "400px";
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    const dialogRef = this.dialog.open(
      UserMenuActionDialogComponent,
      dialogConfig
    );

    return dialogRef;
  }
}
