import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";

import { MatDialog, MatDialogConfig } from "@angular/material/dialog";

import { environment } from 'src/environments/environment';

import { MethodResponse } from "../_core/method-response";

import { CommentDialogComponent } from "src/app/correspondence/_dialogs/doc-comment/comment-dialog/comment-dialog.component";

@Injectable({
  providedIn: "root",
})
export class CorrespondenceCommentService {

  apiUrl: string = environment.apiURL;

  httpHeaders = new HttpHeaders({
    "Content-Type": "application/json",
  });

  constructor(private http: HttpClient, public dialog: MatDialog) {}

  getAll(correspondence_id, direction, request_type?) {
    var url: string;
    if (direction == "internal") {
      url = `${this.apiUrl}crmgt/correspondence/internal/comments?correspondence_id=${correspondence_id}`
    } else {
      url = `${this.apiUrl}crmgt/correspondence/external/comments?correspondence_id=${correspondence_id}&request_type=${request_type}`
    }
    return this.http.get<MethodResponse>(url,
      {
        headers: this.httpHeaders,
      }
    );
  }

  get_url(direction, request_type) {
    var url: string;
    if (direction == "internal") {
      url = `${this.apiUrl}crmgt/correspondence/internal/comments`
    } else {
      url = `${this.apiUrl}crmgt/correspondence/external/comments?request_type=${request_type}`
    }
    return url;
  }

  create(content, correspondence_id, direction, request_type?) {
    return this.http.post<MethodResponse>(this.get_url(direction, request_type),
      {
        correspondence_comment: {
          correspondence_id: correspondence_id,
          content: content
        }
      },
      {
        headers: this.httpHeaders,
      }
    );
  }

  update(content, comment, direction, request_type) {
    return this.http.put<MethodResponse>(this.get_url(direction, request_type),
      { 
        id: comment.id,
        correspondence_comment: {          
          content: content
        }
      },
      {
        headers: this.httpHeaders,
      }
    );
  }

  openCommentDialog(title, comment?) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = {
      title: title,
      comment: comment,
    };

    dialogConfig.width = "400px";
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    const dialogRef = this.dialog.open(CommentDialogComponent, dialogConfig);

    return dialogRef;
  }
}
