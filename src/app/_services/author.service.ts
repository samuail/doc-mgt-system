import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { MatDialog, MatDialogConfig } from "@angular/material/dialog";

import { environment } from 'src/environments/environment';

import { MethodResponse } from "src/app/_core/method-response";
import { Author } from "src/app/_models/author";

import { AuthorDialogComponent } from "src/app/author/author-dialog/author-dialog.component";
import { AuthorContact } from '../_models/author-contact';

@Injectable({
  providedIn: 'root'
})
export class AuthorService {

  apiUrl: string = environment.apiURL;

  httpHeaders = new HttpHeaders({
    "Content-Type": "application/json",
  });

  constructor(private http: HttpClient, public dialog: MatDialog) {}

  openAuthorDialog(title, author?) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = {
      title: title,
      author: author
    };
    
    dialogConfig.width = "300px";
    dialogConfig.autoFocus = true;

    if (title == "Add Author") {
      dialogConfig.disableClose = true;
    } else {
      dialogConfig.disableClose = false;
    }

    const dialogRef = this.dialog.open(
      AuthorDialogComponent,
      dialogConfig
    );

    return dialogRef;
  }

  assignAuthorValue(data) {
    let author: Author = new Author();
    author = {
      id: data["id"],
      firstName: data["first_name"],
      fatherName: data["father_name"],
      grandFatherName: data["grand_father_name"],
      email: data["email"],
      telephone: data["telephone"],
      bankName: data["bank_name"],
      bankAcc: data["bank_acc"],
      tin: data["tin"],
      fullName: data["first_name"] + " " + data["father_name"]  + " " + data["grand_father_name"],
      hasContact: data["has_contact"],
      authorContact: this.assignAuthorContactValue(data["author_contact"])
    }
    return author; 
  }

  assignAuthorContactValue(data) {
    let authorContact: AuthorContact = new AuthorContact();
    if (data != null) {
      authorContact = {
        id: data["id"],
        firstName: data["first_name"],
        fatherName: data["father_name"],
        email: data["email"],
        telephone: data["telephone"]
      }
    }
    return authorContact;
  }

  getAll() {
    return this.http.get<MethodResponse>(
      `${this.apiUrl}/crmgt/business_settings/author`,
      {
        headers: this.httpHeaders,
      }
    );
  }

  create(data) {
    var formData = data.authorForm.value;
    return this.http.post<MethodResponse>(
      `${this.apiUrl}/crmgt/business_settings/author`,
      {
        author: {
          first_name: formData.firstName,
          father_name: formData.lastName,
          grand_father_name: formData.dFatherName,
          email: formData.email,
          telephone: formData.telephone,
          has_contact: formData.hasContact
        }
      },
      {
        headers: this.httpHeaders,
      }
    );
  }

  addContact(data, author){
    var formData = data.authorForm.value;
    return this.http.post<MethodResponse>(
      `${this.apiUrl}/crmgt/business_settings/author/contact`,
      {
        id: author.id,
        author_contact: {
          author_id: author.id,
          first_name: formData.firstName,
          father_name: formData.lastName,
          email: formData.email,
          telephone: formData.telephone
        }
      },
      {
        headers: this.httpHeaders,
      }
    );
  }

  editContact(data, author){
    var formData = data.authorForm.value;
    return this.http.put<MethodResponse>(
      `${this.apiUrl}/crmgt/business_settings/author/edit_contact`,
      {
        id: author.authorContact.id,
        author_contact: {
          first_name: formData.firstName,
          father_name: formData.lastName,
          email: formData.email,
          telephone: formData.telephone
        }
      },
      {
        headers: this.httpHeaders,
      }
    );
  }

  update(data, author) {
    var formData = data.authorForm.value;
    return this.http.put<MethodResponse>(
      `${this.apiUrl}/crmgt/business_settings/author`,
      {
        id: author.id,
        author: {
          first_name: formData.firstName,
          father_name: formData.lastName,
          grand_father_name: formData.dFatherName,
          email: formData.email,
          telephone: formData.telephone,
          has_contact: formData.hasContact
        }
      },
      {
        headers: this.httpHeaders,
      }
    );
  }

  update_payment(data, author) {
    var formData = data.authorForm.value;
    return this.http.put<MethodResponse>(
      `${this.apiUrl}/crmgt/business_settings/author`,
      {
        id: author.id,
        author: {
          bank_name: formData.bName,
          bank_acc: formData.bAcc,
          tin: formData.tin
        }
      },
      {
        headers: this.httpHeaders,
      }
    );
  }
}
