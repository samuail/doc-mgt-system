import { TestBed } from '@angular/core/testing';

import { CorrespondenceUploadsService } from './correspondence-uploads.service';

describe('CorrespondenceUploadsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CorrespondenceUploadsService = TestBed.get(CorrespondenceUploadsService);
    expect(service).toBeTruthy();
  });
});
