import { TestBed } from '@angular/core/testing';

import { ManuscriptRoutingService } from './manuscript-routing.service';

describe('ManuscriptRoutingService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ManuscriptRoutingService = TestBed.get(ManuscriptRoutingService);
    expect(service).toBeTruthy();
  });
});
