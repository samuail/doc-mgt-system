import { TestBed } from '@angular/core/testing';

import { AuthorRoyaltyService } from './author-royalty.service';

describe('AuthorRoyaltyService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AuthorRoyaltyService = TestBed.get(AuthorRoyaltyService);
    expect(service).toBeTruthy();
  });
});
