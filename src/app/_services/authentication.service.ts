import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { BehaviorSubject, Observable } from "rxjs";
import { map } from "rxjs/operators";

import { environment } from 'src/environments/environment';

import { User } from "../_models/user";
import { MenuItem } from "../_models/menu-item";
import { Role } from "../_models/role";
import { DepartmentRole } from "src/app/_models/department-role";

@Injectable({
  providedIn: "root",
})
export class AuthenticationService {

  apiUrl: string = environment.apiURL;

  user: User = new User();

  private currentUserSubject: BehaviorSubject<User>;
  public userRoleSubject: BehaviorSubject<Role>;
  public userMenusSubject: BehaviorSubject<MenuItem[]>;
  public menuActionsSubject: BehaviorSubject<any[]>;
  public userDepartmentSubject: BehaviorSubject<DepartmentRole>;

  public currentUser: Observable<User>;
  public userRole: Observable<Role>;
  public userMenus: Observable<MenuItem[]>;
  public menuActions: Observable<any[]>;
  public userDepartment: Observable<DepartmentRole>;

  constructor(private http: HttpClient) {
    this.currentUserSubject = new BehaviorSubject<User>(
      JSON.parse(localStorage.getItem("currentUser"))
    );

    this.userRoleSubject = new BehaviorSubject<Role>(
      JSON.parse(localStorage.getItem("userRole"))
    );

    this.userMenusSubject = new BehaviorSubject<MenuItem[]>(
      JSON.parse(localStorage.getItem("userMenus"))
    );

    this.menuActionsSubject = new BehaviorSubject<any[]>(
      JSON.parse(localStorage.getItem("userMenuActions"))
    );

    this.userDepartmentSubject = new BehaviorSubject<DepartmentRole>(
      JSON.parse(localStorage.getItem("userDepartment"))
    );

    this.currentUser = this.currentUserSubject.asObservable();
    this.userRole = this.userRoleSubject.asObservable();
    this.userMenus = this.userMenusSubject.asObservable();
    this.menuActions = this.menuActionsSubject.asObservable();
    this.userDepartment = this.userDepartmentSubject.asObservable();
  }

  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  login(username: string, password: string) {
    return this.http
      .post<any>(`${this.apiUrl}/crmgt/auth/login`, {
        email: username,
        password: password,
      })
      .pipe(
        map((response) => {
          if (response && response.data["access_token"]) {
            this.user = {
              id: response.data["user"]["id"],
              firstName: response.data["user"]["first_name"],
              lastName: response.data["user"]["last_name"],
              userName: response.data["user"]["email"],
              password: "",
              active: response.data["user"]["active"],
              token: response.data["access_token"],
            };

            localStorage.setItem("currentUser", JSON.stringify(this.user));
            localStorage.setItem(
              "App_module",
              JSON.stringify(response.data["user"]["application_module_id"])
            );
            this.currentUserSubject.next(this.user);
          }

          return response;
        })
      );
  }

  logout() {
    return this.http.get<any>(`${this.apiUrl}/crmgt/auth/logout`).pipe(
      map((response) => {
        if (response.success) {
          localStorage.removeItem("currentUser");
          localStorage.removeItem("userMenus");
          localStorage.removeItem("userMenuActions");
          localStorage.removeItem("userRole");
          localStorage.removeItem("userDepartment");
          this.currentUserSubject.next(null);
          this.userMenusSubject.next(null);  
          this.menuActionsSubject.next(null);        
          this.userRoleSubject.next(null);
          this.userDepartmentSubject.next(null);
        }
        return response;
      })
    );
  }

  logout_offline() {
    localStorage.removeItem("currentUser");
    localStorage.removeItem("userMenus");
    localStorage.removeItem("userMenuActions");
    localStorage.removeItem("userRole");
    localStorage.removeItem("userDepartment");
    this.currentUserSubject.next(null);
    this.userMenusSubject.next(null);  
    this.menuActionsSubject.next(null);        
    this.userRoleSubject.next(null);
    this.userDepartmentSubject.next(null);
  }
}
