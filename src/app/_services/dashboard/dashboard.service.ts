import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { map } from 'rxjs/operators';

import { environment } from 'src/environments/environment';

import { MethodResponse } from 'src/app/_core/method-response';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  apiUrl: string = environment.apiURL;

  httpHeaders = new HttpHeaders({
    "Content-Type": "application/json",
  });

  constructor(private http: HttpClient) { }

  getAll() {    
    return this.http.get<MethodResponse>(`${this.apiUrl}crmgt/dashboard`,
      {
        headers: this.httpHeaders,
      }
    );
  }

  getUserStatus() {
    return this.http.get<MethodResponse>(`${this.apiUrl}crmgt/dashboard/user_status`,
      {
        headers: this.httpHeaders,
      }
    );
  }

  getCorrespondenceByType() {
    return this.http.get<MethodResponse>(`${this.apiUrl}/crmgt/dashboard/correspondence/by_type`, 
      {
        headers: this.httpHeaders,
      }
      ).pipe(
        map(response => response)
    );
  }

  getCorrespondenceByMonth() {
    return this.http.get<MethodResponse>(`${this.apiUrl}/crmgt/dashboard/correspondence/by_month`, 
      {
        headers: this.httpHeaders,
      }
      ).pipe(
        map(response => response)
    );
  }

  getCorrespondenceByStatus() {
    return this.http.get<MethodResponse>(`${this.apiUrl}/crmgt/dashboard/correspondence/by_status`, 
      {
        headers: this.httpHeaders,
      }
      ).pipe(
        map(response => response)
    );
  }

  getCorrespondenceMonthlyPayment() {
    return this.http.get<MethodResponse>(`${this.apiUrl}/crmgt/dashboard/correspondence/monthly_payment`, 
      {
        headers: this.httpHeaders,
      }
      ).pipe(
        map(response => response)
    );
  }

  getCenterOfficeByStatus() {
    return this.http.get<MethodResponse>(`${this.apiUrl}/crmgt/dashboard/correspondence/center_departments`, 
      {
        headers: this.httpHeaders,
      }
      ).pipe(
        map(response => response)
    );
  }

  getCollegeOfficeByStatus() {
    return this.http.get<MethodResponse>(`${this.apiUrl}/crmgt/dashboard/correspondence/college_departments`, 
      {
        headers: this.httpHeaders,
      }
      ).pipe(
        map(response => response)
    );
  }

  getOfficeOutgoingByStatus() {
    return this.http.get<MethodResponse>(`${this.apiUrl}/crmgt/dashboard/correspondence/department_outgoings`, 
      {
        headers: this.httpHeaders,
      }
      ).pipe(
        map(response => response)
    );
  }

  getUsersStatus() {
    return this.http.get<MethodResponse>(`${this.apiUrl}/crmgt/dashboard/users_status`, 
      {
        headers: this.httpHeaders,
      }
      ).pipe(
        map(response => response)
    );
  }

  getManuscriptByStage() {
    return this.http.get<MethodResponse>(`${this.apiUrl}/crmgt/dashboard/manuscript/by_stage`, 
      {
        headers: this.httpHeaders,
      }
      ).pipe(
        map(response => response)
    );
  }

  getManuscriptByMonth() {
    return this.http.get<MethodResponse>(`${this.apiUrl}/crmgt/dashboard/manuscript/by_month`, 
      {
        headers: this.httpHeaders,
      }
      ).pipe(
        map(response => response)
    );
  }

  getManuscriptUsersStatus() {
    return this.http.get<MethodResponse>(`${this.apiUrl}/crmgt/dashboard/manuscript/user_status`, 
      {
        headers: this.httpHeaders,
      }
      ).pipe(
        map(response => response)
    );
  }

}
