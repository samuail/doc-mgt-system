import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { MatDialog, MatDialogConfig } from "@angular/material/dialog";

import { environment } from 'src/environments/environment';

import { MethodResponse } from "../_core/method-response";
import { RoleDialogComponent } from "../role/role-dialog/role-dialog.component";
import { Role } from "../_models/role";

import { AuthenticationService } from "src/app/_services/authentication.service";

@Injectable({
  providedIn: "root",
})
export class RoleService {

  apiUrl: string = environment.apiURL;

  userRole: Role;

  httpHeaders = new HttpHeaders({
    "Content-Type": "application/json",
  });

  app_module_id: number = Number(localStorage.getItem("App_module"));

  constructor(private http: HttpClient, 
              public dialog: MatDialog, public authenticationService: AuthenticationService) {}

  getUserRole() {
    this.authenticationService.userRole.subscribe(
      (x) => { this.userRole = x; }
    );
    return this.userRole;
  }

  getAll() {
    return this.http.get<MethodResponse>(`${this.apiUrl}/crmgt/auth/user_roles`, {
      headers: this.httpHeaders,
    });
  }

  create(roleName: string) {
    return this.http.post<MethodResponse>(
      `${this.apiUrl}/crmgt/auth/user_roles`,
      { name: roleName, application_module_id: this.app_module_id },
      {
        headers: this.httpHeaders,
      }
    );
  }

  update(roleId: number, roleName: string) {
    return this.http.put<MethodResponse>(
      `${this.apiUrl}/crmgt/auth/user_roles/${roleId}`,
      { name: roleName, application_module_id: this.app_module_id },
      {
        headers: this.httpHeaders,
      }
    );
  }

  getRoleUsers(roleId) {
    return this.http.get<MethodResponse>(
      `${this.apiUrl}/crmgt/auth/user_roles/${roleId}/users`,
      {
        headers: this.httpHeaders,
      }
    );
  }

  openRoleDialog(title, role, roles, users) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = {
      title: title,
      role: role,
      roles: roles,
      users: users,
    };

    dialogConfig.width = "400px";
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    const dialogRef = this.dialog.open(RoleDialogComponent, dialogConfig);

    return dialogRef;
  }
}
