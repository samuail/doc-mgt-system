import { Component, OnInit, OnDestroy, ViewChild } from "@angular/core";
import { first } from "rxjs/operators";
import { Subscription } from "rxjs";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { MatTableDataSource } from "@angular/material/table";
import { Router } from "@angular/router";

import { Message } from "src/app/_models/_helpers/message";

import { Organization } from "src/app/_models/organization";
import { OrganizationService } from "src/app/_services/organization.service";

import { AuthenticationService } from "src/app/_services/authentication.service";
import { InfoDialogService } from "src/app/_services/info-dialog.service";
import { MenuService } from "src/app/_services/menu.service";

@Component({
  selector: "app-organization",
  templateUrl: "./organization.component.html",
  styleUrls: ["./organization.component.css"],
})
export class OrganizationComponent implements OnInit, OnDestroy {
  subManager = new Subscription();

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  title: string;
  message: Message = new Message();

  organizations: Organization[] = [];
  organization: Organization = new Organization();

  dataSource = new MatTableDataSource<Organization>();
  displayedColumns = ["Code", "Description", "Type", "operations"];

  loading = false;
  resultsLength = 0;

  menuActions: string[] = [];

  canAdd: boolean = false;
  canEdit: boolean = false;
  canDelete: boolean = false;

  constructor(
    private router: Router,
    private organizationService: OrganizationService,
    private authenticationService: AuthenticationService,
    private menuService: MenuService,
    public infoDialogService: InfoDialogService
  ) {
    this.menuActions = this.menuService.getMenuActions("Organizations");
    for (var action of this.menuActions) {
      if (action == "Add") {
        this.canAdd = true;
      } else if (action == "Edit") {
        this.canEdit = true;
      } else if (action == "Delete") {
        this.canDelete = true;
      }
    }
  }

  ngOnInit() {
    this.getAll();
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngOnDestroy() {
    this.subManager.unsubscribe();
  }

  getAll(): void {
    this.loading = true;
    let getAll = this.organizationService
      .getAll()
      .pipe(first())
      .subscribe(
        (response) => {
          if (response.success) {
            for (var value of response.data) {
              this.organization = {
                id: value["id"],
                code: value["code"],
                description: value["description"],
                organization_type: value["organization_type"],
              };
              this.organizations.push(this.organization);
            }
            this.loading = false;
            this.resultsLength = response.total;
            this.dataSource.data = this.organizations;
          } else {
            this.message.title = "Organization";
            this.message.type = "error";
            this.message.content = response.errors;
            const infoDialogRef = this.infoDialogService.openInfoDialog(
              this.message
            );
            infoDialogRef.afterClosed().subscribe(() => {
              if (response.errors[0] == "Signature has expired") {
                this.authenticationService.logout_offline();
                this.router.navigate(["/login"]);
              }
            });
          }
        },
        (error) => {
          this.loading = false;
          this.infoDialogService.openInfoDialog(error);
        }
      );
    this.subManager.add(getAll);
  }

  addOrganization() {
    this.title = "Add Organization";
    const dialogRef = this.organizationService.openOrganizationDialog(
      this.title,
      new Organization()
    );
    const sub = dialogRef.componentInstance.onOrganizationSave.subscribe(
      (data) => {
        if (data) {
          data.loading = true;
          let create = this.organizationService
            .create(
              data.organizationForm.value.code,
              data.organizationForm.value.description,
              data.organizationForm.value.type
            )
            .pipe(first())
            .subscribe(
              (response) => {
                if (response.success) {
                  this.message.type = "success";
                  this.message.title = this.title;
                  this.message.content = [response.message];
                  const infoDialogRef = this.infoDialogService.openInfoDialog(
                    this.message
                  );
                  infoDialogRef.afterClosed().subscribe(() => {
                    this.organization = {
                      id: response.data["id"],
                      code: response.data["code"],
                      description: response.data["description"],
                      organization_type: response.data["organization_type"],
                    };
                    this.organizations.push(this.organization);
                    this.resultsLength = this.resultsLength + 1;
                    this.dataSource = new MatTableDataSource(
                      this.organizations
                    );
                    data.close();
                  });
                } else {
                  this.message.title = this.title;
                  this.message.content = [];
                  this.message.type = "error";
                  for (var value of response.errors) {
                    this.message.content.push(value);
                  }
                  const infoDialogRef = this.infoDialogService.openInfoDialog(
                    this.message
                  );
                  infoDialogRef.afterClosed().subscribe(() => {
                    data.loading = false;
                  });
                }
              },
              (error) => {
                data.loading = false;
                this.infoDialogService.openInfoDialog(error);
              }
            );
          this.subManager.add(create);
        }
      }
    );

    dialogRef.afterClosed().subscribe(() => {
      sub.unsubscribe();
    });
  }

  editOrganization(organization) {
    this.title = "Edit Organization";
    const dialogRef = this.organizationService.openOrganizationDialog(
      this.title,
      organization
    );
    const sub = dialogRef.componentInstance.onOrganizationSave.subscribe(
      (data) => {
        if (data) {
          data.loading = true;
          let create = this.organizationService
            .update(
              organization.id,
              data.organizationForm.value.code,
              data.organizationForm.value.description,
              data.organizationForm.value.organization_type_id
            )
            .pipe(first())
            .subscribe(
              (response) => {
                if (response.success) {
                  this.message.type = "success";
                  this.message.title = this.title;
                  this.message.content = [response.message];
                  const infoDialogRef = this.infoDialogService.openInfoDialog(
                    this.message
                  );
                  infoDialogRef.afterClosed().subscribe(() => {
                    this.organizations = this.organizations.filter(
                      (item) => item.id !== organization.id
                    );
                    this.organization = {
                      id: response.data["id"],
                      code: response.data["code"],
                      description: response.data["description"],
                      organization_type: response.data["organization_type"],
                    };
                    this.organizations.push(this.organization);
                    this.dataSource = new MatTableDataSource(
                      this.organizations
                    );
                    data.close();
                  });
                } else {
                  this.message.title = this.title;
                  this.message.content = [];
                  this.message.type = "error";
                  for (var value of response.errors) {
                    this.message.content.push(value);
                  }
                  const infoDialogRef = this.infoDialogService.openInfoDialog(
                    this.message
                  );
                  infoDialogRef.afterClosed().subscribe(() => {
                    data.loading = false;
                  });
                }
              },
              (error) => {
                data.loading = false;
                this.infoDialogService.openInfoDialog(error);
              }
            );
          this.subManager.add(create);
        }
      }
    );

    dialogRef.afterClosed().subscribe(() => {
      sub.unsubscribe();
    });
  }

  deleteOrganization(organization) {}
}
