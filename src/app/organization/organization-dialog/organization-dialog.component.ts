import { Component, OnInit, Inject, EventEmitter } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
import { first } from "rxjs/operators";
import { Subscription } from "rxjs";
import { Router } from "@angular/router";

import { Message } from "src/app/_models/_helpers/message";
import { Organization } from "src/app/_models/organization";
import { OrganizationType } from "src/app/_models/organization-type";

import { OrganizationTypeService } from "src/app/_services/organization-type.service";
import { AuthenticationService } from "src/app/_services/authentication.service";
import { InfoDialogService } from "src/app/_services/info-dialog.service";

@Component({
  selector: "app-organization-dialog",
  templateUrl: "./organization-dialog.component.html",
  styleUrls: ["./organization-dialog.component.css"],
})
export class OrganizationDialogComponent implements OnInit {
  subManager = new Subscription();

  loading = false;
  organizationForm: FormGroup;
  title: string;
  message: Message = new Message();

  organization: Organization = new Organization();
  organizationType: OrganizationType = new OrganizationType();
  organizationTypes: OrganizationType[] = [];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private router: Router,
    private dialogRef: MatDialogRef<OrganizationDialogComponent>,
    private organizationTypeService: OrganizationTypeService,
    private authenticationService: AuthenticationService,
    public infoDialogService: InfoDialogService
  ) {
    this.title = data.title;
    this.organization = data.organization;
    this.getAllOrganizationTypes();
  }

  ngOnInit() {
    if (this.title == "Add Organization") {
      this.organizationForm = this.fb.group({
        type: ["", Validators.required],
        code: ["", Validators.required],
        description: ["", Validators.required],
      });
    } else if (this.title == "Edit Organization") {
      this.organizationForm = this.fb.group({
        type: [this.organization.organization_type.id, Validators.required],
        code: [this.organization.code, Validators.required],
        description: [this.organization.description, Validators.required],
      });
    }
  }

  ngOnDestroy() {
    this.subManager.unsubscribe();
  }

  getAllOrganizationTypes(): void {
    this.loading = true;
    let getAll = this.organizationTypeService
      .getAll()
      .pipe(first())
      .subscribe(
        (response) => {
          if (response.success) {
            for (var value of response.data) {
              this.organizationType = {
                id: value["id"],
                code: value["code"],
                name: value["name"],
              };
              this.organizationTypes.push(this.organizationType);
            }
            this.loading = false;
          } else {
            this.message.title = "Organization Type";
            this.message.type = "error";
            this.message.content = response.errors;
            const infoDialogRef = this.infoDialogService.openInfoDialog(
              this.message
            );
            infoDialogRef.afterClosed().subscribe(() => {
              if (response.errors[0] == "Signature has expired") {
                this.authenticationService.logout_offline();
                this.router.navigate(["/login"]);
              }
            });
          }
        },
        (error) => {
          this.loading = false;
          this.infoDialogService.openInfoDialog(error);
        }
      );
    this.subManager.add(getAll);
  }

  onOrganizationSave = new EventEmitter();

  save() {
    if (this.organizationForm.invalid) {
      return;
    }
    this.onOrganizationSave.emit(this);
  }

  close() {
    this.dialogRef.close(false);
  }
}
