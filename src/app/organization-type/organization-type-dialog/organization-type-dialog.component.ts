import { Component, OnInit, Inject, EventEmitter } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";

import { OrganizationType } from "src/app/_models/organization-type";

@Component({
  selector: "app-organization-type-dialog",
  templateUrl: "./organization-type-dialog.component.html",
  styleUrls: ["./organization-type-dialog.component.css"],
})
export class OrganizationTypeDialogComponent implements OnInit {
  organizationTypeForm: FormGroup;
  loading: boolean = false;
  title: string;
  organizationType: OrganizationType = new OrganizationType();

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<OrganizationTypeDialogComponent>
  ) {
    this.title = data.title;
    this.organizationType = data.organizationType;
  }

  ngOnInit() {
    if (this.title == "Add Organization Type") {
      this.organizationTypeForm = this.fb.group({
        code: ["", Validators.required],
        name: ["", Validators.required],
      });
    } else if (this.title == "Edit Organization Type") {
      this.organizationTypeForm = this.fb.group({
        code: [this.organizationType.code, Validators.required],
        name: [this.organizationType.name, Validators.required],
      });
    }
  }

  onOrganizationTypeSave = new EventEmitter();

  save() {
    if (this.organizationTypeForm.invalid) {
      return;
    }
    this.onOrganizationTypeSave.emit(this);
  }

  close() {
    this.dialogRef.close(false);
  }
}
