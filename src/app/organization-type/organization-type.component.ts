import { Component, OnInit, OnDestroy, ViewChild } from "@angular/core";
import { first } from "rxjs/operators";
import { Subscription } from "rxjs";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { MatTableDataSource } from "@angular/material/table";
import { Router } from "@angular/router";

import { Message } from "src/app/_models/_helpers/message";

import { OrganizationType } from "src/app/_models/organization-type";
import { OrganizationTypeService } from "src/app/_services/organization-type.service";

import { AuthenticationService } from "src/app/_services/authentication.service";
import { InfoDialogService } from "src/app/_services/info-dialog.service";
import { MenuService } from "src/app/_services/menu.service";

@Component({
  selector: "app-organization-type",
  templateUrl: "./organization-type.component.html",
  styleUrls: ["./organization-type.component.css"],
})
export class OrganizationTypeComponent implements OnInit, OnDestroy {
  subManager = new Subscription();

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  title: string;
  message: Message = new Message();

  organizationTypes: OrganizationType[] = [];
  organizationType: OrganizationType = new OrganizationType();

  dataSource = new MatTableDataSource<OrganizationType>();
  displayedColumns = ["Code", "Name", "operations"];

  loading = false;
  resultsLength = 0;

  menuActions: string[] = [];

  canAdd: boolean = false;
  canEdit: boolean = false;
  canDelete: boolean = false;

  constructor(
    private router: Router,
    private organizationTypeService: OrganizationTypeService,
    private authenticationService: AuthenticationService,
    private menuService: MenuService,
    public infoDialogService: InfoDialogService
  ) {
    this.menuActions = this.menuService.getMenuActions("Organization Types");
    for (var action of this.menuActions) {
      if (action == "Add") {
        this.canAdd = true;
      } else if (action == "Edit") {
        this.canEdit = true;
      } else if (action == "Delete") {
        this.canDelete = true;
      }
    }
  }

  ngOnInit() {
    this.getAll();
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngOnDestroy() {
    this.subManager.unsubscribe();
  }

  getAll(): void {
    this.loading = true;
    let getAll = this.organizationTypeService
      .getAll()
      .pipe(first())
      .subscribe(
        (response) => {
          if (response.success) {
            for (var value of response.data) {
              this.organizationType = {
                id: value["id"],
                code: value["code"],
                name: value["name"],
              };
              this.organizationTypes.push(this.organizationType);
            }
            this.loading = false;
            this.resultsLength = response.total;
            this.dataSource.data = this.organizationTypes;
          } else {
            this.message.title = "Organization Type";
            this.message.type = "error";
            this.message.content = response.errors;
            const infoDialogRef = this.infoDialogService.openInfoDialog(
              this.message
            );
            infoDialogRef.afterClosed().subscribe(() => {
              if (response.errors[0] == "Signature has expired") {
                this.authenticationService.logout_offline();
                this.router.navigate(["/login"]);
              }
            });
          }
        },
        (error) => {
          this.loading = false;
          this.infoDialogService.openInfoDialog(error);
        }
      );
    this.subManager.add(getAll);
  }

  addOrganizationType() {
    this.title = "Add Organization Type";
    const dialogRef = this.organizationTypeService.openOrganizationTypeDialog(
      this.title,
      new OrganizationType()
    );
    const sub = dialogRef.componentInstance.onOrganizationTypeSave.subscribe(
      (data) => {
        if (data) {
          data.loading = true;
          let create = this.organizationTypeService
            .create(
              data.organizationTypeForm.value.code,
              data.organizationTypeForm.value.name
            )
            .pipe(first())
            .subscribe(
              (response) => {
                if (response.success) {
                  this.message.type = "success";
                  this.message.title = this.title;
                  this.message.content = [response.message];
                  const infoDialogRef = this.infoDialogService.openInfoDialog(
                    this.message
                  );
                  infoDialogRef.afterClosed().subscribe(() => {
                    this.organizationType = {
                      id: response.data["id"],
                      code: response.data["code"],
                      name: response.data["name"],
                    };
                    this.organizationTypes.push(this.organizationType);
                    this.resultsLength = this.resultsLength + 1;
                    this.dataSource = new MatTableDataSource(
                      this.organizationTypes
                    );
                    data.close();
                  });
                } else {
                  this.message.title = this.title;
                  this.message.content = [];
                  this.message.type = "error";
                  for (var value of response.errors) {
                    this.message.content.push(value);
                  }
                  const infoDialogRef = this.infoDialogService.openInfoDialog(
                    this.message
                  );
                  infoDialogRef.afterClosed().subscribe(() => {
                    data.loading = false;
                  });
                }
              },
              (error) => {
                data.loading = false;
                this.infoDialogService.openInfoDialog(error);
              }
            );
          this.subManager.add(create);
        }
      }
    );

    dialogRef.afterClosed().subscribe(() => {
      sub.unsubscribe();
    });
  }

  editOrganizationType(organizationType) {
    this.title = "Edit Organization Type";
    const dialogRef = this.organizationTypeService.openOrganizationTypeDialog(
      this.title,
      organizationType
    );
    const sub = dialogRef.componentInstance.onOrganizationTypeSave.subscribe(
      (data) => {
        if (data) {
          data.loading = true;
          let create = this.organizationTypeService
            .update(
              organizationType.id,
              data.organizationTypeForm.value.code,
              data.organizationTypeForm.value.name
            )
            .pipe(first())
            .subscribe(
              (response) => {
                if (response.success) {
                  this.message.type = "success";
                  this.message.title = this.title;
                  this.message.content = [response.message];
                  const infoDialogRef = this.infoDialogService.openInfoDialog(
                    this.message
                  );
                  infoDialogRef.afterClosed().subscribe(() => {
                    this.organizationTypes = this.organizationTypes.filter(
                      (item) => item.id !== organizationType.id
                    );
                    this.organizationType = {
                      id: response.data["id"],
                      code: response.data["code"],
                      name: response.data["name"],
                    };
                    this.organizationTypes.push(this.organizationType);
                    this.dataSource = new MatTableDataSource(
                      this.organizationTypes
                    );
                    data.close();
                  });
                } else {
                  this.message.title = this.title;
                  this.message.content = [];
                  this.message.type = "error";
                  for (var value of response.errors) {
                    this.message.content.push(value);
                  }
                  const infoDialogRef = this.infoDialogService.openInfoDialog(
                    this.message
                  );
                  infoDialogRef.afterClosed().subscribe(() => {
                    data.loading = false;
                  });
                }
              },
              (error) => {
                data.loading = false;
                this.infoDialogService.openInfoDialog(error);
              }
            );
          this.subManager.add(create);
        }
      }
    );

    dialogRef.afterClosed().subscribe(() => {
      sub.unsubscribe();
    });
  }

  deleteOrganizationType(organizationType) {}
}
