import { Correspondence } from "./correspondence";
import { Department } from "./department";
import { Organization } from "./organization";
export class ExternalOutgoingCorrespondence extends Correspondence {
  source: Department;
  destination: Organization;
  hasCc?: boolean;
  types?: string;
}
