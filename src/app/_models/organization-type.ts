export class OrganizationType {
  id: number;
  name: string;
  code: string;
}
