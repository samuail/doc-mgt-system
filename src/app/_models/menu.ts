export class Menu {
	id: number;
	text: string;
  	icon_cls: string;
  	class_name: string;
  	location: string;
  	parent_id: number;
  	parent_name: string;
}