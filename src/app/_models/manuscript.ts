import { Author } from "./author";
import { Department } from "./department";
import { User } from "./user";
import { ManuscriptArchive } from "./manuscript-archive";

export class Manuscript {
    id: number;
    referenceNo: string;
    referenceNoFull: string;
    receivedDate: string;
    receivedDateEC?: string;
    destination: Department;
    title: string;    
    keyWord: string;
    status: string;
    keyInBy?: User;
    authors: Author;
    currentStage: string;
    manuscriptArchive: ManuscriptArchive;
}
