export class OfficeMemo {
	id: number;
    date: string;
    subject: string;
	content: string;	
    commentDate: string;
}