export interface MenuItem {
  displayName: string;
  // disabled?: boolean;
  route?: string;
  iconName: string;
  children?: MenuItem[];
}