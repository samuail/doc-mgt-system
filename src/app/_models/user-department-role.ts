import { User } from "./user";
import { DepartmentRole } from "./department-role";

export class UserDepartmentRole {
  id: number;
  user: User;
  department_user_role: DepartmentRole;
}
