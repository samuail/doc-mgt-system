import { Manuscript } from "./manuscript";
import { User } from "./user";
export class ManuscriptAssignment {
  id: number;
  stage: string;
  assigneeType?: string;
  fromUser: User;
  toInternalUser?: User;
  toExternalUser?: string;
  assignee: any;
  manuscript: Manuscript;  
  assignedDate: string;
  receivedDate: string;
  status: string;
}
