import { DepartmentType } from "./department-type";

export class Department {
  id: number;
  code: string;
  description: string;
  department_type: DepartmentType;
  parent?: Department;
}
