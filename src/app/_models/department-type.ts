export class DepartmentType {
	id: number;
	name: string;
	code: string;
}