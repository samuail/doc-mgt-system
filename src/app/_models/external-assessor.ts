export class ExternalAssessor {
    id: number;
    title: string;
    firstName: string;
    fatherName: string;
    grandFatherName: string;
    email: string;    
    telephone: string; 
    fullName?: string;
    areaOfExpertise: string;
    institution: string;
    bankName: string;
    bankAcc: string;
    tin: string;
}
