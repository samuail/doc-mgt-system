import { User } from "./user";

export class ManuscriptArchive {
    id: number;
    remark: string;
    noPages: number;
    noCopies: number;
    hasArchiveAtt: boolean;
    archivedBy: User;
    archivedDate: string;
}
