import { Author } from "./author";
import { Manuscript } from "./manuscript";
import { User } from "./user";

export class Royalty {
    id: number;
    manuscript: Manuscript;
    author: Author;
    amount: string;
    referenceNo: string;    
    remark: string; 
    paymentDate: string;
    keyInBy?: User;
}
