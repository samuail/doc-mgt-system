import { User } from "./user";
export class ManuscriptComment {
	id: number;
	commentedBy: User;
	manuscript: any;  
	content: string;
    commentDate: string;
}
