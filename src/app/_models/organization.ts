import { OrganizationType } from "./organization-type";

export class Organization {
  id: number;
  code: string;
  description: string;
  organization_type: OrganizationType;
}
