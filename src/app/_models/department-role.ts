import { Department } from "./department";
import { Role } from "./role";

export class DepartmentRole {
  id: number;
  department_id: number;
  department_name: string;
  department_code: string;
  user_role_id: number;
  user_role_name: string;
  is_head: boolean;
}
