export class AuthorContact {
    id: number;
    firstName: string;
    fatherName: string;
    email: string;    
    telephone: string; 
}
