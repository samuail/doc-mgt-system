import { User } from "./user";
import { CorrespondenceCarbonCopy } from "./correspondence-carbon-copy";

export class Correspondence {
  id: number;
  referenceNo: string;
  letterDate: string;
  letterDateEC?: string;
  subject: string;
  keyWord: string;
  receivedDate?: string;
  status: string;
  sentDate?: string;
  imagePath?: string;
  keyInBy?: User;
  correspondenceCarbonCopies?: CorrespondenceCarbonCopy[];
}
