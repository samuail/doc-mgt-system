import { Author } from "./author";
import { Manuscript } from "./manuscript";

export class ManuscriptAuthor {
    id: number;
    manuscript: Manuscript;
    author: Author;
}
