import { Correspondence } from "./correspondence";
import { Department } from "./department";
import { Organization } from "./organization";
export class ExternalIncomingCorrespondence extends Correspondence {
  source: Organization;
  destination: Department;
  hasCc?: boolean;
  types?: string;
  docReferenceNo?: string;
  amount?: number;
  remark?: string;
  noDocuments?: number;
  archivedDate?: string;
}
