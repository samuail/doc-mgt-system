import { DepartmentType } from "./department-type";
import { Department } from "./department";

export class DepartmentItem {
  id: number;
  code: string;
  description: string;
  department_type_id: number;
  parent_id?: number;
  children?: DepartmentItem[];
}
