import { User } from "./user";
export class CorrespondenceComment {
	id: number;
	commentedBy: User;
	correspondence: any;  
	content: string;	
	order: number;
    commentDate: string;
}