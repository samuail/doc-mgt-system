export class Message {
  type: string;
  title: string;
  content: string[];
}
