import { User } from "./user";
export class CorrespondenceAssignment {
  id: number;
  fromUser: User;
  toUser: User;
  correspondence: any;
  order: number;
  assignedDate: string;
  receivedDate: string;
  status: string;
  rejectionRemark?: string;
}
