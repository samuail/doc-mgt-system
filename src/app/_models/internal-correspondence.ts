import { Correspondence } from "./correspondence";
import { Department } from "./department";
export class InternalCorrespondence extends Correspondence {
  source: Department;
  destination: Department;
  hasCc?: boolean;
  types?: string;
  docReferenceNo?: string;
  amount?: number;
  remark?: string;
  noDocuments?: number;
  archivedDate?: string;
}
