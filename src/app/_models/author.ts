import { AuthorContact } from "./author-contact";

export class Author {
    id: number;
    firstName: string;
    fatherName: string;
    grandFatherName: string;
    email: string;    
    telephone: string; 
    bankName: string;
    bankAcc: string;
    tin: string; 
    fullName?: string;
    hasContact?: boolean;
    authorContact?: AuthorContact;
}
