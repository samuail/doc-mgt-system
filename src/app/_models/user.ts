export class User {
  id: number;
  firstName: string;
  lastName: string;
  fullName?: string;
  userName: string;
  password: string;
  active: boolean;
  token?: string;
}
