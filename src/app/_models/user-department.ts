import { User } from "./user";
import { Department } from "./department";
import { Role } from "./role";

export class UserDepartment {
  id: number;
  user: User;
  department: Department;
  user_role: Role;
}
