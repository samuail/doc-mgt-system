import { BrowserModule, DomSanitizer } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { platformBrowserDynamic } from "@angular/platform-browser-dynamic";
import { NgModule } from "@angular/core";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { FlexLayoutModule } from "@angular/flex-layout";

import { ChartsModule } from "ng2-charts";
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import { MatPaginatorIntl } from '@angular/material/paginator';

import { MatPaginatorI18nService } from './_core/app-paginator/paginator-i18.service';
import { MAT_DATE_LOCALE } from '@angular/material/core'
import { PdfViewerModule } from 'ng2-pdf-viewer';

import {
  FooterComponent,
  HeaderComponent,
  SideNavComponent,
  SharedModule,
} from "./_shared";
import { AppMaterialModule } from "./_core/app-material/app-material.module";
import { AppRoutingModule } from "./_core/app.routing.module";
import { AuthGuard } from "./_core/auth/auth.guard";
import { AuthService } from "./_core/auth/auth.service";
import { DataService } from "./_core/data.service";
import { InfoDialogComponent } from "./_core/info-dialog/info-dialog.component";
//import { AuthenticationService } from './_services/authentication.service';
import { UserService } from "./_services/user.service";

import { JwtInterceptor } from "./_helpers/jwt.interceptor";
import { ErrorInterceptor } from "./_helpers/error.interceptor";
import { ConvertDatePipe } from "./_helpers/pipes/convert-date.pipe";
import { TruncateStringPipe } from "./_helpers/pipes/truncate-string.pipe";

import { AppComponent } from "./app.component";
import { LoginComponent } from "./login/login.component";
import { AdminpanelComponent } from "./adminpanel/adminpanel.component";
import { DashboardComponent } from "./adminpanel/dashboard/dashboard.component";
import { UserComponent } from "./user/user.component";
import { UserPanelComponent } from "./user-panel/user-panel.component";
import { UserDashboardComponent } from "./user-panel/user-dashboard/user-dashboard.component";
import { ForgotPasswordComponent } from "./forgot-password/forgot-password.component";
import { AboutComponent } from "./about/about.component";
import { ContactComponent } from "./contact/contact.component";
import { UserDialogComponent } from "./user/user-dialog/user-dialog.component";
import { UserProfileComponent } from "./user-profile/user-profile.component";
import { RoleDialogComponent } from "./role/role-dialog/role-dialog.component";
import { RoleComponent } from "./role/role.component";
import { MenuComponent } from "./menu/menu.component";
import { MenuDialogComponent } from "./menu/menu-dialog/menu-dialog.component";
import { DocPreviewerComponent } from "./correspondence/_dialogs/doc-previewer/doc-previewer.component";
import { DocCommentComponent } from "./correspondence/_dialogs/doc-comment/doc-comment.component";
import { CorrespondenceDialogComponent } from "./correspondence/_dialogs/correspondence-dialog/correspondence-dialog.component";
import { CommentDialogComponent } from "./correspondence/_dialogs/doc-comment/comment-dialog/comment-dialog.component";
import { IncomingCorrespondenceComponent } from "./correspondence/incoming-correspondence/incoming-correspondence.component";
import { OutgoingCorrespondenceComponent } from "./correspondence/outgoing-correspondence/outgoing-correspondence.component";
import { OfficeMemoCorrespondenceComponent } from "./correspondence/office-memo-correspondence/office-memo-correspondence.component";
import { DepartmentComponent } from "./department/department.component";
import { DepartmentDialogComponent } from "./department/_dialogs/department-dialog/department-dialog.component";
import { DepartmentTypeComponent } from "./department-type/department-type.component";
import { DepartmentTypeDialogComponent } from "./department-type/_dialogs/department-type-dialog/department-type-dialog.component";
import { OrganizationTypeComponent } from "./organization-type/organization-type.component";
import { OrganizationTypeDialogComponent } from "./organization-type/organization-type-dialog/organization-type-dialog.component";
import { OrganizationComponent } from "./organization/organization.component";
import { OrganizationDialogComponent } from "./organization/organization-dialog/organization-dialog.component";
import { UserDepartmentDialogComponent } from "./user/user-department-dialog/user-department-dialog.component";
import { DocUploadsComponent } from "./correspondence/_dialogs/doc-uploads/doc-uploads.component";
import { UploadsDialogComponent } from "./correspondence/_dialogs/doc-uploads/uploads-dialog/uploads-dialog.component";
import { ImgPreviewerDialogComponent } from "./correspondence/_dialogs/doc-uploads/img-previewer-dialog/img-previewer-dialog.component";
import { AssignCorrespondenceComponent } from "./correspondence/_dialogs/assign-correspondence/assign-correspondence.component";
import { AssignCorrespondenceDialogComponent } from "./correspondence/_dialogs/assign-correspondence/assign-correspondence-dialog/assign-correspondence-dialog.component";
import { UserMenuActionDialogComponent } from "./user/user-menu-action-dialog/user-menu-action-dialog.component";
import { CenterComponent } from './correspondence/incomings/center/center.component';
import { CollegeComponent } from './correspondence/incomings/college/college.component';
import { ExternalComponent } from './correspondence/incomings/external/external.component';
import { DragDropFileUploadDirective } from './_core/dnd/drag-drop-file-upload.directive';
import { InternalComponent } from './correspondence/outgoings/internal/internal.component';
import { OutgoingExternalComponent } from './correspondence/outgoings/outgoing-external/outgoing-external.component';
import { ConfirmationDialogComponent } from './_core/confirmation-dialog/confirmation-dialog.component';
import { DocCcComponent } from './correspondence/_dialogs/doc-cc/doc-cc.component';
import { CcDialogComponent } from './correspondence/_dialogs/doc-cc/cc-dialog/cc-dialog.component';
import { DocArchiverComponent } from './correspondence/_dialogs/doc-archiver/doc-archiver.component';
import { InternalArchivedComponent } from './correspondence/incomings/archived/internal-archived/internal-archived.component';
import { ExternalArchivedComponent } from './correspondence/incomings/archived/external-archived/external-archived.component';
import { ManuscriptComponent } from './correspondence/incomings/manuscript/manuscript.component';
import { ManuscriptDialogComponent } from './correspondence/incomings/manuscript/_dialog/manuscript-dialog/manuscript-dialog.component';
import { AuthorComponent } from './author/author.component';
import { AuthorDialogComponent } from './author/author-dialog/author-dialog.component';
import { ManuscriptUploaderComponent } from './correspondence/incomings/manuscript/_dialog/manuscript-uploader/manuscript-uploader.component';
import { ManuscriptAuthorComponent } from './correspondence/incomings/manuscript/_dialog/manuscript-author/manuscript-author.component';
import { ManuscriptAuthorDialogComponent } from './correspondence/incomings/manuscript/_dialog/manuscript-author/manuscript-author-dialog/manuscript-author-dialog.component';
import { ManuscriptRoutingComponent } from './correspondence/incomings/manuscript/_dialog/manuscript-routing/manuscript-routing.component';
import { ManuscriptRoutingDialogComponent } from './correspondence/incomings/manuscript/_dialog/manuscript-routing/manuscript-routing-dialog/manuscript-routing-dialog.component';
import { ManuscriptCommentComponent } from './correspondence/incomings/manuscript/_dialog/manuscript-comment/manuscript-comment.component';
import { ManuscriptCommentDialogComponent } from './correspondence/incomings/manuscript/_dialog/manuscript-comment/manuscript-comment-dialog/manuscript-comment-dialog.component';
import { AuthorRoyaltyDialogComponent } from './author/author-royalty-dialog/author-royalty-dialog.component';
import { RoyaltyDialogComponent } from './author/author-royalty-dialog/royalty-dialog/royalty-dialog.component';
import { ManuscriptArchiverComponent } from './correspondence/incomings/manuscript/_dialog/manuscript-archiver/manuscript-archiver.component';
import { ManuscriptArchivedComponent } from './correspondence/incomings/archived/manuscript-archived/manuscript-archived.component';
import { UserDashboardNewComponent } from './user-panel/user-dashboard-new/user-dashboard-new.component';
import { ExternalAssessorComponent } from './external-assessor/external-assessor.component';
import { AssessorDialogComponent } from './external-assessor/_dialog/assessor-dialog/assessor-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    AdminpanelComponent,
    DashboardComponent,
    UserComponent,
    InfoDialogComponent,
    FooterComponent,
    HeaderComponent,
    SideNavComponent,
    UserPanelComponent,
    UserDashboardComponent,
    ForgotPasswordComponent,
    AboutComponent,
    ContactComponent,
    UserDialogComponent,
    UserProfileComponent,
    RoleDialogComponent,
    RoleComponent,
    MenuComponent,
    MenuDialogComponent,
    DocPreviewerComponent,
    DocCommentComponent,
    CorrespondenceDialogComponent,
    CommentDialogComponent,
    IncomingCorrespondenceComponent,
    OutgoingCorrespondenceComponent,
    OfficeMemoCorrespondenceComponent,
    DepartmentComponent,
    DepartmentDialogComponent,
    DepartmentTypeComponent,
    DepartmentTypeDialogComponent,
    OrganizationTypeComponent,
    OrganizationTypeDialogComponent,
    OrganizationComponent,
    OrganizationDialogComponent,
    UserDepartmentDialogComponent,
    DocUploadsComponent,
    UploadsDialogComponent,
    ImgPreviewerDialogComponent,
    AssignCorrespondenceComponent,
    AssignCorrespondenceDialogComponent,
    UserMenuActionDialogComponent,
    CenterComponent,
    CollegeComponent,
    ExternalComponent,
    DragDropFileUploadDirective,
    InternalComponent,
    OutgoingExternalComponent,
    ConfirmationDialogComponent,
    DocCcComponent,
    CcDialogComponent,
    DocArchiverComponent,
    InternalArchivedComponent,
    ExternalArchivedComponent,
    ConvertDatePipe,
    TruncateStringPipe,
    ManuscriptComponent,
    ManuscriptDialogComponent,
    AuthorComponent,
    AuthorDialogComponent,
    ManuscriptUploaderComponent,
    ManuscriptAuthorComponent,
    ManuscriptAuthorDialogComponent,
    ManuscriptRoutingComponent,
    ManuscriptRoutingDialogComponent,
    ManuscriptCommentComponent,
    ManuscriptCommentDialogComponent,
    AuthorRoyaltyDialogComponent,
    RoyaltyDialogComponent,
    ManuscriptArchiverComponent,
    ManuscriptArchivedComponent,
    UserDashboardNewComponent,
    ExternalAssessorComponent,
    AssessorDialogComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    FlexLayoutModule,
    ChartsModule,
    AppMaterialModule,
    AppRoutingModule,
    SharedModule,
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
      }
    }),
    PdfViewerModule
  ],
  entryComponents: [
    InfoDialogComponent,
    ConfirmationDialogComponent,
    UserDialogComponent,
    RoleDialogComponent,
    MenuDialogComponent,
    CorrespondenceDialogComponent,
    DocCommentComponent,
    CommentDialogComponent,
    DocPreviewerComponent,
    DepartmentDialogComponent,
    DepartmentTypeDialogComponent,
    OrganizationTypeDialogComponent,
    OrganizationDialogComponent,
    UserDepartmentDialogComponent,
    UserMenuActionDialogComponent,
    DocUploadsComponent,
    UploadsDialogComponent,
    ImgPreviewerDialogComponent,
    AssignCorrespondenceComponent,
    AssignCorrespondenceDialogComponent,
    DocCcComponent,
    CcDialogComponent,
    DocArchiverComponent,
    ManuscriptDialogComponent,
    AuthorDialogComponent,
    ManuscriptUploaderComponent,
    ManuscriptAuthorComponent,
    ManuscriptAuthorDialogComponent,
    ManuscriptRoutingComponent,
    ManuscriptRoutingDialogComponent,
    ManuscriptCommentComponent,
    ManuscriptCommentDialogComponent,
    AuthorRoyaltyDialogComponent,
    RoyaltyDialogComponent,
    ManuscriptArchiverComponent,
    AssessorDialogComponent
  ],
  providers: [
    AuthGuard,
    AuthService,
    DataService,
    UserService,
    {
      provide: MatPaginatorIntl,
      useClass: MatPaginatorI18nService,
    },
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    { provide: MAT_DATE_LOCALE, useValue: 'en-GB' }
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }

// platformBrowserDynamic().bootstrapModule(AppModule);

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
