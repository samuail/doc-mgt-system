import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DepartmentTypeDialogComponent } from './department-type-dialog.component';

describe('DepartmentTypeDialogComponent', () => {
  let component: DepartmentTypeDialogComponent;
  let fixture: ComponentFixture<DepartmentTypeDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DepartmentTypeDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DepartmentTypeDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
