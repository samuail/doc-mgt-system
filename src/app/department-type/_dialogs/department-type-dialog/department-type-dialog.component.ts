import { Component, OnInit, Inject, EventEmitter } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";

import { DepartmentType } from "src/app/_models/department-type";

@Component({
  selector: "app-department-type-dialog",
  templateUrl: "./department-type-dialog.component.html",
  styleUrls: ["./department-type-dialog.component.css"],
})
export class DepartmentTypeDialogComponent implements OnInit {
  departmentTypeForm: FormGroup;
  loading: boolean = false;
  title: string;
  departmentType: DepartmentType = new DepartmentType();

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<DepartmentTypeDialogComponent>
  ) {
    this.title = data.title;
    this.departmentType = data.departmentType;
  }

  ngOnInit() {
    if (this.title == "Add Department Type") {
      this.departmentTypeForm = this.fb.group({
        code: ["", Validators.required],
        name: ["", Validators.required],
      });
    } else if (this.title == "Edit Department Type") {
      this.departmentTypeForm = this.fb.group({
        code: [this.departmentType.code, Validators.required],
        name: [this.departmentType.name, Validators.required],
      });
    }
  }

  onDepartmentTypeSave = new EventEmitter();

  save() {
    if (this.departmentTypeForm.invalid) {
      return;
    }
    this.onDepartmentTypeSave.emit(this);
  }

  close() {
    this.dialogRef.close(false);
  }
}
