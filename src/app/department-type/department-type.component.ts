import { Component, OnInit, OnDestroy, ViewChild } from "@angular/core";
import { first } from "rxjs/operators";
import { Subscription } from "rxjs";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { MatTableDataSource } from "@angular/material/table";
import { Router } from "@angular/router";

import { Message } from "src/app/_models/_helpers/message";

import { DepartmentType } from "src/app/_models/department-type";
import { DepartmentTypeService } from "src/app/_services/department-type.service";

import { AuthenticationService } from "src/app/_services/authentication.service";
import { InfoDialogService } from "src/app/_services/info-dialog.service";
import { MenuService } from "src/app/_services/menu.service";

@Component({
  selector: "app-department-type",
  templateUrl: "./department-type.component.html",
  styleUrls: ["./department-type.component.css"],
})
export class DepartmentTypeComponent implements OnInit, OnDestroy {
  subManager = new Subscription();

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  title: string;
  message: Message = new Message();

  departmentTypes: DepartmentType[] = [];
  departmentType: DepartmentType = new DepartmentType();

  dataSource = new MatTableDataSource<DepartmentType>();
  displayedColumns = ["Code", "Name", "operations"];

  loading = false;
  resultsLength = 0;

  menuActions: string[] = [];

  canAdd: boolean = false;
  canEdit: boolean = false;
  canDelete: boolean = false;

  constructor(
    private router: Router,
    private departmentTypeService: DepartmentTypeService,
    private authenticationService: AuthenticationService,
    private menuService: MenuService,
    public infoDialogService: InfoDialogService
  ) {
    this.menuActions = this.menuService.getMenuActions("Department Types");
    for (var action of this.menuActions) {
      if (action == "Add") {
        this.canAdd = true;
      } else if (action == "Edit") {
        this.canEdit = true;
      } else if (action == "Delete") {
        this.canDelete = true;
      }
    }
  }

  ngOnInit() {
    this.getAll();
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngOnDestroy() {
    this.subManager.unsubscribe();
  }

  getAll(): void {
    this.loading = true;
    let getAll = this.departmentTypeService
      .getAll()
      .pipe(first())
      .subscribe(
        (response) => {
          if (response.success) {
            for (var value of response.data) {
              this.departmentType = {
                id: value["id"],
                code: value["code"],
                name: value["name"],
              };
              this.departmentTypes.push(this.departmentType);
            }
            this.loading = false;
            this.resultsLength = response.total;
            this.dataSource.data = this.departmentTypes;
          } else {
            this.message.title = "Department Type";
            this.message.type = "error";
            this.message.content = response.errors;
            const infoDialogRef = this.infoDialogService.openInfoDialog(
              this.message
            );
            infoDialogRef.afterClosed().subscribe(() => {
              if (response.errors[0] == "Signature has expired") {
                this.authenticationService.logout_offline();
                this.router.navigate(["/login"]);
              }
            });
          }
        },
        (error) => {
          this.loading = false;
          this.infoDialogService.openInfoDialog(error);
        }
      );
    this.subManager.add(getAll);
  }

  addDepartmentType() {
    this.title = "Add Department Type";
    const dialogRef = this.departmentTypeService.openDepartmentTypeDialog(
      this.title,
      new DepartmentType()
    );
    const sub = dialogRef.componentInstance.onDepartmentTypeSave.subscribe(
      (data) => {
        if (data) {
          data.loading = true;
          let create = this.departmentTypeService
            .create(
              data.departmentTypeForm.value.code,
              data.departmentTypeForm.value.name
            )
            .pipe(first())
            .subscribe(
              (response) => {
                if (response.success) {
                  this.message.type = "success";
                  this.message.title = this.title;
                  this.message.content = [response.message];
                  const infoDialogRef = this.infoDialogService.openInfoDialog(
                    this.message
                  );
                  infoDialogRef.afterClosed().subscribe(() => {
                    this.departmentType = {
                      id: response.data["id"],
                      code: response.data["code"],
                      name: response.data["name"],
                    };
                    this.departmentTypes.push(this.departmentType);
                    this.resultsLength = this.resultsLength + 1;
                    this.dataSource = new MatTableDataSource(
                      this.departmentTypes
                    );
                    data.close();
                  });
                } else {
                  this.message.title = this.title;
                  this.message.content = [];
                  this.message.type = "error";
                  for (var value of response.errors) {
                    this.message.content.push(value);
                  }
                  const infoDialogRef = this.infoDialogService.openInfoDialog(
                    this.message
                  );
                  infoDialogRef.afterClosed().subscribe(() => {
                    data.loading = false;
                  });
                }
              },
              (error) => {
                data.loading = false;
                this.infoDialogService.openInfoDialog(error);
              }
            );
          this.subManager.add(create);
        }
      }
    );

    dialogRef.afterClosed().subscribe(() => {
      sub.unsubscribe();
    });
  }

  editDepartmentType(departmentType) {
    this.title = "Edit Department Type";
    const dialogRef = this.departmentTypeService.openDepartmentTypeDialog(
      this.title,
      departmentType
    );
    const sub = dialogRef.componentInstance.onDepartmentTypeSave.subscribe(
      (data) => {
        if (data) {
          data.loading = true;
          let create = this.departmentTypeService
            .update(
              departmentType.id,
              data.departmentTypeForm.value.code,
              data.departmentTypeForm.value.name
            )
            .pipe(first())
            .subscribe(
              (response) => {
                if (response.success) {
                  this.message.type = "success";
                  this.message.title = this.title;
                  this.message.content = [response.message];
                  const infoDialogRef = this.infoDialogService.openInfoDialog(
                    this.message
                  );
                  infoDialogRef.afterClosed().subscribe(() => {
                    this.departmentTypes = this.departmentTypes.filter(
                      (item) => item.id !== departmentType.id
                    );
                    this.departmentType = {
                      id: response.data["id"],
                      code: response.data["code"],
                      name: response.data["name"],
                    };
                    this.departmentTypes.push(this.departmentType);
                    this.dataSource = new MatTableDataSource(
                      this.departmentTypes
                    );
                    data.close();
                  });
                } else {
                  this.message.title = this.title;
                  this.message.content = [];
                  this.message.type = "error";
                  for (var value of response.errors) {
                    this.message.content.push(value);
                  }
                  const infoDialogRef = this.infoDialogService.openInfoDialog(
                    this.message
                  );
                  infoDialogRef.afterClosed().subscribe(() => {
                    data.loading = false;
                  });
                }
              },
              (error) => {
                data.loading = false;
                this.infoDialogService.openInfoDialog(error);
              }
            );
          this.subManager.add(create);
        }
      }
    );

    dialogRef.afterClosed().subscribe(() => {
      sub.unsubscribe();
    });
  }

  deleteDepartmentType(departmentType) {}
}
