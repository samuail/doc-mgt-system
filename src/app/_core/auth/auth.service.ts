import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';


import { User } from './user';
import { MethodResponse } from '../method-response'
import { DataService } from '../data.service';
import { InfoDialogComponent } from '../info-dialog/info-dialog.component';

@Injectable()
export class AuthService {

  url = 'http://localhost:3000';

  acctType: string;

  private loggedIn = new BehaviorSubject<boolean>(false); 

  get isLoggedIn() {
    return this.loggedIn.asObservable(); 
  }

  //const authHeader = request.headers.get('Authorization');

  constructor(
    private router: Router,
    private http: HttpClient,
    public dataService: DataService,
    public dialog: MatDialog
  ) {}

  login(user: User){
    this.http.post<MethodResponse>(`${this.url}/auth/login`, {email: user.userName, password: user.password})
             .subscribe(
                response => {
                    if (response.success) {
                       this.loggedIn.next(true);
                       this.acctType = response.data["role"];
                       if (this.acctType == 'Administrator') {
                          //this.dataService.homeLink = '/admin';
                          this.router.navigate(['/admin']);
                       } else {
                          //this.dataService.homeLink = '/user';
                          this.router.navigate(['/user']);
                       }
                       this.dataService.userName = response.data["full_name"];
                       sessionStorage.setItem('user_name', response.data["full_name"]);
                       this.dataService.acctType = this.acctType;                      
                    } else {
                      this.openInfoDialog("Login", 
                              response.errors[0]);     
                    }
                },
                error => {
                    this.openInfoDialog("Login", error.error.errors.user_authentication[0]);
                }
          );
  }

  /*fetchMenu(){
    this.http.get<MethodResponse>(`${this.url}/auth/menu`)
             .subscribe(
                response => { 
                  console.log(response);
                },
                error => {
                    this.openInfoDialog("Fetch Menu", error.message);
                }
            );

  }*/

  openInfoDialog(title, message) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = {
      id: 1,
      title: title,
      message: message
    };

    this.dialog.open(InfoDialogComponent, dialogConfig);
  }

  logout() {     
     this.http.get<MethodResponse>(`${this.url}/auth/logout`)
             .subscribe(
                response => {
                  if (response.success) { 
                    this.loggedIn.next(false);
                    this.router.navigate(['/login']);
                  }
                });                      
  }
}
