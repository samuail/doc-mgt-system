import { Component, OnInit, Inject, EventEmitter } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";

import { Message } from "src/app/_models/_helpers/message";

@Component({
  selector: "app-info-dialog",
  templateUrl: "./info-dialog.component.html",
  styleUrls: ["./info-dialog.component.css"],
})
export class InfoDialogComponent implements OnInit {
  message: Message = new Message();

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialogRef: MatDialogRef<InfoDialogComponent>
  ) {
    this.message.title = data.title;
    this.message.content = data.message;
    this.message.type = data.type;
  }

  ngOnInit() {}

  onInfoDialogClose = new EventEmitter();

  close() {
    this.dialogRef.close(false);
    this.onInfoDialogClose.emit(this);
  }
}
