import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { LoginComponent } from "../login/login.component";
import { AdminpanelComponent } from "../adminpanel/adminpanel.component";
import { DashboardComponent } from "../adminpanel/dashboard/dashboard.component";
import { UserPanelComponent } from "../user-panel/user-panel.component";
import { UserDashboardComponent } from "../user-panel/user-dashboard/user-dashboard.component";
import { ForgotPasswordComponent } from "../forgot-password/forgot-password.component";
import { AboutComponent } from "../about/about.component";
import { ContactComponent } from "../contact/contact.component";
import { UserProfileComponent } from "../user-profile/user-profile.component";
import { AuthGuard } from "./auth/auth.guard";
import { UserComponent } from "../user/user.component";
import { RoleComponent } from "../role/role.component";
import { MenuComponent } from "../menu/menu.component";
import { IncomingCorrespondenceComponent } from "src/app/correspondence/incoming-correspondence/incoming-correspondence.component";
import { OutgoingCorrespondenceComponent } from "src/app/correspondence/outgoing-correspondence/outgoing-correspondence.component";
import { OfficeMemoCorrespondenceComponent } from "src/app/correspondence/office-memo-correspondence/office-memo-correspondence.component";
import { DepartmentComponent } from "src/app/department/department.component";
import { DepartmentTypeComponent } from "src/app/department-type/department-type.component";
import { OrganizationTypeComponent } from "src/app/organization-type/organization-type.component";
import { OrganizationComponent } from "src/app/organization/organization.component";
import { CenterComponent } from "src/app/correspondence/incomings/center/center.component";
import { CollegeComponent } from "src/app/correspondence/incomings/college/college.component";
import { ExternalComponent } from "src/app/correspondence/incomings/external/external.component";
import { InternalComponent } from "src/app/correspondence/outgoings/internal/internal.component";
import { OutgoingExternalComponent } from "src/app/correspondence/outgoings/outgoing-external/outgoing-external.component";
import { InternalArchivedComponent } from "src/app/correspondence/incomings/archived/internal-archived/internal-archived.component";
import { ExternalArchivedComponent } from "src/app/correspondence/incomings/archived/external-archived/external-archived.component";
import { ManuscriptComponent } from "src/app/correspondence/incomings/manuscript/manuscript.component";
import { AuthorComponent } from "src/app/author/author.component";
import { ManuscriptArchivedComponent } from "../correspondence/incomings/archived/manuscript-archived/manuscript-archived.component";
import { UserDashboardNewComponent } from "src/app/user-panel/user-dashboard-new/user-dashboard-new.component";
import { ExternalAssessorComponent } from "src/app/external-assessor/external-assessor.component";

const routes: Routes = [
  { path: "", component: LoginComponent },
  { path: "login", component: LoginComponent },
  {
    // AdminpanelComponent
    path: "admin",
    component: UserPanelComponent,
    canActivate: [AuthGuard],
    children: [
      {
        // DashboardComponent
        path: "",
        component: UserDashboardNewComponent,
      },
      //      {
      //      	path: 'my_profile',
      //      	component: UserProfileComponent
      //      },
      //      {
      //      	path: 'about',
      // component: AboutComponent
      //      },
      //      {
      //      	path: 'contact',
      // component: ContactComponent
      //      },
      {
        path: "users",
        component: UserComponent,
      },
      {
        path: "roles",
        component: RoleComponent,
      },
      {
        path: "menus",
        component: MenuComponent,
      },
      {
        path: "department_types",
        component: DepartmentTypeComponent,
      },
      {
        path: "departments",
        component: DepartmentComponent,
      },
      {
        path: "organization_types",
        component: OrganizationTypeComponent,
      },
      {
        path: "organizations",
        component: OrganizationComponent,
      },
      {
        path: "authors",
        component: AuthorComponent,
      },
      {
        path: "external_assessor",
        component: ExternalAssessorComponent,
      },
      {
        path: "incomings",
        component: IncomingCorrespondenceComponent,
      },
      {
        path: "outgoings",
        component: OutgoingCorrespondenceComponent,
      },
      {
        path: "office_memos",
        component: OfficeMemoCorrespondenceComponent,
      },
      {
        path: "incoming_centers",
        component: CenterComponent,
      },
      {
        path: "incoming_colleges",
        component: CollegeComponent,
      },
      {
        path: "incoming_external",
        component: ExternalComponent,
      },
      {
        path: "incoming_manuscripts",
        component: ManuscriptComponent,
      },      
      {
        path: "outgoing_internal",
        component: InternalComponent,
      },
      {
        path: "outgoing_external",
        component: OutgoingExternalComponent,
      },
      {
        path: "internal_archived",
        component: InternalArchivedComponent
      },
      {
        path: "external_archived",
        component: ExternalArchivedComponent
      },
      {
        path: "manuscript_archived",
        component: ManuscriptArchivedComponent
      }
    ],
  },
  {
    path: "user",
    component: UserPanelComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: "",
        component: UserDashboardNewComponent,
      },
      //      {
      //      	path: 'my_profile',ExternalComponent
      //      	component: UserProfileComponent
      //      },
      //      {
      //      	path: 'about',
      // component: AboutComponent
      //      },
      //      {
      //      	path: 'contact',
      // component: ContactComponent
      //      }
    ],
  },
  { path: "users", component: UserComponent, canActivate: [AuthGuard] },
  { path: "forgot_password", component: ForgotPasswordComponent },
  {
    path: "my_profile",
    component: UserProfileComponent,
    canActivate: [AuthGuard],
  },
  { path: "about", component: AboutComponent },
  { path: "contact", component: ContactComponent },
  { path: "roles", component: RoleComponent, canActivate: [AuthGuard] },
  { path: "menus", component: MenuComponent, canActivate: [AuthGuard] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  declarations: [],
})
export class AppRoutingModule {}
