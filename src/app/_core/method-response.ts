export class MethodResponse {
  success: boolean;
  message: string;
  data: any;
  errors: string[];
  total: number;
}