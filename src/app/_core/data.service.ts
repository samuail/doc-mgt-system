import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {  
  public homeLinkSubject: BehaviorSubject<string> = new BehaviorSubject<string>(
      JSON.parse(localStorage.getItem("homeLink"))
  );
	public homeLink: Observable<string> = this.homeLinkSubject.asObservable();
  //homeLink: string;	
  userName: string;
  acctType: string;
}
