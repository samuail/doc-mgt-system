import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExternalAssessorComponent } from './external-assessor.component';

describe('ExternalAssessorComponent', () => {
  let component: ExternalAssessorComponent;
  let fixture: ComponentFixture<ExternalAssessorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExternalAssessorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExternalAssessorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
