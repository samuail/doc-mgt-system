import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AssessorDialogComponent } from './assessor-dialog.component';

describe('AssessorDialogComponent', () => {
  let component: AssessorDialogComponent;
  let fixture: ComponentFixture<AssessorDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AssessorDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AssessorDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
