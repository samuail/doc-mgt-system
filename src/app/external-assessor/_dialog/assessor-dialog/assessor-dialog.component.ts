import { Component, OnInit, Inject, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

import { ExternalAssessor } from 'src/app/_models/external-assessor';

@Component({
  selector: 'app-assessor-dialog',
  templateUrl: './assessor-dialog.component.html',
  styleUrls: ['./assessor-dialog.component.css']
})
export class AssessorDialogComponent implements OnInit {

  loading = false;
  assessorForm: FormGroup;
  title: string;

  titles: any[] = [
    { value: "Prof.", viewValue: "Professor" },
    { value: "Dr.", viewValue: "Doc"},
    { value: "Ato", viewValue: "Ato" },
    { value: "W/ro", viewValue: "W/ro" },
    { value: "W/rt", viewValue: "W/rt" },
    { value: "Mr", viewValue: "Mister" },
    { value: "Mrs", viewValue: "Miss" },
    { value: "Ms", viewValue: "Misses" }
  ];

  assessor: ExternalAssessor = new ExternalAssessor();

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, 
  			  private fb: FormBuilder,
  			  private dialogRef: MatDialogRef<AssessorDialogComponent>
          ) {
 	  this.title = data.title;
    this.assessor = data.assessor;
  }

  ngOnInit(): void {
    if(this.title == 'Add Assessor') {
    	this.assessorForm = this.fb.group({
        title: ['', Validators.required],
    		firstName: ['', Validators.required],
      	lastName: ['', Validators.required],
        dFatherName: ['', Validators.required],
      	email:['', [Validators.required]],
    		telephone: ['', Validators.required],
        areaOfExpertise: ['', Validators.required],
        institutions:  ['', null],
        tin:  ['', Validators.required],
        bName: ['', Validators.required],
        bAcc:  ['', Validators.required]
    	});    
    } else if(this.title == 'Edit Assessor') {
    	this.assessorForm = this.fb.group({
        title: [this.assessor.title, Validators.required],
    		firstName: [this.assessor.firstName, Validators.required],
      	lastName: [this.assessor.fatherName, Validators.required],
        dFatherName: [this.assessor.grandFatherName, Validators.required],
      	email:[this.assessor.email, [Validators.required]],
    		telephone: [this.assessor.telephone, Validators.required],
        areaOfExpertise: [this.assessor.areaOfExpertise, Validators.required],
        institutions:  [this.assessor.institution, null],
        tin:  [this.assessor.tin, Validators.required],
        bName: [this.assessor.bankName, Validators.required],
        bAcc:  [this.assessor.bankAcc, Validators.required]
    	});  
    }
  }

  onAssessorSave = new EventEmitter();

  save() {
    if (this.assessorForm.invalid) {
          return;
    }
    this.onAssessorSave.emit(this);
  }

  close() {
	 this.dialogRef.close();
  }

}
