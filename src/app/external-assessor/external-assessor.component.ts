import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from "@angular/animations";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { MatTable, MatTableDataSource } from "@angular/material/table";
import { Subscription } from "rxjs";
import { first } from "rxjs/operators";
import { Router } from "@angular/router";

import { Message } from "src/app/_models/_helpers/message";
import { ExternalAssessor } from 'src/app/_models/external-assessor';

import { MenuService } from "src/app/_services/menu.service";
import { ExternalAssessorService } from "src/app/_services/external-assessor.service";
import { AuthenticationService } from "src/app/_services/authentication.service";
import { InfoDialogService } from "src/app/_services/info-dialog.service";

@Component({
  selector: 'app-external-assessor',
  templateUrl: './external-assessor.component.html',
  styleUrls: ['./external-assessor.component.css'],
  animations: [
    trigger("detailExpand", [
      state(
        "collapsed",
        style({ height: "0px", minHeight: "0", display: "none" })
      ),
      state("expanded", style({ height: "*" })),
      transition(
        "expanded <=> collapsed",
        animate("225ms cubic-bezier(0.4, 0.0, 0.2, 1)")
      ),
    ]),
  ]
})
export class ExternalAssessorComponent implements OnInit, OnDestroy {

  subManager = new Subscription();

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatPaginator) set matPaginator(mp: MatPaginator) {
    this.paginator = mp;
    this.setDataSourceAttributes();
  }

  setDataSourceAttributes() {
    this.externalAssessorDataSource.paginator = this.paginator;
    this.externalAssessorDataSource.sort = this.sort;
  }

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatTable) table: MatTable<any>;

  title: string;

  loading = false;
  message: Message = new Message();

  menuActions: string[] = [];

  canAdd: boolean = false;
  canEdit: boolean = false;
  canDelete: boolean = false;

  externalAssessor: ExternalAssessor = new ExternalAssessor();
  externalAssessors: ExternalAssessor[] = [];

  externalAssessorDataSource = new MatTableDataSource<ExternalAssessor>();

  assessorResultsLength = 0;

  displayedColumns = [
    "title",
    "firstName",
    "fatherName",
    "grandFatherName",
    "email",
    "telephone",
    "operations"
  ];

  expandedAssessor: ExternalAssessor | null;

  constructor(private menuService: MenuService,
        private externalAssessorService: ExternalAssessorService,
        private router: Router,
        private authenticationService: AuthenticationService,
        public infoDialogService: InfoDialogService) {
    this.menuActions = this.menuService.getMenuActions("Authors");
    for (var action of this.menuActions) {
      if (action == "Add") {
        this.canAdd = true;
      } else if (action == "Edit") {
        this.canEdit = true;
      } else if (action == "Delete") {
        this.canDelete = true;
      }
    }
  }

  ngOnInit(): void {
    this.getAllAssessor();
  }

  ngAfterViewInit() {
    this.externalAssessorDataSource.paginator = this.paginator;
    this.externalAssessorDataSource.sort = this.sort;
  }

  ngOnDestroy() {
    this.subManager.unsubscribe();
  }

  getAllAssessor() {
    this.loading = true;
    let getAll = this.externalAssessorService
      .getAll()
      .pipe(first())
      .subscribe(
        (response) => {
          if (response.success) {
            for (var value of response.data) {             
              this.externalAssessor = this.externalAssessorService.assignAssessorValue(value);
              this.externalAssessors.push(this.externalAssessor);
            }
            this.loading = false;
            this.assessorResultsLength = response.total;
            this.externalAssessorDataSource.data = this.externalAssessors;
          } else {
            this.message.title = "External Assessor";
            this.message.type = "error";
            this.message.content = response.errors;
            this.errorDialog(this.message, response);
            this.loading = false;
          }
        },
        (error) => {
          this.errorDialog(error, null);
          this.loading = false;
        }
      );
    this.subManager.add(getAll);
  }

  addAssessor() {
    this.title = "Add Assessor";
    const dialogRef = this.externalAssessorService.openAssessorDialog(
      this.title
    );

    const sub = dialogRef.componentInstance.onAssessorSave.subscribe(
      (data) => {
        if (data) {
          data.loading = true;
          let create = this.externalAssessorService
            .create(data)
            .pipe(first())
            .subscribe(
              (response) => {
                if (response.success) {
                  this.message.title = this.title;
                  this.message.content = [response.message];
                  this.message.type = "success";
                  const infoDialogRef = this.infoDialogService.openInfoDialog(
                    this.message
                  );
                  infoDialogRef.afterClosed().subscribe(() => {                    
                    this.externalAssessor = this.externalAssessorService.assignAssessorValue(response.data);
                    this.externalAssessors.splice(0, 0, this.externalAssessor);
                    this.assessorResultsLength = this.assessorResultsLength + 1;
                    this.externalAssessorDataSource = new MatTableDataSource(
                      this.externalAssessors
                    );
                    this.externalAssessorDataSource.paginator = this.paginator;
                    this.externalAssessorDataSource.sort = this.sort;                    
                    data.close();
                  });
                } else {
                  this.message.title = this.title;
                  this.message.type = "error";
                  this.message.content = response.errors;
                  this.errorDialog(this.message, response);
                  data.loading = false;                  
                }
              },
              (error) => {
                this.errorDialog(error, null);
                data.loading = false;
              }
            );
          this.subManager.add(create);
        }
      }
    );
    dialogRef.afterClosed().subscribe(() => {
      sub.unsubscribe();
    });
  }

  editAssessor(assessor) {
    this.title = "Edit Assessor";
    const dialogRef = this.externalAssessorService.openAssessorDialog(
      this.title,
      assessor
    );

    const sub = dialogRef.componentInstance.onAssessorSave.subscribe(
      (data) => {
        if (data) {
          data.loading = true;
          let create = this.externalAssessorService
            .update(data, assessor)
            .pipe(first())
            .subscribe(
              (response) => {
                if (response.success) {
                  this.message.title = this.title;
                  this.message.content = [response.message];
                  this.message.type = "success";
                  const infoDialogRef = this.infoDialogService.openInfoDialog(
                    this.message
                  );
                  infoDialogRef.afterClosed().subscribe(() => {
                    let index = this.externalAssessors.findIndex(x => x.id === assessor.id);
                    this.externalAssessor = this.externalAssessorService.assignAssessorValue(response.data);
                    this.externalAssessors.splice(index, 1, this.externalAssessor);
                    this.externalAssessorDataSource = new MatTableDataSource(
                      this.externalAssessors
                    );
                    this.externalAssessorDataSource.paginator = this.paginator;
                    this.externalAssessorDataSource.sort = this.sort;                    
                    data.close();
                  });
                } else {
                  this.message.title = this.title;
                  this.message.type = "error";
                  this.message.content = response.errors;
                  this.errorDialog(this.message, response);
                  data.loading = false;
                }
              },
              (error) => {
                this.errorDialog(error, null);
                data.loading = false;
              }
            );
          this.subManager.add(create);
        }
      }
    );
    dialogRef.afterClosed().subscribe(() => {
      sub.unsubscribe();
    });
  }

  deleteAssessor(assessor) {
    console.log("Delete External Assessor");
  }

  errorDialogOpened: boolean = false;

  errorDialog(message, response) {
    let infoDialogRef = null;
    if (!this.errorDialogOpened) {
      if (response != null) {
        if (response.errors[0] == "Signature has expired") {
          this.errorDialogOpened = true;
          infoDialogRef = this.infoDialogService.openInfoDialog(
            message
          );
        } else {
          infoDialogRef = this.infoDialogService.openInfoDialog(
            message
          );
        }
      } else {
        infoDialogRef = this.infoDialogService.openInfoDialog(
          message
        );
      }
      infoDialogRef.afterClosed().subscribe(() => {
        if (response != null) {
          if (response.errors[0] == "Signature has expired") {
            this.authenticationService.logout_offline();
            this.router.navigate(["/login"]);
          }
        }
      });
    }     
  }
}
