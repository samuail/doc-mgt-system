import { Component, OnInit, OnDestroy, ViewChild } from "@angular/core";
import { first } from "rxjs/operators";
import { Subscription } from "rxjs";
import { MatPaginator } from "@angular/material/paginator";
import { MatSnackBar } from "@angular/material/snack-bar";
import { MatSort } from "@angular/material/sort";
import { MatTableDataSource } from "@angular/material/table";
import { Router } from "@angular/router";

import { Message } from "../_models/_helpers/message";

import { Menu } from "../_models/menu";
import { MenuService } from "../_services/menu.service";

import { AuthenticationService } from "../_services/authentication.service";
import { InfoDialogService } from "../_services/info-dialog.service";

@Component({
  selector: "app-menu",
  templateUrl: "./menu.component.html",
  styleUrls: ["./menu.component.css"],
})
export class MenuComponent implements OnInit, OnDestroy {
  subManager = new Subscription();

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  title: string;
  message: Message = new Message();

  parent_name: string = "";

  menus: Menu[] = [];
  menu: Menu = new Menu();

  displayedColumns: string[] = ["text", "iconCls", "parentId", "operations"];
  dataSource = new MatTableDataSource<Menu>();

  resultsLength = 0;
  isLoadingResults = false;

  menuActions: string[] = [];

  canEdit: boolean = false;
  canListUser: boolean = false;

  constructor(
    private router: Router,
    public snackBar: MatSnackBar,
    private menuService: MenuService,
    private authenticationService: AuthenticationService,
    public infoDialogService: InfoDialogService
  ) {
    this.menuActions = this.menuService.getMenuActions("Menus");
    for (var action of this.menuActions) {
      if (action == "Edit") {
        this.canEdit = true;
      } else if (action == "List User") {
        this.canListUser = true;
      }
    }    
  }

  ngOnInit() {
    this.getAll();
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngOnDestroy() {
    this.subManager.unsubscribe();
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  getAll(): void {
    this.isLoadingResults = true;
    let getAll = this.menuService
      .getAll()
      .pipe(first())
      .subscribe(
        (response) => {
          if (response.success) {
            for (var value of response.data) {
              if (value["parent_id"] != null) {
                this.parent_name = response.data.filter(
                  (item) => item.id === value["parent_id"]
                )[0].text;
              } else {
                this.parent_name = "";
              }
              this.menu = {
                id: value["id"],
                text: value["text"],
                icon_cls: value["icon_cls"],
                class_name: value["class_name"],
                location: value["location"],
                parent_id: value["parent_id"],
                parent_name: this.parent_name,
              };
              this.menus.push(this.menu);
            }
            this.isLoadingResults = false;
            this.resultsLength = response.total;
            this.dataSource.data = this.menus;
          } else {
            this.message.title = "Menus";
            this.message.type = "error";
            this.message.content = response.errors;
            const infoDialogRef = this.infoDialogService.openInfoDialog(
              this.message
            );
            infoDialogRef.afterClosed().subscribe(() => {
              if (response.errors[0] == "Signature has expired") {
                this.authenticationService.logout_offline();
                this.router.navigate(["/login"]);
              }
            });
          }
        },
        (error) => {
          this.isLoadingResults = false;
          this.infoDialogService.openInfoDialog(error);
        }
      );
    this.subManager.add(getAll);
  }

  addMenu() {
    this.title = "Add Menu";
    const dialogRef = this.menuService.openMenuDialog(
      this.title,
      new Menu(),
      [],
      []
    );
    dialogRef.afterClosed().subscribe((data) => {
      // console.log(data);
    });
  }

  editMenu(menu) {
    this.title = "Edit Menu";
    const dialogRef = this.menuService.openMenuDialog(
      this.title,
      menu,
      this.menus,
      []
    );
    const sub = dialogRef.componentInstance.onMenuSave.subscribe((data) => {
      if (data) {
        data.loading = true;
        let update = this.menuService
          .update(menu, data.menuForm.value)
          .pipe(first())
          .subscribe(
            (response) => {
              if (response.success) {
                this.message.type = "success";
                this.message.title = this.title;
                this.message.content = [response.message];
                const infoDialogRef = this.infoDialogService.openInfoDialog(
                  this.message
                );
                infoDialogRef.afterClosed().subscribe(() => {
                  this.menus = this.menus.filter((item) => item.id !== menu.id);
                  this.parent_name =
                    this.menus.filter(
                      (item) => item.id === response.data["parent_id"]
                    ).length === 0
                      ? ""
                      : this.menus.filter(
                          (item) => item.id === response.data["parent_id"]
                        )[0].text;
                  this.menu = {
                    id: response.data["id"],
                    text: response.data["text"],
                    icon_cls: response.data["icon_cls"],
                    class_name: response.data["class_name"],
                    location: response.data["location"],
                    parent_id: response.data["parent_id"],
                    parent_name: this.parent_name,
                  };
                  this.menus.push(this.menu);
                  this.dataSource = new MatTableDataSource(this.menus);
                  data.close();
                });
              } else {
                this.message.title = this.title;
                this.message.content = [];
                this.message.type = "error";
                for (var value of response.errors) {
                  this.message.content.push(value);
                }
                const infoDialogRef = this.infoDialogService.openInfoDialog(
                  this.message
                );
                infoDialogRef.afterClosed().subscribe(() => {
                  data.loading = false;
                });
              }
            },
            (error) => {
              data.loading = false;
              this.infoDialogService.openInfoDialog(error);
            }
          );
        this.subManager.add(update);
      }
    });

    dialogRef.afterClosed().subscribe(() => {
      sub.unsubscribe();
    });
  }

  listUsers(menu) {
    let getMenuUsers = this.menuService
      .getMenuUsers(menu.id)
      .pipe(first())
      .subscribe(
        (response) => {
          if (response.success) {
            const dialogRef = this.menuService.openMenuDialog(
              "List of Users",
              menu,
              [],
              response.data
            );
          }
        },
        (error) => {
          this.infoDialogService.openInfoDialog(error);
        }
      );
    this.subManager.add(getMenuUsers);
  }

  deleteMenu(menu) {}
}
