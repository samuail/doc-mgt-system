import { Component, OnInit, Inject, EventEmitter } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

import { FormGroup, FormBuilder, FormControl, FormArray, Validators, ValidatorFn } from '@angular/forms';

import { Menu } from '../../_models/menu';
import { MenuItem } from '../../_models/menu-item';

import { User } from '../../_models/user';

@Component({
  selector: 'app-menu-dialog',
  templateUrl: './menu-dialog.component.html',
  styleUrls: ['./menu-dialog.component.css']
})
export class MenuDialogComponent implements OnInit {

	loading = false;
	menuForm: FormGroup;
	title: string;
	selected: number;

	menu: Menu = new Menu();
	menus: Menu [] = [];
	menuItems: MenuItem [] = [];

	allMenus: Menu[] = [];
	assignedMenus: Menu[] = [];
	assignedMenu: Menu = new Menu();
	parentMenus: Menu[] = [];

	users: User[] = [];

	constructor(@Inject(MAT_DIALOG_DATA) public data: any, 
  			private fb: FormBuilder,
  			private dialogRef: MatDialogRef<MenuDialogComponent>) {
 		this.title = data.title;
 		if (this.title === "Assign Menus") {
 			this.menuItems = data.menu;
 		} else {
 			this.menu = data.menu;
 		} 		
 		this.menus = data.menus;
 		this.users = data.users;
  	}

	ngOnInit() {
		if(this.title == 'Add Menu') {
	  		this.menuForm = this.fb.group({
	  			menuName: ['', Validators.required]
	  		});    
	  	} else if(this.title == 'Edit Menu') {
	  		
	  		this.allMenus = this.menus.filter(item => item.id !== this.menu.id);

	  		if (this.menu.parent_id === null) {
	  			this.selected = 0;
	  		} else {
	  			this.selected = this.menus.filter(item => item.id === this.menu.parent_id)[0].id;
	  		}	    

	      	this.menuForm = this.fb.group({
	        	menuName: [this.menu.text, Validators.required],
	        	icon: [this.menu.icon_cls, Validators.required], 
	        	parentMenus: [this.selected]
	      	});    
	    } else if(this.title == 'Assign Menus') {
	    	
	    	this.parentMenus = [];
	    	this.allMenus = [];
	    	this.assignedMenus = [];

	    	this.parentMenus = this.menus.filter (item => item.parent_id === null );
	    	for (var value of this.parentMenus) {
	    		this.allMenus.push(value);
	    		for (var child of this.menus.filter (item => item.parent_id === value["id"])) {
	    			this.allMenus.push(child)
	    		}
	    	}

	    	for (var menu of this.menuItems) {
	    		this.assignedMenu = 
                  {
                    id: menu["id"],
                    text: menu["displayName"],                                                          
                    icon_cls: menu["iconName"],
                    class_name: menu["route"],
                    location: null,
                    parent_id: null,
                    parent_name: null 
                  };
            	this.assignedMenus.push(this.assignedMenu);
            	for (var childMenu of menu["children"]){
            		this.assignedMenu = 
	                  {
	                    id: childMenu["id"],
	                    text: childMenu["displayName"],                                                          
	                    icon_cls: childMenu["iconName"],
	                    class_name: childMenu["route"],
	                    location: null,
	                    parent_id: null,
	                    parent_name: null 
	                  };
	            	this.assignedMenus.push(this.assignedMenu);            	
            	}
	    	}
	    	
	      	const menuControls = this.allMenus.map(
	      		c => this.assignedMenus.find(
	      			ob => ob.text === c.text) ? new FormControl(true) : new FormControl(false));

	  		this.menuForm = this.fb.group({
	  			allMenus: new FormArray(menuControls, this.minSelectedCheckboxes(1))
	  		});
	  	} else {
	  		this.menuForm = this.fb.group({
	  		});
	  	}
	}

	 minSelectedCheckboxes(min = 1) {
	    const validator: ValidatorFn = (formArray: FormArray) => {
	      const totalSelected = formArray.controls
	        .map(control => control.value)
	        .reduce((prev, next) => next ? prev + next : prev, 0);

	      return totalSelected >= min ? null : { required: true };
	    };

	    return validator;
	}

	onMenuSave = new EventEmitter();

	save() {   
	  	if (this.menuForm.invalid) {
	        return;
	    }
	    this.onMenuSave.emit(this);
	}

	close() {
	  this.dialogRef.close(false);
	}

}
