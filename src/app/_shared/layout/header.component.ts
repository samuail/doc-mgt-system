import { Component, OnInit, Renderer2 } from "@angular/core";
import { Observable } from "rxjs";
import { Router } from "@angular/router";
import { first } from "rxjs/operators";
import { TranslateService } from '@ngx-translate/core';

import { AuthService } from "../../_core/auth/auth.service";
import { DataService } from "../../_core/data.service";

import { AuthenticationService } from "../../_services/authentication.service";
import { InfoDialogService } from "../../_services/info-dialog.service";
import { User } from "../../_models/user";

import { Message } from "../../_models/_helpers/message";

@Component({
  selector: "app-layout-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.css"],
})
export class HeaderComponent implements OnInit {
  isLoggedIn$: Observable<boolean>;
  currentUser: User;

  homeLink: string;
  // title: string;
  // message: string;
  // type: string;
  badgeCount: number = 5;
  visible: boolean = true;

  message: Message = new Message();

  constructor(
    private translate: TranslateService,
    private renderer: Renderer2,
    private authService: AuthService,
    public dataService: DataService,
    private router: Router,
    private authenticationService: AuthenticationService,
    public infoDialogService: InfoDialogService
  ) {}

  ngOnInit() {
    this.isLoggedIn$ = this.authService.isLoggedIn;
    this.authenticationService.currentUser.subscribe(
      (x) => (this.currentUser = x)
    );
    this.dataService.homeLink.subscribe((value) => {
      this.homeLink = value;
    });
  }

  clearCount() {
    if (this.badgeCount > 0) {
      this.badgeCount = this.badgeCount - 1;
    } else {
      this.visible = false;
    }
  }

  languages = ["en", "አማ"];
  selectedLanguage: string = "አማ";  

  onLanguageSelect({ value: language }) {
    this.selectedLanguage = language;
    if (language == "አማ")
      language = "am";
    this.translate.use(language);
    this.renderer.setAttribute(document.querySelector('html'), 'lang', language);
  }


  get userName(): string {
    if (this.currentUser) {
      return this.currentUser.firstName + " " + this.currentUser.lastName;
    }
    return "";
  }

  // get homeLink():string {
  // return this.dataService.homeLink;
  // }

  /*onLogout(){
    this.authService.logout();                      
  }*/

  onLogout() {
    this.authenticationService
      .logout()
      .pipe(first())
      .subscribe(
        (response) => {
          if (response.success) {
            this.message.type = "success";
            this.message.title = "Logout";
            this.message.content = [response.message];
            const dialogRef = this.infoDialogService.openInfoDialog(
              this.message
            );
            const sub = dialogRef.componentInstance.onInfoDialogClose.subscribe(
              (data) => {
                if (data) {
                  this.router.navigate(["/login"]);
                }
              }
            );
            dialogRef.afterClosed().subscribe(() => {
              sub.unsubscribe();
            });
          } else {
            this.message.title = "Logout";
            this.message.type = "error";
            this.message.content = response.errors;
            const infoDialogRef = this.infoDialogService.openInfoDialog(
              this.message
            );
            infoDialogRef.afterClosed().subscribe(() => {
              if (response.errors[0] == "Signature has expired") {
                this.authenticationService.logout_offline();
                this.router.navigate(["/login"]);
              }
            });
          }
        },
        (error) => {
          this.infoDialogService.openInfoDialog(error);
          this.authenticationService.logout_offline();
          this.router.navigate(["/login"]);
        }
      );
  }
}
