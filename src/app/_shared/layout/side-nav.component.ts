import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { Router } from '@angular/router';

import { AppComponent } from '../../app.component';

import { AuthenticationService } from '../../_services/authentication.service';

import { MenuItem } from '../../_models/menu-item';
import { DepartmentRole } from "src/app/_models/department-role";


@Component({
  selector: 'app-layout-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.css']
})

export class SideNavComponent implements OnInit {
  
  sidenav: MatSidenav;

  userMenus: MenuItem [] = [];

  userDepartment: DepartmentRole;

  constructor(public appComponent: AppComponent, 
  			private authenticationService: AuthenticationService,
  			private router: Router) {
  }	

  ngOnInit() {
  	this.sidenav = this.appComponent.sidenav;
  	this.authenticationService.userMenus.subscribe(x => this.userMenus = x);  
    this.authenticationService.userDepartment.subscribe(
      (x) =>  (this.userDepartment = x)
    );
  }
}
