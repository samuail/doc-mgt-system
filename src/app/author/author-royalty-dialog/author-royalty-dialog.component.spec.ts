import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthorRoyaltyDialogComponent } from './author-royalty-dialog.component';

describe('AuthorRoyaltyDialogComponent', () => {
  let component: AuthorRoyaltyDialogComponent;
  let fixture: ComponentFixture<AuthorRoyaltyDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuthorRoyaltyDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthorRoyaltyDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
