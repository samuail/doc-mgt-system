import { Component, OnInit, Inject, EventEmitter } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { first } from "rxjs/operators";
import { Subscription } from "rxjs";
import { Router } from "@angular/router";

import { Message } from 'src/app/_models/_helpers/message';
import { Royalty } from 'src/app/_models/royalty';
import { Author } from 'src/app/_models/author';
import { Manuscript } from 'src/app/_models/manuscript';

import { ManuscriptService } from 'src/app/_services/manuscript.service';
import { InfoDialogService } from 'src/app/_services/info-dialog.service';
import { AuthenticationService } from 'src/app/_services/authentication.service';

@Component({
  selector: 'app-royalty-dialog',
  templateUrl: './royalty-dialog.component.html',
  styleUrls: ['./royalty-dialog.component.css']
})
export class RoyaltyDialogComponent implements OnInit {
  
  subManager = new Subscription();

  title: string;
  message: Message = new Message();
  
  royalty: Royalty;

  author: Author;

  manuscript: Manuscript = new Manuscript();
  manuscripts: Manuscript[] = [];

  royaltyForm: FormGroup;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, 
    private dialogRef: MatDialogRef<RoyaltyDialogComponent>,
    private fb: FormBuilder,
    public manuscriptService: ManuscriptService,
    private router: Router,
    private infoDialogService: InfoDialogService,
    private authenticationService: AuthenticationService) {
    this.title = data.title;
    this.author = data.author;
    this.royalty = data.royalty;
  }

  ngOnInit() {
    this.getAllManuscript();
    if (this.title == "Add Royalty") {
      this.royaltyForm = this.fb.group({
        manuscript: ["", Validators.required],
        referenceNo: ["", Validators.required],
        amount: ["", Validators.required],
        remark: ["", null]
      });
    } else {
      this.royaltyForm = this.fb.group({
        manuscript: [this.royalty.manuscript.id, Validators.required],
        referenceNo: [this.royalty.referenceNo, Validators.required],
        amount: [this.royalty.amount, Validators.required],
        remark: [this.royalty.remark, null]
      });
    }
  }

  ngOnDestroy() {
    this.subManager.unsubscribe();
  }

  onRoyaltySave = new EventEmitter();

  getAllManuscript() {
    let getAll = this.manuscriptService
      .getAllManuscriptsByAuthor(this.author)
      .pipe(first())
      .subscribe(
        (response) => {
          if (response.success) {
            for (var value of response.data) {
              this.manuscript = this.manuscriptService.assignManuscriptValue(value);
              this.manuscripts.push(this.manuscript);
            }
          } else {
            this.message.title = "Author Manuscripts";
            this.message.type = "error";
            this.message.content = response.errors;
            this.errorDialog(this.message, response);
          }
        },
        (error) => {
          this.errorDialog(error, null);
        }
      );
    this.subManager.add(getAll);
  }

  save() {
    if (this.royaltyForm.invalid) {
      return;
    }
    this.onRoyaltySave.emit(this);
  }

  close() {
    this.dialogRef.close();
  }

  errorDialogOpened: boolean = false;

  errorDialog(message, response) {
    let infoDialogRef = null;
    if (!this.errorDialogOpened) {
      if (response != null) {
        if (response.errors[0] == "Signature has expired") {
          this.errorDialogOpened = true;
          infoDialogRef = this.infoDialogService.openInfoDialog(
            message
          );
        } else {
          infoDialogRef = this.infoDialogService.openInfoDialog(
            message
          );
        }
      } else {
        infoDialogRef = this.infoDialogService.openInfoDialog(
          message
        );
      }
      infoDialogRef.afterClosed().subscribe(() => {
        if (response != null) {
          if (response.errors[0] == "Signature has expired") {
            this.authenticationService.logout_offline();
            this.router.navigate(["/login"]);
          }
        }
      });
    }     
  }

}
