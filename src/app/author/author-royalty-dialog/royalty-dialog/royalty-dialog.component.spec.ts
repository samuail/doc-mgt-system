import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoyaltyDialogComponent } from './royalty-dialog.component';

describe('RoyaltyDialogComponent', () => {
  let component: RoyaltyDialogComponent;
  let fixture: ComponentFixture<RoyaltyDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoyaltyDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoyaltyDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
