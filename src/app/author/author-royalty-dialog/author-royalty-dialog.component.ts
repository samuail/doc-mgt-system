import { Component, OnInit,Inject, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA } from "@angular/material/dialog";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { MatTable, MatTableDataSource } from "@angular/material/table";
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from "@angular/animations";
import { first } from "rxjs/operators";
import { Subscription } from "rxjs";
import { Router } from "@angular/router";

import { Author } from "src/app/_models/author";
import { User } from "src/app/_models/user";
import { Royalty } from "src/app/_models/royalty";
import { Message } from 'src/app/_models/_helpers/message';

import { AuthorRoyaltyService } from "src/app/_services/author-royalty.service";
import { InfoDialogService } from 'src/app/_services/info-dialog.service';
import { AuthenticationService } from 'src/app/_services/authentication.service';

@Component({
  selector: 'app-author-royalty-dialog',
  templateUrl: './author-royalty-dialog.component.html',
  styleUrls: ['./author-royalty-dialog.component.css'],
  animations: [
    trigger("detailExpand", [
      state(
        "collapsed",
        style({ height: "0px", minHeight: "0", display: "none" })
      ),
      state("expanded", style({ height: "*" })),
      transition(
        "expanded <=> collapsed",
        animate("225ms cubic-bezier(0.4, 0.0, 0.2, 1)")
      ),
    ]),
  ]
})
export class AuthorRoyaltyDialogComponent implements OnInit {
  subManager = new Subscription();

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatPaginator) set matPaginator(mp: MatPaginator) {
    this.paginator = mp;
    this.setDataSourceAttributes();
  }

  setDataSourceAttributes() {
    this.royaltyDataSource.paginator = this.paginator;
    this.royaltyDataSource.sort = this.sort;
  }

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatTable) table: MatTable<any>;

  title: string;
  title2: string;
  message: Message = new Message();
  loading: boolean = false;

  canAdd: boolean = true;
  canEdit: boolean = true;
  canDelete: boolean = false;

  currentUser: User;
  author: Author;

  royalty: Royalty = new Royalty();
  royalties: Royalty[] = [];
  
  royaltyDataSource = new MatTableDataSource<Royalty>();

  displayedColumns = ["manuscript", "referenceNo", "amount", "paymentDate", "operations"];

  royaltyResultsLength = 0;

  expandedRoyalty: Royalty | null;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
        private router: Router,
        private authorRoyaltyService: AuthorRoyaltyService,
        private infoDialogService: InfoDialogService,
        private authenticationService: AuthenticationService) {
    this.title = data.title;
    this.author = data.author;

    this.authenticationService.currentUser.subscribe(
      (x) => (this.currentUser = x)
    );
  }

  ngOnInit() {
    this.getAll();
  }

  ngAfterViewInit() {
    this.royaltyDataSource.paginator = this.paginator;
    this.royaltyDataSource.sort = this.sort;
  }

  ngOnDestroy() {
    this.subManager.unsubscribe();
  }

  getAll(){
    this.loading = true;
    let getAll = this.authorRoyaltyService
      .getAllRoyalty(this.author)
      .pipe(first())
      .subscribe(
        (response) => {
          if (response.success) {
            for (var value of response.data) {
              this.royalty = this.authorRoyaltyService.assignRoyaltyValue(value);
              this.royalties.push(this.royalty);
            }
            this.loading = false;
            this.royaltyResultsLength = response.total;            
            this.royaltyDataSource.data = this.royalties;
          } else {
            this.message.title = "Manuscript";
            this.message.type = "error";
            this.message.content = response.errors;
            this.errorDialog(this.message, response);
            this.loading = false;
          }
        },
        (error) => {
          this.loading = false;
          this.errorDialog(error, null);
        }
      );
    this.subManager.add(getAll);
  }

  addRoyalty() {
    this.title2 = "Add Royalty";
    const dialogRef = this.authorRoyaltyService.openRoyaltyDialog(      
      this.title2,
      this.author
    );

    const sub = dialogRef.componentInstance.onRoyaltySave.subscribe(
      (data) => {
        if (data) {
          data.loading = true;
          let create = this.authorRoyaltyService
            .create(data, this.author)
            .pipe(first())
            .subscribe(
              (response) => {
                if (response.success) {
                  this.message.title = this.title2;
                  this.message.content = [response.message];
                  this.message.type = "success";
                  const infoDialogRef = this.infoDialogService.openInfoDialog(
                    this.message
                  );
                  infoDialogRef.afterClosed().subscribe(() => {                    
                    this.royalty = this.authorRoyaltyService.assignRoyaltyValue(response.data);
                    this.royalties.splice(0, 0, this.royalty);
                    this.royaltyResultsLength = this.royaltyResultsLength + 1;
                    this.royaltyDataSource = new MatTableDataSource(
                      this.royalties
                    );
                    this.royaltyDataSource.paginator = this.paginator;
                    this.royaltyDataSource.sort = this.sort;                    
                    data.close();
                  });
                } else {
                  this.message.title = this.title2;
                  this.message.type = "error";
                  this.message.content = response.errors;
                  this.errorDialog(this.message, response);
                  data.loading = false;                  
                }
              },
              (error) => {
                this.errorDialog(error, null);
                data.loading = false;
              }
            );
          this.subManager.add(create);
        }
      }
    );
    dialogRef.afterClosed().subscribe(() => {
      sub.unsubscribe();
    });
  }

  editRoyalty(royalty) {
    this.title2 = "Edit Royalty";
    const dialogRef = this.authorRoyaltyService.openRoyaltyDialog(
      this.title2,
      this.author,
      royalty
    );

    const sub = dialogRef.componentInstance.onRoyaltySave.subscribe(
      (data) => {
        if (data) {
          data.loading = true;
          let update = this.authorRoyaltyService
            .update(data, this.author, royalty)
            .pipe(first())
            .subscribe(
              (response) => {
                if (response.success) {
                  this.message.title = this.title2;
                  this.message.content = [response.message];
                  this.message.type = "success";
                  const infoDialogRef = this.infoDialogService.openInfoDialog(
                    this.message
                  );
                  infoDialogRef.afterClosed().subscribe(() => {                    
                    this.royalty = this.authorRoyaltyService.assignRoyaltyValue(response.data);
                    let index = this.royalties.findIndex(x => x.id === royalty.id);
                    this.royalties.splice(index, 1, this.royalty);
                    this.royaltyDataSource = new MatTableDataSource(
                      this.royalties
                    );
                    this.royaltyDataSource.paginator = this.paginator;
                    this.royaltyDataSource.sort = this.sort;                    
                    data.close();
                  });
                } else {
                  this.message.title = this.title2;
                  this.message.type = "error";
                  this.message.content = response.errors;
                  this.errorDialog(this.message, response);
                  data.loading = false;                  
                }
              },
              (error) => {
                this.errorDialog(error, null);
                data.loading = false;
              }
            );
          this.subManager.add(update);
        }
      }
    );
    dialogRef.afterClosed().subscribe(() => {
      sub.unsubscribe();
    });
  }

  previewRoyalty(royalty) {
    const dialogRef = this.authorRoyaltyService.openRoyaltyPreviewDialog(
      "Preview Royalty",
      royalty
    );
  }

  deleteRoyalty(royalty) {}

  errorDialogOpened: boolean = false;

  errorDialog(message, response) {
    let infoDialogRef = null;
    if (!this.errorDialogOpened) {
      if (response != null) {
        if (response.errors[0] == "Signature has expired") {
          this.errorDialogOpened = true;
          infoDialogRef = this.infoDialogService.openInfoDialog(
            message
          );
        } else {
          infoDialogRef = this.infoDialogService.openInfoDialog(
            message
          );
        }
      } else {
        infoDialogRef = this.infoDialogService.openInfoDialog(
          message
        );
      }
      infoDialogRef.afterClosed().subscribe(() => {
        if (response != null) {
          if (response.errors[0] == "Signature has expired") {
            this.authenticationService.logout_offline();
            this.router.navigate(["/login"]);
          }
        }
      });
    }     
  }

}
