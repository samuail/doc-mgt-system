import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from "@angular/animations";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { MatTable, MatTableDataSource } from "@angular/material/table";
import { Subscription } from "rxjs";
import { first } from "rxjs/operators";
import { Router } from "@angular/router";

import { Message } from "src/app/_models/_helpers/message";
import { Author } from "src/app/_models/author";

import { MenuService } from "src/app/_services/menu.service";
import { AuthorService } from "src/app/_services/author.service";
import { AuthenticationService } from "src/app/_services/authentication.service";
import { InfoDialogService } from "src/app/_services/info-dialog.service";
import { RoyaltyService } from 'src/app/_services/royalty.service';

@Component({
  selector: 'app-author',
  templateUrl: './author.component.html',
  styleUrls: ['./author.component.css'],
  animations: [
    trigger("detailExpand", [
      state(
        "collapsed",
        style({ height: "0px", minHeight: "0", display: "none" })
      ),
      state("expanded", style({ height: "*" })),
      transition(
        "expanded <=> collapsed",
        animate("225ms cubic-bezier(0.4, 0.0, 0.2, 1)")
      ),
    ]),
  ]
})
export class AuthorComponent implements OnInit, OnDestroy {
  subManager = new Subscription();

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatPaginator) set matPaginator(mp: MatPaginator) {
    this.paginator = mp;
    this.setDataSourceAttributes();
  }

  setDataSourceAttributes() {
    this.authorDataSource.paginator = this.paginator;
    this.authorDataSource.sort = this.sort;
  }

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatTable) table: MatTable<any>;

  title: string;

  loading = false;
  message: Message = new Message();

  menuActions: string[] = [];

  canAdd: boolean = false;
  canEdit: boolean = false;
  canDelete: boolean = false;

  author: Author = new Author();
  authors: Author[] = [];

  authorDataSource = new MatTableDataSource<Author>();

  authorResultsLength = 0;

  displayedColumns = [
    "firstName",
    "fatherName",
    "grandFatherName",
    "email",
    "telephone",
    "operations",
  ];

  expandedAuthor: Author | null;

  constructor(private menuService: MenuService,
        private authorService: AuthorService,
        private router: Router,
        private authenticationService: AuthenticationService,
        public infoDialogService: InfoDialogService, 
        public royaltyService: RoyaltyService) {
    this.menuActions = this.menuService.getMenuActions("Authors");
    for (var action of this.menuActions) {
      if (action == "Add") {
        this.canAdd = true;
      } else if (action == "Edit") {
        this.canEdit = true;
      } else if (action == "Delete") {
        this.canDelete = true;
      }
    }
  }

  ngOnInit() {
    this.getAllAuthor();
  }

  ngAfterViewInit() {
    this.authorDataSource.paginator = this.paginator;
    this.authorDataSource.sort = this.sort;
  }

  ngOnDestroy() {
    this.subManager.unsubscribe();
  }

  getAllAuthor() {
    this.loading = true;
    let getAll = this.authorService
      .getAll()
      .pipe(first())
      .subscribe(
        (response) => {
          if (response.success) {
            for (var value of response.data) {             
              this.author = this.authorService.assignAuthorValue(value);
              this.authors.push(this.author);
            }
            this.loading = false;
            this.authorResultsLength = response.total;
            this.authorDataSource.data = this.authors;
          } else {
            this.message.title = "Author";
            this.message.type = "error";
            this.message.content = response.errors;
            this.errorDialog(this.message, response);
            this.loading = false;
          }
        },
        (error) => {
          this.errorDialog(error, null);
          this.loading = false;
        }
      );
    this.subManager.add(getAll);
  }

  addAuthor(event) {
    this.title = "Add Author";
    const dialogRef = this.authorService.openAuthorDialog(
      this.title
    );

    const sub = dialogRef.componentInstance.onAuthorSave.subscribe(
      (data) => {
        if (data) {
          data.loading = true;
          let create = this.authorService
            .create(data)
            .pipe(first())
            .subscribe(
              (response) => {
                if (response.success) {
                  this.message.title = this.title;
                  this.message.content = [response.message];
                  this.message.type = "success";
                  const infoDialogRef = this.infoDialogService.openInfoDialog(
                    this.message
                  );
                  infoDialogRef.afterClosed().subscribe(() => {                    
                    this.author = this.authorService.assignAuthorValue(response.data);
                    this.authors.splice(0, 0, this.author);
                    this.authorResultsLength = this.authorResultsLength + 1;
                    this.authorDataSource = new MatTableDataSource(
                      this.authors
                    );
                    this.authorDataSource.paginator = this.paginator;
                    this.authorDataSource.sort = this.sort;                    
                    data.close();
                  });
                } else {
                  this.message.title = this.title;
                  this.message.type = "error";
                  this.message.content = response.errors;
                  this.errorDialog(this.message, response);
                  data.loading = false;                  
                }
              },
              (error) => {
                this.errorDialog(error, null);
                data.loading = false;
              }
            );
          this.subManager.add(create);
        }
      }
    );
    dialogRef.afterClosed().subscribe(() => {
      sub.unsubscribe();
    });
  }

  editAuthor(author) {
    this.title = "Edit Author";
    const dialogRef = this.authorService.openAuthorDialog(
      this.title,
      author
    );

    const sub = dialogRef.componentInstance.onAuthorSave.subscribe(
      (data) => {
        if (data) {
          data.loading = true;
          let create = this.authorService
            .update(data, author)
            .pipe(first())
            .subscribe(
              (response) => {
                if (response.success) {
                  this.message.title = this.title;
                  this.message.content = [response.message];
                  this.message.type = "success";
                  const infoDialogRef = this.infoDialogService.openInfoDialog(
                    this.message
                  );
                  infoDialogRef.afterClosed().subscribe(() => {
                    let index = this.authors.findIndex(x => x.id === author.id);
                    this.author = this.authorService.assignAuthorValue(response.data);
                    this.authors.splice(index, 1, this.author);                    
                    this.authorDataSource = new MatTableDataSource(
                      this.authors
                    );
                    this.authorDataSource.paginator = this.paginator;
                    this.authorDataSource.sort = this.sort;                    
                    data.close();
                  });
                } else {
                  this.message.title = this.title;
                  this.message.type = "error";
                  this.message.content = response.errors;
                  this.errorDialog(this.message, response);
                  data.loading = false;
                }
              },
              (error) => {
                this.errorDialog(error, null);
                data.loading = false;
              }
            );
          this.subManager.add(create);
        }
      }
    );
    dialogRef.afterClosed().subscribe(() => {
      sub.unsubscribe();
    });
  }

  addContactDetails(author) {
    this.title = "Contact Details";
    const dialogRef = this.authorService.openAuthorDialog(
      this.title,
      author
    );

    const sub = dialogRef.componentInstance.onAuthorSave.subscribe(
      (data) => {
        if (data) {
          data.loading = true;
          let functionName = Object.keys(author.authorContact).length == 0 ? this.authorService.addContact(data, author) : this.authorService.editContact(data, author);
          let addContact = functionName.pipe(first())
            .subscribe(
              (response) => {
                if (response.success) {
                  this.message.title = this.title;
                  this.message.content = [response.message];
                  this.message.type = "success";
                  const infoDialogRef = this.infoDialogService.openInfoDialog(
                    this.message
                  );
                  infoDialogRef.afterClosed().subscribe(() => {
                    let index = this.authors.findIndex(x => x.id === author.id);
                    this.authors[index].authorContact.id = response.data["id"];
                    this.authors[index].authorContact.firstName = response.data["first_name"];
                    this.authors[index].authorContact.fatherName = response.data["father_name"];
                    this.authors[index].authorContact.email = response.data["email"];
                    this.authors[index].authorContact.telephone = response.data["telephone"];
                    this.authorDataSource = new MatTableDataSource(
                      this.authors
                    );
                    this.authorDataSource.paginator = this.paginator;
                    this.authorDataSource.sort = this.sort;                    
                    data.close();
                  });
                } else {
                  this.message.title = this.title;
                  this.message.type = "error";
                  this.message.content = response.errors;
                  this.errorDialog(this.message, response);
                  data.loading = false;                  
                }
              },
              (error) => {
                this.errorDialog(error, null);
                data.loading = false;
              }
            );
          this.subManager.add(addContact);
        }
      }
    );
    dialogRef.afterClosed().subscribe(() => {
      sub.unsubscribe();
    });
  }

  addPaymentDetails(author) {
    this.title = "Payment Detail";
    const dialogRef = this.authorService.openAuthorDialog(
      this.title,
      author
    );

    const sub = dialogRef.componentInstance.onAuthorSave.subscribe(
      (data) => {
        if (data) {
          data.loading = true;
          let create = this.authorService
            .update_payment(data, author)
            .pipe(first())
            .subscribe(
              (response) => {
                if (response.success) {
                  this.message.title = this.title;
                  this.message.content = [response.message];
                  this.message.type = "success";
                  const infoDialogRef = this.infoDialogService.openInfoDialog(
                    this.message
                  );
                  infoDialogRef.afterClosed().subscribe(() => {
                    let index = this.authors.findIndex(x => x.id === author.id);
                    this.author = this.authorService.assignAuthorValue(response.data);
                    this.authors.splice(index, 1, this.author);                    
                    this.authorDataSource = new MatTableDataSource(
                      this.authors
                    );
                    this.authorDataSource.paginator = this.paginator;
                    this.authorDataSource.sort = this.sort;                    
                    data.close();
                  });
                } else {
                  this.message.title = this.title;
                  this.message.type = "error";
                  this.message.content = response.errors;
                  this.errorDialog(this.message, response);
                  data.loading = false;                
                }
              },
              (error) => {
                data.loading = false;
                this.errorDialog(error, null);
              }
            );
          this.subManager.add(create);
        }
      }
    );
    dialogRef.afterClosed().subscribe(() => {
      sub.unsubscribe();
    });
  }

  addRoyaltyDetails(author) {
    this.title = "Royalty Payment";
    const dialogRef = this.royaltyService.openRoyaltyPaymentDialog(
      this.title,
      author
    );    
  }

  deleteAuthor(author) {
    console.log("You can delete!!");
  }

  errorDialogOpened: boolean = false;

  errorDialog(message, response) {
    let infoDialogRef = null;
    if (!this.errorDialogOpened) {
      if (response != null) {
        if (response.errors[0] == "Signature has expired") {
          this.errorDialogOpened = true;
          infoDialogRef = this.infoDialogService.openInfoDialog(
            message
          );
        } else {
          infoDialogRef = this.infoDialogService.openInfoDialog(
            message
          );
        }
      } else {
        infoDialogRef = this.infoDialogService.openInfoDialog(
          message
        );
      }
      infoDialogRef.afterClosed().subscribe(() => {
        if (response != null) {
          if (response.errors[0] == "Signature has expired") {
            this.authenticationService.logout_offline();
            this.router.navigate(["/login"]);
          }
        }
      });
    }     
  }
}
