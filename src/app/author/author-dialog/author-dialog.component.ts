import { Component, OnInit, Inject, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

import { Author } from "src/app/_models/author";

@Component({
  selector: 'app-author-dialog',
  templateUrl: './author-dialog.component.html',
  styleUrls: ['./author-dialog.component.css']
})
export class AuthorDialogComponent implements OnInit {

  loading = false;
  authorForm: FormGroup;
  authorContactForm: FormGroup;
  title: string;

  author: Author = new Author();

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, 
  			  private fb: FormBuilder,
  			  private dialogRef: MatDialogRef<AuthorDialogComponent>
          ) {
 	  this.title = data.title;
    this.author = data.author;
  }

  ngOnInit() {
    if(this.title == 'Add Author') {
    	this.authorForm = this.fb.group({
    		firstName: ['', Validators.required],
      	lastName: ['', Validators.required],
        dFatherName: ['', Validators.required],
      	email:['', [Validators.required]],
    		telephone: ['', Validators.required],
        hasContact: ['', null]
    	});    
    } else if(this.title == 'Edit Author') {
    	this.authorForm = this.fb.group({
    		firstName: [this.author.firstName, Validators.required],
      	lastName: [this.author.fatherName, Validators.required],
        dFatherName: [this.author.grandFatherName, Validators.required],
      	email:[this.author.email, [Validators.required]],
    		telephone: [this.author.telephone, Validators.required],
        hasContact: [this.author.hasContact, null]
    	});  
    } else if(this.title == 'Payment Detail') {
      this.authorForm = this.fb.group({
        bName:[this.author.bankName, null],
        bAcc: [this.author.bankAcc, null],
      	tin: [this.author.tin, null]
      });
    } else if(this.title == 'Contact Details') {
      let firstName = Object.keys(this.author.authorContact).length == 0 ? "" : this.author.authorContact.firstName;
      let fatherName = Object.keys(this.author.authorContact).length == 0 ? "" : this.author.authorContact.fatherName;
      let email = Object.keys(this.author.authorContact).length == 0 ? "" : this.author.authorContact.email;
      let telephone = Object.keys(this.author.authorContact).length == 0 ? "" : this.author.authorContact.telephone;
      this.authorForm = this.fb.group({
        firstName: [firstName, Validators.required],
      	lastName: [fatherName, Validators.required],
        email:[email, [Validators.required]],
    		telephone: [telephone, Validators.required]
      });
    }
  }

  onAuthorSave = new EventEmitter();

  save() {
    if (this.authorForm.invalid) {
          return;
    }
    this.onAuthorSave.emit(this);
  }

  close() {
	 this.dialogRef.close();
  }

}
