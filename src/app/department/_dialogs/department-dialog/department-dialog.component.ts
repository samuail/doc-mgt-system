import {
  Component,
  OnInit,
  OnDestroy,
  Inject,
  EventEmitter,
} from "@angular/core";
import { FormGroup, FormControl, FormArray, FormBuilder, Validators, ValidatorFn } from "@angular/forms";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
import { first } from "rxjs/operators";
import { Subscription } from "rxjs";
import { Router } from "@angular/router";

import { Message } from "src/app/_models/_helpers/message";
import { Department } from "src/app/_models/department";
import { DepartmentItem } from "src/app/_models/department-item";
import { DepartmentType } from "src/app/_models/department-type";
import { Role } from "src/app/_models/role";

import { DepartmentTypeService } from "src/app/_services/department-type.service";
import { AuthenticationService } from "src/app/_services/authentication.service";
import { InfoDialogService } from "src/app/_services/info-dialog.service";

@Component({
  selector: "app-department-dialog",
  templateUrl: "./department-dialog.component.html",
  styleUrls: ["./department-dialog.component.css"],
})
export class DepartmentDialogComponent implements OnInit, OnDestroy {
  subManager = new Subscription();

  loading = false;
  loadingRoles = false;
  departmentForm: FormGroup;
  title: string;
  message: Message = new Message();

  parentId: number;
  department_type_id: number;

  department: Department = new Department();
  departmentType: DepartmentType = new DepartmentType();
  departmentTypes: DepartmentType[] = [];

  allDepartments: Department[] = [];

  departmentItem: DepartmentItem = new DepartmentItem();

  assignedRoles: any[] = [];

  allRoles: Role[] = [];

  departmentHeadRoleId: any;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private router: Router,
    private dialogRef: MatDialogRef<DepartmentDialogComponent>,
    private departmentTypeService: DepartmentTypeService,
    private authenticationService: AuthenticationService,
    public infoDialogService: InfoDialogService
  ) {
    this.title = data.title;
    this.allDepartments = data.allDepartments;
    this.parentId = data.node;
    this.department_type_id = data.department.department_type_id;
    this.department = data.department;
    this.allRoles = data.allRoles;
    this.assignedRoles = data.assignedRoles;
    this.departmentHeadRoleId = data.departmentHeadRoleId;
    this.getAllDepartmentTypes();
  }

  ngOnInit() {
    if (this.title == "Add Department") {
      this.departmentForm = this.fb.group({
        type: ["", Validators.required],
        code: ["", Validators.required],
        description: ["", Validators.required],
      });
    } else if (this.title == "Edit Department") {
      if (this.parentId == 0) {
        this.departmentForm = this.fb.group({
          type: [this.department_type_id, Validators.required],
          code: [this.department.code, Validators.required],
          description: [this.department.description, Validators.required],
        });
      } else {
        this.departmentForm = this.fb.group({
          type: [this.department_type_id, Validators.required],
          code: [this.department.code, Validators.required],
          description: [this.department.description, Validators.required],
          parent: [this.parentId, null],
        });
      }
    } else if (this.title == "Define Department Roles"){      
      const roleControls = this.allRoles.map((c) =>
      this.assignedRoles.find((ob) => {        
          return ob.id === c.id;        
        })
        ? new FormControl({value: true, disabled: true})
        : new FormControl(false)
      );

      this.departmentForm = this.fb.group({
        allRoles: new FormArray(
          roleControls,
          this.minSelectedCheckboxes(1)
        )
      });
    } else {
      this.departmentForm = this.fb.group({
        allRoles: [this.departmentHeadRoleId, Validators.required]
      });      
    }
  }

  minSelectedCheckboxes(min = 1) {
    const validator: ValidatorFn = (formArray: FormArray) => {
      const totalSelected = formArray.controls
        .map((control) => control.value)
        .reduce((prev, next) => (next ? prev + next : prev), 0);

      return totalSelected >= min ? null : { required: true };
    };
    return validator;
  }

  ngOnDestroy() {
    this.subManager.unsubscribe();
  }

  getAllDepartmentTypes(): void {
    this.loading = true;
    let getAll = this.departmentTypeService
      .getAll()
      .pipe(first())
      .subscribe(
        (response) => {
          if (response.success) {
            for (var value of response.data) {
              this.departmentType = {
                id: value["id"],
                code: value["code"],
                name: value["name"],
              };
              this.departmentTypes.push(this.departmentType);
            }
            this.loading = false;
          } else {
            this.message.title = "Users";
            this.message.type = "error";
            this.message.content = response.errors;
            const infoDialogRef = this.infoDialogService.openInfoDialog(
              this.message
            );
            infoDialogRef.afterClosed().subscribe(() => {
              if (response.errors[0] == "Signature has expired") {
                this.authenticationService.logout_offline();
                this.router.navigate(["/login"]);
              }
            });
          }
        },
        (error) => {
          this.loading = false;
          this.infoDialogService.openInfoDialog(error);
        }
      );
    this.subManager.add(getAll);
  }

  onDepartmentSave = new EventEmitter();

  save() {
    if (this.departmentForm.invalid) {
      return;
    }
    this.onDepartmentSave.emit(this);
  }

  close() {
    this.dialogRef.close();
  }
}
