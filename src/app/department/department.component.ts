import { Component, OnInit, OnDestroy } from "@angular/core";
import { NestedTreeControl } from "@angular/cdk/tree";
import { MatTreeNestedDataSource } from "@angular/material/tree";
import { BehaviorSubject } from "rxjs";
import { first } from "rxjs/operators";
import { Subscription } from "rxjs";
import { Router } from "@angular/router";

import { Message } from "src/app/_models/_helpers/message";
import { Department } from "src/app/_models/department";
import { DepartmentItem } from "src/app/_models/department-item";
import { Role } from "src/app/_models/role";  

import { AuthenticationService } from "src/app/_services/authentication.service";
import { DepartmentService } from "src/app/_services/department.service";
import { InfoDialogService } from "src/app/_services/info-dialog.service";
import { MenuService } from "../_services/menu.service";
import { RoleService } from "src/app/_services/role.service";

@Component({
  selector: "app-department",
  templateUrl: "./department.component.html",
  styleUrls: ["./department.component.css"],
})
export class DepartmentComponent implements OnInit, OnDestroy {
  menuActions: string[] = [];

  canAddToRoot: boolean = false;
  canAdd: boolean = false;
  canEdit: boolean = false;
  canDelete: boolean = false;

  allRoles: Role[] = [];
  role: Role = new Role();

  constructor(
    private router: Router,
    private departmentService: DepartmentService,
    public infoDialogService: InfoDialogService,
    private authenticationService: AuthenticationService,
    private menuService: MenuService,
    private roleService: RoleService
  ) {
    this.treeSource = new MatTreeNestedDataSource<DepartmentItem>();
    this.dataSource$ = new BehaviorSubject<DepartmentItem[]>([]);
    this.dataSource$.subscribe((items) => {
      this.treeSource.data = null;
      this.treeSource.data = items;
    });
    this.initData();
    this.menuActions = this.menuService.getMenuActions("Departments");
    for (var action of this.menuActions) {
      if (action == "Add to Root") {
        this.canAddToRoot = true;
      } else if (action == "Add") {
        this.canAdd = true;
      } else if (action == "Edit") {
        this.canEdit = true;
      } else if (action == "Delete") {
        this.canDelete = true;
      }
    }
  }

  readonly dataSource$: BehaviorSubject<DepartmentItem[]>;
  readonly treeSource: MatTreeNestedDataSource<DepartmentItem>;
  readonly treeControl = new NestedTreeControl<DepartmentItem>(
    (node) => node.children
  );
  readonly hasChild = (_: number, node: DepartmentItem) =>
    !!node.children && node.children.length > 0;
  readonly trackBy = (_: number, node: DepartmentItem) => node.id;

  subManager = new Subscription();

  title: string;
  message: Message = new Message();

  oneDepartmentItem: DepartmentItem = new DepartmentItem();
  departmentItems: DepartmentItem[] = [];

  oneDepartment: Department = new Department();
  allDepartments: Department[] = [];

  assignedRoles: Role[] = [];

  departmentHeadRoleId: any;

  initData() {
    this.title = "Department";
    let getAllForTree = this.departmentService
      .getAllForTree()
      .pipe(first())
      .subscribe(
        (response) => {
          if (response.success) {
            this.departmentItems = response.data;
            this.dataSource$.next(this.departmentItems);
          } else {
            this.message.title = this.title;
            this.message.content = [];
            this.message.type = "error";
            for (var value of response.errors) {
              this.message.content.push(value);
            }
            const infoDialogRef = this.infoDialogService.openInfoDialog(
              this.message
            );
            infoDialogRef.afterClosed().subscribe(() => {
              if (response.errors[0] == "Signature has expired") {
                this.authenticationService.logout_offline();
                this.router.navigate(["/login"]);
              }
            });
          }
        },
        (error) => {
          this.infoDialogService.openInfoDialog(error);
        }
      );
    this.subManager.add(getAllForTree);
  }

  ngOnInit() {
    this.getAllDepartments();
    this.getAllRoles();
  }

  ngOnDestroy() {
    this.subManager.unsubscribe();
  }

  getAllDepartments(): void {
    let getAll = this.departmentService
      .getAll("All")
      .pipe(first())
      .subscribe(
        (response) => {
          if (response.success) {
            for (var value of response.data) {
              this.oneDepartment = {
                id: value["id"],
                code: value["code"],
                description: value["description"],
                department_type: value["department_type"],
              };
              this.allDepartments.push(this.oneDepartment);
            }
          } else {
            this.message.title = this.title;
            this.message.content = [];
            this.message.type = "error";
            for (var err of response.errors) {
              this.message.content.push(err);
            }
            const infoDialogRef = this.infoDialogService.openInfoDialog(
              this.message
            );
            infoDialogRef.afterClosed().subscribe(() => {
              if (response.errors[0] == "Signature has expired") {
                this.authenticationService.logout_offline();
                this.router.navigate(["/login"]);
              }
            });
          }
        },
        (error) => {
          this.infoDialogService.openInfoDialog(error);
        }
      );
    this.subManager.add(getAll);
  }

  assignRole (values) {
    let role: Role;
    let roles: Role [] = [];
    for (var value of values) {
      role = {
        id: value["id"],
        name: value["name"],
      };
      roles.push(role);
    }
    return roles;
  }

  getAllRoles(): void {    
    let getAll = this.roleService
      .getAll()
      .pipe(first())
      .subscribe(
        (response) => {
          if (response.success) {
            this.allRoles = this.assignRole(response.data);            
          } else {
            this.message.title = "User Roles";
            this.message.type = "error";
            this.message.content = response.errors;
            const infoDialogRef = this.infoDialogService.openInfoDialog(
              this.message
            );
            infoDialogRef.afterClosed().subscribe(() => {
              if (response.errors[0] == "Signature has expired") {
                this.authenticationService.logout_offline();
                this.router.navigate(["/login"]);
              }
            });
          }
        },
        (error) => {
          this.infoDialogService.openInfoDialog(error);
        }
      );
    this.subManager.add(getAll);
  }

  addDepartment(node?: DepartmentItem) {
    this.title = "Add Department";
    const dialogRef = this.departmentService.openDepartmentDialog(
      this.title,
      this.allDepartments,
      node,
      new Department()
    );
    const sub = dialogRef.componentInstance.onDepartmentSave.subscribe(
      (data) => {
        if (data) {
          data.loading = true;
          let parentId;
          if (node) {
            parentId = node.id;
          } else {
            parentId = null;
          }
          let create = this.departmentService
            .create(
              data.departmentForm.value.type,
              data.departmentForm.value.code,
              data.departmentForm.value.description,
              parentId
            )
            .pipe(first())
            .subscribe(
              (response) => {
                if (response.success) {
                  this.message.type = "success";
                  this.message.title = this.title;
                  this.message.content = [response.message];
                  const infoDialogRef = this.infoDialogService.openInfoDialog(
                    this.message
                  );
                  infoDialogRef.afterClosed().subscribe(() => {
                    if (node) {
                      node.children = [
                        ...(node.children || []),
                        {
                          id: response.data["id"],
                          code: response.data["code"],
                          description: response.data["description"],
                          department_type_id:
                            response.data["department_type"]["id"],
                          parent_id: node.id,
                          children: [],
                        },
                      ];
                      if (!this.treeControl.isExpanded(node)) {
                        this.treeControl.expand(node);
                      }
                    } else {
                      this.dataSource$.next([
                        ...this.dataSource$.value,
                        {
                          id: response.data["id"],
                          code: response.data["code"],
                          description: response.data["description"],
                          department_type_id: response.data["department_type"],
                          children: [],
                        },
                      ]);
                    }

                    this.dataSource$.next(this.dataSource$.value);

                    data.close();
                  });
                } else {
                  this.message.title = this.title;
                  this.message.content = [];
                  this.message.type = "error";
                  for (var value of response.errors) {
                    this.message.content.push(value);
                  }
                  const infoDialogRef = this.infoDialogService.openInfoDialog(
                    this.message
                  );
                  infoDialogRef.afterClosed().subscribe(() => {
                    data.loading = false;
                  });
                }
              },
              (error) => {
                data.loading = false;
                this.infoDialogService.openInfoDialog(error);
              }
            );
          this.subManager.add(create);
        }
      }
    );
    dialogRef.afterClosed().subscribe(() => {
      sub.unsubscribe();
    });
  }

  editDepartment(node) {
    this.title = "Edit Department";
    const dialogRef = this.departmentService.openDepartmentDialog(
      this.title,
      this.allDepartments,
      node.parent_id,
      node
    );
  }

  defineRoleForDepartment(node) {
    this.title = "Define Department Roles";
    let getAssignedRoleForDepartment = this.departmentService
      .getAssignedRoleForDepartment(node.id)
      .pipe(first())
      .subscribe(
        (response) => {
          if (response.success) {
            this.assignedRoles = this.assignRole(response.data);

            const dialogRef = this.departmentService.openDepartmentDialog(
              this.title,
              this.allDepartments,
              node.parent_id,
              node,              
              this.allRoles.sort((a,b) => a.name.localeCompare(b.name)),
              this.assignedRoles
            );
            this.assignRoleForDepartment(this.title, dialogRef, this.allRoles, node);
          } else {
            this.message.title = this.title;
            this.message.type = "error";
            this.message.content = response.errors;
            const infoDialogRef = this.infoDialogService.openInfoDialog(
              this.message
            );
            infoDialogRef.afterClosed().subscribe(() => {
              if (response.errors[0] == "Signature has expired") {
                this.authenticationService.logout_offline();
                this.router.navigate(["/login"]);
              }
            });
          }
        },
        (error) => {
          this.infoDialogService.openInfoDialog(error);
        }
      );
    this.subManager.add(getAssignedRoleForDepartment);
  }

  assignRoleForDepartment(title, dialogRef, allRoles, department) {    
    const sub = dialogRef.componentInstance.onDepartmentSave.subscribe(
      (data) => {
        if (data) {
          let unique = allRoles.filter(x => !this.assignedRoles.some(y => x.id === y.id));
          const selectedRoleIds = data.departmentForm.value.allRoles
            .map((v, i) => (v ? unique[i].id : null))
            .filter((v) => v !== null);          
          data.loading = true;
          let assignRoles = this.departmentService
            .assignRoles(department, selectedRoleIds)
            .pipe(first())
            .subscribe(
              (response) => {
                if (response.success) {
                  this.message.title = title;
                  this.message.content = [response.message];
                  this.message.type = "success";
                  const infoDialogRef = this.infoDialogService.openInfoDialog(
                    this.message
                  );
                  infoDialogRef.afterClosed().subscribe(() => {
                    data.close();
                  });                  
                } else {
                  this.message.title = title;
                  this.message.type = "error";
                  this.message.content = response.errors;
                  const infoDialogRef = this.infoDialogService.openInfoDialog(
                    this.message
                  );
                  infoDialogRef.afterClosed().subscribe(() => {
                    if (response.errors[0] == "Signature has expired") {
                      data.close();
                      this.authenticationService.logout_offline();
                      this.router.navigate(["/login"]);
                    }
                  });
                }
              },
              (error) => {
                data.loading = false;
                this.infoDialogService.openInfoDialog(error);
              }
            );
          this.subManager.add(assignRoles);
        }
      }
    );
  }

  defineHeadRoleForDepartment(node) {
    this.title = "Define Department Head";
    let getAssignedRoleForDepartment = this.departmentService
      .getAssignedRoleForDepartment(node.id)
      .pipe(first())
      .subscribe(
        (response) => {
          if (response.success) {
            this.assignedRoles = this.assignRole(response.data);

            let getDepartmentHeads = this.departmentService
              .getDepartmentHead(node)
              .pipe(first())
              .subscribe(
                (response) => {
                  if (response.success) {
                    let department_role = response.data.filter((x) => x.is_head ? x.user_role_id : null);
                    this.departmentHeadRoleId = department_role.length > 0 ? department_role[0].user_role_id : 0;

                    const dialogRef = this.departmentService.openDepartmentDialog(
                    this.title,
                    this.allDepartments,
                    node.parent_id,
                    node,              
                    null,
                    this.assignedRoles.sort((a,b) => a.name.localeCompare(b.name)),
                    this.departmentHeadRoleId
                  );
                  this.assignHeadForDepartment(this.title, dialogRef, this.allRoles, node);
                  } else {
                    this.message.title = this.title;
                    this.message.type = "error";
                    this.message.content = response.errors;
                    const infoDialogRef = this.infoDialogService.openInfoDialog(
                      this.message
                    );
                    infoDialogRef.afterClosed().subscribe(() => {
                      if (response.errors[0] == "Signature has expired") {
                        this.authenticationService.logout_offline();
                        this.router.navigate(["/login"]);
                      }
                    });
                  }
                },
                (error) => {
                  this.infoDialogService.openInfoDialog(error);
                }
              );
            this.subManager.add(getDepartmentHeads)            
          } else {
            this.message.title = this.title;
            this.message.type = "error";
            this.message.content = response.errors;
            const infoDialogRef = this.infoDialogService.openInfoDialog(
              this.message
            );
            infoDialogRef.afterClosed().subscribe(() => {
              if (response.errors[0] == "Signature has expired") {
                this.authenticationService.logout_offline();
                this.router.navigate(["/login"]);
              }
            });
          }
        },
        (error) => {
          this.infoDialogService.openInfoDialog(error);
        }
      );
    this.subManager.add(getAssignedRoleForDepartment);
  }

  assignHeadForDepartment(title, dialogRef, allRoles, department) {    
    const sub = dialogRef.componentInstance.onDepartmentSave.subscribe(
      (data) => {
        if (data) {
          const selectedRoleIds = data.departmentForm.value.allRoles;          
          data.loading = true;
          let assignRoles = this.departmentService
            .assignDepartmentHead(department, selectedRoleIds)
            .pipe(first())
            .subscribe(
              (response) => {
                if (response.success) {
                  this.message.title = title;
                  this.message.content = [response.message];
                  this.message.type = "success";
                  const infoDialogRef = this.infoDialogService.openInfoDialog(
                    this.message
                  );
                  infoDialogRef.afterClosed().subscribe(() => {
                    data.close();
                  });                  
                } else {
                  this.message.title = title;
                  this.message.type = "error";
                  this.message.content = response.errors;
                  const infoDialogRef = this.infoDialogService.openInfoDialog(
                    this.message
                  );
                  infoDialogRef.afterClosed().subscribe(() => {
                    if (response.errors[0] == "Signature has expired") {
                      data.close();
                      this.authenticationService.logout_offline();
                      this.router.navigate(["/login"]);
                    }
                  });
                }
              },
              (error) => {
                data.loading = false;
                this.infoDialogService.openInfoDialog(error);
              }
            );
          this.subManager.add(assignRoles);
        }
      }
    );
  }

  deleteDepartment(node) {
    // console.log(node);
  }
}
