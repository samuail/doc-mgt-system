import { Component, OnInit, OnDestroy } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { first } from "rxjs/operators";
import { Subscription } from "rxjs";
import { HttpErrorResponse } from "@angular/common/http";

import { AuthService } from "../_core/auth/auth.service";
import { DataService } from "../_core/data.service";

import { AuthenticationService } from "../_services/authentication.service";
import { UserService } from "../_services/user.service";
import { InfoDialogService } from "../_services/info-dialog.service";

import { User } from "../_models/user";
import { MenuItem } from "../_models/menu-item";
import { Role } from "../_models/role";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"],
})
export class LoginComponent implements OnInit, OnDestroy {
  subManager = new Subscription();

  form: FormGroup;
  private formSubmitAttempt: boolean;
  loading = false;

  title: string;
  message: string;
  type: string;

  currentUser: User;
  userRole: Role = new Role();
  userMenus: MenuItem[] = [];

  allMenuActions: any[] = [];

  getUserRole: any;
  getUserMenus: any;
  getUserMenuActions: any;
  getUserDepartments: any;

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router,
    public dataService: DataService,
    private authenticationService: AuthenticationService,
    private userService: UserService,
    public infoDialogService: InfoDialogService
  ) {}

  ngOnInit() {
    this.form = this.fb.group({
      userName: ["", Validators.required],
      password: ["", Validators.required],
    });

    this.authenticationService.logout();
  }

  ngOnDestroy() {
    this.subManager.unsubscribe();
  }

  isFieldInvalid(field: string) {
    return (
      (!this.form.get(field).valid && this.form.get(field).touched) ||
      (this.form.get(field).untouched && this.formSubmitAttempt)
    );
  }

  onSubmit() {
    this.formSubmitAttempt = true;

    if (this.form.invalid) {
      return;
    }

    this.loading = true;
    let login = this.authenticationService
      .login(this.form.value.userName, this.form.value.password)
      .pipe(first())
      .subscribe(
        (response) => {
          if (response.success) {
            this.authenticationService.currentUser.subscribe(
              (x) => (this.currentUser = x)
            );
            if (this.currentUser) {              
              this.getUserRole = this.userService
                .getUserRoles(this.currentUser.id)
                .pipe(first())
                .subscribe(
                  (response) => {
                    if (response.success) {
                      this.userRole.id = response.data[0]["id"];
                      this.userRole.name = response.data[0]["name"];
                      localStorage.setItem(
                        "userRole",
                        JSON.stringify(this.userRole)
                      );
                      this.authenticationService.userRoleSubject.next(
                        this.userRole
                      );

                      if (this.userRole.name == "Administrator") {
                        this.router.navigate(["/admin"]);
                        localStorage.setItem(
                          "homeLink",
                          JSON.stringify("/admin")
                        );
                        this.dataService.homeLinkSubject.next("/admin");
                      } else {
                        this.router.navigate(["/user"]);
                        localStorage.setItem(
                          "homeLink",
                          JSON.stringify("/user")
                        );
                        this.dataService.homeLinkSubject.next("/user");
                      }
                    }
                  }, 
                  (error) => {
                    this.loading = false;
                    this.infoDialogService.openInfoDialog(error);
                  }
                );
              this.getUserMenus = this.userService
                .getUserMenus(this.currentUser.id)
                .pipe(first())
                .subscribe(
                  (response) => {
                    if (response.success) {
                      this.userMenus = response.data;
                      localStorage.setItem(
                        "userMenus",
                        JSON.stringify(this.userMenus)
                      );
                      this.authenticationService.userMenusSubject.next(
                        this.userMenus
                      );
                    }
                  },
                  (error) => {
                    this.loading = false;
                    this.infoDialogService.openInfoDialog(error);
                  }
                );
              this.getUserMenuActions = this.userService
                .getAssignedMenuActions(this.currentUser.id)
                .pipe(first())
                .subscribe(
                  (responseAssigned) => {
                    if (responseAssigned.success) {
                      this.allMenuActions = responseAssigned.data;
                      localStorage.setItem(
                        "userMenuActions",
                        JSON.stringify(this.allMenuActions)
                      );
                      this.authenticationService.menuActionsSubject.next(
                        this.allMenuActions
                      );
                    }
                  },
                  (error) => {
                    this.infoDialogService.openInfoDialog(error);
                  }
                );
              this.getUserDepartments = this.userService
                .getUserDepartments(this.currentUser.id)
                .pipe(first())
                .subscribe(
                  (responseUserDepartment) => {
                    let userDepartment;
                    if(responseUserDepartment.data != null) {
                      userDepartment = responseUserDepartment.data.department_user_role;
                    }
                    localStorage.setItem(
                        "userDepartment",
                        JSON.stringify(userDepartment)
                      );
                      this.authenticationService.userDepartmentSubject.next(
                        responseUserDepartment.data.department_user_role
                      );
                  },
                  (error) => {
                    this.infoDialogService.openInfoDialog(error);
                  }
                );
            }
          }
        },
        (error) => {
          this.loading = false;
          this.infoDialogService.openInfoDialog(error);
        }
      );
    this.subManager.add(this.getUserRole);
    this.subManager.add(this.getUserMenus);
    this.subManager.add(this.getUserMenuActions);
    this.subManager.add(this.getUserDepartments);
    this.subManager.add(login);
  }

  forgot_password(): void {
    this.router.navigate(["forgot_password"]);
  }
}
