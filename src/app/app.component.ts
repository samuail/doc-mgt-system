import { Component, ViewChild } from "@angular/core";
import { Observable } from "rxjs";
import {TranslateService} from '@ngx-translate/core';

import { AuthService } from "./_core/auth/auth.service";
import { DataService } from "./_core/data.service";

import { AuthenticationService } from "./_services/authentication.service";
import { User } from "./_models/user";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"],
})
export class AppComponent {
  @ViewChild("sidenav", { static: true }) public sidenav;
  appName = "Correspondence Management System";
  isLoggedIn$: Observable<boolean>;
  currentUser: User;

  constructor(
    private translate: TranslateService,
    private authService: AuthService,
    public dataService: DataService,
    private authenticationService: AuthenticationService
  ) { 
    this.translate.setDefaultLang('am');
  }

  ngOnInit() {
    this.isLoggedIn$ = this.authService.isLoggedIn;
    this.authenticationService.currentUser.subscribe(
      (x) => (this.currentUser = x)
    );
  }
}
