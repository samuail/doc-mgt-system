import { Component, OnInit } from "@angular/core";

import { DataService } from "../../_core/data.service";

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.css"],
})
export class DashboardComponent implements OnInit {
  //@ViewChild('myCanvas') canvasRef: ElementRef;
  //@ViewChild('canvasEl') canvasEl: ElementRef;

  //private context: CanvasRenderingContext2D;

  constructor(public dataService: DataService) {}

  ngOnInit() {}

  get acctType(): string {
    return this.dataService.acctType;
  }

  // Users
  public usersChartLabels: string[] = ["Inactive", "Active"];
  public usersChartData: number[] = [2, 50];
  public usersChartType: string = "doughnut";
  public usersChartColors;

  public usersChartOptions: any = {
    responsive: true,
  };

  // events
  public usersChartClicked(e: any): void {
    // console.log(e);
  }

  public usersChartHovered(e: any): void {
    // console.log(e);
  }

  // Offices
  public officesChartLabels: string[] = [
    "President",
    "V/P",
    "Colleges",
    "Institutions",
    "Schools",
    "Departments",
    "Directors",
  ];
  public officesChartData: number[] = [1, 4, 5, 3, 2, 30, 20];
  public officesChartType: string = "doughnut";
  public officesChartColors: Array<any> = [
    {
      hoverBorderColor: ["rgba(0, 0, 0, 0.1)", "rgba(0, 0, 0, 0.1)"],
      hoverBorderWidth: 0,
      backgroundColor: ["#4D5360", "#46BFBD"],
      hoverBackgroundColor: ["#616774", "#5AD3D1"],
    },
  ];

  public officesChartOptions: any = {
    responsive: true,
  };

  // events
  public officesChartClicked(e: any): void {
    // console.log(e);
  }

  public officesChartHovered(e: any): void {
    // console.log(e);
  }

  // Documents
  public documentsChartLabels: string[] = ["Incoming", "Outcoming"];
  public documentsChartData: number[] = [150, 200];
  public documentsChartType: string = "doughnut";
  public documentsChartColors: Array<any> = [
    {
      hoverBorderColor: ["rgba(0, 0, 0, 0.1)", "rgba(0, 0, 0, 0.1)"],
      hoverBorderWidth: 0,
      backgroundColor: ["#4D5360", "#46BFBD"],
      hoverBackgroundColor: ["#616774", "#5AD3D1"],
    },
  ];

  public documentsChartOptions: any = {
    responsive: true,
  };

  // events
  public documentsChartClicked(e: any): void {
    // console.log(e);
  }

  public documentsChartHovered(e: any): void {
    // console.log(e);
  }

  //Document Origin
  public documentOriginsChartLabels: string[] = ["Internal", "External"];
  public documentOriginsChartData: number[] = [150, 200];
  public documentOriginsChartType: string = "doughnut";
  public documentOriginsChartColors: Array<any> = [
    {
      hoverBorderColor: ["rgba(0, 0, 0, 0.1)", "rgba(0, 0, 0, 0.1)"],
      hoverBorderWidth: 0,
      backgroundColor: ["#4D5360", "#46BFBD"],
      hoverBackgroundColor: ["#616774", "#5AD3D1"],
    },
  ];

  public documentOriginsChartOptions: any = {
    responsive: true,
  };

  // events
  public documentOriginsChartClicked(e: any): void {
    // console.log(e);
  }

  public documentOriginsChartHovered(e: any): void {
    // console.log(e);
  }
}
