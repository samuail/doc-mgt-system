import { Component, OnInit, OnDestroy, ViewChild } from "@angular/core";
import { first } from "rxjs/operators";
import { Subscription } from "rxjs";
import { MatPaginator } from "@angular/material/paginator";
import { MatSnackBar } from "@angular/material/snack-bar";
import { MatSort } from "@angular/material/sort";
import { MatTableDataSource } from "@angular/material/table";
import { Router } from "@angular/router";

import { Message } from "../_models/_helpers/message";

import { Role } from "../_models/role";
import { RoleService } from "../_services/role.service";
import { MenuService } from "src/app/_services/menu.service";

import { AuthenticationService } from "../_services/authentication.service";
import { InfoDialogService } from "../_services/info-dialog.service";

@Component({
  selector: "app-role",
  templateUrl: "./role.component.html",
  styleUrls: ["./role.component.css"],
})
export class RoleComponent implements OnInit, OnDestroy {
  subManager = new Subscription();

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatPaginator, { static: true }) set matPaginator(mp: MatPaginator) {
    this.paginator = mp;
    this.setDataSourceAttributes();
  }

  setDataSourceAttributes() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  @ViewChild(MatSort, { static: true }) sort: MatSort;

  title: string;
  message: Message = new Message();

  roles: Role[] = [];
  role: Role = new Role();

  dataSource = new MatTableDataSource<Role>();
  displayedColumns = ["roleName", "operations"];

  loading = false;
  resultsLength = 0;

  menuActions: string[] = [];

  canAdd: boolean = false;
  canEdit: boolean = false;
  canDelete: boolean = false;
  canListUser: boolean = false;

  constructor(
    public snackBar: MatSnackBar,
    private router: Router,
    private roleService: RoleService,
    private menuService: MenuService,
    private authenticationService: AuthenticationService,
    public infoDialogService: InfoDialogService
  ) {
    this.menuActions = this.menuService.getMenuActions("Roles");
    for (var action of this.menuActions) {
      if (action == "Add") {
        this.canAdd = true;
      } else if (action == "Edit") {
        this.canEdit = true;
      } else if (action == "Delete") {
        this.canDelete = true;
      } else if (action == "List User") {
        this.canListUser = true;
      }
    }
  }

  ngOnInit() {
    this.getAll();
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngOnDestroy() {
    this.subManager.unsubscribe();
  }

  getAll(): void {
    this.loading = true;
    let getAll = this.roleService
      .getAll()
      .pipe(first())
      .subscribe(
        (response) => {
          if (response.success) {
            for (var value of response.data) {
              this.role = {
                id: value["id"],
                name: value["name"],
              };
              this.roles.push(this.role);
            }
            this.loading = false;
            this.resultsLength = response.total;
            this.dataSource.data = this.roles;
          } else {
            this.message.title = "User Roles";
            this.message.type = "error";
            this.message.content = response.errors;
            const infoDialogRef = this.infoDialogService.openInfoDialog(
              this.message
            );
            infoDialogRef.afterClosed().subscribe(() => {
              if (response.errors[0] == "Signature has expired") {
                this.authenticationService.logout_offline();
                this.router.navigate(["/login"]);
              }
            });
          }
        },
        (error) => {
          this.loading = false;
          this.infoDialogService.openInfoDialog(error);
        }
      );
    this.subManager.add(getAll);
  }

  addRole() {
    this.title = "Add User Role";
    const dialogRef = this.roleService.openRoleDialog(
      "Add Role",
      new Role(),
      [new Role()],
      []
    );
    const sub = dialogRef.componentInstance.onRoleSave.subscribe((data) => {
      if (data) {
        data.loading = true;
        let create = this.roleService
          .create(data.roleForm.value.roleName)
          .pipe(first())
          .subscribe(
            (response) => {
              if (response.success) {
                this.message.type = "success";
                this.message.title = this.title;
                this.message.content = [response.message];
                const infoDialogRef = this.infoDialogService.openInfoDialog(
                  this.message
                );
                infoDialogRef.afterClosed().subscribe(() => {
                  this.role = {
                    id: response.data["id"],
                    name: response.data["name"],
                  };
                  this.roles.push(this.role);
                  this.resultsLength = this.resultsLength + 1;
                  this.dataSource = new MatTableDataSource(this.roles);
                  data.close();
                });
              } else {
                this.message.title = this.title;
                this.message.content = [];
                this.message.type = "error";
                for (var value of response.errors) {
                  this.message.content.push(value);
                }
                const infoDialogRef = this.infoDialogService.openInfoDialog(
                  this.message
                );
                infoDialogRef.afterClosed().subscribe(() => {
                  data.loading = false;
                });
              }
            },
            (error) => {
              data.loading = false;
              this.infoDialogService.openInfoDialog(error);
            }
          );
        this.subManager.add(create);
      }
    });

    dialogRef.afterClosed().subscribe(() => {
      sub.unsubscribe();
    });
  }

  editRole(role) {
    this.title = "Edit User Role";
    const dialogRef = this.roleService.openRoleDialog(
      "Edit Role",
      [role],
      [],
      []
    );
    const sub = dialogRef.componentInstance.onRoleSave.subscribe((data) => {
      if (data) {
        data.loading = true;
        let update = this.roleService
          .update(role.id, data.roleForm.value.roleName)
          .pipe(first())
          .subscribe(
            (response) => {
              if (response.success) {
                this.message.type = "success";
                this.message.title = this.title;
                this.message.content = [response.message];
                const infoDialogRef = this.infoDialogService.openInfoDialog(
                  this.message
                );
                infoDialogRef.afterClosed().subscribe(() => {
                  this.roles = this.roles.filter((item) => item.id !== role.id);
                  this.role = {
                    id: response.data["id"],
                    name: response.data["name"],
                  };
                  this.roles.push(this.role);
                  this.dataSource = new MatTableDataSource(this.roles);
                  data.close();
                });
              } else {
                this.message.title = this.title;
                this.message.content = [];
                this.message.type = "error";
                for (var value of response.errors) {
                  this.message.content.push(value);
                  // this.message.content += "_";
                }
                const infoDialogRef = this.infoDialogService.openInfoDialog(
                  this.message
                );
                infoDialogRef.afterClosed().subscribe(() => {
                  data.loading = false;
                });
              }
            },
            (error) => {
              data.loading = false;
              // this.type = "error";
              // if (error.title === ""){
              //   error.title = "Edit User Role Error";
              // }
              this.infoDialogService.openInfoDialog(error);
            }
          );
        this.subManager.add(update);
      }
    });

    dialogRef.afterClosed().subscribe(() => {
      sub.unsubscribe();
    });
  }

  deleteRole(role) {
    let snackBarRef = this.snackBar.open(`Deleting role #${role}`);
  }

  listUsers(role) {
    let getRoleUsers = this.roleService
      .getRoleUsers(role.id)
      .pipe(first())
      .subscribe(
        (response) => {
          if (response.success) {
            const dialogRef = this.roleService.openRoleDialog(
              "List of Users",
              role,
              [],
              response.data
            );
          }
        },
        (error) => {
          // this.type = "error";
          // if (error.title === ""){
          //   error.title = "List Role Users Error";
          // }
          this.infoDialogService.openInfoDialog(error);
        }
      );
    this.subManager.add(getRoleUsers);
  }
}
