import { Component, OnInit, Inject, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

import { Role } from '../../_models/role';

import { User } from '../../_models/user';


@Component({
  selector: 'app-role-dialog',
  templateUrl: './role-dialog.component.html',
  styleUrls: ['./role-dialog.component.css']
})
export class RoleDialogComponent implements OnInit {
	
	roleForm: FormGroup;
  
  loading: boolean = false;
  title: string;
  
  roles: Role [] = [];
  role: Role [] = [];
  users: User[] = [];

	constructor(@Inject(MAT_DIALOG_DATA) public data: any, 
  			  private fb: FormBuilder,
  			  private dialogRef: MatDialogRef<RoleDialogComponent>) {
    this.title = data.title;
 		this.role = data.role;
    this.roles = data.roles;
    this.users = data.users;
  }

	ngOnInit() {   
		if(this.title == 'Add Role') {
  		this.roleForm = this.fb.group({
  			roleName: ['', Validators.required]
  		});    
  	} else if(this.title == 'Edit Role') {
      this.roleForm = this.fb.group({
        roleName: [this.role[0].name, Validators.required]
      });    
    } else if(this.title == 'Assign Roles') {
      const defaultValue = this.role.length !== 0 ? this.role[0].id : null;

      this.roleForm = this.fb.group({
        allRoles: [defaultValue, Validators.required]
      });
  	} else {
      this.roleForm = this.fb.group({
      });
    }
	}

  onRoleSave = new EventEmitter();

	save() {   
  	if (this.roleForm.invalid) {
          return;
    }
    this.onRoleSave.emit(this);
	}

	close() {
	  this.dialogRef.close(false);
	}

}
