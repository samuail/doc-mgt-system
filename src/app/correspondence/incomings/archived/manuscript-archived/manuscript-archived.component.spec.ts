import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManuscriptArchivedComponent } from './manuscript-archived.component';

describe('ManuscriptArchivedComponent', () => {
  let component: ManuscriptArchivedComponent;
  let fixture: ComponentFixture<ManuscriptArchivedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManuscriptArchivedComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManuscriptArchivedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
