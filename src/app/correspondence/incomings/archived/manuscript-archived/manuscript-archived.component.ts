import { Component, OnInit, ViewChild, OnDestroy, ElementRef } from "@angular/core";
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from "@angular/animations";
import { first } from "rxjs/operators";
import { debounceTime, distinctUntilChanged, tap, filter } from 'rxjs/operators';
import { Subscription } from "rxjs";
import { fromEvent } from "rxjs";
import { MatPaginator } from "@angular/material/paginator";
import { Router } from "@angular/router";

import { ManuscriptService } from "src/app/_services/manuscript.service";
import { AuthenticationService } from "src/app/_services/authentication.service";
import { InfoDialogService } from "src/app/_services/info-dialog.service";
import { ConfirmationDialogService } from "src/app/_services/confirmation-dialog.service";
import { RoleService } from "src/app/_services/role.service";
import { MenuService } from "src/app/_services/menu.service";
import { DepartmentService } from "src/app/_services/department.service";
import { UserService } from "src/app/_services/user.service";
import { ManuscriptRoutingService } from "src/app/_services/manuscript-routing.service";

import { Message } from "src/app/_models/_helpers/message";
import { DepartmentItem } from "src/app/_models/department-item";
import { DepartmentRole } from "src/app/_models/department-role";
import { Manuscript } from "src/app/_models/manuscript";
import { ManuscriptAssignment } from "src/app/_models/manuscript-assignment";

import { ManuscriptDataSource } from "src/app/_datasource/manuscript.datasource";


@Component({
  selector: 'app-manuscript-archived',
  templateUrl: './manuscript-archived.component.html',
  styleUrls: ['./manuscript-archived.component.css'],
  animations: [
    trigger("detailExpand", [
      state(
        "collapsed",
        style({ height: "0px", minHeight: "0", display: "none" })
      ),
      state("expanded", style({ height: "*" })),
      transition(
        "expanded <=> collapsed",
        animate("225ms cubic-bezier(0.4, 0.0, 0.2, 1)")
      ),
    ]),
  ]
})
export class ManuscriptArchivedComponent implements OnInit, OnDestroy {
  subManager = new Subscription();

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('input') input: ElementRef;

  title: string;
  type: string = "Center";
  message: Message = new Message();

  manuscriptAssignment: ManuscriptAssignment = new ManuscriptAssignment();
  manuscriptAssignments: ManuscriptAssignment[] = [];

  archivedDataSource: ManuscriptDataSource;

  displayedColumns = [
    "referenceNo",
    "receivedDate",
    "title",
    "operations",
  ];

  expandedArchivedManuscript: Manuscript | null;

  loading = false;
  archivedResultsLength = 0;
  userDepartment: DepartmentRole;

  menuActions: string[] = [];

  loadingDeptTree = false;
  departmentItems: DepartmentItem[] = [];

  loadingAssignment = false;
  
  constructor(private router: Router,
    private manuscriptService: ManuscriptService,
    private authenticationService: AuthenticationService,    
    private menuService: MenuService,
    public infoDialogService: InfoDialogService,
    public confirmationDialogService: ConfirmationDialogService,
    private departmentService: DepartmentService,
    private userService: UserService,
    private manuscriptRoutingService: ManuscriptRoutingService
  ) {
    this.userDepartment = this.userService.getCurrentUserDepartments();
    this.menuActions = this.menuService.getMenuActions("Manuscripts");
   }

  ngOnInit(): void {
    this.archivedDataSource = new ManuscriptDataSource(this.manuscriptService, this.router, this.infoDialogService, this.authenticationService);
    this.archivedDataSource.loadManuscript('archived', '', 'dsc', 0, 10);
    this.initDepartmentTree()
  }

  ngAfterViewInit() {
    this.paginator.page
        .pipe(
            tap(() => this.loadArchivedManuscriptPage())
        )
        .subscribe();
    
    fromEvent(this.input.nativeElement,'keyup')
        .pipe(
            debounceTime(150),
            filter((e: KeyboardEvent) => e.key === "Enter"),
            distinctUntilChanged(),
            tap(() => {
                this.paginator.pageIndex = 0;

                this.loadArchivedManuscriptPage();
            })
        )
        .subscribe();
  }

  loadArchivedManuscriptPage() {
    this.archivedDataSource.loadManuscript(
                      'archived',
                      this.input.nativeElement.value, 
                      'asc', 
                      this.paginator.pageIndex, 
                      this.paginator.pageSize);
  }

  ngOnDestroy() {
    this.subManager.unsubscribe();
  }

  initDepartmentTree() {
    this.loadingDeptTree = true;
    let getAllforTree = this.departmentService
      .getAllForTree()
      .pipe(first())
      .subscribe(
        (response) => {
          if (response.success) {    
            this.loadingDeptTree = false;
            this.departmentItems = response.data;            
          } else {
            this.message.title = "Get Department Tree";
            this.message.type = "error";
            this.message.content = response.errors;
            this.errorDialog(this.message, response);
          }
        },
        (error) => {
          this.errorDialog(error, null);
        }
      );
    this.subManager.add(getAllforTree);
  }

  returnDepartmentTree(dept_id){  
    var departments = this.departmentItems;
    for (var college of departments) {
      for (var department of college["children"]) {
        for (var office of department["children"]) {
          if (office.id == dept_id) {
            return college["code"] + " / " + department["code"] + " / " + office["description"];
          }
        }
      }
    }
  }

  ArchiveAttachment(manuscript) {
    const dialogRef = this.manuscriptService.openDocArchiverDialog(      
      "Attach Manuscript Archive",
      manuscript
    );
  }

  previewArchiveAttachment(manuscript) {
    const dialogRef = this.manuscriptService.openManuscriptPreviewerDialog(
      this.menuActions.filter((s) => s.includes("Preview")),
      "Manuscript Archive Preview",
      manuscript
    );
  }

  previewManuscript(manuscript) {
    const dialogRef = this.manuscriptService.openManuscriptPreviewerDialog(
      this.menuActions.filter((s) => s.includes("Preview")),
      "Manuscript Preview",
      manuscript
    );
  }

  previewManuscriptAssignmentHistory(manuscript) {
    this.manuscriptAssignments = [];
    this.loadingAssignment = true;
    let getRoutingDetails = this.manuscriptRoutingService
      .getAll(manuscript)
      .pipe(first())
      .subscribe(
        (response) => {
          if (response.success) {
            for (var value of response.data) {              
              this.manuscriptAssignments.push(
                this.manuscriptRoutingService.assignManuscriptAssignmentValue(value)
              );
            }            
            this.loadingAssignment = false;
            
            const dialogRef = this.manuscriptRoutingService.openAssignmentPreviewDialog(
              "Preview Manuscript Assignment",
              this.manuscriptAssignments
            );
          } else {
            this.message.title = "Preview Assignments";
            this.message.type = "error";
            this.message.content = response.errors;
            this.errorDialog(this.message, response);
          }
        },
        (error) => {
          this.errorDialog(error, null);
        }
      );
    this.subManager.add(getRoutingDetails);
  }

  errorDialogOpened: boolean = false;

  errorDialog(message, response) {
    let infoDialogRef = null;
    if (!this.errorDialogOpened) {
      if (response != null) {
        if (response.errors[0] == "Signature has expired") {
          this.errorDialogOpened = true;
          infoDialogRef = this.infoDialogService.openInfoDialog(
            message
          );
        } else {
          infoDialogRef = this.infoDialogService.openInfoDialog(
            message
          );
        }
      } else {
        infoDialogRef = this.infoDialogService.openInfoDialog(
          message
        );
      }
      infoDialogRef.afterClosed().subscribe(() => {
        if (response != null) {
          if (response.errors[0] == "Signature has expired") {
            this.authenticationService.logout_offline();
            this.router.navigate(["/login"]);
          }
        }
      });
    }     
  }

}
