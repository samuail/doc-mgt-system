import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InternalArchivedComponent } from './internal-archived.component';

describe('InternalArchivedComponent', () => {
  let component: InternalArchivedComponent;
  let fixture: ComponentFixture<InternalArchivedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InternalArchivedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InternalArchivedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
