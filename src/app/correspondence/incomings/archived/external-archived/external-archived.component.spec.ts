import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExternalArchivedComponent } from './external-archived.component';

describe('ExternalArchivedComponent', () => {
  let component: ExternalArchivedComponent;
  let fixture: ComponentFixture<ExternalArchivedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExternalArchivedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExternalArchivedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
