import { Component, OnInit, ViewChild, OnDestroy, ElementRef } from "@angular/core";
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from "@angular/animations";
import { first } from "rxjs/operators";
import { debounceTime, distinctUntilChanged, tap, filter } from 'rxjs/operators';
import { Subscription } from "rxjs";
import { fromEvent } from "rxjs";
import { MatPaginator } from "@angular/material/paginator";
import { Router } from "@angular/router";

import { CorrespondenceService } from "src/app/_services/correspondence.service";
import { AuthenticationService } from "src/app/_services/authentication.service";
import { InfoDialogService } from "src/app/_services/info-dialog.service";
import { ConfirmationDialogService } from "src/app/_services/confirmation-dialog.service";
import { RoleService } from "src/app/_services/role.service";
import { MenuService } from "src/app/_services/menu.service";
import { DepartmentService } from "src/app/_services/department.service";
import { DateFormatterService } from "src/app/_services/date-formatter.service";
import { UserService } from "src/app/_services/user.service";
import { CorrespondenceAssignmentService } from "src/app/_services/correspondence-assignment.service";

import { Message } from "src/app/_models/_helpers/message";
import { ExternalIncomingCorrespondence } from "src/app/_models/external-incoming-correspondence";
import { DepartmentItem } from "src/app/_models/department-item";
import { CorrespondenceCarbonCopy } from "src/app/_models/correspondence-carbon-copy";
import { DepartmentRole } from "src/app/_models/department-role";
import { CorrespondenceAssignment } from "src/app/_models/correspondence-assignment";
import { ExternalIncomingsDataSource } from "src/app/_datasource/external-incomings.datasource";

@Component({
  selector: 'app-external-archived',
  templateUrl: './external-archived.component.html',
  styleUrls: ['./external-archived.component.css'],
  animations: [
    trigger("detailExpand", [
      state(
        "collapsed",
        style({ height: "0px", minHeight: "0", display: "none" })
      ),
      state("expanded", style({ height: "*" })),
      transition(
        "expanded <=> collapsed",
        animate("225ms cubic-bezier(0.4, 0.0, 0.2, 1)")
      ),
    ]),
  ]
})
export class ExternalArchivedComponent implements OnInit, OnDestroy {
  subManager = new Subscription();

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('input') input: ElementRef;

  title: string;
  type: string = "Center";
  message: Message = new Message();

  externalCorrespondence: ExternalIncomingCorrespondence = new ExternalIncomingCorrespondence();
  externalCorrespondences: ExternalIncomingCorrespondence[] = [];

  correspondenceCarbonCopy: CorrespondenceCarbonCopy = new CorrespondenceCarbonCopy();
  correspondenceCarbonCopies: CorrespondenceCarbonCopy [] = [];

  correspondenceAssignments: CorrespondenceAssignment[] = [];
  correspondenceAssignment: CorrespondenceAssignment = new CorrespondenceAssignment();

  archivedDataSource: ExternalIncomingsDataSource;

  displayedColumns = [
    "referenceNo",
    "docReferenceNo",
    "letterDate",
    "source",
    "subject",    
    "operations",
  ];

  expandedArchivedCorrespondence: ExternalIncomingCorrespondence | null;

  loading = false;
  archivedResultsLength = 0;
  userDepartment: DepartmentRole;

  menuActions: string[] = [];

  loadingDeptTree = false;
  departmentItems: DepartmentItem[] = [];

  loadingAssignment = false;

  constructor(
    private router: Router,
    private correspondenceService: CorrespondenceService,
    private authenticationService: AuthenticationService,
    private roleService: RoleService,
    private menuService: MenuService,
    public infoDialogService: InfoDialogService,
    public confirmationDialogService: ConfirmationDialogService,
    private departmentService: DepartmentService,
    private dateFormatterService: DateFormatterService,
    private userService: UserService,
    private correspondenceAssignmentService: CorrespondenceAssignmentService
  ) {
    this.userDepartment = this.userService.getCurrentUserDepartments();
    this.menuActions = this.menuService.getMenuActions("Outside AAU");
   }

  ngOnInit() {
    this.archivedDataSource = new ExternalIncomingsDataSource(this.correspondenceService, this.router, this.infoDialogService, this.authenticationService);    
    this.archivedDataSource.loadExternalIncomings('archived', '', '', 'dsc', 0, 10);
    this.initDepartmentTree();
  }

  ngAfterViewInit() {
    this.paginator.page
        .pipe(
            tap(() => this.loadExternalArchivedPage())
        )
        .subscribe();
    
    fromEvent(this.input.nativeElement,'keyup')
        .pipe(
            debounceTime(150),
            filter((e: KeyboardEvent) => e.key === "Enter"),
            distinctUntilChanged(),
            tap(() => {
                this.paginator.pageIndex = 0;

                this.loadExternalArchivedPage();
            })
        )
        .subscribe();
  }

  loadExternalArchivedPage() {
    this.archivedDataSource.loadExternalIncomings(
                      'archived',                    
                      '', 
                      this.input.nativeElement.value, 
                      'asc', 
                      this.paginator.pageIndex, 
                      this.paginator.pageSize);
  }

  ngOnDestroy() {
    this.subManager.unsubscribe();
  }

  initDepartmentTree() {
    this.loadingDeptTree = true;
    let getAllforTree = this.departmentService
      .getAllForTree()
      .pipe(first())
      .subscribe(
        (response) => {
          if (response.success) {    
            this.loadingDeptTree = false;
            this.departmentItems = response.data;            
          } else {
            this.message.title = "Get Department Tree";
            this.message.type = "error";
            this.message.content = response.errors;
            const infoDialogRef = this.infoDialogService.openInfoDialog(
              this.message
            );
            infoDialogRef.afterClosed().subscribe(() => {
              if (response.errors[0] == "Signature has expired") {
                this.authenticationService.logout_offline();
                this.router.navigate(["/login"]);
              }
            });
          }
        },
        (error) => {
          this.infoDialogService.openInfoDialog(error);
        }
      );
    this.subManager.add(getAllforTree);
  }

  returnDepartmentTree(dept_id){  
    var departments = this.departmentItems;
    for (var college of departments) {
      for (var department of college["children"]) {
        for (var office of department["children"]) {
          if (office.id == dept_id) {
            return college["code"] + " / " + department["code"] + " / " + office["description"];
          }
        }
      }
    }
  }

  previewCorrespondence(correspondence) {
    const dialogRef = this.correspondenceService.openDocPreviewerDialog(
      this.menuActions.filter((s) => s.includes("Preview")),
      this.type,
      "Correspondence Preview",
      correspondence
    );
  }

  assignCorrespondenceAssignmentValue (value) {
    let correspondenceAssignment: CorrespondenceAssignment = new CorrespondenceAssignment();
    correspondenceAssignment = {
          id: value["id"],
          fromUser: value["from_user"],
          toUser: value["to_user"],
          correspondence: value["correspondence"],
          order: value["order"],
          assignedDate: value["assigned_date"],
          receivedDate: value["received_date"],
          status: value["status"],
          rejectionRemark: value["rejection_remark"]
        };
    return correspondenceAssignment;
  }

  previewCorrespondenceAssignmentHistory(correspondence) {
    this.loadingAssignment = true;
    this.correspondenceAssignments = [];
    let getRoutingDetails = this.correspondenceAssignmentService
      .getAll(correspondence.id, "internal")
      .pipe(first())
      .subscribe(
        (response) => {
          if (response.success) {
            for (var value of response.data) {
              this.correspondenceAssignment = this.assignCorrespondenceAssignmentValue(value);
              this.correspondenceAssignments.push(
                this.correspondenceAssignment
              );              
            }
            this.loadingAssignment = false;

            this.title = "Preview Assignment";
            const dialogRef = this.correspondenceAssignmentService.openAssignmentPreviewDialog(
              this.title,
              this.correspondenceAssignments
            );

          } else {
            this.message.title = "Correspondence Assignments";
            this.message.type = "error";
            this.message.content = response.errors;
            const infoDialogRef = this.infoDialogService.openInfoDialog(
              this.message
            );
            infoDialogRef.afterClosed().subscribe(() => {
              if (response.errors[0] == "Signature has expired") {                
                this.authenticationService.logout_offline();
                this.router.navigate(["/login"]);
              }
            });            
          }
        },
        (error) => {
          this.loadingAssignment = false;
          this.infoDialogService.openInfoDialog(error);
        }
      );
    this.subManager.add(getRoutingDetails);    
  }
}
