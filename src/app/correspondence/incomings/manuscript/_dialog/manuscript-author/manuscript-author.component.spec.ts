import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManuscriptAuthorComponent } from './manuscript-author.component';

describe('ManuscriptAuthorComponent', () => {
  let component: ManuscriptAuthorComponent;
  let fixture: ComponentFixture<ManuscriptAuthorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManuscriptAuthorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManuscriptAuthorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
