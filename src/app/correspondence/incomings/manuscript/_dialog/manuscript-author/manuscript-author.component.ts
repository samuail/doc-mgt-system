import { Component, OnInit, Inject, OnDestroy, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA } from "@angular/material/dialog";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { MatTable, MatTableDataSource } from "@angular/material/table";
import { first } from "rxjs/operators";
import { Subscription } from "rxjs";
import { Router } from "@angular/router";

import { ManuscriptAuthorService } from "src/app/_services/manuscript-author.service";
import { AuthorService } from "src/app/_services/author.service";
import { AuthenticationService } from "src/app/_services/authentication.service";
import { InfoDialogService } from "src/app/_services/info-dialog.service";

import { Message } from "src/app/_models/_helpers/message";
import { Author } from "src/app/_models/author";
import { ManuscriptAuthor } from "src/app/_models/manuscript-author";

@Component({
  selector: 'app-manuscript-author',
  templateUrl: './manuscript-author.component.html',
  styleUrls: ['./manuscript-author.component.css']
})
export class ManuscriptAuthorComponent implements OnInit, OnDestroy {
  subManager = new Subscription();

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatPaginator) set matPaginator(mp: MatPaginator) {
    this.paginator = mp;
    this.setDataSourceAttributes();
  }

  setDataSourceAttributes() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatTable) table: MatTable<any>;

  loading: boolean = false;
  title: string;
  message: Message = new Message();
  authorActions: string [] = [];
  manuscript: any;

  author: Author = new Author();
  authors: Author[] = [];

  manuscriptAuthor: ManuscriptAuthor = new ManuscriptAuthor();
  manuscriptAuthors: ManuscriptAuthor[] = [];

  resultsLength = 0;
  dataSource = new MatTableDataSource<ManuscriptAuthor>();

  displayedColumns = ["fullName", "email", "telephone", "operations"];

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
          private router: Router,
          private manuscriptAuthorService: ManuscriptAuthorService,
          private authorService: AuthorService,
          private authenticationService: AuthenticationService,
          public infoDialogService: InfoDialogService) { 
    this.title = data.title;
    this.authorActions = data.authorActions;
    this.manuscript = data.manuscript;
  }

  ngOnInit() {
    this.getAllAuthors();
    this.getAll();
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngOnDestroy() {
    this.subManager.unsubscribe();
  }

  assignManuscriptAuthor(value) {
    let manuscriptAuthor: ManuscriptAuthor = new ManuscriptAuthor();
    manuscriptAuthor = {
      id: value["id"],
      manuscript: value["manuscript"],
      author: this.authorService.assignAuthorValue(value["author"])
    }
    return manuscriptAuthor;
  }

  getAll() {
    this.loading = true;
    let getAll = this.manuscriptAuthorService
      .getAll(this.manuscript)
      .pipe(first())
      .subscribe(
        (response) => {
          if(response.success) {
            for(var value of response.data) {
              this.manuscriptAuthor = {
                id: value["id"],
                manuscript: value["manuscript"],
                author: this.authorService.assignAuthorValue(value["author"])
              }
              this.manuscriptAuthors.push(this.manuscriptAuthor);
            }
            this.manuscriptAuthors.sort((a, b) => {
              return a.author.firstName.localeCompare(b.author.firstName);
            });
            this.resultsLength = response.total;
            this.dataSource.data = this.manuscriptAuthors;
            this.loading = false;
          } else {
            this.message.title = "Manuscript Authors";
            this.message.type = "error";
            this.message.content = response.errors;
            const infoDialogRef = this.infoDialogService.openInfoDialog(
              this.message
            );
            infoDialogRef.afterClosed().subscribe(() => {
              if (response.errors[0] == "Signature has expired") {
                this.data.close();
                this.authenticationService.logout_offline();
                this.router.navigate(["/login"]);
              }
            });
          }
        },
        (error) => {
          this.loading = false;
          this.infoDialogService.openInfoDialog(error);
        }
      );
    this.subManager.add(getAll);
  }

  getAllAuthors() {
    let getAllAuthors = this.authorService
      .getAll()
      .pipe(first())
      .subscribe(
        (response) => {
          if (response.success) {
            for (var value of response.data) {             
              this.author = this.authorService.assignAuthorValue(value);
              this.authors.push(this.author);              
            }      
            this.authors.sort((a, b) => {
                return a.firstName.localeCompare(b.firstName);
              });   
          } else {
            this.message.title = "Author";
            this.message.type = "error";
            this.message.content = response.errors;
            const infoDialogRef = this.infoDialogService.openInfoDialog(
              this.message
            );
            infoDialogRef.afterClosed().subscribe(() => {
              if (response.errors[0] == "Signature has expired") {
                this.authenticationService.logout_offline();
                this.router.navigate(["/login"]);
              }
            });
          }
        },
        (error) => {
          this.infoDialogService.openInfoDialog(error);
        }
      );
    this.subManager.add(getAllAuthors);
  }

  addAuthor() {
    this.title = "Add Author";
    const dialogRef = this.manuscriptAuthorService.openAuthorDialog(
      this.title,
      this.authors,
      this.manuscriptAuthors
    );
    
    const sub = dialogRef.componentInstance.onManuscriptAuthorSave.subscribe(
      (data) => {
        if (data) {
          data.loading = true;
          let create = this.manuscriptAuthorService
            .create(data, this.manuscript)
            .pipe(first())
            .subscribe(
              (response) => {
                if (response.success) {
                  this.message.title = this.title;
                  this.message.content = [response.message];
                  this.message.type = "success";
                  const infoDialogRef = this.infoDialogService.openInfoDialog(
                    this.message
                  );
                  infoDialogRef.afterClosed().subscribe(() => {    
                    this.manuscriptAuthor = this.assignManuscriptAuthor(response.data);
                    this.manuscriptAuthors.splice(0, 0, this.manuscriptAuthor);
                    this.resultsLength += 1;
                    this.dataSource.data = this.manuscriptAuthors;
                    this.dataSource.paginator = this.paginator;
                    this.dataSource.sort = this.sort;
                    data.close();
                  });
                } else {
                  this.message.title = this.title;
                  this.message.type = "error";
                  this.message.content = response.errors;
                  const infoDialogRef = this.infoDialogService.openInfoDialog(
                    this.message
                  );
                  infoDialogRef.afterClosed().subscribe(() => {
                    data.loading = false;
                    if (response.errors[0] == "Signature has expired") {
                      this.authenticationService.logout_offline();
                      this.router.navigate(["/login"]);
                    }
                  });
                }
              },
              (error) => {
                data.loading = false;
                this.infoDialogService.openInfoDialog(error);
              }
            );
          this.subManager.add(create);
        }
      }
    );
    dialogRef.afterClosed().subscribe(() => {
      sub.unsubscribe();
    });
  }

  editAuthor(manuscript_author) {
    this.title = "Edit Author";
    const dialogRef = this.manuscriptAuthorService.openAuthorDialog(
      this.title,
      this.authors,
      this.manuscriptAuthors,
      manuscript_author
    );

    const sub = dialogRef.componentInstance.onManuscriptAuthorSave.subscribe(
      (data) => {
        if (data) {
          data.loading = true;
          let update = this.manuscriptAuthorService
            .update(data, manuscript_author)
            .pipe(first())
            .subscribe(
              (response) => {
                if (response.success) {
                  this.message.title = this.title;
                  this.message.content = [response.message];
                  this.message.type = "success";
                  const infoDialogRef = this.infoDialogService.openInfoDialog(
                    this.message
                  );
                  infoDialogRef.afterClosed().subscribe(() => {    
                    this.manuscriptAuthor = this.assignManuscriptAuthor(response.data);
                    let index = this.manuscriptAuthors.findIndex(x => x.id === manuscript_author.id);
                    this.manuscriptAuthors.splice(index, 1, this.manuscriptAuthor);
                    this.dataSource = new MatTableDataSource(
                      this.manuscriptAuthors
                    );
                    this.dataSource.paginator = this.paginator;
                    this.dataSource.sort = this.sort;
                    data.close();
                  });
                } else {
                  this.message.title = this.title;
                  this.message.type = "error";
                  this.message.content = response.errors;
                  const infoDialogRef = this.infoDialogService.openInfoDialog(
                    this.message
                  );
                  infoDialogRef.afterClosed().subscribe(() => {
                    data.loading = false;
                    if (response.errors[0] == "Signature has expired") {
                      this.authenticationService.logout_offline();
                      this.router.navigate(["/login"]);
                    }
                  });
                }
              },
              (error) => {
                data.loading = false;
                this.infoDialogService.openInfoDialog(error);
              }
            );
          this.subManager.add(update);
        }
      }
    );
    dialogRef.afterClosed().subscribe(() => {
      sub.unsubscribe();
    });
  }

  previewAuthor(manuscript_author) {
    this.title = "Preview Author";
    const dialogRef = this.manuscriptAuthorService.openAuthorDialog(
      this.title,
      null,
      null,
      manuscript_author
    );
  }

  deleteAuthor(manuscript_author) {}
}
