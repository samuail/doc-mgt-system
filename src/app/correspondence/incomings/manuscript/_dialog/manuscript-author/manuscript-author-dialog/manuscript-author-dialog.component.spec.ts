import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManuscriptAuthorDialogComponent } from './manuscript-author-dialog.component';

describe('ManuscriptAuthorDialogComponent', () => {
  let component: ManuscriptAuthorDialogComponent;
  let fixture: ComponentFixture<ManuscriptAuthorDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManuscriptAuthorDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManuscriptAuthorDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
