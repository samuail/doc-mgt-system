import { Component, OnInit, Inject, EventEmitter, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
import { Subscription } from "rxjs";
import { first } from "rxjs/operators";
import { Router } from "@angular/router";

import { AuthorService } from "src/app/_services/author.service";
import { AuthenticationService } from "src/app/_services/authentication.service";
import { InfoDialogService } from "src/app/_services/info-dialog.service";

import { Author } from "src/app/_models/author";
import { Message } from "src/app/_models/_helpers/message";
import { ManuscriptAuthor } from "src/app/_models/manuscript-author";

@Component({
  selector: 'app-manuscript-author-dialog',
  templateUrl: './manuscript-author-dialog.component.html',
  styleUrls: ['./manuscript-author-dialog.component.css']
})
export class ManuscriptAuthorDialogComponent implements OnInit, OnDestroy {
  subManager = new Subscription();
  loading = false;
  message: Message = new Message();
  authorForm: FormGroup;
  title: string;

  author: Author = new Author();
  authors: Author[] = [];
  manuscriptAuthors: ManuscriptAuthor[] = [];
  anotherAuthor: Author = new Author();
  
  constructor(@Inject(MAT_DIALOG_DATA) public data: any, 
  			  private fb: FormBuilder,
          public authorService: AuthorService,
          private router: Router,
          private authenticationService: AuthenticationService,
          public infoDialogService: InfoDialogService,
  			  private dialogRef: MatDialogRef<ManuscriptAuthorDialogComponent>) { 
    this.title = data.title;
    this.authors = data.authors;
    this.manuscriptAuthors = data.manuscript_authors;
    if(this.title == 'Add Author' || this.title == 'Edit Author'){
      this.authors = this.authors.filter((a) => {
        return this.manuscriptAuthors.filter((m_a) => {
          return m_a.author.id == a.id;
        }).length == 0
      });
    }
    if(this.title == 'Edit Author' || this.title == 'Preview Author'){
      this.anotherAuthor = data.manuscript_author.author;
    }    
  }

  ngOnInit() {
    if(this.title == 'Add Author') {
      this.authorForm = this.fb.group({
        authors: ['', Validators.required]
      });
    } else if(this.title == 'Edit Author') {
      let fullName = this.anotherAuthor.firstName + " " + this.anotherAuthor.fatherName;
      this.authorForm = this.fb.group({
        author: [fullName, null],
        authors: [this.anotherAuthor.id, Validators.required]
      });
    } else {
      this.authorForm = this.fb.group({});
    }
  }

  ngOnDestroy() {
    this.subManager.unsubscribe();
  }

  addNewAuthor() {
    const dialogRef = this.authorService.openAuthorDialog(
      "Add Author"
    );

    const sub = dialogRef.componentInstance.onAuthorSave.subscribe(
      (data) => {
        if (data) {
          data.loading = true;
          let create = this.authorService
            .create(data)
            .pipe(first())
            .subscribe(
              (response) => {
                if (response.success) {
                  this.message.title = this.title;
                  this.message.content = [response.message];
                  this.message.type = "success";
                  const infoDialogRef = this.infoDialogService.openInfoDialog(
                    this.message
                  );
                  infoDialogRef.afterClosed().subscribe(() => {                    
                    this.author = this.authorService.assignAuthorValue(response.data);
                    this.authors.splice(0, 0, this.author);              
                    data.close();
                  });
                } else {
                  this.message.title = this.title;
                  this.message.type = "error";
                  this.message.content = response.errors;
                  const infoDialogRef = this.infoDialogService.openInfoDialog(
                    this.message
                  );
                  infoDialogRef.afterClosed().subscribe(() => {
                    data.loading = false;
                    if (response.errors[0] == "Signature has expired") {
                      this.authenticationService.logout_offline();
                      this.router.navigate(["/login"]);
                    }
                  });
                }
              },
              (error) => {
                data.loading = false;
                this.infoDialogService.openInfoDialog(error);
              }
            );
          this.subManager.add(create);
        }
      }
    );
    dialogRef.afterClosed().subscribe(() => {
      sub.unsubscribe();
    });
  }

  onManuscriptAuthorSave = new EventEmitter();

  save() {
    if (this.authorForm.invalid) {
          return;
    }
    this.onManuscriptAuthorSave.emit(this);
  }

  close() {
	 this.dialogRef.close();
  }

}
