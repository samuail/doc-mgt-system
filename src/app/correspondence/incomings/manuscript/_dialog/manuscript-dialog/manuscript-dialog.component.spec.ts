import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManuscriptDialogComponent } from './manuscript-dialog.component';

describe('ManuscriptDialogComponent', () => {
  let component: ManuscriptDialogComponent;
  let fixture: ComponentFixture<ManuscriptDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManuscriptDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManuscriptDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
