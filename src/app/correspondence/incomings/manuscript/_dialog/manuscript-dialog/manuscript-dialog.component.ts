import { Component, OnInit, OnDestroy, Inject, EventEmitter , ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";

import { Message } from "src/app/_models/_helpers/message";
import { Author } from "src/app/_models/author";

import { InfoDialogService } from "src/app/_services/info-dialog.service";
import { Manuscript } from 'src/app/_models/manuscript';

declare var jQuery: any;

@Component({
  selector: 'app-manuscript-dialog',
  templateUrl: './manuscript-dialog.component.html',
  styleUrls: ['./manuscript-dialog.component.css']
})
export class ManuscriptDialogComponent implements OnInit, OnDestroy {
  
  loading = false;
  manuscriptForm: FormGroup;
  title: string;
  type: string;
  nextNumber: string;
  today: number = Date.now();
  message: Message = new Message();

  authors: Author[] = [];
  selectedAuthors: Number[] = [];
  manuscript: Manuscript;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<ManuscriptDialogComponent>,
    public infoDialogService: InfoDialogService) { 
      this.type = data.type;
      this.title = data.title; 
      this.authors = data.authors;
      this.nextNumber = data.nextNumber;        
      this.manuscript = data.manuscript;      
      if (data.nextNumber < 10) {
        this.nextNumber = '000' + data.nextNumber;
      } else if (data.nextNumber < 100) {
        this.nextNumber = '00' + data.nextNumber;
      } else if (data.nextNumber < 1000) {
        this.nextNumber = '0' + data.nextNumber;
      } else {
        this.nextNumber = data.nextNumber;
      }
    }

  ngOnInit() {      
    if (this.title == "Add Manuscript") {
      this.manuscriptForm = this.fb.group({          
        referenceNo: [this.nextNumber, Validators.required],
        title: ["", Validators.required],
        keyWord: ["", null]          
      });
    } else {
      var keyWords = this.manuscript.keyWord.toString().replace(/,(?=[^\s])/g, ", ");
      this.manuscriptForm = this.fb.group({          
        referenceNo: [this.manuscript.referenceNo, Validators.required],
        title: [this.manuscript.title, Validators.required],
        keyWord: [keyWords, null]          
      });
    }    
  }

  ngAfterViewInit() {   
  }

  ngOnDestroy() {
  }

  onManuscriptSave = new EventEmitter();

  save() {
    if (this.manuscriptForm.invalid) {
      return;
    }
    this.onManuscriptSave.emit(this);
  }

  close() {
    this.dialogRef.close();
  }

}
