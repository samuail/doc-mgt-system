import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManuscriptUploaderComponent } from './manuscript-uploader.component';

describe('ManuscriptUploaderComponent', () => {
  let component: ManuscriptUploaderComponent;
  let fixture: ComponentFixture<ManuscriptUploaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManuscriptUploaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManuscriptUploaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
