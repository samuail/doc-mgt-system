import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { MAT_DIALOG_DATA } from "@angular/material/dialog";
import { first } from "rxjs/operators";
import { Subscription } from "rxjs";
import { Router } from "@angular/router";
import { DomSanitizer } from '@angular/platform-browser';

import { environment } from 'src/environments/environment';

import { Manuscript } from 'src/app/_models/manuscript';
import { Message } from "src/app/_models/_helpers/message";

import { ManuscriptUploadsService } from 'src/app/_services/manuscript-uploads.service';
import { AuthenticationService } from "src/app/_services/authentication.service";
import { InfoDialogService } from "src/app/_services/info-dialog.service";

@Component({
  selector: 'app-manuscript-uploader',
  templateUrl: './manuscript-uploader.component.html',
  styleUrls: ['./manuscript-uploader.component.css']
})
export class ManuscriptUploaderComponent implements OnInit, OnDestroy {
  apiUrl: string = environment.apiURL;

  subManager = new Subscription();

  title: string;
  loadingUploads: boolean = false;
  manuscript: Manuscript;
  message: Message = new Message();

  uploadActions: string[] = [];

  canAddUpload: boolean = false;
  canEditUpload: boolean = false;
  canDeleteUpload: boolean = false;
  canPreviewUpload: boolean = false;

  documents: any[] = [];

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, 
        private router: Router,
        private manuscriptUploadsService: ManuscriptUploadsService,
        private authenticationService: AuthenticationService,
        public infoDialogService: InfoDialogService, 
        private _sanitizer: DomSanitizer) {
    this.title = data.title;
    this.uploadActions = data.uploadActions;
    this.manuscript = data.manuscript;

    for (var action of this.uploadActions) {
      if (action == "Upload Add") {
        this.canAddUpload = true;
      } else if (action == "Upload Edit") {
        this.canEditUpload = true;
      } else if (action == "Upload Delete") {
        this.canDeleteUpload = true;
      } else if (action == "Upload Preview") {
        this.canPreviewUpload = true;
      }
    }

    this.initUploadedDocument();
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.subManager.unsubscribe();
  }

  initUploadedDocument() {
    this.loadingUploads = true;
    let getUploadedImage = this.manuscriptUploadsService
      .getUploaded(this.manuscript, "Manuscript Preview")
      .pipe(first())
      .subscribe(
        (response) => {  
          if(response.success == undefined) {
            this.documents = response.documents;
            this.loadingUploads = false;
          } else {
            this.message.title = "Uploads";
            this.message.type = "error";
            this.message.content = response.errors;
            const infoDialogRef = this.infoDialogService.openInfoDialog(
              this.message
            );
            infoDialogRef.afterClosed().subscribe(() => {
              if (response.errors[0] == "Signature has expired") {
                this.data.close();
                this.authenticationService.logout_offline();
                this.router.navigate(["/login"]);
              }
            });
            this.loadingUploads = false;
          }
        },
        (error) => {
          this.infoDialogService.openInfoDialog(error);
        }
      );
    this.subManager.add(getUploadedImage);
  }

  uploadDocument() {
    let title = "Upload Manuscripts"
    const dialogRef = this.manuscriptUploadsService.openUploadsDialog(
      title, 
      this.manuscript
    );

    const sub = dialogRef.componentInstance.onDialogClose
          .subscribe((data) => {
            this.initUploadedDocument();
          });
  }

  previewUploadedDocument(document) {
    if (!this.canPreviewUpload) {
      return;
    }
    const dialogRef = this.manuscriptUploadsService.openDocPreviewerDialog(
      "Document Preview",
      document
    );
  }

  editUploadedDocument(image) {
    let title = "Edit Manuscripts"
    const dialogRef = this.manuscriptUploadsService.openUploadsDialog(
      title, 
      this.manuscript,
      image
    );

    const sub = dialogRef.componentInstance.onDialogClose
          .subscribe((data) => {
            this.initUploadedDocument();
          });
  }

  sanitize(url: string) {
    return this._sanitizer.bypassSecurityTrustUrl(this.apiUrl + url);
  }  
}
