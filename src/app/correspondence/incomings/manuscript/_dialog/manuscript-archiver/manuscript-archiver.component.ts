import { Component, OnInit, Inject, EventEmitter } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { catchError, map, first } from 'rxjs/operators';
import { Observable, of, Subscription } from 'rxjs';
import { DomSanitizer } from '@angular/platform-browser';
import { HttpEvent, HttpEventType, HttpErrorResponse } from '@angular/common/http';

import { environment } from 'src/environments/environment';

import { InfoDialogService } from "src/app/_services/info-dialog.service";
import { UploadService } from "src/app/_services/upload/upload.service";
import { ManuscriptUploadsService } from 'src/app/_services/manuscript-uploads.service';

@Component({
  selector: 'app-manuscript-archiver',
  templateUrl: './manuscript-archiver.component.html',
  styleUrls: ['./manuscript-archiver.component.css']
})
export class ManuscriptArchiverComponent implements OnInit {
  apiUrl: string = environment.apiURL;
  subManager = new Subscription();

  loading = false;
  title: string;

  manuscript: any;

  archiverForm: FormGroup;

  image: any;

  contentTypeError: boolean = false;
  sizeError: boolean = false;

  fileArr = [];
  imgArr = [];
  fileObj = [];

  checkedToUpload: boolean = false;

  manuscript_documents: any[] = [];
  public documents: string[] = [];

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
              private fb: FormBuilder,
              private sanitizer: DomSanitizer,
              private dialogRef: MatDialogRef<ManuscriptArchiverComponent>,
              public infoDialogService: InfoDialogService, 
              private uploadService: UploadService,
              public manuscriptUploadsService: ManuscriptUploadsService) {
    this.title = data.title;
    this.manuscript = data.manuscript;
  }

  ngOnInit(): void {
    if (this.title == 'Archive Manuscript') {
      this.archiverForm = this.fb.group({
        noPages: ["", Validators.required],
        noCopies: ["", Validators.required],
        remark: ["", Validators.required],
        hasArchiveAtt: ["", null]
      });
    } else {
      this.archiverForm = this.fb.group({
        avatar: [null]
      });
    }
  }

  checkToUpload(event) {
    this.checkedToUpload = event.checked;
  }

  onArchiverSave = new EventEmitter();

  save() {
    if (this.archiverForm.invalid) {
      return;
    }
    this.onArchiverSave.emit(this);
  }

  close() {
    this.dialogRef.close();
  }

  formatBytes(bytes, decimals = 2) {
    if (bytes === 0) {
      return "0 Bytes";
    }
    const k = 1024;
    const dm = decimals <= 0 ? 0 : decimals;
    const sizes = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"];
    const i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + " " + sizes[i];
  }

  uploadManuscript(e) {
    this.fileArr = [];
    this.imgArr = [];
    this.fileObj = [];

    const fileListAsArray = Array.from(e);
    const message = "Manuscript Archive";
    fileListAsArray.forEach((item, i) => {
      const file = (e as HTMLInputElement);
      const url = URL.createObjectURL(file[i]);
      if (file[i].type != "application/pdf" && file[i].type != "image/png" && file[i].type != "image/jpg" && file[i].type != "image/jpeg") {
        this.fileArr.push({ item, url: url, inProgress: false, progress: 0, status: 'error', 
                            message: message + ' must be a PDF and/or an Image' });
        this.contentTypeError = true;
      } else if (file[i].size > 1024 * 1024 * 1024 * 1024 * 1024) {
        this.fileArr.push({ item, url: url, inProgress: false, progress: 0, status: 'error', 
                            message: message + ' is too big' });
        this.sizeError = true;                    
      } else {
        this.imgArr.push(url);
        this.fileArr.push({ item, url: url, inProgress: false, progress: 0, status: 'success', message: '' });
      }      
    });

    this.fileArr.forEach((item) => {
      this.fileObj.push(item.item);      
      this.uploadManuscriptFile(item);
    })

    this.archiverForm.patchValue({
      avatar: this.fileObj
    })

    this.archiverForm.get('avatar').updateValueAndValidity();
  }

  uploadManuscriptFile(file) {
    let formData = new FormData();
    file.inProgress = true;
    formData.append('id', this.manuscript.id);
    formData.append('file', file.item);    
    let upload: Observable<HttpEvent<any>>;
    if (this.title == "Attach Manuscript Archive") {
      upload = this.uploadService.uploadManuscriptArchive(formData);
    } else {
      if (this.contentTypeError || this.sizeError) {
        return;
      } else {
        formData.append('existing_image', this.image.id);
        upload = this.uploadService.changeManuscriptArchive(formData);
      }      
    }
    upload.pipe(
      map(event => {
        switch (event.type) {  
          case HttpEventType.UploadProgress:  
            file.progress = Math.round(event.loaded * 100 / event.total);  
            break;  
          case HttpEventType.Response:  
            return event;  
        }
      }),
      catchError((error: HttpErrorResponse) => {
        file.inProgress = false;  
        return of(`${file.item.name} upload failed.`);  
      })).subscribe((event: any) => {  
        if (typeof (event) === 'object') {  
          if (!event.body.success) {
            file.status = 'error';
            file.message = event.body.errors[0];
          }          
        }  
      });
  }

  sanitize(url: string) {
    return this.sanitizer.bypassSecurityTrustUrl(url);
  }

  sanitize_uploaded(url: string) {
    return this.sanitizer.bypassSecurityTrustUrl(this.apiUrl + url);
  }

}
