import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManuscriptArchiverComponent } from './manuscript-archiver.component';

describe('ManuscriptArchiverComponent', () => {
  let component: ManuscriptArchiverComponent;
  let fixture: ComponentFixture<ManuscriptArchiverComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManuscriptArchiverComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManuscriptArchiverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
