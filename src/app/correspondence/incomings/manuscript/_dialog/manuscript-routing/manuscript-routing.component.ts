import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from "@angular/material/dialog";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { MatTable, MatTableDataSource } from "@angular/material/table";
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from "@angular/animations";
import { Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { Subscription } from 'rxjs';

import { DepartmentRole } from 'src/app/_models/department-role';
import { Manuscript } from 'src/app/_models/manuscript';
import { ManuscriptAssignment } from 'src/app/_models/manuscript-assignment';
import { User } from 'src/app/_models/user';
import { Message } from 'src/app/_models/_helpers/message';

import { AuthenticationService } from 'src/app/_services/authentication.service';
import { InfoDialogService } from 'src/app/_services/info-dialog.service';
import { DateFormatterService } from "src/app/_services/date-formatter.service";
import { ManuscriptRoutingService } from 'src/app/_services/manuscript-routing.service';
import { UserService } from "src/app/_services/user.service";

@Component({
  selector: 'app-manuscript-routing',
  templateUrl: './manuscript-routing.component.html',
  styleUrls: ['./manuscript-routing.component.css'],
  animations: [
    trigger("detailExpand", [
      state(
        "collapsed",
        style({ height: "0px", minHeight: "0", display: "none" })
      ),
      state("expanded", style({ height: "*" })),
      transition(
        "expanded <=> collapsed",
        animate("225ms cubic-bezier(0.4, 0.0, 0.2, 1)")
      ),
    ]),
  ]
})
export class ManuscriptRoutingComponent implements OnInit {
  subManager = new Subscription();

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatPaginator) set matPaginator(mp: MatPaginator) {
    this.paginator = mp;
    this.setDataSourceAttributes();
  }

  setDataSourceAttributes() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
  
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatTable) table: MatTable<any>;

  title: string;
  manuscript: Manuscript;
  message: Message = new Message();

  userDepartment: DepartmentRole = new DepartmentRole();

  manuscriptAssignment: ManuscriptAssignment = new ManuscriptAssignment();
  manuscriptAssignments: ManuscriptAssignment[] = [];

  dataSource = new MatTableDataSource<ManuscriptAssignment>();
  displayedColumns = ["stage", "assignedBy", "assignedTo", "assignedDate", "operations"];

  expandedManuscriptAssignment: ManuscriptAssignment | null;

  loading = false;
  resultsLength = 0;

  routeActions: string[] = [];
  currentUser: User;

  canAdd: boolean = false;
  canEdit: boolean = false;
  canDelete: boolean = false;
  canPreview: boolean = false;
  canAccept: boolean = false;
  canReject: boolean = false;


  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
    public router: Router,
    private authenticationService: AuthenticationService,
    private infoDialogService: InfoDialogService,
    private dateFormatterService: DateFormatterService,
    private manuscriptRoutingService: ManuscriptRoutingService,
    private userService: UserService) { 
    this.authenticationService.currentUser.subscribe(
      (x) => (this.currentUser = x)
    );

    this.authenticationService.userDepartment.subscribe(
      (userDepartment) => (this.userDepartment = userDepartment)
    );

    this.title = data.title;
    this.routeActions = data.routeActions;
    this.manuscript = data.manuscript;

    for (var action of this.routeActions) {
      if (action == "Route Add") {
        this.canAdd = true;
      } else if (action == "Route Edit") {
        this.canEdit = true;
      } else if (action == "Route Delete") {
        this.canDelete = true;
      } else if (action == "Route Preview") {
        this.canPreview = true;
      } else if (action == "Route Accept") {
        this.canAccept = true;
      } else if (action == "Route Reject") {
        this.canReject = true;
      }
    }
  }

  ngOnInit() {
    this.getAll();
  }

  ngOnDestroy() {
    this.subManager.unsubscribe();
  }

  getAll() {
    this.loading = true;
    let getRoutingDetails = this.manuscriptRoutingService
      .getAll(this.manuscript)
      .pipe(first())
      .subscribe(
        (response) => {
          if (response.success) {
            for (var value of response.data) {
              this.manuscriptAssignment = this.manuscriptRoutingService.assignManuscriptAssignmentValue(value);                      
              this.manuscriptAssignments.push(
                this.manuscriptAssignment
              );
            }            
            this.loading = false;
            this.resultsLength = response.total;
            this.dataSource.data = this.manuscriptAssignments;
          } else {
            this.message.title = "Manuscript Assignments";
            this.message.type = "error";
            this.message.content = response.errors;
            this.errorDialog(this.message, response);
          }
        },
        (error) => {
          this.loading = false
          this.errorDialog(error, null);
        }
      );
    this.subManager.add(getRoutingDetails);
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  returnStageLabel(stage) {
    let lblStage: string;
    switch (stage) {
      case 1: {
        lblStage = "Preliminary Assessment";
        break;
      }
      case 2: {
        lblStage = "Main Assessment";
        break;
      }
      case 3: {
        lblStage = "Incorporate Comments";
        break;
      }
      case 4: {
        lblStage = "Editorial";
        break;
      }
      case 5: {
        lblStage = "Typesetter";
        break;
      }
      case 6: {
        lblStage = "Printing";
        break;
      }
    }
    return lblStage;
  }

  addAssignment() {
    this.title = "Add Assignment";
    const dialogRef = this.manuscriptRoutingService.openAddAssignmentDialog(this.title, this.manuscript);

    const sub = dialogRef.componentInstance.onAssignmentSave
        .subscribe(
          (data) => {
            if(data) {
              data.loading = true;
              let create = this.manuscriptRoutingService
                    .create(data, this.manuscript)
                      .pipe(first())
                      .subscribe(
                        (response) => {
                          if(response.success) {
                            this.message.title = this.title;
                            this.message.content = [response.message];
                            this.message.type = "success";
                            const infoDialogRef = this.infoDialogService.openInfoDialog(
                              this.message
                            );

                            infoDialogRef.afterClosed().subscribe(() => {
                              this.manuscriptAssignment = this.manuscriptRoutingService.assignManuscriptAssignmentValue(response.data);
                              this.manuscriptAssignments.splice(
                                0, 0, this.manuscriptAssignment
                              );
                              this.resultsLength += 1;
                              this.dataSource = new MatTableDataSource(
                                this.manuscriptAssignments
                              );
                              this.manuscript.currentStage = data.routingForm.value.stages;
                              this.dataSource.paginator = this.paginator;
                              this.dataSource.sort = this.sort;
                              data.close();
                            });
                          } else {
                            this.message.title = "Assign to User";
                            this.message.type = "error";
                            this.message.content = response.errors;                            
                            this.errorDialog(this.message, response);
                            data.loading = false;
                          }
                        },
                        (error) => {
                          data.loading = false;
                          this.errorDialog(error, null);
                        }
                      );
              this.subManager.add(create);
            }
          }
        );
    dialogRef.afterClosed().subscribe(() => {
      sub.unsubscribe();
    });
  }


  editAssignedManuscript(assignment) {
    this.title = "Edit Assignment";
    let userRoleId = "";    
    if (assignment.assigneeType == "internal") {   
      let getAssignedUserRole = this.userService
            .getUserDepartments(assignment.assignee.id)
            .pipe(first())
            .subscribe(
              (response) => {
                if (response.success) {
                  userRoleId = response.data.department_user_role.user_role_id;
                  const dialogRef = this.manuscriptRoutingService.openAddAssignmentDialog(this.title, this.manuscript, assignment, userRoleId);

                  const sub = dialogRef.componentInstance.onAssignmentSave
                        .subscribe(
                          (data) => {
                            if (data) {
                              this.saveAssignment(data, assignment, this.manuscript);
                            }
                          }
                        );
                  dialogRef.afterClosed().subscribe(() => {
                    sub.unsubscribe();
                  });
                } else {
                  this.message.title = "Get Assigned User Department";
                  this.message.type = "error";
                  this.message.content = response.errors;
                  this.errorDialog(this.message, response);                  
                }
              },
              (error) => {
                this.errorDialog(error, null);
              }
            );
          this.subManager.add(getAssignedUserRole);
    } else {
      const dialogRef = this.manuscriptRoutingService.openAddAssignmentDialog(this.title, this.manuscript, assignment, userRoleId);

      const sub = dialogRef.componentInstance.onAssignmentSave
            .subscribe(
              (data) => {
                if (data) {
                  this.saveAssignment(data, assignment, this.manuscript);
                }
              }
            );
      dialogRef.afterClosed().subscribe(() => {
        sub.unsubscribe();
      });
    }  
  }

  saveAssignment(data, assignment, manuscript) {
    data.loading = true;
    let update = this.manuscriptRoutingService
        .update(data, assignment, manuscript)
        .pipe(first())
        .subscribe(
          (response) => {
            if(response.success) {
                this.message.title = this.title;
                this.message.content = [response.message];
                this.message.type = "success";
                const infoDialogRef = this.infoDialogService.openInfoDialog(
                  this.message
                );

                infoDialogRef.afterClosed().subscribe(() => {
                  this.manuscriptAssignment = this.manuscriptRoutingService.assignManuscriptAssignmentValue(response.data);
                  let index = this.manuscriptAssignments.findIndex(x => x.id === assignment.id);
                  this.manuscriptAssignments.splice(
                    index, 1, this.manuscriptAssignment
                  );
                  this.dataSource = new MatTableDataSource(
                    this.manuscriptAssignments
                  );
                  this.dataSource.paginator = this.paginator;
                  this.dataSource.sort = this.sort;
                  data.close();
                });
              } else {
                this.message.title = "Assign to User";
                this.message.type = "error";
                this.message.content = response.errors;                            
                this.errorDialog(this.message, response);
                data.loading = false;
              }                        
          },
          (error) => {
            data.loading = false;
            this.errorDialog(error, null);
          }
        );
    this.subManager.add(update);
  }

  previewAssignment() {
    const dialogRef = this.manuscriptRoutingService.openAssignmentPreviewDialog(
      "Preview Manuscript Assignment",
      this.manuscriptAssignments
    );
  }

  errorDialogOpened: boolean = false;

  errorDialog(message, response) {
    let infoDialogRef = null;
    if (!this.errorDialogOpened) {
      if (response != null) {
        if (response.errors[0] == "Signature has expired") {
          this.errorDialogOpened = true;
          infoDialogRef = this.infoDialogService.openInfoDialog(
            message
          );
        } else {
          infoDialogRef = this.infoDialogService.openInfoDialog(
            message
          );
        }
      } else {
        infoDialogRef = this.infoDialogService.openInfoDialog(
          message
        );
      }
      infoDialogRef.afterClosed().subscribe(() => {
        if (response != null) {
          if (response.errors[0] == "Signature has expired") {
            this.authenticationService.logout_offline();
            this.router.navigate(["/login"]);
          }
        }
      });
    }     
  }

}
