import { Component, Inject, OnInit, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { first } from "rxjs/operators";
import { Subscription } from "rxjs";
import { Router } from "@angular/router";

import { Message } from 'src/app/_models/_helpers/message';
import { DepartmentRole } from 'src/app/_models/department-role';
import { Role } from 'src/app/_models/role';
import { User } from 'src/app/_models/user';
import { Manuscript } from 'src/app/_models/manuscript';
import { ManuscriptAssignment } from 'src/app/_models/manuscript-assignment';
import { ExternalAssessor } from 'src/app/_models/external-assessor';

import { UserService } from 'src/app/_services/user.service';
import { AuthenticationService } from 'src/app/_services/authentication.service';
import { InfoDialogService } from 'src/app/_services/info-dialog.service';
import { RoleService } from 'src/app/_services/role.service';
import { DepartmentService } from 'src/app/_services/department.service';
import { ExternalAssessorService } from 'src/app/_services/external-assessor.service';

@Component({
  selector: 'app-manuscript-routing-dialog',
  templateUrl: './manuscript-routing-dialog.component.html',
  styleUrls: ['./manuscript-routing-dialog.component.css']
})
export class ManuscriptRoutingDialogComponent implements OnInit {
  subManager = new Subscription();

  title: string;
  loading = false;
  message: Message = new Message();

  manuscript: Manuscript;

  manuscript_assignment: ManuscriptAssignment;
  manuscript_assignments: ManuscriptAssignment[];
  user_role_id;

  routingForm: FormGroup;

  stages: any[] = [
    { value: "1", viewValue: "Preliminary Assessment" },
    { value: "2", viewValue: "Main Assessment" },
    { value: "3", viewValue: "Incorporate Comments"},
    { value: "4", viewValue: "Editorial" },
    { value: "5", viewValue: "Typesetter" },
    { value: "6", viewValue: "Printing" }
  ];

  userDepartment: DepartmentRole;

  assignedRoles: Role[] = [];

  users: User[] = [];
  user: User = new User();

  externalAssessor: ExternalAssessor = new ExternalAssessor();
  externalAssessors: ExternalAssessor[] = [];

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
        private dialogRef: MatDialogRef<ManuscriptRoutingDialogComponent>,
        private fb: FormBuilder,
        private userService: UserService,
        private departmentService: DepartmentService,
        private authenticationService: AuthenticationService,
        private infoDialogService: InfoDialogService,
        private router: Router,
        private externalAssessorService: ExternalAssessorService) {
    this.title = data.title;
    this.manuscript = data.manuscript;
    this.manuscript_assignment = data.manuscript_assignment;
    this.manuscript_assignments = data.manuscript_assignments;
    this.user_role_id = data.user_role_id;
    this.userDepartment = this.userService.getCurrentUserDepartments();
  }

  internalAssignee: boolean = true;
  externalAssignee: boolean = false;
  authorAssignee: boolean = false;

  ngOnInit() {
    this.getAllExternalAssessor();
    if (this.title == "Add Assignment") {
      let stage = this.returnNextStageLabel(this.manuscript.currentStage);
      this.routingForm = this.fb.group({
        stages: [stage, Validators.required],
        type: ["internal"],
        userRole: ["", Validators.required],
        assignedTo: ["", Validators.required],
      });
      if (stage == "Incorporate Comments") {
        this.authorAssignee = true;
        this.internalAssignee = false;
        this.externalAssignee = false;
        this.routingForm.get('userRole').clearValidators();
        this.routingForm.get('userRole').updateValueAndValidity();
      } else {
        this.authorAssignee = false;
        this.internalAssignee = true;
        this.externalAssignee = false;
      }
      this.routingForm.get('type').valueChanges.subscribe(response => {
        this.routingForm.get('assignedTo').setValue("");
        if(response == "external"){
          this.internalAssignee = false;
          this.externalAssignee = true;          
          this.routingForm.get('userRole').clearValidators();          
        } else {
          this.internalAssignee = true;
          this.externalAssignee = false;
          this.routingForm.get('userRole').setValidators(Validators.required);
        }
        this.routingForm.get('userRole').updateValueAndValidity();
      });

      this.routingForm.get('stages').valueChanges.subscribe(response => {
        this.routingForm.get('assignedTo').setValue("");
        if(response == "Incorporate Comments"){
          this.authorAssignee = true;
          this.internalAssignee = false;
          this.externalAssignee = false;
          this.routingForm.get('userRole').clearValidators();
          this.routingForm.get('userRole').updateValueAndValidity();
        } else {          
          this.authorAssignee = false;
          if(this.routingForm.get('type').value == "external") {
            this.internalAssignee = false;
            this.externalAssignee = true;
            this.routingForm.get('userRole').clearValidators();
          } else {
            this.internalAssignee = true;
            this.externalAssignee = false;
            this.routingForm.get('userRole').setValidators(Validators.required);            
          }
          this.routingForm.get('userRole').updateValueAndValidity();
        }
      });
    } else if(this.title == "Edit Assignment") {      
      if (this.manuscript_assignment.assigneeType != "external") {
        this.onUserRoleSelect(this.user_role_id);
      }
      let stage = this.manuscript_assignment.stage;
      let type = this.manuscript_assignment.assigneeType;
      this.routingForm = this.fb.group({
        stages: [stage, Validators.required],
        type: [type],
        userRole: [this.user_role_id, Validators.required],
        assignedTo: [this.manuscript_assignment.assignee.id, Validators.required],
      });
      if (stage == "Incorporate Comments") {
        this.authorAssignee = true;
        this.internalAssignee = false;
        this.externalAssignee = false;
        this.routingForm.get('userRole').clearValidators();
        this.routingForm.get('userRole').updateValueAndValidity();
      } else {
        this.authorAssignee = false;
          if(type == "external") {
            this.internalAssignee = false;
            this.externalAssignee = true;
            this.routingForm.get('userRole').clearValidators();
          } else {
            this.internalAssignee = true;
            this.externalAssignee = false;
            this.routingForm.get('userRole').setValidators(Validators.required);            
          }
          this.routingForm.get('userRole').updateValueAndValidity();
      }

      this.routingForm.get('type').valueChanges.subscribe(response => {        
        if(response == "external"){
          this.internalAssignee = false;
          this.externalAssignee = true;          
          this.routingForm.get('userRole').clearValidators();          
        } else {
          this.internalAssignee = true;
          this.externalAssignee = false;
          this.routingForm.get('userRole').setValidators(Validators.required);
        }
        this.routingForm.get('userRole').updateValueAndValidity();
      });

      this.routingForm.get('stages').valueChanges.subscribe(response => {        
        if(response == "Incorporate Comments"){
          this.authorAssignee = true;
          this.internalAssignee = false;
          this.externalAssignee = false;
          this.routingForm.get('userRole').clearValidators();
          this.routingForm.get('userRole').updateValueAndValidity();
        } else {          
          this.authorAssignee = false;
          if(this.routingForm.get('type').value == "external") {
            this.internalAssignee = false;
            this.externalAssignee = true;
            this.routingForm.get('userRole').clearValidators();
          } else {
            this.internalAssignee = true;
            this.externalAssignee = false;
            this.routingForm.get('userRole').setValidators(Validators.required);            
          }
          this.routingForm.get('userRole').updateValueAndValidity();
        }
      });
    }

    this.initDepartmentRoles(this.userDepartment.department_id);
  }

  ngOnDestroy() {
    this.subManager.unsubscribe();
  }  

  assignRole (values) {
    let role: Role;
    let roles: Role [] = [];
    for (var value of values) {
      role = {
        id: value["id"],
        name: value["name"],
      };
      roles.push(role);
    }
    return roles;
  }

  initDepartmentRoles(deptId) {
    let getAssignedRoleForDepartment = this.departmentService
      .getAssignedRoleForDepartment(deptId)
      .pipe(first())
      .subscribe(
        (response) => {
          if (response.success) {
            this.assignedRoles = this.assignRole(response.data);
            this.assignedRoles.sort((a,b) => a.name.localeCompare(b.name));
          } else {
            this.message.title = "Department Roles";
            this.message.type = "error";
            this.message.content = response.errors;
            this.errorDialog(this.message, response);        
          }
        },
        (error) => {
          this.errorDialog(error, null);
        }
      );
    this.subManager.add(getAssignedRoleForDepartment);
  }

  onUserRoleSelect(role_id) {
    let departmentId = this.userDepartment.department_id;
    this.getAllUsersWithTheRole(departmentId, role_id);    
  }

  getAllUsersWithTheRole(departmentId, roleId): void {
    this.users = [];
    let getAllUsers = this.userService
      .getAllUsersWithTheRole(departmentId, roleId)
      .pipe(first())
      .subscribe(
        (response) => {
          if (response.success) {
              for (var value of response.data) {
                if (value != null && value["active"]) {
                  this.user = {
                    id: value["id"],
                    firstName: value["first_name"],
                    lastName: value["last_name"],
                    fullName: value["first_name"] + " " + value["last_name"],
                    userName: value["email"],
                    password: "",
                    active: value["active"],
                    token: "",
                  };
                  this.users.push(this.user);
                }   
              }           
            this.users.sort((a,b) => a.firstName.localeCompare(b.firstName));           
          } else {
            this.message.title = "Users";
            this.message.type = "error";
            this.message.content = response.errors;
            this.errorDialog(this.message, response);
          }
        },
        (error) => {
          this.errorDialog(error, null);
        }
      );
    this.subManager.add(getAllUsers);
  }

  returnStageLabel(stage) {
    let lblStage;
    switch (stage) {
      case "Preliminary Assessment": {
        lblStage = 1;
        break;
      }
      case "Main Assessment": {
        lblStage = 2;
        break;
      }
      case "Incorporate Comments": {
        lblStage = 3;
        break;
      }
      case "Editorial": {
        lblStage = 4;
        break;
      }
      case "Typesetter": {
        lblStage = 5;
        break;
      }
      case "Printing": {
        lblStage = 6;
        break;
      }
    }
    return lblStage;
  }

  returnNextStageLabel(stage) {
    let lblStage;
    switch (stage) {
      case "Preliminary Assessment": {
        lblStage = "Main Assessment";
        break;
      }
      case "Main Assessment": {
        lblStage = "Incorporate Comments";
        break;
      }
      case "Incorporate Comments": {
        lblStage = "Editorial";
        break;
      }
      case "Editorial": {
        lblStage = "Typesetter";
        break;
      }
      case "Typesetter": {
        lblStage = "Printing";
        break;
      }
    }
    return lblStage;
  }

  getAllExternalAssessor() {
    let getAll = this.externalAssessorService
      .getAll()
      .pipe(first())
      .subscribe(
        (response) => {
          if (response.success) {
            for (var value of response.data) {             
              this.externalAssessor = this.externalAssessorService.assignAssessorValue(value);
              this.externalAssessors.push(this.externalAssessor);
            }
          } else {
            this.message.title = "External Assessor";
            this.message.type = "error";
            this.message.content = response.errors;
            this.errorDialog(this.message, response);
            this.loading = false;
          }
        },
        (error) => {
          this.errorDialog(error, null);
          this.loading = false;
        }
      );
    this.subManager.add(getAll);
  }

  onAssignmentSave = new EventEmitter();

  save() {
    if (this.routingForm.invalid) {
      return;
    }    
    this.onAssignmentSave.emit(this);
  }

  close() {
    this.dialogRef.close();
  }

  errorDialogOpened: boolean = false;

  errorDialog(message, response) {
    let infoDialogRef = null;
    if (!this.errorDialogOpened) {
      if (response != null) {
        if (response.errors[0] == "Signature has expired") {
          this.errorDialogOpened = true;
          infoDialogRef = this.infoDialogService.openInfoDialog(
            message
          );
        } else {
          infoDialogRef = this.infoDialogService.openInfoDialog(
            message
          );
        }
      } else {
        infoDialogRef = this.infoDialogService.openInfoDialog(
          message
        );
      }
      infoDialogRef.afterClosed().subscribe(() => {
        if (response != null) {
          if (response.errors[0] == "Signature has expired") {
            this.authenticationService.logout_offline();
            this.router.navigate(["/login"]);
          }
        }
      });
    }     
  }

}
