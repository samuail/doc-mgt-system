import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManuscriptRoutingDialogComponent } from './manuscript-routing-dialog.component';

describe('ManuscriptRoutingDialogComponent', () => {
  let component: ManuscriptRoutingDialogComponent;
  let fixture: ComponentFixture<ManuscriptRoutingDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManuscriptRoutingDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManuscriptRoutingDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
