import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManuscriptRoutingComponent } from './manuscript-routing.component';

describe('ManuscriptRoutingComponent', () => {
  let component: ManuscriptRoutingComponent;
  let fixture: ComponentFixture<ManuscriptRoutingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManuscriptRoutingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManuscriptRoutingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
