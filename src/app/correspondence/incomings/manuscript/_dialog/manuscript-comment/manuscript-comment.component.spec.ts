import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManuscriptCommentComponent } from './manuscript-comment.component';

describe('ManuscriptCommentComponent', () => {
  let component: ManuscriptCommentComponent;
  let fixture: ComponentFixture<ManuscriptCommentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManuscriptCommentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManuscriptCommentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
