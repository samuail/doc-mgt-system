import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from "@angular/material/dialog";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { MatTable, MatTableDataSource } from "@angular/material/table";
import { Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { DatePipe } from '@angular/common';

import { Message } from 'src/app/_models/_helpers/message';
import { User } from 'src/app/_models/user';
import { Manuscript } from 'src/app/_models/manuscript';
import { ManuscriptComment } from "src/app/_models/manuscript-comment";

import { AuthenticationService } from "src/app/_services/authentication.service";
import { InfoDialogService } from "src/app/_services/info-dialog.service";
import { ManuscriptCommentService } from "src/app/_services/manuscript-comment.service";

@Component({
  selector: 'app-manuscript-comment',
  templateUrl: './manuscript-comment.component.html',
  styleUrls: ['./manuscript-comment.component.css']
})
export class ManuscriptCommentComponent implements OnInit {
  subManager = new Subscription();

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatPaginator) set matPaginator(mp: MatPaginator) {
    this.paginator = mp;
    this.setDataSourceAttributes();
  }

  setDataSourceAttributes() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
  
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatTable) table: MatTable<any>;

  title: string;
  manuscript: Manuscript;
  message: Message = new Message();

  manuscriptComment: ManuscriptComment = new ManuscriptComment();
  manuscriptComments: ManuscriptComment[] = [];

  dataSource = new MatTableDataSource<ManuscriptComment>();
  displayedColumns = ["order", "commentedBy", "content", "commentDate", "operations"];

  loading = false;
  resultsLength = 0;

  commentActions: string[] = [];
  currentUser: User;

  canAdd: boolean = false;
  canEdit: boolean = false;
  canDelete: boolean = false;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
      public router: Router,
      private authenticationService: AuthenticationService,
      private infoDialogService: InfoDialogService,
      public manuscriptCommentService: ManuscriptCommentService) { 
    this.title = data.title;
    this.commentActions = data.commentActions;
    this.manuscript = data.manuscript;

    this.authenticationService.currentUser.subscribe(
      (x) => (this.currentUser = x)
    );

    for (var action of this.commentActions) {
      if (action == "Comment Add") {
        this.canAdd = true;
      } else if (action == "Comment Edit") {
        this.canEdit = true;
      } else if (action == "Comment Delete") {
        this.canDelete = true;
      }
    }
  }

  ngOnInit() {
    this.getAll();
  }

  ngOnDestroy() {
    this.subManager.unsubscribe();
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  getAll() {
    this.loading = true;
    let getCommentDetails = this.manuscriptCommentService
      .getAll(this.manuscript)
      .pipe(first())
      .subscribe(
        (response) => {
          if (response.success) {
            for (var value of response.data) {
              this.manuscriptComment = this.manuscriptCommentService.assignManuscriptCommentValue(value);                      
              this.manuscriptComments.push(
                this.manuscriptComment
              );
            }            
            this.loading = false;
            this.resultsLength = response.total;
            this.dataSource.data = this.manuscriptComments;            
          } else {
            this.message.title = "Manuscript Comment";
            this.message.type = "error";
            this.message.content = response.errors;
            this.errorDialog(this.message, response);
          }
        },
        (error) => {
          this.loading = false
          this.errorDialog(error, null);
        }
      );
    this.subManager.add(getCommentDetails);
  }

  addComment() {
    const dialogRef = this.manuscriptCommentService.openCommentDialog(
      "Add Comment"
    );

    const sub = dialogRef.componentInstance.onCommentSave
        .subscribe(
          (data) => {
            if(data) {
              data.loading = true;
              let content = data.commentForm.value.content;
              let create = this.manuscriptCommentService
                    .create(content, this.manuscript.id)
                      .pipe(first())
                      .subscribe(
                        (response) => {
                          if (response.success) {
                            this.message.title = this.title;
                            this.message.content = [response.message];
                            this.message.type = "success";
                            const infoDialogRef = this.infoDialogService.openInfoDialog(
                              this.message
                            );
                            infoDialogRef.afterClosed().subscribe(() => {
                              let comment: ManuscriptComment = new ManuscriptComment();
                              comment = this.manuscriptCommentService.assignManuscriptCommentValue(response.data);
                              this.manuscriptComments.splice(0, 0, comment);
                              this.resultsLength = this.resultsLength + 1;
                              this.dataSource = new MatTableDataSource(
                                this.manuscriptComments
                              );
                              this.dataSource.paginator = this.paginator;
                              this.dataSource.sort = this.sort;
                              data.close();
                          });
                          } else {
                            this.message.title = "Add Comment";
                            this.message.type = "error";
                            this.message.content = response.errors;
                            data.loading = false;
                            this.errorDialog(this.message, response);
                          }
                        },
                        (error) => {
                          data.loading = false;
                          this.errorDialog(error, null);
                        }
                      );
              this.subManager.add(create);
            }
          }
        );
    dialogRef.afterClosed().subscribe(() => {
      sub.unsubscribe();
    });
  }

  editComment(comment) {
    const dialogRef = this.manuscriptCommentService.openCommentDialog(
      "Edit Comment",
      comment
    );

    const sub = dialogRef.componentInstance.onCommentSave
        .subscribe(
          (data) => {
            if(data) {
              data.loading = true;
              let content = data.commentForm.value.content;
              let create = this.manuscriptCommentService
                    .update(content, comment)
                      .pipe(first())
                      .subscribe(
                        (response) => {
                          if (response.success) {
                            this.message.title = this.title;
                            this.message.content = [response.message];
                            this.message.type = "success";
                            const infoDialogRef = this.infoDialogService.openInfoDialog(
                              this.message
                            );
                            infoDialogRef.afterClosed().subscribe(() => {
                              let comment: ManuscriptComment = new ManuscriptComment();
                              comment = this.manuscriptCommentService.assignManuscriptCommentValue(response.data);
                              let index = this.manuscriptComments.findIndex(x => x.id === comment.id);
                              this.manuscriptComments.splice(index, 1, comment);                            
                              this.dataSource = new MatTableDataSource(
                                this.manuscriptComments
                              );
                              this.dataSource.paginator = this.paginator;
                              this.dataSource.sort = this.sort;
                              data.close();
                          });
                          } else {
                            this.message.title = "Add Comment";
                            this.message.type = "error";
                            this.message.content = response.errors;
                            data.loading = false;
                            this.errorDialog(this.message, response);
                          }
                        },
                        (error) => {
                          data.loading = false;
                          this.errorDialog(error, null);
                        }
                      );
              this.subManager.add(create);
            }
          }
        );
    dialogRef.afterClosed().subscribe(() => {
      sub.unsubscribe();
    });
  }

  errorDialogOpened: boolean = false;

  errorDialog(message, response) {
    if (!this.errorDialogOpened) {
      this.errorDialogOpened = true;
      const infoDialogRef = this.infoDialogService.openInfoDialog(
        message
      );
      infoDialogRef.afterClosed().subscribe(() => {
        if (response != null) {
          if (response.errors[0] == "Signature has expired") {
            this.authenticationService.logout_offline();
            this.router.navigate(["/login"]);
          }
        }
      });
    }    
  }

}
