import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManuscriptCommentDialogComponent } from './manuscript-comment-dialog.component';

describe('ManuscriptCommentDialogComponent', () => {
  let component: ManuscriptCommentDialogComponent;
  let fixture: ComponentFixture<ManuscriptCommentDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManuscriptCommentDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManuscriptCommentDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
