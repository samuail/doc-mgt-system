import { Component, OnInit, Inject, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Subscription } from "rxjs";
import { Router } from "@angular/router";

import { Message } from 'src/app/_models/_helpers/message';
import { ManuscriptComment } from 'src/app/_models/manuscript-comment';

import { UserService } from 'src/app/_services/user.service';
import { AuthenticationService } from 'src/app/_services/authentication.service';
import { InfoDialogService } from 'src/app/_services/info-dialog.service';
import { RoleService } from 'src/app/_services/role.service';
import { DepartmentService } from 'src/app/_services/department.service';

@Component({
  selector: 'app-manuscript-comment-dialog',
  templateUrl: './manuscript-comment-dialog.component.html',
  styleUrls: ['./manuscript-comment-dialog.component.css']
})
export class ManuscriptCommentDialogComponent implements OnInit {
  subManager = new Subscription();

  title: string;
  loading = false;
  message: Message = new Message();

  comment: ManuscriptComment;

  commentForm: FormGroup;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
        private dialogRef: MatDialogRef<ManuscriptCommentDialogComponent>,
        private fb: FormBuilder) {
    this.title = data.title;
    this.comment = data.comment;
  }

  ngOnInit() {
    if (this.title == "Add Comment") {
      this.commentForm = this.fb.group({
        content: ['', Validators.required]  
      });
    } else if(this.title == 'Edit Comment'){
    	this.commentForm = this.fb.group({
        content: [this.comment.content, Validators.required]
    	});
    }
  }

  onCommentSave = new EventEmitter();

  save() {
    if (this.commentForm.invalid) {
          return;
    }
    this.onCommentSave.emit(this);
  }

  close() {
	 this.dialogRef.close();
  }

}
