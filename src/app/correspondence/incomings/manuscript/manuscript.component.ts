import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from "@angular/animations";
import { first } from "rxjs/operators";
import { debounceTime, distinctUntilChanged, tap, filter } from 'rxjs/operators';
import { Subscription } from "rxjs";
import { fromEvent } from "rxjs";
import { Router } from "@angular/router";
import { MatPaginator } from "@angular/material/paginator";

import { RoleService } from "src/app/_services/role.service";
import { MenuService } from "src/app/_services/menu.service";
import { UserService } from "src/app/_services/user.service";
import { ManuscriptService } from "src/app/_services/manuscript.service";
import { AuthenticationService } from "src/app/_services/authentication.service";
import { InfoDialogService } from "src/app/_services/info-dialog.service";
import { AuthorService } from "src/app/_services/author.service";
import { DepartmentService } from "src/app/_services/department.service";
import { ConfirmationDialogService } from "src/app/_services/confirmation-dialog.service";

import { Message } from "src/app/_models/_helpers/message";
import { Role } from "src/app/_models/role";
import { DepartmentRole } from "src/app/_models/department-role";
import { Manuscript } from "src/app/_models/manuscript";
import { Author } from "src/app/_models/author";
import { DepartmentItem } from "src/app/_models/department-item";
import { ManuscriptAssignment } from "src/app/_models/manuscript-assignment";
import { ManuscriptRoutingService } from "src/app/_services/manuscript-routing.service";

import { ManuscriptDataSource } from "src/app/_datasource/manuscript.datasource";

@Component({
  selector: 'app-manuscript',
  templateUrl: './manuscript.component.html',
  styleUrls: ['./manuscript.component.css'],
  animations: [
    trigger("detailExpand", [
      state(
        "collapsed",
        style({ height: "0px", minHeight: "0", display: "none" })
      ),
      state("expanded", style({ height: "*" })),
      transition(
        "expanded <=> collapsed",
        animate("225ms cubic-bezier(0.4, 0.0, 0.2, 1)")
      ),
    ]),
  ]
})
export class ManuscriptComponent implements OnInit, OnDestroy {
  
  subManager = new Subscription();

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('input') input: ElementRef;

  title: string;
  type: string = "Outside AAU";
  message: Message = new Message();

  incomingManuscript: Manuscript = new Manuscript();
  incomingManuscripts: Manuscript[] = [];

  manuscriptDataSource: ManuscriptDataSource;
  dataSize: string = "";

  displayedColumns = [
    "referenceNo",
    "receivedDate",
    "title",
    "operations",
  ];

  expandedIncomingManuscript: Manuscript | null;

  loading = false;

  loadingDeptTree = false;

  menuActions: string[] = [];
  userRole: Role;
  userDepartment: DepartmentRole;

  canAdd: boolean = false;
  canEdit: boolean = false;
  canDelete: boolean = false;
  canUpload: boolean = false;
  canComment: boolean = false;
  canPreview: boolean = false;
  canRoute: boolean = false;
  canArchive: boolean = false;

  author: Author = new Author();
  authors: Author[] = [];

  departmentItems: DepartmentItem[] = [];

  constructor(
    private router: Router,
    private roleService: RoleService,
    private userService: UserService,
    private menuService: MenuService,
    private manuscriptService: ManuscriptService,
    private authenticationService: AuthenticationService,
    public infoDialogService: InfoDialogService,
    public authorService: AuthorService,
    private departmentService: DepartmentService,
    public confirmationDialogService: ConfirmationDialogService,
    private manuscriptRoutingService: ManuscriptRoutingService) {
    this.userRole = this.roleService.getUserRole();
    this.userDepartment = this.userService.getCurrentUserDepartments();
    this.menuActions = this.menuService.getMenuActions("Manuscripts");
    for (var action of this.menuActions) {
      if (action == "Add") {
        this.canAdd = true;
      } else if (action == "Edit") {
        this.canEdit = true;
      } else if (action == "Delete") {
        this.canDelete = true;
      } else if (action == "Upload") {
        this.canUpload = true;
      } else if (action == "Comment") {
        this.canComment = true;
      } else if (action == "Preview") {
        this.canPreview = true;
      } else if (action == "Route") {
        this.canRoute = true;
      } else if (action == "Archive") {
        this.canArchive = true;
      } 
    }
  }

  ngOnInit() {
    this.getAllAuthors();
    this.manuscriptDataSource = new ManuscriptDataSource(this.manuscriptService, this.router, this.infoDialogService, this.authenticationService);
    this.initDepartmentTree();
    // if (this.userRole.name == "Administrator" || this.userRole.name == "Business Administrator" || this.userRole.name == "Secretary") {
      this.dataSize = "all";
      this.manuscriptDataSource.loadManuscript(this.dataSize, '', 'dsc', 0, 10);
    // } else {
      // this.dataSize = "assigned";
      // this.manuscriptDataSource.loadManuscript(this.dataSize, '', 'dsc', 0, 10);
    // }
  }

  ngAfterViewInit() {
    this.paginator.page
        .pipe(
            tap(() => this.loadManuscriptsPage())
        )
        .subscribe();
    
    fromEvent(this.input.nativeElement,'keyup')
        .pipe(
            debounceTime(150),
            filter((e: KeyboardEvent) => e.key === "Enter"),
            distinctUntilChanged(),
            tap(() => {
                this.paginator.pageIndex = 0;

                this.loadManuscriptsPage();
            })
        )
        .subscribe();    
  }

  loadManuscriptsPage() {
    this.manuscriptDataSource.loadManuscript(
                      this.dataSize, 
                      this.input.nativeElement.value, 
                      'asc', 
                      this.paginator.pageIndex, 
                      this.paginator.pageSize);
  }

  ngOnDestroy() {
    this.subManager.unsubscribe();
  }

  initDepartmentTree() {
    this.loadingDeptTree = true;
    let getAllforTree = this.departmentService
      .getAllForTree()
      .pipe(first())
      .subscribe(
        (response) => {
          if (response.success) {    
            this.loadingDeptTree = false;
            this.departmentItems = response.data;            
          } else {
            this.message.title = "Get Department Tree";
            this.message.type = "error";
            this.message.content = response.errors;
            this.errorDialog(this.message, response);
          }
        },
        (error) => {
          this.errorDialog(error, null);        
        }
      );
    this.subManager.add(getAllforTree);
  }

  returnDepartmentTree(dept_id){  
    var departments = this.departmentItems;
    for (var college of departments) {
      for (var department of college["children"]) {
        for (var office of department["children"]) {
          if (office.id == dept_id) {
            return college["code"] + " / " + department["code"] + " / " + office["description"];
          }
        }
      }
    }
  }

  getAllAuthors() {
    let getAllAuthors = this.authorService
      .getAll()
      .pipe(first())
      .subscribe(
        (response) => {
          if (response.success) {
            for (var value of response.data) {             
              this.author = this.authorService.assignAuthorValue(value);
              this.authors.push(this.author);
            }         
          } else {
            this.message.title = "Author";
            this.message.type = "error";
            this.message.content = response.errors;
            this.errorDialog(this.message, response);
          }
        },
        (error) => {
          this.errorDialog(error, null);
        }
      );
    this.subManager.add(getAllAuthors);
  }

  addManuscript(e) {    
    this.title = "Add Manuscript";
    var nextNumber: number = 0;

    let getNextNo = this.manuscriptService.getNextNumber()
      .pipe(first())
      .subscribe(
        (response) => {
          if (response.success) {
            nextNumber = response.data;
            const dialogRef = this.manuscriptService.openManuscriptDialog(
              this.type,
              this.title,
              this.authors,
              nextNumber
            );

            const sub = dialogRef.componentInstance.onManuscriptSave.subscribe(
              (data) => {
                if (data) {
                  data.loading = true;
                  let create = this.manuscriptService
                    .create(data)
                    .pipe(first())
                    .subscribe(
                      (response) => {
                        if (response.success) {
                          this.message.title = this.title;
                          this.message.content = [response.message];
                          this.message.type = "success";
                          const infoDialogRef = this.infoDialogService.openInfoDialog(
                            this.message
                          );
                          infoDialogRef.afterClosed().subscribe(() => {                    
                            this.loadManuscriptsPage();                   
                            data.close();
                          });
                        } else {
                          this.message.title = this.title;
                          this.message.type = "error";
                          this.message.content = response.errors;
                          this.errorDialog(this.message, response);
                        }
                      },
                      (error) => {
                        data.loading = false;
                        this.errorDialog(error, null);
                      }
                    );
                  this.subManager.add(create);
                }
              }
            );
            dialogRef.afterClosed().subscribe(() => {
              sub.unsubscribe();
            });
          } else {
            this.message.title = this.title;
            this.message.type = "error";
            this.message.content = response.errors;
            this.errorDialog(this.message, response);
          }
        },
        (error) => {
          this.errorDialog(error, null);
        }
      );
    this.subManager.add(getNextNo);
  }

  editManuscript(manuscript) {
    this.title = "Edit Manuscript";

    const dialogRef = this.manuscriptService.openManuscriptDialog(
      this.type,
      this.title,
      this.authors,
      0,
      manuscript
    );

    const sub = dialogRef.componentInstance.onManuscriptSave.subscribe(
      (data) => {
        if (data) {
          data.loading = true;
          let update = this.manuscriptService
            .update(manuscript, data)
            .pipe(first())
            .subscribe(
              (response) => {
                if (response.success) {
                  this.message.title = this.title;
                  this.message.content = [response.message];
                  this.message.type = "success";
                  const infoDialogRef = this.infoDialogService.openInfoDialog(
                    this.message
                  );
                  infoDialogRef.afterClosed().subscribe(() => {                    
                    this.loadManuscriptsPage();                    
                    data.close();
                  });
                } else {
                  this.message.title = this.title;
                  this.message.type = "error";
                  this.message.content = response.errors;
                  this.errorDialog(this.message, response);
                }
              },
              (error) => {
                data.loading = false;
                this.errorDialog(error, null);
              }
            );
          this.subManager.add(update);
        }
      }
    );
    dialogRef.afterClosed().subscribe(() => {
      sub.unsubscribe();
    });
  }

  addAuthors(manuscript) {
    const dialogRef = this.manuscriptService.openAuthorsDialog(
      this.menuActions.filter((s) => s.includes("Upload")),      
      "Add Manuscript Author",
      manuscript
    );
  }

  uploadDocument(manuscript) {
    const dialogRef = this.manuscriptService.openManuscriptUploaderDialog(
      this.menuActions.filter((s) => s.includes("Upload")),      
      "Upload Document",
      manuscript
    );
  }

  previewManuscript(manuscript) {
    const dialogRef = this.manuscriptService.openManuscriptPreviewerDialog(
      this.menuActions.filter((s) => s.includes("Preview")),
      "Manuscript Preview",
      manuscript
    );
  }

  acceptManuscript(manuscript) {
    this.message.title = "Confirmation";
    this.message.type = "success";
    var message = 'Do you confirm the Acceptance of the Manuscript with the reference no: "' + manuscript.referenceNoFull + '"?';
    this.message.content = [message];
    const confirmationDialogRef = this.confirmationDialogService.openConfirmationDialog(
      this.message
    );

    confirmationDialogRef.afterClosed().subscribe(result => {
      this.title = "Accept Manuscript";
      if(result) {
        let updateStatus = this.manuscriptService
          .updateStatus(manuscript, "Accepted")
          .pipe(first())
          .subscribe(
            (response) => {
              if (response.success) {
                this.message.title = this.title;
                  this.message.content = [response.message];
                  this.message.type = "success";
                  const infoDialogRef = this.infoDialogService.openInfoDialog(
                    this.message
                  );
                  infoDialogRef.afterClosed().subscribe(() => {                    
                    this.loadManuscriptsPage();
                  });
              } else {
                this.message.title = this.title;
                this.message.type = "error";
                this.message.content = response.errors;
                this.errorDialog(this.message, response);
              }
            },
            (error) => {
              this.errorDialog(error, null);
            }
          );
        this.subManager.add(updateStatus);
      }
    });
  }

  rejectManuscript(manuscript) {
    this.message.title = "Confirmation";
    this.message.type = "success";
    var message = 'Do you confirm the Rejection of the Manuscript with the reference no: "' + manuscript.referenceNoFull + '"?';
    this.message.content = [message];
    const confirmationDialogRef = this.confirmationDialogService.openConfirmationDialog(
      this.message
    );

    confirmationDialogRef.afterClosed().subscribe(result => {
      this.title = "Reject Manuscript";
      if(result) {
        let updateStatus = this.manuscriptService
          .updateStatus(manuscript, "Rejected")
          .pipe(first())
          .subscribe(
            (response) => {
              if (response.success) {
                this.message.title = this.title;
                  this.message.content = [response.message];
                  this.message.type = "success";
                  const infoDialogRef = this.infoDialogService.openInfoDialog(
                    this.message
                  );
                  infoDialogRef.afterClosed().subscribe(() => {
                    this.loadManuscriptsPage();
                  });
              } else {
                this.message.title = this.title;
                this.message.type = "error";
                this.message.content = response.errors;
                this.errorDialog(this.message, response);
              }
            },
            (error) => {
              this.errorDialog(error, null);
            }
          );
        this.subManager.add(updateStatus);
      }
    });
  }

  assignManuscript(manuscript) {
    const dialogRef = this.manuscriptService.openAssignManuscriptDialog(
      this.menuActions.filter((s) => s.includes("Route")),
      "Assign Manuscript",
      manuscript
    );    
  }

  loadingAssignment: boolean = false;
  manuscriptAssignments: ManuscriptAssignment[] = [];

  previewManuscriptAssignmentHistory(manuscript) {
    this.manuscriptAssignments = [];
    this.loadingAssignment = true;
    let getRoutingDetails = this.manuscriptRoutingService
      .getAll(manuscript)
      .pipe(first())
      .subscribe(
        (response) => {
          if (response.success) {
            for (var value of response.data) {              
              this.manuscriptAssignments.push(
                this.manuscriptRoutingService.assignManuscriptAssignmentValue(value)
              );
            }            
            this.loadingAssignment = false;
            
            const dialogRef = this.manuscriptRoutingService.openAssignmentPreviewDialog(
              "Preview Manuscript Assignment",
              this.manuscriptAssignments
            );
          } else {
            this.message.title = "Preview Assignments";
            this.message.type = "error";
            this.message.content = response.errors;
            this.errorDialog(this.message, response);
          }
        },
        (error) => {
          this.errorDialog(error, null);
        }
      );
    this.subManager.add(getRoutingDetails);
  }

  commentManuscript(manuscript) {
    const dialogRef = this.manuscriptService.openManuscriptCommentsDialog(
      this.menuActions.filter((s) => s.includes("Comment")),      
      "Manuscript Comments",
      manuscript
    );   
  }

  archiveManuscript(manuscript) {    
    const dialogRef = this.manuscriptService.openDocArchiverDialog(      
      "Archive Manuscript",
      manuscript
    );

    const sub = dialogRef.componentInstance.onArchiverSave.subscribe(
      (data) => {
        if (data) {
          data.loading = true;
          let formData = data.archiverForm.value;
          let update = this.manuscriptService
            .archive(manuscript, formData)
            .pipe(first())
            .subscribe(
              (response) => {
                if (response.success) {
                  this.message.title = "Archive Manuscript";
                  this.message.content = [response.message];
                  this.message.type = "success";
                  const infoDialogRef = this.infoDialogService.openInfoDialog(
                    this.message
                  );
                  infoDialogRef.afterClosed().subscribe(() => {                    
                    this.loadManuscriptsPage();                    
                    data.close();
                  });
                } else {
                  this.message.title = "Archive Manuscript";
                  this.message.type = "error";
                  this.message.content = response.errors;
                  this.errorDialog(this.message, response);
                }
              },
              (error) => {
                data.loading = false;
                this.errorDialog(error, null);
              }
            );
          this.subManager.add(update);
        }
      }
    );
    dialogRef.afterClosed().subscribe(() => {
      sub.unsubscribe();
    });
  }

  errorDialogOpened: boolean = false;

  errorDialog(message, response) {
    let infoDialogRef = null;
    if (!this.errorDialogOpened) {
      if (response != null) {
        if (response.errors[0] == "Signature has expired") {
          this.errorDialogOpened = true;
          infoDialogRef = this.infoDialogService.openInfoDialog(
            message
          );
        } else {
          infoDialogRef = this.infoDialogService.openInfoDialog(
            message
          );
        }
      } else {
        infoDialogRef = this.infoDialogService.openInfoDialog(
          message
        );
      }
      infoDialogRef.afterClosed().subscribe(() => {
        if (response != null) {
          if (response.errors[0] == "Signature has expired") {
            this.authenticationService.logout_offline();
            this.router.navigate(["/login"]);
          }
        }
      });
    }     
  }

}
