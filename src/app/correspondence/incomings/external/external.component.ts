import { Component, OnInit, ViewChild, OnDestroy, ElementRef } from "@angular/core";
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from "@angular/animations";
import { first } from "rxjs/operators";
import { debounceTime, distinctUntilChanged, tap, filter } from 'rxjs/operators';
import { Subscription } from "rxjs";
import { fromEvent } from "rxjs";
import { MatPaginator } from "@angular/material/paginator";
import { Router } from "@angular/router";

import { CorrespondenceService } from "src/app/_services/correspondence.service";
import { AuthenticationService } from "src/app/_services/authentication.service";
import { InfoDialogService } from "src/app/_services/info-dialog.service";
import { ConfirmationDialogService } from "src/app/_services/confirmation-dialog.service";
import { RoleService } from "src/app/_services/role.service";
import { MenuService } from "src/app/_services/menu.service";
import { DepartmentService } from "src/app/_services/department.service";
import { DateFormatterService } from "src/app/_services/date-formatter.service";
import { UserService } from "src/app/_services/user.service";
import { CorrespondenceAssignmentService } from "src/app/_services/correspondence-assignment.service";

import { Message } from "src/app/_models/_helpers/message";
import { ExternalIncomingCorrespondence } from "src/app/_models/external-incoming-correspondence";
import { Role } from "src/app/_models/role";
import { DepartmentItem } from "src/app/_models/department-item";
import { CorrespondenceCarbonCopy } from "src/app/_models/correspondence-carbon-copy";
import { DepartmentRole } from "src/app/_models/department-role";
import { CorrespondenceAssignment } from "src/app/_models/correspondence-assignment";
import { ExternalIncomingsDataSource } from "src/app/_datasource/external-incomings.datasource";

@Component({
  selector: "app-external",
  templateUrl: "./external.component.html",
  styleUrls: ["./external.component.css"],
  animations: [
    trigger("detailExpand", [
      state(
        "collapsed",
        style({ height: "0px", minHeight: "0", display: "none" })
      ),
      state("expanded", style({ height: "*" })),
      transition(
        "expanded <=> collapsed",
        animate("225ms cubic-bezier(0.4, 0.0, 0.2, 1)")
      ),
    ]),
  ]
})
export class ExternalComponent implements OnInit, OnDestroy {
  subManager = new Subscription();

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('input') input: ElementRef;

  title: string;
  type: string = "Outside AAU";
  message: Message = new Message();

  incomingExternalCorrespondence: ExternalIncomingCorrespondence = new ExternalIncomingCorrespondence();
  incomingExternalCorrespondences: ExternalIncomingCorrespondence[] = [];

  correspondenceCarbonCopy: CorrespondenceCarbonCopy = new CorrespondenceCarbonCopy();
  correspondenceCarbonCopies: CorrespondenceCarbonCopy [] = [];

  externalDataSource: ExternalIncomingsDataSource;
  dataSize: string = "";

  displayedColumns = [
    "referenceNo",
    "letterDate",
    "source",
    "subject",
    "operations",
  ];

  expandedExternalCorrespondence: ExternalIncomingCorrespondence | null;

  loading = false;
  loadingDeptTree = false;
  externalResultsLength = 0;

  menuActions: string[] = [];
  userRole: Role;
  userDepartment: DepartmentRole;

  departmentItems: DepartmentItem[] = [];

  loadingAssignment = false;
  correspondenceAssignments: CorrespondenceAssignment[] = [];
  correspondenceAssignment: CorrespondenceAssignment = new CorrespondenceAssignment();

  canAdd: boolean = false;
  canEdit: boolean = false;
  canDelete: boolean = false;
  canUpload: boolean = false;
  canComment: boolean = false;
  canPreview: boolean = false;
  canRoute: boolean = false;
  canArchive: boolean = false;
  canAccept: boolean = false;
  canReject: boolean = false;

  constructor(
    private router: Router,  
    private correspondenceService: CorrespondenceService,
    private authenticationService: AuthenticationService,
    private roleService: RoleService,
    private menuService: MenuService,
    public infoDialogService: InfoDialogService,
    public confirmationDialogService: ConfirmationDialogService,
    private departmentService: DepartmentService,
    private dateFormatterService: DateFormatterService,
    private userService: UserService,
    private correspondenceAssignmentService: CorrespondenceAssignmentService
  ) {
    this.userRole = this.roleService.getUserRole();
    this.userDepartment = this.userService.getCurrentUserDepartments();
    this.menuActions = this.menuService.getMenuActions("Outside AAU");
    for (var action of this.menuActions) {
      if (action == "Add") {
        this.canAdd = true;
      } else if (action == "Edit") {
        this.canEdit = true;
      } else if (action == "Delete") {
        this.canDelete = true;
      } else if (action == "Upload") {
        this.canUpload = true;
      } else if (action == "Comment") {
        this.canComment = true;
      } else if (action == "Preview") {
        this.canPreview = true;
      } else if (action == "Route") {
        this.canRoute = true;
      } else if (action == "Archive") {
        this.canArchive = true;
      } else if (action == "Accept") {
        this.canAccept = true;
      } else if (action == "Reject") {
        this.canReject = true;
      }
    }
  }

  ngOnInit() {
    this.externalDataSource = new ExternalIncomingsDataSource(this.correspondenceService, this.router, this.infoDialogService, this.authenticationService);
    if (this.userRole.name == "Administrator" || this.userRole.name == "Business Administrator" || this.userRole.name == "Secretary") {
      this.dataSize = "all";
      this.externalDataSource.loadExternalIncomings(this.dataSize, 'incoming', '', 'dsc', 0, 10);
    } else {
      this.dataSize = "assigned";
      this.externalDataSource.loadExternalIncomings(this.dataSize, 'incoming', '', 'dsc', 0, 10);
    }
    this.initDepartmentTree();
  }

  ngAfterViewInit() {
    this.paginator.page
        .pipe(
            tap(() => this.loadExternalIncomingsPage())
        )
        .subscribe();
    
    fromEvent(this.input.nativeElement,'keyup')
        .pipe(
            debounceTime(150),
            filter((e: KeyboardEvent) => e.key === "Enter"),
            distinctUntilChanged(),
            tap(() => {
                this.paginator.pageIndex = 0;

                this.loadExternalIncomingsPage();
            })
        )
        .subscribe();
  }

  loadExternalIncomingsPage() {
    this.externalDataSource.loadExternalIncomings(
                      this.dataSize,
                      'incoming',
                      this.input.nativeElement.value, 
                      'asc', 
                      this.paginator.pageIndex, 
                      this.paginator.pageSize);
  }

  ngOnDestroy() {
    this.subManager.unsubscribe();
  }

  initDepartmentTree() {
    this.loadingDeptTree = true;
    let getAllforTree = this.departmentService
      .getAllForTree()
      .pipe(first())
      .subscribe(
        (response) => {
          if (response.success) {  
            this.loadingDeptTree = false;  
            this.departmentItems = response.data;
          } else {
            this.message.title = "Get Department Tree";
            this.message.type = "error";
            this.message.content = response.errors;
            this.loadingDeptTree = false;
            const infoDialogRef = this.infoDialogService.openInfoDialog(
              this.message
            );
            infoDialogRef.afterClosed().subscribe(() => {
              if (response.errors[0] == "Signature has expired") {
                this.authenticationService.logout_offline();
                this.router.navigate(["/login"]);
              }
            });
          }
        },
        (error) => {
          this.loadingDeptTree = false;
          this.infoDialogService.openInfoDialog(error);
        }
      );
    this.subManager.add(getAllforTree);
  }

  returnDepartmentTree(dept_id){  
    var department = this.departmentItems;
    if (department != undefined){
      for (var dept of department)
      {
        for (var value of dept["children"]) {
          for (var value2 of value["children"]){
            if (value2.id == dept_id) {            
              return dept["code"] + " / " + value["code"] + " / " + value2["description"];
            }
          }          
        }
      }
    }
  }

  addCorrespondence() {
    this.title = "Add Correspondence";
    const dialogRef = this.correspondenceService.openCorrespondenceDialog(
      this.type,
      this.title,
      null
    );
    const sub = dialogRef.componentInstance.onCorrespondenceSave.subscribe(
      (data) => {
        if (data) {
          data.loading = true;
          let create = this.correspondenceService
            .createExternal(data, "incoming")
            .pipe(first())
            .subscribe(
              (response) => {
                if (response.success) {
                  this.message.title = this.title;
                  this.message.content = [response.message];
                  this.message.type = "success";
                  const infoDialogRef = this.infoDialogService.openInfoDialog(
                    this.message
                  );
                  infoDialogRef.afterClosed().subscribe(() => {                    
                    this.loadExternalIncomingsPage();
                    data.close();
                  });
                } else {
                  this.message.title = this.title;
                  this.message.type = "error";
                  this.message.content = response.errors;
                  const infoDialogRef = this.infoDialogService.openInfoDialog(
                    this.message
                  );
                  infoDialogRef.afterClosed().subscribe(() => {
                    data.loading = false;
                    if (response.errors[0] == "Signature has expired") {
                      this.authenticationService.logout_offline();
                      this.router.navigate(["/login"]);
                    }
                  });
                }
              },
              (error) => {
                data.loading = false;
                this.infoDialogService.openInfoDialog(error);
              }
            );
          this.subManager.add(create);
        }
      }
    );
    dialogRef.afterClosed().subscribe(() => {
      sub.unsubscribe();
    });
  }

  addCorrespondenceCC(correspondence) {
    this.title = "Add Correspondence CC";
    const dialogRef = this.correspondenceService.openCorrespondenceCcDialog(
      this.canAdd,
      this.canEdit,
      this.canDelete,
      this.type,
      this.title,      
      correspondence,
      this.departmentItems,
      "incoming"
    );
  }

  acceptCorrespondence(correspondence) {
    this.message.title = "Confirmation";
    this.message.type = "success";
    var message = 'Do you confirm the acceptance of the correspondence with the reference no: "' + correspondence.referenceNo + '"?';
    this.message.content = [message];
    const confirmationDialogRef = this.confirmationDialogService.openConfirmationDialog(
      this.message
    );

    confirmationDialogRef.afterClosed().subscribe(result => {
      this.title = "Accept Correspondence";
      if(result) {
        let updateStatus = this.correspondenceService
          .updateExternalStatus(correspondence, "Accepted")
          .pipe(first())
          .subscribe(
            (response) => {
              if (response.success) {
                this.message.title = this.title;
                  this.message.content = [response.message];
                  this.message.type = "success";
                  const infoDialogRef = this.infoDialogService.openInfoDialog(
                    this.message
                  );
                  infoDialogRef.afterClosed().subscribe(() => {
                    this.loadExternalIncomingsPage();
                  });
              } else {
                this.message.title = this.title;
                this.message.type = "error";
                this.message.content = response.errors;
                const infoDialogRef = this.infoDialogService.openInfoDialog(
                  this.message
                );
                infoDialogRef.afterClosed().subscribe(() => {
                  if (response.errors[0] == "Signature has expired") {
                    this.authenticationService.logout_offline();
                    this.router.navigate(["/login"]);
                  }
                });
              }
            },
            (error) => {
              this.infoDialogService.openInfoDialog(error);
            }
          );
        this.subManager.add(updateStatus);
      }
    });
  }

  rejectCorrespondence(correspondence){
    this.message.title = "Confirmation";
    this.message.type = "error";
    var message = 'Do you confirm the rejection of the correspondence with the reference no: "' + correspondence.referenceNo + '"?';
    this.message.content = [message];
    const confirmationDialogRef = this.confirmationDialogService.openConfirmationDialog(
      this.message
    );

    confirmationDialogRef.afterClosed().subscribe(result => {
      this.title = "Reject Correspondence";
      if(result) {
        let updateStatus = this.correspondenceService
          .updateExternalStatus(correspondence, "Rejected")
          .pipe(first())
          .subscribe(
            (response) => {
              if (response.success) {
                this.message.title = this.title;
                  this.message.content = [response.message];
                  this.message.type = "success";
                  const infoDialogRef = this.infoDialogService.openInfoDialog(
                    this.message
                  );
                  infoDialogRef.afterClosed().subscribe(() => {
                    this.loadExternalIncomingsPage();
                  });
              } else {
                this.message.title = this.title;
                this.message.type = "error";
                this.message.content = response.errors;
                const infoDialogRef = this.infoDialogService.openInfoDialog(
                  this.message
                );
                infoDialogRef.afterClosed().subscribe(() => {
                  if (response.errors[0] == "Signature has expired") {
                    this.authenticationService.logout_offline();
                    this.router.navigate(["/login"]);
                  }
                });
              }
            },
            (error) => {
              this.infoDialogService.openInfoDialog(error);
            }
          );
        this.subManager.add(updateStatus);
      }
    });
  }

  editCorrespondence(correspondence) {
    this.title = "Edit Correspondence";
    const dialogRef = this.correspondenceService.openCorrespondenceDialog(
      this.type,
      this.title,
      null,
      correspondence
    );
    const sub = dialogRef.componentInstance.onCorrespondenceSave.subscribe(
      (data) => {
        if (data) {
          data.loading = true;
          let update = this.correspondenceService
            .updateExternal(correspondence, data, "incoming")
            .pipe(first())
            .subscribe(
              (response) => {
                if (response.success) {
                  this.message.title = this.title;
                  this.message.content = [response.message];
                  this.message.type = "success";
                  const infoDialogRef = this.infoDialogService.openInfoDialog(
                    this.message
                  );
                  infoDialogRef.afterClosed().subscribe(() => {
                    this.loadExternalIncomingsPage();
                    data.close();
                  });
                } else {
                  this.message.title = this.title;
                  this.message.type = "error";
                  this.message.content = response.errors;
                  const infoDialogRef = this.infoDialogService.openInfoDialog(
                    this.message
                  );
                  infoDialogRef.afterClosed().subscribe(() => {
                    data.loading = false;
                    if (response.errors[0] == "Signature has expired") {
                      this.authenticationService.logout_offline();
                      this.router.navigate(["/login"]);
                    }
                  });
                }
              },
              (error) => {
                data.loading = false;
                this.infoDialogService.openInfoDialog(error);
              }
            );
          this.subManager.add(update);
        }
      }
    );
  }

  uploadDocument(correspondence) {
    const dialogRef = this.correspondenceService.openDocImagesDialog(
      this.menuActions.filter((s) => s.includes("Upload")),
      this.type,
      "Upload Document",
      correspondence,
      "incoming"
    );
  }

  previewCorrespondence(correspondence) {
    this.title = "Correspondence Preview";
    const dialogRef = this.correspondenceService.openDocPreviewerDialog(
      this.menuActions.filter((s) => s.includes("Preview")),
      this.type,
      this.title,
      correspondence,
      "incoming"
    );
  }

  assignCorrespondenceAssignmentValue (value) {
    let correspondenceAssignment: CorrespondenceAssignment = new CorrespondenceAssignment();
    correspondenceAssignment = {
          id: value["id"],
          fromUser: value["from_user"],
          toUser: value["to_user"],
          correspondence: value["correspondence"],
          order: value["order"],
          assignedDate: value["assigned_date"],
          receivedDate: value["received_date"],
          status: value["status"],
          rejectionRemark: value["rejection_remark"]
        };
    return correspondenceAssignment;
  }

  previewCorrespondenceAssignmentHistory(correspondence) {
    this.loadingAssignment = true;
    this.correspondenceAssignments = [];
    let getRoutingDetails = this.correspondenceAssignmentService
      .getAll(correspondence.id, "external", "incoming")
      .pipe(first())
      .subscribe(
        (response) => {
          if (response.success) {
            for (var value of response.data) {
              this.correspondenceAssignment = this.assignCorrespondenceAssignmentValue(value);
              this.correspondenceAssignments.push(
                this.correspondenceAssignment
              );              
            }
            this.loadingAssignment = false;

            this.title = "Preview Assignment";
            const dialogRef = this.correspondenceAssignmentService.openAssignmentPreviewDialog(
              this.title,
              this.correspondenceAssignments
            );

          } else {
            this.message.title = "Correspondence Assignments";
            this.message.type = "error";
            this.message.content = response.errors;
            const infoDialogRef = this.infoDialogService.openInfoDialog(
              this.message
            );
            infoDialogRef.afterClosed().subscribe(() => {
              if (response.errors[0] == "Signature has expired") {                
                this.authenticationService.logout_offline();
                this.router.navigate(["/login"]);
              }
            });            
          }
        },
        (error) => {
          this.loadingAssignment = false;
          this.infoDialogService.openInfoDialog(error);
        }
      );
    this.subManager.add(getRoutingDetails);    
  }

  assignCorrespondence(correspondence) {
    const dialogRef = this.correspondenceService.openAssignCorrespondenceDialog(
      this.menuActions.filter((s) => s.includes("Route")),
      this.type,
      "Assign Correspondence",
      correspondence,
      this.departmentItems,
      "incoming"
    );
  }

  commentCorrespondence(correspondence) {
    const dialogRef = this.correspondenceService.openDocCommentsDialog(
      this.menuActions.filter((s) => s.includes("Comment")),
      this.type,
      "Correspondence Comments",
      correspondence,
      "incoming"
    );
  }

  archiveCorrespondence(correspondence) {
    this.title = "Archive Correspondence";
    const dialogRef = this.correspondenceService.openDocArchiverDialog(      
      this.type,
      "Correspondence Archive",
      correspondence,
      "incoming"
    );

    const sub = dialogRef.componentInstance.onCorrespondenceSave
        .subscribe( (data) => {
          if(data){
            data.loading = true;
            let formData = data.archiverForm.value;            
            let archive = this.correspondenceService
                .archiveExternal(formData, correspondence)
                  .pipe(first())
                  .subscribe(
                    (response) => {
                      if (response.success) {
                        this.message.title = this.title;
                        this.message.content = [response.message];
                        this.message.type = "success";
                        const infoDialogRef = this.infoDialogService.openInfoDialog(
                          this.message
                        );
                        infoDialogRef.afterClosed().subscribe(() => {
                          this.loadExternalIncomingsPage();
                          data.close();
                        });
                      } else {
                        this.message.title = this.title;
                        this.message.type = "error";
                        this.message.content = response.errors;
                        const infoDialogRef = this.infoDialogService.openInfoDialog(
                          this.message
                        );
                        infoDialogRef.afterClosed().subscribe(() => {
                          data.loading = false;
                          if (response.errors[0] == "Signature has expired") {
                            this.authenticationService.logout_offline();
                            this.router.navigate(["/login"]);
                          }
                        });
                      }
                    },
                    (error) => {
                      data.loading = false;
                      this.infoDialogService.openInfoDialog(error);
                    }
                );
            this.subManager.add(archive);
          }
        });
  }

  deleteCorrespondence(correspondence) {}
}
