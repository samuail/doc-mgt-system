import { Component, OnInit, OnDestroy, ViewChild } from "@angular/core";
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from "@angular/animations";
import { first } from "rxjs/operators";
import { Subscription } from "rxjs";
import { MatPaginator } from "@angular/material/paginator";
import { MatSnackBar } from "@angular/material/snack-bar";
import { MatSort } from "@angular/material/sort";
import { MatTableDataSource, MatTable } from "@angular/material/table";
import { MatTabChangeEvent } from "@angular/material/tabs";
import { Router } from "@angular/router";

import { CorrespondenceService } from "src/app/_services/correspondence.service";
import { AuthenticationService } from "src/app/_services/authentication.service";
import { InfoDialogService } from "src/app/_services/info-dialog.service";

import { Message } from "src/app/_models/_helpers/message";

import { Correspondence } from "src/app/_models/correspondence";
import { InternalCorrespondence } from "src/app/_models/internal-correspondence";
// import { ExternalOutgoingCorrespondence } from "src/app/_models/external-outgoing-correspondence";
import { ExternalOutgoingCorrespondence } from "src/app/_models/external-outgoing-correspondence";

@Component({
  selector: "app-outgoing-correspondence",
  templateUrl: "./outgoing-correspondence.component.html",
  styleUrls: ["./outgoing-correspondence.component.css"],
  animations: [
    trigger("detailExpand", [
      state(
        "collapsed",
        style({ height: "0px", minHeight: "0", display: "none" })
      ),
      state("expanded", style({ height: "*" })),
      transition(
        "expanded <=> collapsed",
        animate("225ms cubic-bezier(0.4, 0.0, 0.2, 1)")
      ),
    ]),
  ],
})
export class OutgoingCorrespondenceComponent implements OnInit, OnDestroy {
  subManager = new Subscription();

  @ViewChild(MatPaginator, { static: true }) internalPaginator: MatPaginator;
  @ViewChild(MatPaginator, { static: true }) externalPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatTable, { static: true }) table: MatTable<any>;

  title: string;
  type: string = "Internal";
  message: Message = new Message();

  // correspondences: Correspondence[] = [];
  // correspondence: Correspondence = new Correspondence();
  // dataSource = new MatTableDataSource<Correspondence>();

  internalCorrespondence: InternalCorrespondence = new InternalCorrespondence();
  internalCorrespondences: InternalCorrespondence[] = [];
  outgoingInternalDataSource = new MatTableDataSource<InternalCorrespondence>();

  outgoingExternalCorrespondence: ExternalOutgoingCorrespondence = new ExternalOutgoingCorrespondence();
  outgoingExternalCorrespondences: ExternalOutgoingCorrespondence[] = [];
  outgoingExternalDataSource = new MatTableDataSource<
    ExternalOutgoingCorrespondence
  >();

  displayedColumns = [
    "referenceNo",
    "letterDate",
    "destination",
    "sentDate",
    "operations",
  ];

  // expandedCorrespondence: Correspondence | null;
  expandedInternalCorrespondence: InternalCorrespondence | null;
  expandedExternalCorrespondence: ExternalOutgoingCorrespondence | null;

  loading = false;
  internalResultsLength = 0;
  externalResultsLength = 0;

  constructor(
    private router: Router,
    private correspondenceService: CorrespondenceService,
    private authenticationService: AuthenticationService,
    public infoDialogService: InfoDialogService
  ) {}

  ngOnInit(): void {
    this.getInternalAll();
    this.getExternalAll();
  }

  ngAfterViewInit() {
    // this.dataSource.paginator = this.internalPaginator;
    // this.dataSource.sort = this.sort;
    this.outgoingInternalDataSource.paginator = this.internalPaginator;
    this.outgoingInternalDataSource.sort = this.sort;
    this.outgoingExternalDataSource.paginator = this.externalPaginator;
    this.outgoingExternalDataSource.sort = this.sort;
  }

  ngOnDestroy() {
    this.subManager.unsubscribe();
  }

  applyFilter(filterValue: string) {
    this.outgoingInternalDataSource.filter = filterValue.trim().toLowerCase();
    this.outgoingExternalDataSource.filter = filterValue.trim().toLowerCase();
  }

  outgoingTabChange = (tabChangeEvent: MatTabChangeEvent): void => {
    if (tabChangeEvent.index == 0) {
      this.type = "Internal";
    } else {
      this.type = "External";
    }
  };

  getInternalAll(): void {
    this.loading = true;
    let getAll = this.correspondenceService
      .getInternalAll("outgoing")
      .pipe(first())
      .subscribe(
        (response) => {
          if (response.success) {
            for (var value of response.data) {
              this.internalCorrespondence = {
                id: value["id"],
                referenceNo: value["reference_no"],
                letterDate: value["letter_date"],
                subject: value["subject"],
                keyWord: value["search_keywords"],
                source: value["source"],
                destination: value["destination"],
                sentDate: value["received_date"],
                status: value["status"],
              };
              this.internalCorrespondences.push(this.internalCorrespondence);
            }
            this.loading = false;
            this.internalResultsLength = response.total;
            this.outgoingInternalDataSource.data = this.internalCorrespondences;
          } else {
            this.message.title = "Outgoing Correspondences";
            this.message.type = "error";
            this.message.content = response.errors;
            const infoDialogRef = this.infoDialogService.openInfoDialog(
              this.message
            );
            infoDialogRef.afterClosed().subscribe(() => {
              if (response.errors[0] == "Signature has expired") {
                this.authenticationService.logout_offline();
                this.router.navigate(["/login"]);
              }
            });
          }
        },
        (error) => {
          this.loading = false;
          this.infoDialogService.openInfoDialog(error);
        }
      );
    this.subManager.add(getAll);
  }

  getExternalAll(): void {
    this.loading = true;
    let getAll = this.correspondenceService
      .getExternalAll("outgoing")
      .pipe(first())
      .subscribe(
        (response) => {
          if (response.success) {
            for (var value of response.data) {
              this.outgoingExternalCorrespondence = {
                id: value["id"],
                referenceNo: value["reference_no"],
                letterDate: value["letter_date"],
                subject: value["subject"],
                keyWord: value["search_keywords"],
                source: value["source"],
                destination: value["destination"],
                sentDate: value["received_date"],
                status: value["status"],
              };
              this.outgoingExternalCorrespondences.push(
                this.outgoingExternalCorrespondence
              );
            }
            this.loading = false;
            this.externalResultsLength = response.total;
            this.outgoingExternalDataSource.data = this.outgoingExternalCorrespondences;
          } else {
            this.message.title = "Outgoing Correspondences";
            this.message.type = "error";
            this.message.content = response.errors;
            const infoDialogRef = this.infoDialogService.openInfoDialog(
              this.message
            );
            infoDialogRef.afterClosed().subscribe(() => {
              if (response.errors[0] == "Signature has expired") {
                this.authenticationService.logout_offline();
                this.router.navigate(["/login"]);
              }
            });
          }
        },
        (error) => {
          this.loading = false;
          this.infoDialogService.openInfoDialog(error);
        }
      );
    this.subManager.add(getAll);
  }

  addCorrespondence() {
    this.title = "Add Correspondence";
    const dialogRef = this.correspondenceService.openCorrespondenceDialog(
      this.type,
      this.title,
      new Correspondence()
    );
    const sub = dialogRef.componentInstance.onCorrespondenceSave.subscribe(
      (data) => {
        if (data) {
          data.loading = true;
          if (data.form.value.type == "Internal") {
            let create = this.correspondenceService
              .createInternal(data.form.value, "")
              .pipe(first())
              .subscribe(
                (response) => {
                  if (response.success) {
                    this.message.title = this.title;
                    this.message.content = [response.message];
                    this.message.type = "success";
                    const infoDialogRef = this.infoDialogService.openInfoDialog(
                      this.message
                    );
                    infoDialogRef.afterClosed().subscribe(() => {
                      this.internalCorrespondence = {
                        id: response.data["id"],
                        referenceNo: response.data["reference_no"],
                        letterDate: response.data["letter_date"],
                        subject: response.data["subject"],
                        keyWord: response.data["search_keywords"],
                        source: response.data["source"],
                        destination: response.data["destination"],
                        sentDate: response.data["received_date"],
                        status: response.data["status"],
                      };
                      this.internalCorrespondences.push(
                        this.internalCorrespondence
                      );
                      this.internalResultsLength =
                        this.internalResultsLength + 1;
                      this.outgoingInternalDataSource = new MatTableDataSource(
                        this.internalCorrespondences
                      );
                      this.outgoingInternalDataSource.paginator = this.internalPaginator;
                      this.outgoingInternalDataSource.sort = this.sort;
                      this.table.renderRows();
                      data.close();
                    });
                  } else {
                    this.message.title = this.title;
                    this.message.type = "error";
                    this.message.content = response.errors;
                    const infoDialogRef = this.infoDialogService.openInfoDialog(
                      this.message
                    );
                    infoDialogRef.afterClosed().subscribe(() => {
                      data.loading = false;
                      if (response.errors[0] == "Signature has expired") {
                        this.authenticationService.logout_offline();
                        this.router.navigate(["/login"]);
                      }
                    });
                  }
                },
                (error) => {
                  data.loading = false;
                  this.infoDialogService.openInfoDialog(error);
                }
              );
            this.subManager.add(create);
          } else {
            let create = this.correspondenceService
              .createExternalOutgoing(data.form.value)
              .pipe(first())
              .subscribe(
                (response) => {
                  if (response.success) {
                    this.message.title = this.title;
                    this.message.content = [response.message];
                    this.message.type = "success";
                    const infoDialogRef = this.infoDialogService.openInfoDialog(
                      this.message
                    );
                    infoDialogRef.afterClosed().subscribe(() => {
                      this.outgoingExternalCorrespondence = {
                        id: response.data["id"],
                        referenceNo: response.data["reference_no"],
                        letterDate: response.data["letter_date"],
                        subject: response.data["subject"],
                        keyWord: response.data["search_keywords"],
                        source: response.data["source"],
                        destination: response.data["destination"],
                        sentDate: response.data["received_date"],
                        status: response.data["status"],
                      };
                      this.outgoingExternalCorrespondences.push(
                        this.outgoingExternalCorrespondence
                      );
                      this.externalResultsLength =
                        this.externalResultsLength + 1;
                      this.outgoingExternalDataSource = new MatTableDataSource(
                        this.outgoingExternalCorrespondences
                      );
                      this.outgoingExternalDataSource.paginator = this.externalPaginator;
                      this.outgoingExternalDataSource.sort = this.sort;
                      this.table.renderRows();
                      data.close();
                    });
                  } else {
                    this.message.title = this.title;
                    this.message.type = "error";
                    this.message.content = response.errors;
                    const infoDialogRef = this.infoDialogService.openInfoDialog(
                      this.message
                    );
                    infoDialogRef.afterClosed().subscribe(() => {
                      data.loading = false;
                      if (response.errors[0] == "Signature has expired") {
                        this.authenticationService.logout_offline();
                        this.router.navigate(["/login"]);
                      }
                    });
                  }
                },
                (error) => {
                  data.loading = false;
                  this.infoDialogService.openInfoDialog(error);
                }
              );
            this.subManager.add(create);
          }
        }
      }
    );
    dialogRef.afterClosed().subscribe(() => {
      sub.unsubscribe();
    });
  }

  editCorrespondence(correspondence) {
    const dialogRef = this.correspondenceService.openCorrespondenceDialog(
      this.type,
      "Edit Correspondence",
      correspondence
    );
  }

  uploadDocument(correspondence) {
    const dialogRef = this.correspondenceService.openCorrespondenceDialog(
      this.type,
      "Upload Document",
      correspondence
    );
  }

  // commentCorrespondence(correspondence) {
  //   const dialogRef = this.correspondenceService.openDocCommentsDialog(
  //     "Correspondence Comments",
  //     correspondence
  //   );
  // }

  // previewCorrespondence(correspondence) {
  //   const dialogRef = this.correspondenceService.openDocPreviewerDialog(
  //     "Correspondence Preview",
  //     correspondence
  //   );
  // }

  deleteCorrespondence(correspondence) {}
}
