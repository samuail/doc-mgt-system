import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OutgoingExternalComponent } from './outgoing-external.component';

describe('OutgoingExternalComponent', () => {
  let component: OutgoingExternalComponent;
  let fixture: ComponentFixture<OutgoingExternalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OutgoingExternalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutgoingExternalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
