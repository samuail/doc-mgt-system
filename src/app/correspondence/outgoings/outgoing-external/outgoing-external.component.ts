import { Component, OnInit, ViewChild, OnDestroy, ElementRef } from "@angular/core";
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from "@angular/animations";
import { first } from "rxjs/operators";
import { debounceTime, distinctUntilChanged, tap, filter } from 'rxjs/operators';
import { Subscription } from "rxjs";
import { fromEvent } from "rxjs";
import { MatPaginator } from "@angular/material/paginator";
import { Router } from "@angular/router";

import { CorrespondenceService } from "src/app/_services/correspondence.service";
import { AuthenticationService } from "src/app/_services/authentication.service";
import { InfoDialogService } from "src/app/_services/info-dialog.service";
import { MenuService } from "src/app/_services/menu.service";
import { DateFormatterService } from "src/app/_services/date-formatter.service";
import { DepartmentService } from "src/app/_services/department.service";

import { Message } from "src/app/_models/_helpers/message";
import { ExternalOutgoingCorrespondence } from "src/app/_models/external-outgoing-correspondence";
import { DepartmentItem } from "src/app/_models/department-item";
import { CorrespondenceCarbonCopy } from "src/app/_models/correspondence-carbon-copy";
import { ExternalOutgoingsDataSource } from "src/app/_datasource/external-outgoings.datasource";

@Component({
  selector: 'app-outgoing-external',
  templateUrl: './outgoing-external.component.html',
  styleUrls: ['./outgoing-external.component.css'],
  animations: [
    trigger("detailExpand", [
      state(
        "collapsed",
        style({ height: "0px", minHeight: "0", display: "none" })
      ),
      state("expanded", style({ height: "*" })),
      transition(
        "expanded <=> collapsed",
        animate("225ms cubic-bezier(0.4, 0.0, 0.2, 1)")
      ),
    ]),
  ]
})
export class OutgoingExternalComponent implements OnInit, OnDestroy {
  subManager = new Subscription();

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('input') input: ElementRef;

  title: string;
  type: string = "Outgoing External";
  message: Message = new Message();

  externalOutgoingCorrespondence: ExternalOutgoingCorrespondence = new ExternalOutgoingCorrespondence();
  externalOutgoingCorrespondences: ExternalOutgoingCorrespondence[] = [];

  outgoingExternalDataSource: ExternalOutgoingsDataSource;

  correspondenceCarbonCopy: CorrespondenceCarbonCopy = new CorrespondenceCarbonCopy();
  correspondenceCarbonCopies: CorrespondenceCarbonCopy [] = [];

  displayedColumns = [
    "referenceNo",
    "letterDate",
    "destination",
    "subject",
    "operations",
  ];

  expandedCenterCorrespondence: ExternalOutgoingCorrespondence | null;

  loading = false;
  externalResultsLength = 0;

  loadingDeptTree = false;

  menuActions: string[] = [];

  departmentItems: DepartmentItem[] = [];

  canAdd: boolean = false;
  canEdit: boolean = false;
  canDelete: boolean = false;
  canUpload: boolean = false;
  canPreview: boolean = false;
  canArchive: boolean = false;
  canAccept: boolean = false;
  canReject: boolean = false;

  constructor(
    private router: Router,
    private correspondenceService: CorrespondenceService,
    private authenticationService: AuthenticationService,
    private menuService: MenuService,
    public infoDialogService: InfoDialogService,
    private dateFormatterService: DateFormatterService,
    private departmentService: DepartmentService
  ) {
    this.menuActions = this.menuService.getMenuActions("External");
    for (var action of this.menuActions) {
      if (action == "Add") {
        this.canAdd = true;
      } else if (action == "Edit") {
        this.canEdit = true;
      } else if (action == "Delete") {
        this.canDelete = true;
      } else if (action == "Upload") {
        this.canUpload = true;
      } else if (action == "Preview") {
        this.canPreview = true;
      } else if (action == "Archive") {
        this.canArchive = true;
      } else if (action == "Accept") {
        this.canAccept = true;
      } else if (action == "Reject") {
        this.canReject = true;
      }
    }
  }

  ngOnInit() {
    this.outgoingExternalDataSource = new ExternalOutgoingsDataSource(this.correspondenceService, this.router, this.infoDialogService, this.authenticationService);
    this.outgoingExternalDataSource.loadExternalOutgoings('all', 'outgoing', '', 'dsc', 0, 10);
    this.initDepartmentTree();
  }

  ngAfterViewInit() {
    this.paginator.page
        .pipe(
            tap(() => this.loadExternalOutgoingsPage())
        )
        .subscribe();
    
    fromEvent(this.input.nativeElement,'keyup')
        .pipe(
            debounceTime(150),
            filter((e: KeyboardEvent) => e.key === "Enter"),
            distinctUntilChanged(),
            tap(() => {
                this.paginator.pageIndex = 0;

                this.loadExternalOutgoingsPage();
            })
        )
        .subscribe();
  }

  loadExternalOutgoingsPage() {
    this.outgoingExternalDataSource.loadExternalOutgoings(
                      'all',
                      'outgoing',
                      this.input.nativeElement.value, 
                      'asc', 
                      this.paginator.pageIndex, 
                      this.paginator.pageSize);
  }

  ngOnDestroy() {
    this.subManager.unsubscribe();
  }

  initDepartmentTree() {
    this.loadingDeptTree = true;
    let getAllforTree = this.departmentService
      .getAllForTree()
      .pipe(first())
      .subscribe(
        (response) => {
          if (response.success) {    
            this.loadingDeptTree = false;
            this.departmentItems = response.data;            
          } else {
            this.message.title = "Get Department Tree";
            this.message.type = "error";
            this.message.content = response.errors;
            const infoDialogRef = this.infoDialogService.openInfoDialog(
              this.message
            );
            infoDialogRef.afterClosed().subscribe(() => {
              if (response.errors[0] == "Signature has expired") {
                this.authenticationService.logout_offline();
                this.router.navigate(["/login"]);
              }
            });
          }
        },
        (error) => {
          this.infoDialogService.openInfoDialog(error);
        }
      );
    this.subManager.add(getAllforTree);
  }

  returnDepartmentTree(dept_id){  
    var departments = this.departmentItems;
    for (var college of departments) {
      for (var department of college["children"]) {
        for (var office of department["children"]) {
          if (office.id == dept_id) {
            return college["code"] + " / " + department["code"] + " / " + office["description"];
          }
        }
      }
    }
  }

  addCorrespondence() {
    this.title = "Add Correspondence";
    const dialogRef = this.correspondenceService.openCorrespondenceDialog(
      this.type,
      this.title,
      null
    );
    const sub = dialogRef.componentInstance.onCorrespondenceSave.subscribe(
      (data) => {
        if (data) {
          data.loading = true;
          let create = this.correspondenceService
            .createExternal(data, "outgoing")
            .pipe(first())
            .subscribe(
              (response) => {
                if (response.success) {
                  this.message.title = this.title;
                  this.message.content = [response.message];
                  this.message.type = "success";
                  const infoDialogRef = this.infoDialogService.openInfoDialog(
                    this.message
                  );
                  infoDialogRef.afterClosed().subscribe(() => {
                    this.loadExternalOutgoingsPage();
                    data.close();
                  });
                } else {
                  this.message.title = this.title;
                  this.message.type = "error";
                  this.message.content = response.errors;
                  const infoDialogRef = this.infoDialogService.openInfoDialog(
                    this.message
                  );
                  infoDialogRef.afterClosed().subscribe(() => {
                    data.loading = false;
                    if (response.errors[0] == "Signature has expired") {
                      this.authenticationService.logout_offline();
                      this.router.navigate(["/login"]);
                    }
                  });
                }
              },
              (error) => {
                data.loading = false;
                this.infoDialogService.openInfoDialog(error);
              }
            );
          this.subManager.add(create);
        }
      }
    );
    dialogRef.afterClosed().subscribe(() => {
      sub.unsubscribe();
    });
  }

  addCorrespondenceCC(correspondence) {
    this.title = "Add Correspondence CC";
    const dialogRef = this.correspondenceService.openCorrespondenceCcDialog(
      this.canAdd,
      this.canEdit,
      this.canDelete,
      this.type,
      this.title,      
      correspondence,
      this.departmentItems,
      "outgoing"
    );
  }

  acceptCorrespondence(correspondence) {
    this.title = "Accept Correspondence";
    let updateStatus = this.correspondenceService
          .updateInternalStatus(correspondence, "Accepted")
          .pipe(first())
          .subscribe(
            (response) => {
              if (response.success) {
                this.message.title = this.title;
                  this.message.content = [response.message];
                  this.message.type = "success";
                  const infoDialogRef = this.infoDialogService.openInfoDialog(
                    this.message
                  );
                  infoDialogRef.afterClosed().subscribe(() => {
                    this.loadExternalOutgoingsPage();
                  });
              } else {
                this.message.title = this.title;
                this.message.type = "error";
                this.message.content = response.errors;
                const infoDialogRef = this.infoDialogService.openInfoDialog(
                  this.message
                );
                infoDialogRef.afterClosed().subscribe(() => {
                  if (response.errors[0] == "Signature has expired") {
                    this.authenticationService.logout_offline();
                    this.router.navigate(["/login"]);
                  }
                });
              }
            },
            (error) => {
              this.infoDialogService.openInfoDialog(error);
            }
          );
        this.subManager.add(updateStatus);    
  }

  rejectCorrespondence(correspondence){
    this.title = "Reject Correspondence";
    let updateStatus = this.correspondenceService
          .updateInternalStatus(correspondence, "Rejected")
          .pipe(first())
          .subscribe(
            (response) => {
              if (response.success) {
                this.message.title = this.title;
                  this.message.content = [response.message];
                  this.message.type = "success";
                  const infoDialogRef = this.infoDialogService.openInfoDialog(
                    this.message
                  );
                  infoDialogRef.afterClosed().subscribe(() => {
                    this.loadExternalOutgoingsPage();
                  });
              } else {
                this.message.title = this.title;
                this.message.type = "error";
                this.message.content = response.errors;
                const infoDialogRef = this.infoDialogService.openInfoDialog(
                  this.message
                );
                infoDialogRef.afterClosed().subscribe(() => {
                  if (response.errors[0] == "Signature has expired") {
                    this.authenticationService.logout_offline();
                    this.router.navigate(["/login"]);
                  }
                });
              }
            },
            (error) => {
              this.infoDialogService.openInfoDialog(error);
            }
          );
        this.subManager.add(updateStatus);   
  }

  editCorrespondence(correspondence) {
     this.title = "Edit Correspondence";
    const dialogRef = this.correspondenceService.openCorrespondenceDialog(
      this.type,
      this.title,
      null,
      correspondence
    );

    const sub = dialogRef.componentInstance.onCorrespondenceSave.subscribe(
      (data) => {
        if (data) {
          data.loading = true;
          let update = this.correspondenceService
            .updateExternal(correspondence, data, "outgoing")
            .pipe(first())
            .subscribe(
              (response) => {
                if (response.success) {
                  this.message.title = this.title;
                  this.message.content = [response.message];
                  this.message.type = "success";
                  const infoDialogRef = this.infoDialogService.openInfoDialog(
                    this.message
                  );
                  infoDialogRef.afterClosed().subscribe(() => {
                    this.loadExternalOutgoingsPage();
                    data.close();
                  });
                } else {
                  this.message.title = this.title;
                  this.message.type = "error";
                  this.message.content = response.errors;
                  const infoDialogRef = this.infoDialogService.openInfoDialog(
                    this.message
                  );
                  infoDialogRef.afterClosed().subscribe(() => {
                    data.loading = false;
                    if (response.errors[0] == "Signature has expired") {
                      this.authenticationService.logout_offline();
                      this.router.navigate(["/login"]);
                    }
                  });
                }
              },
              (error) => {
                data.loading = false;
                this.infoDialogService.openInfoDialog(error);
              }
            );
          this.subManager.add(update);
        }
      }
    );
  }

  uploadDocument(correspondence) {
    this.title = "Upload Document";
    const dialogRef = this.correspondenceService.openDocImagesDialog(
      this.menuActions.filter((s) => s.includes("Upload")),
      this.type,
      this.title,
      correspondence,
      "outgoing"
    );
  }

  previewCorrespondence(correspondence) {
    this.title = "Correspondence Preview";
    const dialogRef = this.correspondenceService.openDocPreviewerDialog(
      this.menuActions.filter((s) => s.includes("Preview")),
      this.type,
      this.title,
      correspondence,
      "outgoing"
    );
  }

}
