import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from "@angular/animations";
import { first } from "rxjs/operators";
import { debounceTime, distinctUntilChanged, tap, filter } from 'rxjs/operators';
import { Subscription } from "rxjs";
import { fromEvent } from "rxjs";
import { MatPaginator } from "@angular/material/paginator";
import { Router } from "@angular/router";

import { CorrespondenceService } from "src/app/_services/correspondence.service";
import { AuthenticationService } from "src/app/_services/authentication.service";
import { InfoDialogService } from "src/app/_services/info-dialog.service";
import { MenuService } from "src/app/_services/menu.service";
import { DepartmentService } from "src/app/_services/department.service";
import { DateFormatterService } from "src/app/_services/date-formatter.service";
import { CorrespondenceAssignmentService } from "src/app/_services/correspondence-assignment.service";
import { ConfirmationDialogService } from "src/app/_services/confirmation-dialog.service";

import { Message } from "src/app/_models/_helpers/message";
import { InternalCorrespondence } from "src/app/_models/internal-correspondence";
import { DepartmentItem } from "src/app/_models/department-item";
import { CorrespondenceCarbonCopy } from "src/app/_models/correspondence-carbon-copy";
import { CorrespondenceAssignment } from "src/app/_models/correspondence-assignment";
import { InternalDataSource } from "src/app/_datasource/internal.datasource";

@Component({
  selector: 'app-internal',
  templateUrl: './internal.component.html',
  styleUrls: ['./internal.component.css'],
  animations: [
    trigger("detailExpand", [
      state(
        "collapsed",
        style({ height: "0px", minHeight: "0", display: "none" })
      ),
      state("expanded", style({ height: "*" })),
      transition(
        "expanded <=> collapsed",
        animate("225ms cubic-bezier(0.4, 0.0, 0.2, 1)")
      ),
    ]),
  ]
})
export class InternalComponent implements OnInit, OnDestroy {
  
  subManager = new Subscription();

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('input') input: ElementRef;

  title: string;
  type: string = "Outgoing Internal";
  message: Message = new Message();

  internalCorrespondence: InternalCorrespondence = new InternalCorrespondence();
  internalCorrespondences: InternalCorrespondence[] = [];

  correspondenceCarbonCopy: CorrespondenceCarbonCopy = new CorrespondenceCarbonCopy();
  correspondenceCarbonCopies: CorrespondenceCarbonCopy [] = [];

  outgoingInternalDataSource: InternalDataSource;

  displayedColumns = [
    "referenceNo",
    "letterDate",
    "destination",
    "subject",
    "operations",
  ];

  expandedInternalCorrespondence: InternalCorrespondence | null;
  
  loading = false;
  internalResultsLength = 0;

  loadingDeptTree = false;

  menuActions: string[] = [];
  departmentItems: DepartmentItem[] = [];

  loadingAssignment = false;
  correspondenceAssignments: CorrespondenceAssignment[] = [];
  correspondenceAssignment: CorrespondenceAssignment = new CorrespondenceAssignment();

  canAdd: boolean = false;
  canEdit: boolean = false;
  canDelete: boolean = false;
  canUpload: boolean = false;
  canComment: boolean = false;
  canPreview: boolean = false;
  canRoute: boolean = false;

  constructor(
    private router: Router,
    private correspondenceService: CorrespondenceService,
    private authenticationService: AuthenticationService,
    private menuService: MenuService,
    public infoDialogService: InfoDialogService,
    private departmentService: DepartmentService,
    private dateFormatterService: DateFormatterService,
    private correspondenceAssignmentService: CorrespondenceAssignmentService,
    public confirmationDialogService: ConfirmationDialogService
  ) {
    this.menuActions = this.menuService.getMenuActions("Internal");
    for (var action of this.menuActions) {
      if (action == "Add") {
        this.canAdd = true;
      } else if (action == "Edit") {
        this.canEdit = true;
      } else if (action == "Delete") {
        this.canDelete = true;
      } else if (action == "Upload") {
        this.canUpload = true;
      } else if (action == "Comment") {
        this.canComment = true;
      } else if (action == "Preview") {
        this.canPreview = true;
      } else if (action == "Route") {
        this.canRoute = true;
      }
    }
  }

  ngOnInit() {
    this.outgoingInternalDataSource = new InternalDataSource(this.correspondenceService, this.router, this.infoDialogService, this.authenticationService);
    this.outgoingInternalDataSource.loadInternal('all', 'outgoing', '', '', 'dsc', 0, 10);
    this.initDepartmentTree();
  }

  ngAfterViewInit() {
    this.paginator.page
        .pipe(
            tap(() => this.loadInternalOutgoingsPage())
        )
        .subscribe();
    
    fromEvent(this.input.nativeElement,'keyup')
        .pipe(
            debounceTime(150),
            filter((e: KeyboardEvent) => e.key === "Enter"),
            distinctUntilChanged(),
            tap(() => {
                this.paginator.pageIndex = 0;

                this.loadInternalOutgoingsPage();
            })
        )
        .subscribe();
  }

  loadInternalOutgoingsPage() {
    this.outgoingInternalDataSource.loadInternal(
                      'all',
                      'outgoing', 
                      '', 
                      this.input.nativeElement.value, 
                      'asc', 
                      this.paginator.pageIndex, 
                      this.paginator.pageSize);
  }

  ngOnDestroy() {
    this.subManager.unsubscribe();
  }

  initDepartmentTree() {
    this.loadingDeptTree = true;
    let getAllforTree = this.departmentService
      .getAllForTree()
      .pipe(first())
      .subscribe(
        (response) => {
          if (response.success) {
            this.loadingDeptTree = false;
            this.departmentItems = response.data;
          } else {
            this.message.title = "Get Department Tree";
            this.message.type = "error";
            this.message.content = response.errors;
            const infoDialogRef = this.infoDialogService.openInfoDialog(
              this.message
            );
            infoDialogRef.afterClosed().subscribe(() => {
              if (response.errors[0] == "Signature has expired") {
                this.authenticationService.logout_offline();
                this.router.navigate(["/login"]);
              }
            });
          }
        },
        (error) => {
          this.infoDialogService.openInfoDialog(error);
        }
      );
    this.subManager.add(getAllforTree);
  }

  returnDepartmentTree(dept_id){
    var colleges = this.departmentItems;
    if (colleges != undefined){
      for (var college of colleges) {
        for (var value of college["children"]){
          for (var value2 of value["children"]){
            if (value2.id == dept_id) {
              return college["code"] + " / " + value["code"] + " / " + value2["description"];
            }
          }            
        }          
      }
    }  
  }

  addCorrespondence() {
    this.title = "Add Correspondence";
    const dialogRef = this.correspondenceService.openCorrespondenceDialog(
      this.type,
      this.title,
      this.departmentItems
    );

    const sub = dialogRef.componentInstance.onCorrespondenceSave.subscribe(
      (data) => {
        if (data) {
          data.loading = true;
          let create = this.correspondenceService
            .createInternal(data, "outgoing")
            .pipe(first())
            .subscribe(
              (response) => {
                if (response.success) {
                  this.message.title = this.title;
                  this.message.content = [response.message];
                  this.message.type = "success";
                  const infoDialogRef = this.infoDialogService.openInfoDialog(
                    this.message
                  );
                  infoDialogRef.afterClosed().subscribe(() => {                    
                    this.loadInternalOutgoingsPage();
                    data.close();
                  });
                } else {
                  this.message.title = this.title;
                  this.message.type = "error";
                  this.message.content = response.errors;
                  const infoDialogRef = this.infoDialogService.openInfoDialog(
                    this.message
                  );
                  infoDialogRef.afterClosed().subscribe(() => {
                    data.loading = false;
                    if (response.errors[0] == "Signature has expired") {
                      this.authenticationService.logout_offline();
                      this.router.navigate(["/login"]);
                    }
                  });
                }
              },
              (error) => {
                data.loading = false;
                this.infoDialogService.openInfoDialog(error);
              }
            );
          this.subManager.add(create);
        }
      }
    );
    dialogRef.afterClosed().subscribe(() => {
      sub.unsubscribe();
    });
  }

  addCorrespondenceCC(correspondence) {
    this.title = "Add Correspondence CC";
    const dialogRef = this.correspondenceService.openCorrespondenceCcDialog(
      this.canAdd,
      this.canEdit,
      this.canDelete,
      this.type,
      this.title,      
      correspondence,
      this.departmentItems
    );
  }

  editCorrespondence(correspondence) {
    this.title = "Edit Correspondence";
    const dialogRef = this.correspondenceService.openCorrespondenceDialog(
      this.type,
      this.title,
      this.departmentItems,
      correspondence
    );

    const sub = dialogRef.componentInstance.onCorrespondenceSave.subscribe(
      (data) => {
        if (data) {
          data.loading = true;
          let update = this.correspondenceService
            .updateInternal(correspondence, data, "outgoing")
            .pipe(first())
            .subscribe(
              (response) => {
                if (response.success) {
                  this.message.title = this.title;
                  this.message.content = [response.message];
                  this.message.type = "success";
                  const infoDialogRef = this.infoDialogService.openInfoDialog(
                    this.message
                  );
                  infoDialogRef.afterClosed().subscribe(() => {
                    this.loadInternalOutgoingsPage();
                    data.close();
                  });
                } else {
                  this.message.title = this.title;
                  this.message.type = "error";
                  this.message.content = response.errors;
                  const infoDialogRef = this.infoDialogService.openInfoDialog(
                    this.message
                  );
                  infoDialogRef.afterClosed().subscribe(() => {
                    data.loading = false;
                    if (response.errors[0] == "Signature has expired") {
                      this.authenticationService.logout_offline();
                      this.router.navigate(["/login"]);
                    }
                  });
                }
              },
              (error) => {
                data.loading = false;
                this.infoDialogService.openInfoDialog(error);
              }
            );
          this.subManager.add(update);
        }
      }
    );
  }

  uploadDocument(correspondence) {
    const dialogRef = this.correspondenceService.openDocImagesDialog(
      this.menuActions.filter((s) => s.includes("Upload")),      
      this.type,
      "Upload Document",
      correspondence
    );
  }

  previewCorrespondence(correspondence) {
    this.title = "Correspondence Preview";
    const dialogRef = this.correspondenceService.openDocPreviewerDialog(
      this.menuActions.filter((s) => s.includes("Preview")),
      this.type,
      this.title,
      correspondence,
      "outgoing"
    );
  }

  assignCorrespondence(correspondence) {
    const dialogRef = this.correspondenceService.openAssignCorrespondenceDialog(
      this.menuActions.filter((s) => s.includes("Route")),
      this.type,
      "Assign Correspondence",
      correspondence,
      this.departmentItems
    );
  }

  assignCorrespondenceAssignmentValue (value) {
    let correspondenceAssignment: CorrespondenceAssignment = new CorrespondenceAssignment();
    correspondenceAssignment = {
          id: value["id"],
          fromUser: value["from_user"],
          toUser: value["to_user"],
          correspondence: value["correspondence"],
          order: value["order"],
          assignedDate: value["assigned_date"],
          receivedDate: value["received_date"],
          status: value["status"],
          rejectionRemark: value["rejection_remark"]
        };
    return correspondenceAssignment;
  }

  previewCorrespondenceAssignmentHistory(correspondence) {
    this.loadingAssignment = true;
    this.correspondenceAssignments = [];
    let getRoutingDetails = this.correspondenceAssignmentService
      .getAll(correspondence.id, "internal")
      .pipe(first())
      .subscribe(
        (response) => {
          if (response.success) {
            for (var value of response.data) {
              this.correspondenceAssignment = this.assignCorrespondenceAssignmentValue(value);
              this.correspondenceAssignments.push(
                this.correspondenceAssignment
              );              
            }
            this.loadingAssignment = false;

            this.title = "Preview Assignment";
            const dialogRef = this.correspondenceAssignmentService.openAssignmentPreviewDialog(
              this.title,
              this.correspondenceAssignments
            );

          } else {
            this.message.title = "Correspondence Assignments";
            this.message.type = "error";
            this.message.content = response.errors;
            const infoDialogRef = this.infoDialogService.openInfoDialog(
              this.message
            );
            infoDialogRef.afterClosed().subscribe(() => {
              if (response.errors[0] == "Signature has expired") {                
                this.authenticationService.logout_offline();
                this.router.navigate(["/login"]);
              }
            });            
          }
        },
        (error) => {
          this.loadingAssignment = false;
          this.infoDialogService.openInfoDialog(error);
        }
      );
    this.subManager.add(getRoutingDetails);    
  }

  commentCorrespondence(correspondence) {
    const dialogRef = this.correspondenceService.openDocCommentsDialog(
      this.menuActions.filter((s) => s.includes("Comment")),
      this.type,
      "Correspondence Comments",
      correspondence
    );
  }

  sendCorrespondence(correspondence) {
    this.message.title = "Confirmation";
    this.message.type = "success";
    var message = 'Do you confirm SENDING the correspondence with reference no: "' + correspondence.referenceNo + '"?';
    this.message.content = [message];
    const confirmationDialogRef = this.confirmationDialogService.openConfirmationDialog(
      this.message
    );

    confirmationDialogRef.afterClosed().subscribe(result => {
      this.title = "Send Correspondence";
      if(result) {
        let updateStatus = this.correspondenceService
          .updateInternalStatus(correspondence, "Sent")
          .pipe(first())
          .subscribe(
            (response) => {
              if (response.success) {
                this.message.title = this.title;
                  this.message.content = [response.message];
                  this.message.type = "success";
                  const infoDialogRef = this.infoDialogService.openInfoDialog(
                    this.message
                  );
                  infoDialogRef.afterClosed().subscribe(() => {
                    this.loadInternalOutgoingsPage();
                  });
              } else {
                this.message.title = this.title;
                this.message.type = "error";
                this.message.content = response.errors;
                const infoDialogRef = this.infoDialogService.openInfoDialog(
                  this.message
                );
                infoDialogRef.afterClosed().subscribe(() => {
                  if (response.errors[0] == "Signature has expired") {
                    this.authenticationService.logout_offline();
                    this.router.navigate(["/login"]);
                  }
                });
              }
            },
            (error) => {
              this.infoDialogService.openInfoDialog(error);
            }
          );
        this.subManager.add(updateStatus);
      }
    });
  }

}
