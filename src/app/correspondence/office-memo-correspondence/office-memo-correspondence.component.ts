import { Component, OnInit, ViewChild } from '@angular/core';
import { Subscription } from "rxjs";
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

import { Message } from 'src/app/_models/_helpers/message';
import { OfficeMemo } from "src/app/_models/office-memo";

@Component({
  selector: 'app-office-memo-correspondence',
  templateUrl: './office-memo-correspondence.component.html',
  styleUrls: ['./office-memo-correspondence.component.css']
})
export class OfficeMemoCorrespondenceComponent implements OnInit {

  subManager = new Subscription();

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  title: string;
  message: Message = new Message();

  memos: OfficeMemo[] = [];
  memo: OfficeMemo = new OfficeMemo();

	displayedColumns: string[] = ['date', 'subject', 'content', 'operations'];
  dataSource = new MatTableDataSource<OfficeMemo>();

	resultsLength = 0;
  isLoadingResults = false;

  constructor() { }

  ngOnInit() {
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  addOfficeMemo() {}

}
