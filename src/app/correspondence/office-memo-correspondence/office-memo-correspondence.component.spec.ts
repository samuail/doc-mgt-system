import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OfficeMemoCorrespondenceComponent } from './office-memo-correspondence.component';

describe('OfficeMemoCorrespondenceComponent', () => {
  let component: OfficeMemoCorrespondenceComponent;
  let fixture: ComponentFixture<OfficeMemoCorrespondenceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OfficeMemoCorrespondenceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OfficeMemoCorrespondenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
