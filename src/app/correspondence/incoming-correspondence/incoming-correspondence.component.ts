import {
  Component,
  OnInit,
  OnDestroy,
  ViewChild,
  ViewChildren,
  QueryList,
} from "@angular/core";
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from "@angular/animations";
import { first } from "rxjs/operators";
import { Subscription } from "rxjs";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { MatTableDataSource, MatTable } from "@angular/material/table";
import { MatTabChangeEvent } from "@angular/material/tabs";
import { Router } from "@angular/router";

import { CorrespondenceService } from "src/app/_services/correspondence.service";
import { AuthenticationService } from "src/app/_services/authentication.service";
import { InfoDialogService } from "src/app/_services/info-dialog.service";

import { Message } from "src/app/_models/_helpers/message";

import { Correspondence } from "src/app/_models/correspondence";
import { InternalCorrespondence } from "src/app/_models/internal-correspondence";
import { ExternalIncomingCorrespondence } from "src/app/_models/external-incoming-correspondence";

@Component({
  selector: "app-incoming-correspondence",
  templateUrl: "./incoming-correspondence.component.html",
  styleUrls: ["./incoming-correspondence.component.css"],
  animations: [
    trigger("detailExpand", [
      state(
        "collapsed",
        style({ height: "0px", minHeight: "0", display: "none" })
      ),
      state("expanded", style({ height: "*" })),
      transition(
        "expanded <=> collapsed",
        animate("225ms cubic-bezier(0.4, 0.0, 0.2, 1)")
      ),
    ]),
  ],
})
export class IncomingCorrespondenceComponent implements OnInit, OnDestroy {
  subManager = new Subscription();

  // @ViewChild(MatPaginator) internalPaginator: MatPaginator;
  // @ViewChild(MatPaginator) externalPaginator: MatPaginator;
  // @ViewChild(MatSort) sort: MatSort;
  // @ViewChild(MatTable) table: MatTable<any>;

  @ViewChild("internalPaginator") internalPaginator: MatPaginator;
  @ViewChild("externalPaginator") externalPaginator: MatPaginator;
  @ViewChild("paginator") paginator: MatPaginator;
  @ViewChild("paginator2") paginator2: MatPaginator;

  // @ViewChildren(MatPaginator) paginator = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sort = new QueryList<MatSort>();
  @ViewChildren(MatTable) table = new QueryList<MatTable<any>>();

  title: string;
  type: string = "Internal";
  message: Message = new Message();

  correspondences: Correspondence[] = [];
  correspondence: Correspondence = new Correspondence();

  internalCorrespondence: InternalCorrespondence = new InternalCorrespondence();
  internalCorrespondences: InternalCorrespondence[] = [];

  incomingExternalCorrespondence: ExternalIncomingCorrespondence = new ExternalIncomingCorrespondence();
  incomingExternalCorrespondences: ExternalIncomingCorrespondence[] = [];

  // dataSource = new MatTableDataSource<Correspondence>();
  incomingInternalDataSource = new MatTableDataSource<InternalCorrespondence>();
  incomingExternalDataSource = new MatTableDataSource<
    ExternalIncomingCorrespondence
  >();

  displayedColumns = [
    "referenceNo",
    "letterDate",
    "source",
    "receivedDate",
    "operations",
  ];
  expandedInternalCorrespondence: InternalCorrespondence | null;
  expandedExternalCorrespondence: ExternalIncomingCorrespondence | null;

  loading = false;
  internalResultsLength = 0;
  externalResultsLength = 0;

  constructor(
    private router: Router,
    private correspondenceService: CorrespondenceService,
    private authenticationService: AuthenticationService,
    public infoDialogService: InfoDialogService
  ) {}

  ngOnInit(): void {
    this.getInternalAll();
    this.getExternalAll();
  }

  ngAfterViewInit() {
    // this.dataSource.paginator = this.paginator;
    // this.dataSource.sort = this.sort;
    this.incomingInternalDataSource.paginator = this.paginator;
    // this.incomingInternalDataSource.paginator = this.internalPaginator;
    // this.incomingInternalDataSource.paginator = this.paginator.toArray()[0];
    // this.incomingInternalDataSource.sort = this.sort;
    this.incomingInternalDataSource.sort = this.sort.toArray()[0];
    this.incomingExternalDataSource.paginator = this.paginator2;
    // this.incomingExternalDataSource.paginator = this.paginator.toArray()[1];
    // this.incomingExternalDataSource.sort = this.sort;
    this.incomingExternalDataSource.sort = this.sort.toArray()[1];
  }

  ngOnDestroy() {
    this.subManager.unsubscribe();
  }

  applyFilter(filterValue: string) {
    this.incomingInternalDataSource.filter = filterValue.trim().toLowerCase();
    this.incomingExternalDataSource.filter = filterValue.trim().toLowerCase();
  }

  incomingTabChange = (tabChangeEvent: MatTabChangeEvent): void => {
    if (tabChangeEvent.index == 0) {
      this.type = "Internal";
    } else {
      this.type = "External";
    }
  };

  _setDataSource(indexNumber) {
    setTimeout(() => {
      switch (indexNumber) {
        case 0:
          !this.incomingInternalDataSource.paginator
            ? (this.incomingInternalDataSource.paginator = this.paginator)
            : null;
          this.type = "Internal";
          break;
        case 1:
          !this.incomingExternalDataSource.paginator
            ? (this.incomingExternalDataSource.paginator = this.paginator2)
            : null;
          this.type = "External";
      }
    });
  }

  getInternalAll(): void {
    this.loading = true;
    let getAll = this.correspondenceService
      .getInternalAll("incoming")
      .pipe(first())
      .subscribe(
        (response) => {
          if (response.success) {
            for (var value of response.data) {
              this.internalCorrespondence = {
                id: value["id"],
                referenceNo: value["reference_no"],
                letterDate: value["letter_date"],
                subject: value["subject"],
                keyWord: value["search_keywords"],
                source: value["source"],
                destination: value["destination"],
                receivedDate: value["received_date"],
                status: value["status"],
              };
              this.internalCorrespondences.push(this.internalCorrespondence);
            }
            this.loading = false;
            this.internalResultsLength = response.total;
            this.incomingInternalDataSource.data = this.internalCorrespondences;
            // this.incomingInternalDataSource.paginator = this.paginator;
            // this.incomingInternalDataSource.paginator = this.paginator.toArray()[0];
            // this.incomingInternalDataSource.sort = this.sort.toArray()[0];
          } else {
            this.message.title = "Incoming Correspondences";
            this.message.type = "error";
            this.message.content = response.errors;
            const infoDialogRef = this.infoDialogService.openInfoDialog(
              this.message
            );
            infoDialogRef.afterClosed().subscribe(() => {
              if (response.errors[0] == "Signature has expired") {
                this.authenticationService.logout_offline();
                this.router.navigate(["/login"]);
              }
            });
          }
        },
        (error) => {
          this.loading = false;
          this.infoDialogService.openInfoDialog(error);
        }
      );
    this.subManager.add(getAll);
  }

  getExternalAll(): void {
    this.loading = true;
    let getAll = this.correspondenceService
      .getExternalAll("incoming")
      .pipe(first())
      .subscribe(
        (response) => {
          if (response.success) {
            for (var value of response.data) {
              this.incomingExternalCorrespondence = {
                id: value["id"],
                referenceNo: value["reference_no"],
                letterDate: value["letter_date"],
                subject: value["subject"],
                keyWord: value["search_keywords"],
                source: value["source"],
                destination: value["destination"],
                receivedDate: value["received_date"],
                status: value["status"],
              };
              this.incomingExternalCorrespondences.push(
                this.incomingExternalCorrespondence
              );
            }
            this.loading = false;
            this.externalResultsLength = response.total;
            this.incomingExternalDataSource.data = this.incomingExternalCorrespondences;
            // this.incomingExternalDataSource.paginator = this.paginator2;
            // this.incomingExternalDataSource.paginator = this.paginator.toArray()[1];
            // this.incomingExternalDataSource.sort = this.sort.toArray()[1];
          } else {
            this.message.title = "Outgoing Correspondences";
            this.message.type = "error";
            this.message.content = response.errors;
            const infoDialogRef = this.infoDialogService.openInfoDialog(
              this.message
            );
            infoDialogRef.afterClosed().subscribe(() => {
              if (response.errors[0] == "Signature has expired") {
                this.authenticationService.logout_offline();
                this.router.navigate(["/login"]);
              }
            });
          }
        },
        (error) => {
          this.loading = false;
          this.infoDialogService.openInfoDialog(error);
        }
      );
    this.subManager.add(getAll);
  }

  addCorrespondence() {
    this.title = "Add Correspondence";
    const dialogRef = this.correspondenceService.openCorrespondenceDialog(
      this.type,
      this.title,
      new Correspondence()
    );
    const sub = dialogRef.componentInstance.onCorrespondenceSave.subscribe(
      (data) => {
        if (data) {
          data.loading = true;
          if (data.form.value.type == "Internal") {
            let create = this.correspondenceService
              .createInternal(data.form.value, "")
              .pipe(first())
              .subscribe(
                (response) => {
                  if (response.success) {
                    this.message.title = this.title;
                    this.message.content = [response.message];
                    this.message.type = "success";
                    const infoDialogRef = this.infoDialogService.openInfoDialog(
                      this.message
                    );
                    infoDialogRef.afterClosed().subscribe(() => {
                      this.internalCorrespondence = {
                        id: response.data["id"],
                        referenceNo: response.data["reference_no"],
                        letterDate: response.data["letter_date"],
                        subject: response.data["subject"],
                        keyWord: response.data["search_keywords"],
                        source: response.data["source"],
                        destination: response.data["destination"],
                        receivedDate: response.data["received_date"],
                        status: response.data["status"],
                      };
                      this.internalCorrespondences.push(
                        this.internalCorrespondence
                      );
                      this.internalResultsLength =
                        this.internalResultsLength + 1;
                      this.incomingInternalDataSource = new MatTableDataSource(
                        this.internalCorrespondences
                      );
                      this.incomingInternalDataSource.paginator = this.internalPaginator;
                      // this.incomingInternalDataSource.paginator = this.paginator.toArray()[0];
                      this.incomingInternalDataSource.sort = this.sort.toArray()[0];
                      this.table.toArray()[0].renderRows();
                      data.close();
                    });
                  } else {
                    this.message.title = this.title;
                    this.message.type = "error";
                    this.message.content = response.errors;
                    const infoDialogRef = this.infoDialogService.openInfoDialog(
                      this.message
                    );
                    infoDialogRef.afterClosed().subscribe(() => {
                      data.loading = false;
                      if (response.errors[0] == "Signature has expired") {
                        this.authenticationService.logout_offline();
                        this.router.navigate(["/login"]);
                      }
                    });
                  }
                },
                (error) => {
                  data.loading = false;
                  this.infoDialogService.openInfoDialog(error);
                }
              );
            this.subManager.add(create);
          } else {
            let create = this.correspondenceService
              .createExternalIncoming(data.form.value)
              .pipe(first())
              .subscribe(
                (response) => {
                  if (response.success) {
                    this.message.title = this.title;
                    this.message.content = [response.message];
                    this.message.type = "success";
                    const infoDialogRef = this.infoDialogService.openInfoDialog(
                      this.message
                    );
                    infoDialogRef.afterClosed().subscribe(() => {
                      this.incomingExternalCorrespondence = {
                        id: response.data["id"],
                        referenceNo: response.data["reference_no"],
                        letterDate: response.data["letter_date"],
                        subject: response.data["subject"],
                        keyWord: response.data["search_keywords"],
                        source: response.data["source"],
                        destination: response.data["destination"],
                        receivedDate: response.data["received_date"],
                        status: response.data["status"],
                      };
                      this.incomingExternalCorrespondences.push(
                        this.incomingExternalCorrespondence
                      );
                      this.externalResultsLength =
                        this.externalResultsLength + 1;
                      this.incomingExternalDataSource = new MatTableDataSource(
                        this.incomingExternalCorrespondences
                      );
                      this.incomingExternalDataSource.paginator = this.externalPaginator;
                      // this.incomingExternalDataSource.paginator = this.paginator.toArray()[1];
                      this.incomingExternalDataSource.sort = this.sort.toArray()[1];
                      this.table.toArray()[1].renderRows();
                      data.close();
                    });
                  } else {
                    this.message.title = this.title;
                    this.message.type = "error";
                    this.message.content = response.errors;
                    const infoDialogRef = this.infoDialogService.openInfoDialog(
                      this.message
                    );
                    infoDialogRef.afterClosed().subscribe(() => {
                      data.loading = false;
                      if (response.errors[0] == "Signature has expired") {
                        this.authenticationService.logout_offline();
                        this.router.navigate(["/login"]);
                      }
                    });
                  }
                },
                (error) => {
                  data.loading = false;
                  this.infoDialogService.openInfoDialog(error);
                }
              );
            this.subManager.add(create);
          }
        }
      }
    );
    dialogRef.afterClosed().subscribe(() => {
      sub.unsubscribe();
    });
  }

  editCorrespondence(correspondence) {
    const dialogRef = this.correspondenceService.openCorrespondenceDialog(
      this.type,
      "Edit Correspondence",
      correspondence
    );
  }

  uploadDocument(correspondence) {
    const dialogRef = this.correspondenceService.openDocImagesDialog(
      "",
      "Incoming",
      this.type,
      "Upload Document",
      correspondence
    );
  }

  // commentCorrespondence(correspondence) {
  //   const dialogRef = this.correspondenceService.openDocCommentsDialog(
  //     "Correspondence Comments",
  //     correspondence
  //   );
  // }

  // previewCorrespondence(correspondence) {
  //   const dialogRef = this.correspondenceService.openDocPreviewerDialog(
  //     "Correspondence Preview",
  //     correspondence
  //   );
  // }

  // assignCorrespondence(correspondence) {
  //   const dialogRef = this.correspondenceService.openAssignCorrespondenceDialog(
  //     "Assign Correspondence",
  //     correspondence
  //   );
  // }

  deleteCorrespondence(correspondence) {}
}
