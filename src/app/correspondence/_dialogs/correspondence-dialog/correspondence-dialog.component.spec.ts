import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CorrespondenceDialogComponent } from './correspondence-dialog.component';

describe('CorrespondenceDialogComponent', () => {
  let component: CorrespondenceDialogComponent;
  let fixture: ComponentFixture<CorrespondenceDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CorrespondenceDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CorrespondenceDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
