import {
  Component,
  OnInit,
  AfterViewInit,
  Inject,
  EventEmitter,
  OnDestroy,
  ElementRef,
  ViewChild
} from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
import { first } from "rxjs/operators";
import { Subscription } from "rxjs";
import { Router } from "@angular/router";

import { Department } from "src/app/_models/department";
import { DepartmentItem } from "src/app/_models/department-item";
import { Organization } from "src/app/_models/organization";
import { DepartmentRole } from "src/app/_models/department-role";

import { AuthenticationService } from "src/app/_services/authentication.service";
import { InfoDialogService } from "src/app/_services/info-dialog.service";
import { DepartmentService } from "src/app/_services/department.service";
import { OrganizationService } from "src/app/_services/organization.service";

declare var jQuery: any;
@Component({
  selector: "app-correspondence-dialog",
  templateUrl: "./correspondence-dialog.component.html",
  styleUrls: ["./correspondence-dialog.component.css"],
})
export class CorrespondenceDialogComponent implements OnInit, OnDestroy, AfterViewInit {

  @ViewChild('letterDate', { static: true }) input: ElementRef;

  loading = false;
  form: FormGroup;
  title: string;
  type: string;

  subManager = new Subscription();

  correspondence: any;
  department: Department = new Department();
  departments: Department[] = [];
  subDepartment: Department = new Department();
  subDepartments: Department[] = [];

  departmentByType: Department = new Department();
  departmentsByType: Department[] = [];
  
  departmentItems: DepartmentItem[] = [];
  collegeDepartmentItems: DepartmentItem[] = [];

  organization: Organization = new Organization();
  organizations: Organization[] = [];

  userDepartment: DepartmentRole = new DepartmentRole();

  types: any[] = [
    { value: "1", viewValue: "Center" },
    { value: "2", viewValue: "College" },
    { value: "3", viewValue: "Outside AAU" },
    { value: "4", viewValue: "Outgoing Internal"},
    { value: "5", viewValue: "Outgoing External"}
  ];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<CorrespondenceDialogComponent>,
    private departmentService: DepartmentService,
    private organizationService: OrganizationService,
    private router: Router,
    private authenticationService: AuthenticationService,
    public infoDialogService: InfoDialogService
  ) {    
    this.type = data.type;
    this.title = data.title;
    this.departmentItems = data.departmentItems;
    this.correspondence = data.correspondence;
    
    if (this.type == "Outside AAU" || this.type == "Outgoing External") {
      this.initOrganizations();
    }    
    
    this.authenticationService.userDepartment.subscribe(
      (userDepartment) => (this.userDepartment = userDepartment)
    );

    if (this.type == "Center") {
      this.isCenter = true;
      this.isCollege = false;
      this.isExternal = false;
      this.isOutgoingInternal = false;
      this.isOutgoingExternal = false;
      this.isSource = true;
      this.isDestination = false;
    } else if (this.type == "College") {
      this.isCenter = false;
      this.isCollege = true;
      this.isExternal = false;
      this.isOutgoingInternal = false;
      this.isOutgoingExternal = false;
      this.isSource = true;
      this.isDestination = false;
    } else if (this.type == "Outside AAU"){
      this.isCenter = false;
      this.isCollege = false;
      this.isExternal = true;
      this.isOutgoingInternal = false;
      this.isOutgoingExternal = false;
      this.isSource = true;
      this.isDestination = false;
    } else if (this.type == "Outgoing Internal"){
      this.isCenter = false;
      this.isCollege = false;
      this.isExternal = false;
      this.isOutgoingInternal = true;
      this.isOutgoingExternal = false;
      this.isSource = false;
      this.isDestination = true;
    } else if (this.type == "Outgoing External"){
      this.isCenter = false;
      this.isCollege = false;
      this.isExternal = false;
      this.isOutgoingInternal = false;
      this.isOutgoingExternal = true;
      this.isSource = false;
      this.isDestination = true;
    }    
  }

  ngOnInit() {
    if (this.title == "Add Correspondence") {
      if (this.type == "Center" || this.type == "College" || this.type == "Outgoing Internal") {
        this.form = this.fb.group({
          type: [this.type, Validators.required],
          referenceNo: ["", Validators.required],
          letterDate: ["", null],
          letterDateEt: ["", Validators.required],
          subject: ["", Validators.required],
          from0: ["", [Validators.required]],
          from1: ["", [Validators.required]],
          from: ["", [Validators.required]],
          keyWord: ["", null],
          hasCc: ["", null]
        });
      }  else if (this.type == "Outside AAU" || this.type == "Outgoing External") {
        this.form = this.fb.group({
          type: [this.type, Validators.required],
          referenceNo: ["", Validators.required],
          letterDate: ["", null],
          letterDateEt: ["", Validators.required],
          subject: ["", Validators.required],
          from: ["", [Validators.required]],
          keyWord: ["", null],
          hasCc: ["", null]
        });
      }       
    } else if (this.title == "Edit Correspondence") {
      var from_id;
      if (this.type == "Center" || this.type == "College" || this.type == "Outside AAU"){
        from_id = this.correspondence.source.id;         
      } else if (this.type == "Outgoing Internal" || this.type == "Outgoing External"){
        from_id = this.correspondence.destination.id;
      }

      var from0_id;
      var from1_id;
      if (this.type == "Center") {
        let college = this.departmentItems[0];
        from0_id = college["id"];
        for (var value of college["children"]) {
          for (var value2 of value["children"]){
            if (value2.id == from_id) {
              from1_id = value.id;
            }
          }          
        }
      } else if(this.type == "College"){
        let colleges = this.departmentItems;
        for (var college of colleges) {
          for (var value of college["children"]){
            for (var value2 of value["children"]){
              if (value2.id == from_id) {
                from0_id = college.id;
                from1_id = value.id;
              }
            }            
          }          
        }
      } else if (this.type == "Outgoing Internal") {        
        let colleges = this.departmentItems;
        for (var college of colleges) {
          for (var value of college["children"]){
            for (var value2 of value["children"]){
              if (value2.id == from_id) {
                from0_id = college.id;
                from1_id = value.id;
              }
            }            
          }          
        }
      }

      this.onCollegeSelect(from0_id);
      this.onDepartmentSelect(from1_id);

      var date = this.correspondence.letterDateEC;
      var letter_dateEt;
      var year = date.substr(6,4);
      var month;
      var day;
      if (date.length == 10) {
        day = date.substr(0,2);
        month = date.substr(3,2);
      } else if (date.length == 8) {
        day = "0" + date.substr(7,1);
        month = "0" + date.substr(5,1);
      } else {
        var month_reg = date.substr(5,2).match("[0-9.]*")[0];
        if (month_reg.length == 1) {
          month = "0" + month_reg;
          day = date.substr(7,2);
        } else {
          month = month_reg;
          day = "0" + date.substr(8,1);
        }
      }
      letter_dateEt = day + "/" + month + "/" + year;

      var has_css = this.correspondence.hasCc ? true : "";

      if (this.type == "Center" || this.type == "College" || this.type == "Outgoing Internal") {
        this.form = this.fb.group({
          type: [this.type, Validators.required],
          referenceNo: [this.correspondence.referenceNo, Validators.required],
          letterDate: [this.correspondence.letterDate, null],
          letterDateEt: [letter_dateEt, Validators.required],
          subject: [this.correspondence.subject, Validators.required],
          from0: [from0_id, [Validators.required]],
          from1: [from1_id, [Validators.required]],
          from: [from_id, [Validators.required]],
          keyWord: [this.correspondence.keyWord.toString(), null],
          hasCc: [has_css, null]
        });
      }  else if (this.type == "Outside AAU" || this.type == "Outgoing External") {
        this.form = this.fb.group({
          type: [this.type, Validators.required],
          referenceNo: [this.correspondence.referenceNo, Validators.required],
          letterDate: [this.correspondence.letterDate, null],
          letterDateEt: [letter_dateEt, Validators.required],
          subject: [this.correspondence.subject, Validators.required],
          from: [from_id, [Validators.required]],
          keyWord: [this.correspondence.keyWord.toString(), null],
          hasCc: [has_css, null]
        });
      }
    } else if (this.title == "Upload Document") {
      this.form = this.fb.group({
        imagePath: [this.correspondence.imagePath, Validators.required],
      });
    }
  }

  ngAfterViewInit() {
    jQuery("#letter_date").calendarsPicker({
      calendar: jQuery.calendars.instance('ethiopian','am'),
      dateFormat: "dd/mm/yyyy",
      onClose: (dateText, inst) => {        
        if ((dateText.length > 0) && (jQuery("#letter_date").val() != "")){        
          var month = dateText[0]["_month"] <= 9 ? "0" + dateText[0]["_month"] : dateText[0]["_month"];
          var day = dateText[0]["_day"] <= 9 ? "0" + dateText[0]["_day"] : dateText[0]["_day"];
          var date = day + "/" + month + "/" + dateText[0]["_year"];
          this.form.controls['letterDateEt'].setValue(date);
        } else if(jQuery('.letter_date').val() != ""){
          var txtVal = jQuery('.letter_date').val();
          if (isDate(txtVal)) {  
            this.form.controls['letterDateEt'].setValue(txtVal);         
          } else {
            this.form.controls['letterDateEt'].setValue(txtVal);
            this.form.controls['letterDateEt'].setErrors({ format: true }); 
          }
        } else {
          jQuery(".letter_date").val("");
          this.form.controls['letterDateEt'].setValue("");  
        }
      }
    });

    jQuery(".letter_date_btn").mouseover(function() {        
        jQuery(this).css('cursor', 'pointer');        
    });

    function isDate(txt_Idt) {
      var currVal = txt_Idt;
      if (currVal == '') return false;

      var rxDatePattern = /^(\d{1,2})\/(\d{1,2})\/(\d{4})$/;
      var dtArray = currVal.match(rxDatePattern); // is format OK?

      if (dtArray == null) return false;

      let dtMonth = dtArray[2];
      let dtDay = dtArray[1];
      let dtYear = dtArray[3];

      if (dtMonth < 1 || dtMonth > 13) return false;
      else if (dtDay < 1 || dtDay > 30) return false;
      else if (dtMonth == 13) {
        var isleap = (5500 + dtYear) % 4 == 3;
        if (dtDay > 6 || (dtDay == 6 && !isleap)) return false;
      }
      return true;
    }    
  }

  ngOnDestroy() {
    this.subManager.unsubscribe();
  }

  isGCCalendar: boolean = false;

  changeCalendar(event) {
    this.isGCCalendar = event.checked; 
    if(this.isGCCalendar) {
      this.form.get('letterDate').setValidators([Validators.required]);
      this.form.get('letterDate').updateValueAndValidity();

      this.form.get('letterDateEt').clearValidators();
      this.form.get('letterDateEt').updateValueAndValidity();
    } else {
      this.form.get('letterDate').clearValidators();
      this.form.get('letterDate').updateValueAndValidity();

      this.form.get('letterDateEt').setValidators([Validators.required]);
      this.form.get('letterDateEt').updateValueAndValidity();
    }  
  }

  checkSelection(value) {
    return this.type === value ? false : true;
  }

  selectedFiles: any;

  selectFile(event) {
    this.selectedFiles = event.target.files;
  }

  onCorrespondenceSave = new EventEmitter();

  save() {
    if (this.form.invalid) {
      return;
    }
    this.onCorrespondenceSave.emit(this);
  }

  close() {
    this.dialogRef.close();
  }

  isCenter: boolean = true;
  isCollege: boolean = false;
  isExternal: boolean = false;
  isOutgoingInternal: boolean = false;
  isOutgoingExternal: boolean = false;

  isSource: boolean = false;
  isDestination: boolean = false;

  changeType(event) {
    if (event.value == "Center") {
      this.isCenter = true;
      this.isCollege = false;
      this.isExternal = false;
      this.isOutgoingInternal = false;
      this.isOutgoingExternal = false;
    } else if (event.value == "College") {
      this.isCenter = false;
      this.isCollege = true;
      this.isExternal = false;
      this.isOutgoingInternal = false;
      this.isOutgoingExternal = false;
    } else if (event.value == "Outside AAU"){
      this.isCenter = false;
      this.isCollege = false;
      this.isExternal = true;
      this.isOutgoingInternal = false;
      this.isOutgoingExternal = false;
    } else if (event.value == "Outgoing Internal"){
      this.isCenter = false;
      this.isCollege = false;
      this.isExternal = false;
      this.isOutgoingInternal = true;
      this.isOutgoingExternal = false;
    } else if (event.value == "Outgoing External"){
      this.isCenter = false;
      this.isCollege = false;
      this.isExternal = false;
      this.isOutgoingInternal = false;
      this.isOutgoingExternal = true;
    }
  }

  from0_id: number;
  from1_id: number;

  initDepartmentTree() {
    let getAllforTree = this.departmentService
      .getAllForTree()
      .pipe(first())
      .subscribe(
        (response) => {
          if (response.success) {            
            if (this.type == "Center") {
              this.departmentItems = response.data.filter( (dept) => dept.description === "Main Campus" );
              if (this.title == "Edit Correspondence") {
                let colleges = this.departmentItems[0];
                this.from0_id = colleges["id"];
                for (var value of colleges["children"]) {
                  for (var value2 of value["children"]){
                    if (value2.id == this.correspondence.source.id) {
                      this.from1_id = value2.id;
                    }
                  }          
                }
              }
            } else if (this.type == "College") {
              this.departmentItems = response.data.filter( (dept) => dept.description !== "Main Campus" );
              if (this.title == "Edit Correspondence") {
                let colleges = this.departmentItems;
                for (var college of colleges) {
                  for (var value of college["children"]){
                    for (var value2 of value["children"]){
                      if (value2.id == this.correspondence.source.id) {
                        this.from0_id = college.id;
                        this.from1_id = value2.id;
                      }
                    }            
                  }          
                }
              }
            } else if (this.type == "Outgoing Internal") {
              this.departmentItems = response.data;
            }
          } else {
            if (response.errors[0] == "Signature has expired") {
              this.authenticationService.logout_offline();
              this.router.navigate(["/login"]);
            }
          }
        },
        (error) => {
          this.infoDialogService.openInfoDialog(error);
        }
      );
    this.subManager.add(getAllforTree);
  }

  initOrganizations(): void {
    let getAll = this.organizationService
      .getAll()
      .pipe(first())
      .subscribe(
        (response) => {
          if (response.success) {
            for (var value of response.data) {
              this.organization = {
                id: value["id"],
                code: value["code"],
                description: value["description"],
                organization_type: value["organization_type"],
              };
              this.organizations.push(this.organization);
            }
          } else {
            if (response.errors[0] == "Signature has expired") {
              this.authenticationService.logout_offline();
              this.router.navigate(["/login"]);
            }
          }
        },
        (error) => {
          this.infoDialogService.openInfoDialog(error);
        }
      );
    this.subManager.add(getAll);
  }

  onDepartmentSelect(deptId) {
    this.subDepartments = [];
    if (this.collegeDepartmentItems.length > 0) {
      let department = this.collegeDepartmentItems.filter( (dept) => dept.id == deptId )[0]["children"];
      department = department.filter((dept) => dept.id != this.userDepartment.department_id)
      for (var value of department) {
        this.subDepartment = {
          id: value["id"],
          code: value["code"],
          description: value["description"],
          department_type: value["department_type"],
        };
        this.subDepartments.push(this.subDepartment);
      }
    }
  }

  onCollegeSelect(deptId) {
    this.departments = [];
    this.collegeDepartmentItems = [];
    if (this.departmentItems != null) {
      let college = this.departmentItems.filter( (dept) => dept.id == deptId )[0];
      if (college["description"] == "Main Campus"){
        this.isCenter = true;
        this.isCollege = false;
        this.isOutgoingInternal = false;
      } else {
        this.isCenter = false;
        this.isCollege = true;
        this.isOutgoingInternal = false;
      }
      this.collegeDepartmentItems = college["children"];
      for (var value of this.collegeDepartmentItems) {
        this.department = {
          id: value["id"],
          code: value["code"],
          description: value["description"],
          department_type: value["department_type"],
        };
        this.departments.push(this.department);
      }
    }    
  }
}
