import { Component, OnInit, Inject } from "@angular/core";
import { MAT_DIALOG_DATA } from "@angular/material/dialog";

import { DomSanitizer } from '@angular/platform-browser';

import { environment } from 'src/environments/environment';

@Component({
  selector: "app-img-previewer-dialog",
  templateUrl: "./img-previewer-dialog.component.html",
  styleUrls: ["./img-previewer-dialog.component.css"],
})
export class ImgPreviewerDialogComponent implements OnInit {

  apiUrl: string = environment.apiURL;
  
  title: string;
  image: any;
  pdfContent: any;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private _sanitizer: DomSanitizer) {
    this.title = data.title;
    if (this.title == 'Document Preview') {
      this.pdfContent = this.apiUrl + data.image.original;
    } else {
      this.image = data.image;
    }
  }

  ngOnInit() {}

  sanitize(url: string) {
    return this._sanitizer.bypassSecurityTrustUrl(this.apiUrl + url);
  }
}
