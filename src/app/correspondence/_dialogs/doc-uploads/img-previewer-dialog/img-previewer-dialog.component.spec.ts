import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImgPreviewerDialogComponent } from './img-previewer-dialog.component';

describe('ImgPreviewerDialogComponent', () => {
  let component: ImgPreviewerDialogComponent;
  let fixture: ComponentFixture<ImgPreviewerDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImgPreviewerDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImgPreviewerDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
