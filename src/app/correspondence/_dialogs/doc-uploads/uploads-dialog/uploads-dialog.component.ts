import {
  Component,
  OnInit,
  Inject,
  EventEmitter
} from "@angular/core";
import { FormGroup, FormBuilder } from "@angular/forms";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
import { catchError, map } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { DomSanitizer } from '@angular/platform-browser';
import { HttpEvent, HttpEventType, HttpErrorResponse } from '@angular/common/http';

import { environment } from 'src/environments/environment';

import { UploadService } from "src/app/_services/upload/upload.service";

@Component({
  selector: "app-uploads-dialog",
  templateUrl: "./uploads-dialog.component.html",
  styleUrls: ["./uploads-dialog.component.css"],
})
export class UploadsDialogComponent implements OnInit {

  apiUrl: string = environment.apiURL;

  title: string;
  loading = false;
  direction: string;
  request_type: string;
  form: FormGroup;

  correspondence: any;
  manuscript: any;
  type: string;
  image: any;

  contentTypeError: boolean = false;
  sizeError: boolean = false;

  fileArr = [];
  imgArr = [];
  fileObj = [];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private uploadService: UploadService,
    private fb: FormBuilder,
    private sanitizer: DomSanitizer,
    private dialogRef: MatDialogRef<UploadsDialogComponent>
  ) {
    this.title = data.title;
    this.correspondence = data.correspondence;
    this.manuscript = data.manuscript;
    this.type = data.type;
    this.direction = data.direction;
    this.request_type = data.request_type;
    this.image = data.image;
  }

  ngOnInit() {
    if (this.title == "Upload - Main Images" || this.title == "Upload - Attachment Images") {
      this.form = this.fb.group({
        avatar: [null]
      });
    } else if (this.title == "Edit - Main Images" || this.title == "Edit - Attachment Images") {
      this.form = this.fb.group({
        avatar: [null]
      });
    } else if (this.title == "Upload Manuscripts" || this.title == "Edit Manuscripts") {
      this.form = this.fb.group({
        avatar: [null]
      });
    }
  }

  formatBytes(bytes, decimals = 2) {
    if (bytes === 0) {
      return "0 Bytes";
    }
    const k = 1024;
    const dm = decimals <= 0 ? 0 : decimals;
    const sizes = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"];
    const i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + " " + sizes[i];
  }

  upload(e) {
    this.fileArr = [];
    this.imgArr = [];
    this.fileObj = [];

    const fileListAsArray = Array.from(e);
    const message = "Internal Correspondence " + this.type + " Images ";
    fileListAsArray.forEach((item, i) => {
      const file = (e as HTMLInputElement);
      const url = URL.createObjectURL(file[i]);
      if (file[i].type != "image/png" && file[i].type != "image/jpg" && file[i].type != "image/jpeg") {
        this.fileArr.push({ item, url: url, inProgress: false, progress: 0, status: 'error', 
                            message: message + 'must be an image' });
        this.contentTypeError = true;
      } else if (file[i].size > 2097152) {
        this.fileArr.push({ item, url: url, inProgress: false, progress: 0, status: 'error', 
                            message: message + 'is too big' });
        this.sizeError = true;                    
      } else {
        this.imgArr.push(url);
        this.fileArr.push({ item, url: url, inProgress: false, progress: 0, status: 'success', message: '' });
      }      
    })

    this.fileArr.forEach((item) => {
      this.fileObj.push(item.item);      
      this.uploadFile(item);     
    })

    this.form.patchValue({
      avatar: this.fileObj
    })

    this.form.get('avatar').updateValueAndValidity();
  }

  uploadFile(file) {
    let formData = new FormData();
    file.inProgress = true;
    formData.append('id', this.correspondence.id);
    formData.append('type', this.type);
    formData.append('file', file.item);
    if (this.direction != "internal") {
      formData.append('request_type', this.request_type);
    }
    let upload: Observable<HttpEvent<any>>;
    if (this.title == "Upload - Main Images" || this.title == "Upload - Attachment Images") {
      upload = this.uploadService.upload(formData, this.direction);
    } else {
      if (this.contentTypeError || this.sizeError) {
        return;
      } else {
        formData.append('existing_image', this.image.id);
        upload = this.uploadService.change_upload(formData, this.direction);
      }      
    }
    upload.pipe(
      map(event => {
        switch (event.type) {  
          case HttpEventType.UploadProgress:  
            file.progress = Math.round(event.loaded * 100 / event.total);  
            break;  
          case HttpEventType.Response:  
            return event;  
        }
      }),
      catchError((error: HttpErrorResponse) => {
        file.inProgress = false;  
        return of(`${file.item.name} upload failed.`);  
      })).subscribe((event: any) => {  
        if (typeof (event) === 'object') {  
          if (!event.body.success) {
            file.status = 'error';
            file.message = event.body.errors[0];
          }          
        }  
      });          
  }

  previewUploadedImage(image) {
    this.uploadService.openDocPreviewerDialog(      
      image
    );
  }

  previewUploadedDocument(document) {
    this.uploadService.openDocPreviewerDialog(      
      document,
      "Document Preview"
    );
  }
  onDialogClose = new EventEmitter();

  uploadManuscript(e) {
    this.fileArr = [];
    this.imgArr = [];
    this.fileObj = [];

    const fileListAsArray = Array.from(e);
    const message = "Manuscript Documents";
    fileListAsArray.forEach((item, i) => {
      const file = (e as HTMLInputElement);
      const url = URL.createObjectURL(file[i]);
      if (file[i].type != "application/pdf") {
        this.fileArr.push({ item, url: url, inProgress: false, progress: 0, status: 'error', 
                            message: message + ' must be a PDF' });
        this.contentTypeError = true;
      } else if (file[i].size > 5242880) {
        this.fileArr.push({ item, url: url, inProgress: false, progress: 0, status: 'error', 
                            message: message + ' is too big' });
        this.sizeError = true;                    
      } else {
        this.imgArr.push(url);
        this.fileArr.push({ item, url: url, inProgress: false, progress: 0, status: 'success', message: '' });
      }      
    });

    this.fileArr.forEach((item) => {
      this.fileObj.push(item.item);      
      this.uploadManuscriptFile(item);
    })

    this.form.patchValue({
      avatar: this.fileObj
    })

    this.form.get('avatar').updateValueAndValidity();
  }

  uploadManuscriptFile(file) {
    let formData = new FormData();
    file.inProgress = true;
    formData.append('id', this.manuscript.id);
    formData.append('file', file.item);    
    let upload: Observable<HttpEvent<any>>;
    if (this.title == "Upload Manuscripts") {
      upload = this.uploadService.uploadManuscript(formData);
    } else {
      if (this.contentTypeError || this.sizeError) {
        return;
      } else {
        formData.append('existing_image', this.image.id);
        upload = this.uploadService.changeManuscriptUpload(formData);
      }      
    }
    upload.pipe(
      map(event => {
        switch (event.type) {  
          case HttpEventType.UploadProgress:  
            file.progress = Math.round(event.loaded * 100 / event.total);  
            break;  
          case HttpEventType.Response:  
            return event;  
        }
      }),
      catchError((error: HttpErrorResponse) => {
        file.inProgress = false;  
        return of(`${file.item.name} upload failed.`);  
      })).subscribe((event: any) => {  
        if (typeof (event) === 'object') {  
          if (!event.body.success) {
            file.status = 'error';
            file.message = event.body.errors[0];
          }          
        }  
      });
  }

  close() {
    this.dialogRef.close();
    this.onDialogClose.emit(this);
  }

  sanitize(url: string) {
    return this.sanitizer.bypassSecurityTrustUrl(url);
  }

  sanitize_uploaded(url: string) {
    return this.sanitizer.bypassSecurityTrustUrl(this.apiUrl + url);
  }
}
