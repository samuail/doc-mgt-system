import { Component, OnInit, Inject, OnDestroy } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
import { first } from "rxjs/operators";
import { Subscription } from "rxjs";
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from "@angular/router";

import { environment } from 'src/environments/environment';

import { Message } from "src/app/_models/_helpers/message";
import { ExternalIncomingCorrespondence } from "src/app/_models/external-incoming-correspondence";
import { InternalCorrespondence } from "src/app/_models/internal-correspondence";
import { ExternalOutgoingCorrespondence } from "src/app/_models/external-outgoing-correspondence";

import { CorrespondenceUploadsService } from "src/app/_services/correspondence-uploads.service";
import { AuthenticationService } from "src/app/_services/authentication.service";
import { InfoDialogService } from "src/app/_services/info-dialog.service";

@Component({
  selector: "app-doc-uploads",
  templateUrl: "./doc-uploads.component.html",
  styleUrls: ["./doc-uploads.component.css"],
})
export class DocUploadsComponent implements OnInit, OnDestroy {
  
  apiUrl: string = environment.apiURL;
  
  subManager = new Subscription();
  title: string;
  direction: string;
  request_type: string;
  correspondence: any;
  message: Message = new Message();

  main_images: any[] = [];
  attachment_images: any[] = [];

  uploadActions: string[] = [];

  canAddUpload: boolean = false;
  canEditUpload: boolean = false;
  canDeleteUpload: boolean = false;
  canPreviewUpload: boolean = false;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private router: Router,
    private correspondenceUploadsService: CorrespondenceUploadsService,
    private authenticationService: AuthenticationService,
    public infoDialogService: InfoDialogService,
    private _sanitizer: DomSanitizer,
    private dialogRef: MatDialogRef<DocUploadsComponent>
  ) {
    this.title = data.title;
    this.uploadActions = data.uploadActions;
    if (data.type == "Center" || data.type == "College" || data.type == "Outgoing Internal") {
      this.correspondence = new InternalCorrespondence();
      this.correspondence = data.correspondence;
      this.direction = "internal";
    } else if (data.direction == "incoming") {
      this.correspondence = new ExternalIncomingCorrespondence();
      this.correspondence = data.correspondence;
      this.direction = "external";
      this.request_type = data.direction;
    } else if (data.direction == "outgoing") {
      this.correspondence = new ExternalOutgoingCorrespondence();
      this.correspondence = data.correspondence;
      this.direction = "external";
      this.request_type = data.direction;
    }

    for (var action of this.uploadActions) {
      if (action == "Upload Add") {
        this.canAddUpload = true;
      } else if (action == "Upload Edit") {
        this.canEditUpload = true;
      } else if (action == "Upload Delete") {
        this.canDeleteUpload = true;
      } else if (action == "Upload Preview") {
        this.canPreviewUpload = true;
      }
    }

    this.initUploadedImage();
  }

  ngOnInit() {}

  ngOnDestroy() {
    this.subManager.unsubscribe();
  }

  initUploadedImage() {
    let getUploadedImage = this.correspondenceUploadsService
      .getUploaded(this.correspondence, this.direction, this.request_type)
      .pipe(first())
      .subscribe(
        (response) => {  
          if(response.success == undefined) {
            this.main_images = response.main_images;
            this.attachment_images = response.attachment_images;
          } else {
            this.message.title = "Uploads";
            this.message.type = "error";
            this.message.content = response.errors;
            const infoDialogRef = this.infoDialogService.openInfoDialog(
              this.message
            );
            infoDialogRef.afterClosed().subscribe(() => {
              if (response.errors[0] == "Signature has expired") {
                this.dialogRef.close();
                this.authenticationService.logout_offline();
                this.router.navigate(["/login"]);
              }
            });
          }
        },
        (error) => {
          this.infoDialogService.openInfoDialog(error);
        }
      );
    this.subManager.add(getUploadedImage);
  }

  uploadImage(type) {
    let title = "Upload - " + type + " Images";
    const dialogRef = this.correspondenceUploadsService.openUploadsDialog(
      title, this.correspondence, type, this.direction, this.request_type
    );

    const sub = dialogRef.componentInstance.onDialogClose
          .subscribe((data) => {
            this.initUploadedImage();
          });
  }

  editUploadedImage(image) {
    let type = "";
    let title = "";
    if(image.type == "main_images") {
      type = "Main";      
    } else {
      type = "Attachment";      
    }
    title = "Edit - " + type + " Images";
    const dialogRef = this.correspondenceUploadsService.openUploadsDialog(
      title,
      this.correspondence, 
      type,
      this.direction,
      this.request_type,
      image
    );

    const sub = dialogRef.componentInstance.onDialogClose
          .subscribe((data) => {
            this.initUploadedImage();
          });
  }

  previewUploadedImage(image) {
    if (!this.canPreviewUpload) {
      return;
    }
    const dialogRef = this.correspondenceUploadsService.openDocPreviewerDialog(
      "Image Preview",
      image
    );
  }

  sanitize(url: string) {
    return this._sanitizer.bypassSecurityTrustUrl(this.apiUrl + url);
  }
}
