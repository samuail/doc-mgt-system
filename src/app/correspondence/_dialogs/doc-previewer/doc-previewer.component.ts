import { Component, OnInit, Inject, OnDestroy } from "@angular/core";
import { MAT_DIALOG_DATA } from "@angular/material/dialog";
import { first } from "rxjs/operators";
import { Subscription } from "rxjs";
import { DomSanitizer } from '@angular/platform-browser';

import { environment } from 'src/environments/environment';

import { InternalCorrespondence } from "src/app/_models/internal-correspondence";
import { ExternalIncomingCorrespondence } from "src/app/_models/external-incoming-correspondence";
import { ExternalOutgoingCorrespondence } from "src/app/_models/external-outgoing-correspondence";
import { CorrespondenceComment } from "src/app/_models/correspondence-comment";
import { ManuscriptComment } from "src/app/_models/manuscript-comment";

import { InfoDialogService } from "src/app/_services/info-dialog.service";
import { CorrespondenceUploadsService } from "src/app/_services/correspondence-uploads.service";
import { CorrespondenceCommentService } from "src/app/_services/correspondence-comment.service";
import { ManuscriptUploadsService } from "src/app/_services/manuscript-uploads.service";
import { ManuscriptCommentService } from "src/app/_services/manuscript-comment.service";
@Component({
  selector: "app-doc-previewer",
  templateUrl: "./doc-previewer.component.html",
  styleUrls: ["./doc-previewer.component.css"]
})
export class DocPreviewerComponent implements OnInit, OnDestroy {
  
  apiUrl: string = environment.apiURL;

  subManager = new Subscription();

  title: string;
  direction: string;
  request_type: string;
  correspondence: any;
  manuscript: any;

  loadingDocuments: boolean = false;

  public selectedindex: number = 0;
  public images: string[] = [];

  main_images: any[] = [];
  attachment_images: any[] = [];

  manuscript_documents: any[] = [];
  public documents: any[] = [];

  previewActions: string [] = [];

  canPreviewComment: boolean = false;

  comments: CorrespondenceComment[] = [];
  comment: CorrespondenceComment = new CorrespondenceComment();

  manuscriptComment: ManuscriptComment = new ManuscriptComment();
  manuscriptComments: ManuscriptComment[] = [];

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,              
              private correspondenceUploadsService: CorrespondenceUploadsService,
              private correspondenceCommentService: CorrespondenceCommentService,
              public infoDialogService: InfoDialogService,
              private _sanitizer: DomSanitizer,
              private manuscriptUploadsService: ManuscriptUploadsService,
              private manuscriptCommentService: ManuscriptCommentService) {
    this.title = data.title;
    this.previewActions = data.previewActions;
    if (this.title == "Manuscript Preview" || this.title == "Manuscript Archive Preview") {
      this.manuscript = data.manuscript;
      this.initUploadedManuscriptDocument();
      this.initManuscriptComments();
    } else {
      if (data.type == "Center" || data.type == "College" || data.type == "Outgoing Internal") {
        this.correspondence = new InternalCorrespondence();
        this.correspondence = data.correspondence;
        this.direction = "internal";
      } else if (data.direction == "incoming") {
        this.correspondence = new ExternalIncomingCorrespondence();
        this.correspondence = data.correspondence;
        this.direction = "external";
        this.request_type = data.direction;
      } else if (data.direction == "outgoing") {
        this.correspondence = new ExternalOutgoingCorrespondence();
        this.correspondence = data.correspondence;
        this.direction = "external";
        this.request_type = data.direction;
      }
      this.initUploadedImage();
      this.initComments();
    }
    for (var action of this.previewActions) {
      if (action == "Preview Comment") {
        this.canPreviewComment = true;
      } 
    }        
  }

  ngOnInit() {}

  ngOnDestroy() {
    this.subManager.unsubscribe();
  }

  expanded(event) {
    const el = document.getElementById("doc-previewer");
    el.scrollTo({ top: 0, left: 1000, behavior: "smooth" });
  }

  selectImage(index : number) {
      this.selectedindex = index;
  }

  initUploadedImage() {
    let getUploadedImage = this.correspondenceUploadsService
      .getUploaded(this.correspondence, this.direction, this.request_type)
      .pipe(first())
      .subscribe(
        (response) => {      
          this.main_images = response.main_images;
          this.attachment_images = response.attachment_images;
          for (var img of this.main_images) {
            this.images.push(img.original);
          }

          for (var img2 of this.attachment_images) {
            this.images.push(img2.original);
          }
        },
        (error) => {
          this.infoDialogService.openInfoDialog(error);
        }
      );
    this.subManager.add(getUploadedImage);
  }

  initUploadedManuscriptDocument() {
    this.loadingDocuments = true;
    let getUploadedDocument = this.manuscriptUploadsService
      .getUploaded(this.manuscript, this.title)
      .pipe(first())
      .subscribe(
        (response) => {
          this.manuscript_documents = response.documents;        
          for (var img of this.manuscript_documents) {
            let url = img.blob.content_type == "application/pdf" ? this.apiUrl + img.original : img.original
            let image = { url: url, content_type: img.blob.content_type }
            this.documents.push(image);
          }
          this.loadingDocuments = false;
        },
        (error) => {
          this.loadingDocuments = false;
          this.infoDialogService.openInfoDialog(error);
        }
      );
    this.subManager.add(getUploadedDocument);    
  }

  initComments() {
    let getAll = this.correspondenceCommentService
      .getAll(this.correspondence.id, this.direction, this.request_type)
      .pipe(first())
      .subscribe(
        (response) => {
          if (response.success) {
            for (var value of response.data) {
              this.comment = {
                id: value["id"],
                commentedBy: value["commented_by"],
                correspondence: value["correspondence"],
                content: value["content"],
                order: value["order"],
                commentDate: value["comment_date"],
              };
              this.comments.push(this.comment);
            }
          }
        },
        (error) => {
          this.infoDialogService.openInfoDialog(error);
        }
      );
    this.subManager.add(getAll);
  }

  initManuscriptComments() {
    let getAll = this.manuscriptCommentService
      .getAll(this.manuscript)
      .pipe(first())
      .subscribe(
        (response) => {
          if (response.success) {
            for (var value of response.data) {
              this.manuscriptComment = this.manuscriptCommentService.assignManuscriptCommentValue(value);
              this.manuscriptComments.push(this.manuscriptComment);
            }
          }
        },
        (error) => {
          this.infoDialogService.openInfoDialog(error);
        }
      );
    this.subManager.add(getAll);
  }

  sanitize(url: string) {
    return this._sanitizer.bypassSecurityTrustUrl(this.apiUrl + url);
  }
}
