import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocPreviewerComponent } from './doc-previewer.component';

describe('DocPreviewerComponent', () => {
  let component: DocPreviewerComponent;
  let fixture: ComponentFixture<DocPreviewerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocPreviewerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocPreviewerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
