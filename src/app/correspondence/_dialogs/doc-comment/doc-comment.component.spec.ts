import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocCommentComponent } from './doc-comment.component';

describe('DocCommentComponent', () => {
  let component: DocCommentComponent;
  let fixture: ComponentFixture<DocCommentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocCommentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocCommentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
