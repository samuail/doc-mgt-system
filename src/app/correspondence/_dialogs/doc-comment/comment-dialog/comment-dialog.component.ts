import { Component, OnInit, Inject, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

import { CorrespondenceComment } from 'src/app/_models/correspondence-comment';

@Component({
  selector: 'app-comment-dialog',
  templateUrl: './comment-dialog.component.html',
  styleUrls: ['./comment-dialog.component.css']
})
export class CommentDialogComponent implements OnInit {

  title: string;
  loading = false;
  form: FormGroup;

  comment: CorrespondenceComment = new CorrespondenceComment();

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
              private fb: FormBuilder,
              private dialogRef: MatDialogRef<CommentDialogComponent>) {
    this.title = data.title;
 	  this.comment = data.comment;
  }

  ngOnInit() {
    if(this.title == 'Add Comment') {
    	this.form = this.fb.group({
        content: ['', Validators.required]   
    	});    
    } else if(this.title == 'Edit Comment'){
    	this.form = this.fb.group({
        content: [this.comment.content, Validators.required]
    	}); 
    } else if(this.title == 'Preview Comment') {
      this.form = this.fb.group({});
    }
  }

  onCommentSave = new EventEmitter();

  save() {
    if (this.form.invalid) {
          return;
    }
    this.onCommentSave.emit(this);
  }

  close() {
	 this.dialogRef.close();
  }

}
