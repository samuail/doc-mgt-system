import { Component, OnInit, ViewChild, Inject, OnDestroy } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { MatTableDataSource, MatTable } from "@angular/material/table";
import { Router } from "@angular/router";
import { Subscription } from "rxjs";
import { first } from "rxjs/operators";

import { Message } from "src/app/_models/_helpers/message";
import { User } from "src/app/_models/user";
import { CorrespondenceComment } from "src/app/_models/correspondence-comment";

import { InternalCorrespondence } from "src/app/_models/internal-correspondence";
import { ExternalIncomingCorrespondence } from "src/app/_models/external-incoming-correspondence";

import { CorrespondenceCommentService } from "src/app/_services/correspondence-comment.service";
import { AuthenticationService } from "src/app/_services/authentication.service";
import { InfoDialogService } from "src/app/_services/info-dialog.service";

@Component({
  selector: "app-doc-comment",
  templateUrl: "./doc-comment.component.html",
  styleUrls: ["./doc-comment.component.css"]
})
export class DocCommentComponent implements OnInit, OnDestroy {
  subManager = new Subscription();

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatTable) table: MatTable<any>;

  title: string;
  direction: string;
  request_type: string;
  correspondence: any;
  message: Message = new Message();

  comments: CorrespondenceComment[] = [];
  comment: CorrespondenceComment = new CorrespondenceComment();

  dataSource = new MatTableDataSource<CorrespondenceComment>();
  displayedColumns = ["order", "commentedBy", "content", "commentDate", "operations"];

  loading = false;
  resultsLength = 0;

  commentActions: string[] = [];
  currentUser: User;

  canAdd: boolean = false;
  canEdit: boolean = false;
  canDelete: boolean = false;  

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private router: Router,
    private correspondenceCommentService: CorrespondenceCommentService,
    private authenticationService: AuthenticationService,
    public infoDialogService: InfoDialogService,
    private dialogRef: MatDialogRef<DocCommentComponent>
  ) {
    this.title = data.title;
    this.commentActions = data.commentActions;
    if (data.type == "Center" || data.type == "College" || data.type == "Outgoing Internal") {
      this.correspondence = new InternalCorrespondence();
      this.correspondence = data.correspondence;
      this.direction = "internal";
    } else if (data.direction == "incoming") {
      this.correspondence = new ExternalIncomingCorrespondence();
      this.correspondence = data.correspondence;
      this.direction = "external";
      this.request_type = data.direction;
    }
    for (var action of this.commentActions) {
      if (action == "Comment Add") {
        this.canAdd = true;
      } else if (action == "Comment Edit") {
        this.canEdit = true;
      } else if (action == "Comment Delete") {
        this.canDelete = true;
      }
    }

    this.authenticationService.currentUser.subscribe(
      (x) => (this.currentUser = x)
    );
  }

  ngOnInit(): void {
    this.getAll();
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

    this.dataSource.data = this.comments;
  }

  ngOnDestroy() {
    this.subManager.unsubscribe();
  }

  getAll(): void {
    this.loading = true;
    let getAll = this.correspondenceCommentService
      .getAll(this.correspondence.id, this.direction, this.request_type)
      .pipe(first())
      .subscribe(
        (response) => {
          if (response.success) {
            for (var value of response.data) {
              this.comment = {
                id: value["id"],
                commentedBy: value["commented_by"],
                correspondence: value["correspondence"],
                content: value["content"],
                order: value["order"],
                commentDate: value["comment_date"],
              };
              this.comments.push(this.comment);
            }
            this.loading = false;
            this.resultsLength = response.total;            
          } else {
            this.message.title = "Comments";
            this.message.type = "error";
            this.message.content = response.errors;
            const infoDialogRef = this.infoDialogService.openInfoDialog(
              this.message
            );
            infoDialogRef.afterClosed().subscribe(() => {
              if (response.errors[0] == "Signature has expired") {
                this.dialogRef.close();
                this.authenticationService.logout_offline();
                this.router.navigate(["/login"]);
              }
            });
          }
        },
        (error) => {
          this.loading = false;
          this.infoDialogService.openInfoDialog(error);
        }
      );
    this.subManager.add(getAll);
  }

  addComment() {
    this.title = "Add Comment";
    const dialogRef = this.correspondenceCommentService.openCommentDialog(
      this.title
    );

    const sub = dialogRef.componentInstance.onCommentSave
        .subscribe(
          (data) => {
            if(data) {
              data.loading = true;
              let content = data.form.value.content;
              let create = this.correspondenceCommentService
                  .create(content, this.correspondence.id, this.direction, this.request_type)
                    .pipe(first())
                    .subscribe(
                      (response) => {
                        if(response.success) {
                          this.message.title = this.title;
                          this.message.content = [response.message];
                          this.message.type = "success";
                          const infoDialogRef = this.infoDialogService.openInfoDialog(
                            this.message
                          );
                          infoDialogRef.afterClosed().subscribe(() => {
                            this.comment = {
                              id: response.data["id"],
                              commentedBy: response.data["commented_by"],
                              correspondence: response.data["correspondence"],
                              content: response.data["content"],
                              order: response.data["order"],
                              commentDate: response.data["comment_date"]
                            };                            
                            this.comments.push(
                              this.comment
                            );
                            this.resultsLength = this.resultsLength + 1;
                            this.dataSource = new MatTableDataSource(
                              this.comments
                            );
                            this.dataSource.paginator = this.paginator;
                            this.dataSource.sort = this.sort;
                            data.close();
                          });
                        } else {
                          this.message.title = this.title;
                          this.message.type = "error";
                          this.message.content = response.errors;
                          const infoDialogRef = this.infoDialogService.openInfoDialog(
                            this.message
                          );
                          infoDialogRef.afterClosed().subscribe(() => {
                            data.loading = false;
                            if (response.errors[0] == "Signature has expired") {
                              this.authenticationService.logout_offline();
                              this.router.navigate(["/login"]);
                            }
                          });
                        }
                      },
                      (error) => {
                        data.loading = false;
                        this.infoDialogService.openInfoDialog(error);
                      }
                    );
              this.subManager.add(create);
            }
          }          
        );
    dialogRef.afterClosed().subscribe(() => {
      sub.unsubscribe();
    });
  }

  editComment(comment) {
    this.title = "Edit Comment";
    const dialogRef = this.correspondenceCommentService.openCommentDialog(
      this.title,
      comment
    );

    const sub = dialogRef.componentInstance.onCommentSave
        .subscribe(
          (data) => {
            if(data) {
              data.loading = true;
              let content = data.form.value.content;
              let create = this.correspondenceCommentService
                  .update(content, comment, this.direction, this.request_type)
                    .pipe(first())
                    .subscribe(
                      (response) => {
                        if(response.success) {
                          this.message.title = this.title;
                          this.message.content = [response.message];
                          this.message.type = "success";
                          const infoDialogRef = this.infoDialogService.openInfoDialog(
                            this.message
                          );
                          infoDialogRef.afterClosed().subscribe(() => {
                            this.comments = this.comments.filter(
                              (item) => item.id !== comment.id
                            );
                            this.comment = {
                              id: response.data["id"],
                              commentedBy: response.data["commented_by"],
                              correspondence: response.data["correspondence"],
                              content: response.data["content"],
                              order: response.data["order"],
                              commentDate: response.data["comment_date"]
                            };
                            this.comments.push(
                              this.comment
                            );
                            this.resultsLength = this.resultsLength;
                            this.dataSource = new MatTableDataSource(
                              this.comments
                            );
                            this.dataSource.paginator = this.paginator;
                            this.dataSource.sort = this.sort;
                            this.table.renderRows();
                            data.close();
                          });
                        } else {
                          this.message.title = this.title;
                          this.message.type = "error";
                          this.message.content = response.errors;
                          const infoDialogRef = this.infoDialogService.openInfoDialog(
                            this.message
                          );
                          infoDialogRef.afterClosed().subscribe(() => {
                            data.loading = false;
                            if (response.errors[0] == "Signature has expired") {
                              this.authenticationService.logout_offline();
                              this.router.navigate(["/login"]);
                            }
                          });
                        }
                      },
                      (error) => {
                        data.loading = false;
                        this.infoDialogService.openInfoDialog(error);
                      }
                    );
              this.subManager.add(create);
            }
          }          
        );
    dialogRef.afterClosed().subscribe(() => {
      sub.unsubscribe();
    });
  }

  previewComment(comment) {
    this.title = "Preview Comment";
    const dialogRef = this.correspondenceCommentService.openCommentDialog(
      this.title,
      comment
    );
  }

  deleteComment(comment) {}
}
