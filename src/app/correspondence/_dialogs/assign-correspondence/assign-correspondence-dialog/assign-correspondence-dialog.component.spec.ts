import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignCorrespondenceDialogComponent } from './assign-correspondence-dialog.component';

describe('AssignCorrespondenceDialogComponent', () => {
  let component: AssignCorrespondenceDialogComponent;
  let fixture: ComponentFixture<AssignCorrespondenceDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssignCorrespondenceDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignCorrespondenceDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
