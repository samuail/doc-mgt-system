import { Component, OnInit, Inject, EventEmitter } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
import { DatePipe } from '@angular/common';
import { first } from "rxjs/operators";
import { Subscription } from "rxjs";
import { Router } from "@angular/router";

import { CorrespondenceAssignment } from "src/app/_models/correspondence-assignment";
import { Message } from "src/app/_models/_helpers/message";
import { User } from "src/app/_models/user";
import { Department } from "src/app/_models/department";
import { DepartmentItem } from "src/app/_models/department-item";
import { UserDepartmentRole } from "src/app/_models/user-department-role";
import { DepartmentRole } from "src/app/_models/department-role";
import { Role } from "src/app/_models/role";

import { DepartmentService } from "src/app/_services/department.service";
import { UserService } from "src/app/_services/user.service";
import { RoleService } from "src/app/_services/role.service";
import { AuthenticationService } from "src/app/_services/authentication.service";
import { InfoDialogService } from "src/app/_services/info-dialog.service";

@Component({
  selector: "app-assign-correspondence-dialog",
  templateUrl: "./assign-correspondence-dialog.component.html",
  styleUrls: ["./assign-correspondence-dialog.component.css"],
  providers:[DatePipe],
})
export class AssignCorrespondenceDialogComponent implements OnInit {
  subManager = new Subscription();

  title: string;
  loading = false;
  message: Message = new Message();
  form: FormGroup;

  type: string;

  assignment: CorrespondenceAssignment = new CorrespondenceAssignment();
  assignments: CorrespondenceAssignment[] = [];

  users: User[] = [];
  user: User = new User();
  user_assigned_doc = [];

  roles: Role[] = [];
  role: Role = new Role();

  assignedRoles: Role[] = [];

  department: Department = new Department();
  departments: Department[] = [];

  subDepartment: Department = new Department();
  subDepartments: Department[] = [];

  departmentItems: DepartmentItem[] = [];
  collegeDepartmentItems: DepartmentItem[] = [];

  department_id: number;

  userDepartment: DepartmentRole = new DepartmentRole();
  assignedUserDepartment: UserDepartmentRole = new UserDepartmentRole();

  statuses: any[] = [
    { value: "1", viewValue: "New" },
    { value: "2", viewValue: "Accepted" },
  ];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private router: Router,
    public datepipe: DatePipe,
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<AssignCorrespondenceDialogComponent>,
    private departmentService: DepartmentService,
    private roleService: RoleService,
    private userService: UserService,
    private authenticationService: AuthenticationService,
    public infoDialogService: InfoDialogService
  ) {
    this.title = data.title;
    this.assignment = data.assignment;
    this.assignments = data.assignments;
    this.type = data.type;
    this.department_id = data.department_id;
    this.departmentItems = data.departmentItems;
    this.assignedUserDepartment = data.assignedUserDepartment;

    this.authenticationService.userDepartment.subscribe(
      (userDepartment) => (this.userDepartment = userDepartment)
    );

    this.initUserRoles();
  }

  ngOnInit() {
    if (this.title == "Add Assignment") {
      if(this.type == "Within"){
        this.initDepartmentRoles(this.department_id);
        this.form = this.fb.group({
          userRole: ["", Validators.required],        
          assignedTo: ["", Validators.required],
        });
      } else {
        this.form = this.fb.group({
          from0: ["", Validators.required],
          from1: ["", Validators.required],
          departments: ["", Validators.required],
          userRole: ["", Validators.required],
          assignedTo: ["", Validators.required],
        });
      }  
    } else if (this.title == "Edit Assignment") {      
      var from0_id;
      var from1_id;
      var department;
      var role;
      let colleges = this.departmentItems;
        for (var college of colleges) {
          for (var value of college["children"]){
            for (var value2 of value["children"]){
              if (value2.id == this.assignedUserDepartment.department_user_role.department_id) {
                from0_id = college.id;
                from1_id = value.id;
                department = this.assignedUserDepartment.department_user_role.department_id;
                role = this.assignedUserDepartment.department_user_role.user_role_id;
                break;
              }
            }            
          }          
        }
      
      this.form = this.fb.group({
        from0: [from0_id, Validators.required],
        from1: [from1_id, Validators.required],
        departments: [department, Validators.required],
        userRole: [role, Validators.required],
        assignedTo: [this.assignment.toUser.id, Validators.required],
      });
      
      this.onCollegeSelect(from0_id);
      this.onDepartmentSelect(from1_id);
      this.onSubDepartmentSelect(department);
      this.onUserRoleSelect(role);

    } else if(this.title == 'Accept or Reject Assignment'){
      this.form = this.fb.group({
        acceptReject: ["Accepted", null],
        rejectionRemark: ["", null]
      });  
    }  
  }

  ngOnDestroy() {
    this.subManager.unsubscribe();
  }

  initDepartmentTree() {
    let getAllforTree = this.departmentService
      .getAllForTree()
      .pipe(first())
      .subscribe(
        (response) => {
          if (response.success) {    
            this.departmentItems = response.data;            
          } else {
            if (response.errors[0] == "Signature has expired") {
              this.authenticationService.logout_offline();
              this.router.navigate(["/login"]);
            }
          }
        },
        (error) => {
          this.infoDialogService.openInfoDialog(error);
        }
      );
    this.subManager.add(getAllforTree);
  }

  initDepartmentRoles(deptId) {
    let getAssignedRoleForDepartment = this.departmentService
      .getAssignedRoleForDepartment(deptId)
      .pipe(first())
      .subscribe(
        (response) => {
          if (response.success) {
            this.assignedRoles = this.assignRole(response.data);
            this.assignedRoles.sort((a,b) => a.name.localeCompare(b.name));
          } else {
            this.message.title = this.title;
            this.message.type = "error";
            this.message.content = response.errors;
            const infoDialogRef = this.infoDialogService.openInfoDialog(
              this.message
            );
            infoDialogRef.afterClosed().subscribe(() => {
              if (response.errors[0] == "Signature has expired") {
                this.authenticationService.logout_offline();
                this.router.navigate(["/login"]);
              }
            });
          }
        },
        (error) => {
          this.infoDialogService.openInfoDialog(error);
        }
      );
    this.subManager.add(getAssignedRoleForDepartment);
  }

  initUserRoles() {
    let getUserRoles = this.roleService
      .getAll()
      .pipe(first())
      .subscribe(
        (response) => {
          if (response.success) {    
            for (var value of response.data) {
              if (value["name"] != "Administrator") {
                this.role = {
                  id: value["id"],
                  name: value["name"],
                };
                this.roles.push(this.role);
              }
            }
            this.roles.sort((a,b) => a.name.localeCompare(b.name));
          } else {
            if (response.errors[0] == "Signature has expired") {
              this.authenticationService.logout_offline();
              this.router.navigate(["/login"]);
            }
          }
        },
        (error) => {
          this.infoDialogService.openInfoDialog(error);
        }
      );
    this.subManager.add(getUserRoles);    
  }

  getAllUsers(departmentId): void {
    this.users = [];
    let getAllUsers = this.userService
      .getAllUsersWithinDepartment(departmentId)
      .pipe(first())
      .subscribe(
        (response) => {
          if (response.success) {
            for (var value of response.data) {
              if (value["active"]) {
                this.user = {
                  id: value["id"],
                  firstName: value["first_name"],
                  lastName: value["last_name"],
                  fullName: value["first_name"] + " " + value["last_name"],
                  userName: value["email"],
                  password: "",
                  active: value["active"],
                  token: "",
                };
                this.users.push(this.user);
              }              
            }
            this.users.sort((a,b) => a.firstName.localeCompare(b.firstName));
          } else {
            this.message.title = "Users";
            this.message.type = "error";
            this.message.content = response.errors;
            const infoDialogRef = this.infoDialogService.openInfoDialog(
              this.message
            );
            infoDialogRef.afterClosed().subscribe(() => {
              if (response.errors[0] == "Signature has expired") {
                this.authenticationService.logout_offline();
                this.router.navigate(["/login"]);
              }
            });
          }
        },
        (error) => {
          this.infoDialogService.openInfoDialog(error);
        }
      );
    this.subManager.add(getAllUsers);
  }

  getAllUsersWithTheRole(departmentId, roleId): void {
    this.users = [];
    this.user_assigned_doc = [];
    let getAllUsers = this.userService
      .getAllUsersWithTheRole(departmentId, roleId)
      .pipe(first())
      .subscribe(
        (response) => {
          if (response.success) {
              for (var value of response.data) {
                if (value != null && value["active"]) {
                  this.user = {
                    id: value["id"],
                    firstName: value["first_name"],
                    lastName: value["last_name"],
                    fullName: value["first_name"] + " " + value["last_name"],
                    userName: value["email"],
                    password: "",
                    active: value["active"],
                    token: "",
                  };
                  this.users.push(this.user);
                }   
              }           
            this.users.sort((a,b) => a.firstName.localeCompare(b.firstName));
            if (this.users.length > 0) {
              let getUserAssignedDocs = this.userService
              .getUserAssignedDocs(this.users)
              .pipe(first())
              .subscribe(
                (response) => {
                  if (response.success) {
                    this.user_assigned_doc = response.data;
                  }
                },
                (error) => {}
              );
              this.subManager.add(getUserAssignedDocs);
            }            
          } else {
            this.message.title = "Users";
            this.message.type = "error";
            this.message.content = response.errors;
            const infoDialogRef = this.infoDialogService.openInfoDialog(
              this.message
            );
            infoDialogRef.afterClosed().subscribe(() => {
              if (response.errors[0] == "Signature has expired") {
                this.authenticationService.logout_offline();
                this.router.navigate(["/login"]);
              }
            });
          }
        },
        (error) => {
          this.infoDialogService.openInfoDialog(error);
        }
      );
    this.subManager.add(getAllUsers);
  }
  isCenter: boolean = false;
  isCollege: boolean = false;
  isOutgoingInternal: boolean = true;
  
  onCollegeSelect(deptId) {
    this.departments = [];
    this.collegeDepartmentItems = [];
    let college = this.departmentItems.filter( (dept) => dept.id == deptId )[0];
    if (college["description"] == "Main Campus"){
      this.isCenter = true;
      this.isCollege = false;
      this.isOutgoingInternal = false;
    } else {
      this.isCenter = false;
      this.isCollege = true;
      this.isOutgoingInternal = false;
    }
    this.collegeDepartmentItems = college["children"];
    for (var value of this.collegeDepartmentItems) {
      this.department = {
        id: value["id"],
        code: value["code"],
        description: value["description"],
        department_type: value["department_type"],
      };
      this.departments.push(this.department);
    }
  }

  rejection: boolean = false;

  onAcceptOrRejectClick(event) {    
    var status = this.form.get('acceptReject').value;
    if (status == "Accepted") {
      this.rejection = true;
    } else {
      this.rejection = false;
    }
  }

  onDepartmentSelect(deptId) {    
    this.subDepartments = [];    
    let department = this.collegeDepartmentItems.filter( (dept) => dept.id == deptId )[0]["children"];
    if (this.title != "Edit Assignment") {
      department = department.filter((dept) => dept.id != this.userDepartment.department_id)
    }    
    for (var value of department) {
      this.subDepartment = {
        id: value["id"],
        code: value["code"],
        description: value["description"],
        department_type: value["department_type"],
      };
      this.subDepartments.push(this.subDepartment);
    }
  }

  assignRole (values) {
    let role: Role;
    let roles: Role [] = [];
    for (var value of values) {
      role = {
        id: value["id"],
        name: value["name"],
      };
      roles.push(role);
    }
    return roles;
  }

  onSubDepartmentSelect(deptId) {
    let getAssignedRoleForDepartment = this.departmentService
      .getAssignedRoleForDepartment(deptId)
      .pipe(first())
      .subscribe(
        (response) => {
          if (response.success) {
            this.assignedRoles = this.assignRole(response.data);
            this.assignedRoles.sort((a,b) => a.name.localeCompare(b.name));
          } else {
            this.message.title = this.title;
            this.message.type = "error";
            this.message.content = response.errors;
            const infoDialogRef = this.infoDialogService.openInfoDialog(
              this.message
            );
            infoDialogRef.afterClosed().subscribe(() => {
              if (response.errors[0] == "Signature has expired") {
                this.authenticationService.logout_offline();
                this.router.navigate(["/login"]);
              }
            });
          }
        },
        (error) => {
          this.infoDialogService.openInfoDialog(error);
        }
      );
    this.subManager.add(getAssignedRoleForDepartment);
  }

  onUserRoleSelect(role_id){   
    let departmentId = this.type == "Within" ? this.department_id : this.form.value.departments;
    this.getAllUsersWithTheRole(departmentId, role_id);    
  }

  formatDate(date) {
    return this.datepipe.transform(date, 'dd-MM-yyyy');
  }

  onAssignmentSave = new EventEmitter();

  save() {
    if (this.form.invalid) {
      return;
    }
    this.onAssignmentSave.emit(this);
  }

  close() {
    this.dialogRef.close();
  }
}
