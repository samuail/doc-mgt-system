import { Component, OnInit, ViewChild, Inject, OnDestroy } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { MatTableDataSource, MatTable } from "@angular/material/table";
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from "@angular/animations";
import { Router } from "@angular/router";
import { Subscription } from "rxjs";
import { first } from "rxjs/operators";
import { DatePipe } from '@angular/common';

import { Message } from "src/app/_models/_helpers/message";
import { User } from "src/app/_models/user";
import { CorrespondenceAssignment } from "src/app/_models/correspondence-assignment";
import { UserDepartmentRole } from "src/app/_models/user-department-role";
import { DepartmentRole } from "src/app/_models/department-role"
import { DepartmentItem } from "src/app/_models/department-item";

import { InternalCorrespondence } from "src/app/_models/internal-correspondence";
import { ExternalIncomingCorrespondence } from "src/app/_models/external-incoming-correspondence";


import { CorrespondenceAssignmentService } from "src/app/_services/correspondence-assignment.service";
import { UserService } from "src/app/_services/user.service";
import { AuthenticationService } from "src/app/_services/authentication.service";
import { InfoDialogService } from "src/app/_services/info-dialog.service";
import { DateFormatterService } from "src/app/_services/date-formatter.service";

@Component({
  selector: "app-assign-correspondence",
  templateUrl: "./assign-correspondence.component.html",
  styleUrls: ["./assign-correspondence.component.css"],
  animations: [
    trigger("detailExpand", [
      state(
        "collapsed",
        style({ height: "0px", minHeight: "0", display: "none" })
      ),
      state("expanded", style({ height: "*" })),
      transition(
        "expanded <=> collapsed",
        animate("225ms cubic-bezier(0.4, 0.0, 0.2, 1)")
      ),
    ]),
  ],
  providers:[DatePipe],
})
export class AssignCorrespondenceComponent implements OnInit, OnDestroy {
  subManager = new Subscription();

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatPaginator) set matPaginator(mp: MatPaginator) {
    this.paginator = mp;
    this.setDataSourceAttributes();
  }

  setDataSourceAttributes() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
  
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatTable) table: MatTable<any>;

  title: string;
  direction: string;
  request_type: string;
  correspondence: any;
  message: Message = new Message();

  correspondenceAssignments: CorrespondenceAssignment[] = [];
  correspondenceAssignment: CorrespondenceAssignment = new CorrespondenceAssignment();

  users: User[] = [];
  user: User = new User();

  userDepartment: DepartmentRole = new DepartmentRole();
  assignedUserDepartment: UserDepartmentRole = new UserDepartmentRole();

  dataSource = new MatTableDataSource<CorrespondenceAssignment>();
  displayedColumns = ["order", "assignedBy", "assignedTo", "assignedDate", "operations"];

  expandedCorrespondenceAssignment: CorrespondenceAssignment | null;

  loading = false;
  resultsLength = 0;

  routeActions: string[] = [];
  currentUser: User;

  departmentItems: DepartmentItem[] = [];

  canAdd: boolean = false;
  canAddWithin: boolean = false;
  canAddAll: boolean = false;
  canEdit: boolean = false;
  canDelete: boolean = false;
  canPreview: boolean = false;
  canAccept: boolean = false;
  canReject: boolean = false;

  accRejAssigned: boolean = false;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private router: Router,
    public datepipe: DatePipe,
    private correspondenceAssignmentService: CorrespondenceAssignmentService,
    private dateFormatterService: DateFormatterService,
    private userService: UserService,
    private authenticationService: AuthenticationService,
    public infoDialogService: InfoDialogService,
    private dialogRef: MatDialogRef<AssignCorrespondenceComponent>
  ) {
    this.authenticationService.currentUser.subscribe(
      (x) => (this.currentUser = x)
    );

    this.authenticationService.userDepartment.subscribe(
      (userDepartment) => (this.userDepartment = userDepartment)
    );

    this.title = data.title;
    this.routeActions = data.routeActions;
    this.departmentItems = data.departmentItems;
    if (data.type == "Center" || data.type == "College" || data.type == "Outgoing Internal") {
      this.correspondence = new InternalCorrespondence();
      this.correspondence = data.correspondence;
      this.direction = "internal";
    } else if (data.direction == "incoming") {
      this.correspondence = new ExternalIncomingCorrespondence();
      this.correspondence = data.correspondence;
      this.direction = "external";
      this.request_type = data.direction;
    }
    for (var action of this.routeActions) {
      if (action == "Route Add") {
        this.canAdd = true;
      } else if (action == "Route Add Within") {
        this.canAddWithin = true;
      } else if (action == "Route Add All") {
        this.canAddAll = true;
      } else if (action == "Route Edit") {
        this.canEdit = true;
      } else if (action == "Route Delete") {
        this.canDelete = true;
      } else if (action == "Route Preview") {
        this.canPreview = true;
      } else if (action == "Route Accept") {
        this.canAccept = true;
      } else if (action == "Route Reject") {
        this.canReject = true;
      }
    }    
  }

  ngOnInit() {
    this.getRoutingDetails();
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngOnDestroy() {
    this.subManager.unsubscribe();
  }

  assignCorrespondenceAssignmentValue (value) {
    let correspondenceAssignment: CorrespondenceAssignment = new CorrespondenceAssignment();
    correspondenceAssignment = {
          id: value["id"],
          fromUser: value["from_user"],
          toUser: value["to_user"],
          correspondence: value["correspondence"],
          order: value["order"],
          assignedDate: value["assigned_date"],
          receivedDate: value["received_date"],
          status: value["status"],
          rejectionRemark: value["rejection_remark"]
        };
    return correspondenceAssignment;
  }

  getRoutingDetails(): void {
    this.loading = true;
    let getRoutingDetails = this.correspondenceAssignmentService
      .getAll(this.correspondence.id, this.direction, this.request_type)
      .pipe(first())
      .subscribe(
        (response) => {
          if (response.success) {
            for (var value of response.data) {                           
              this.correspondenceAssignment = this.assignCorrespondenceAssignmentValue(value);
              this.correspondenceAssignments.push(
                this.correspondenceAssignment
              );              
            }
            if (this.correspondenceAssignments.length > 0) {          
              var currentAssignments = this.correspondenceAssignments;
              var lastAssignment = currentAssignments.sort((a,b) => (a.order < b.order ? 1 : -1))[0];
              if (this.currentUser.id == lastAssignment["toUser"]["id"]) {
                if (lastAssignment["status"] == "Accepted"){
                  this.accRejAssigned = true;
                }
              } else if (this.currentUser.id == lastAssignment["fromUser"]["id"]) {
                this.accRejAssigned = true;
              }
            } else {
              this.accRejAssigned = true;
            }
            this.loading = false;
            this.resultsLength = response.total;
            this.dataSource.data = this.correspondenceAssignments;            
          } else {
            this.message.title = "Correspondence Assignments";
            this.message.type = "error";
            this.message.content = response.errors;
            const infoDialogRef = this.infoDialogService.openInfoDialog(
              this.message
            );
            infoDialogRef.afterClosed().subscribe(() => {
              if (response.errors[0] == "Signature has expired") {
                this.dialogRef.close();
                this.authenticationService.logout_offline();
                this.router.navigate(["/login"]);
              }
            });            
          }
        },
        (error) => {
          this.loading = false;
          this.infoDialogService.openInfoDialog(error);
        }
      );
    this.subManager.add(getRoutingDetails);
  }

  assignCorrespondence(type) {
    let department_id = type == "Within" ? this.userDepartment.department_id : null;

    this.title = "Add Assignment";
    const dialogRef = this.correspondenceAssignmentService.openAssignmentDialog(
      this.title,
      department_id,
      this.departmentItems,
      null,
      null,
      type
    );

    const sub = dialogRef.componentInstance.onAssignmentSave
        .subscribe(
          (data) => {
            if(data) {
              data.loading = true;
              let to_user_id = data.form.value.assignedTo;
              let create = this.correspondenceAssignmentService
                  .create(to_user_id, this.correspondence.id, this.direction, this.request_type)
                    .pipe(first())
                    .subscribe(
                      (response) => {
                        if(response.success) {
                          this.message.title = this.title;
                          this.message.content = [response.message];
                          this.message.type = "success";
                          const infoDialogRef = this.infoDialogService.openInfoDialog(
                            this.message
                          );
                          infoDialogRef.afterClosed().subscribe(() => {
                            this.correspondenceAssignment = this.assignCorrespondenceAssignmentValue(response.data);
                            this.correspondenceAssignments.push(
                              this.correspondenceAssignment
                            );
                            this.resultsLength = this.resultsLength + 1;
                            this.dataSource = new MatTableDataSource(
                              this.correspondenceAssignments
                            );
                            this.dataSource.paginator = this.paginator;
                            this.dataSource.sort = this.sort;
                            data.close();
                          });
                        } else {
                          this.message.title = "Assign to User";
                          this.message.type = "error";
                          this.message.content = response.errors;
                          const infoDialogRef = this.infoDialogService.openInfoDialog(
                            this.message
                          );
                          infoDialogRef.afterClosed().subscribe(() => {
                            data.loading = false;
                            if (response.errors[0] == "Signature has expired") {
                              this.authenticationService.logout_offline();
                              this.router.navigate(["/login"]);
                            }
                          });
                        }
                      },
                      (error) => {
                        data.loading = false;
                        this.infoDialogService.openInfoDialog(error);
                      }
                    );
              this.subManager.add(create);
            }
          }          
        );
    dialogRef.afterClosed().subscribe(() => {
      sub.unsubscribe();
    });
  }

  getAssignedUsersDepartment(assignment, type){
    let getUserDepartments = this.userService
      .getUserDepartments(assignment.toUser.id)
      .pipe(first())
      .subscribe(
        (response) => {
          if (response.success) {
            this.assignedUserDepartment = response.data;
            let department_id = this.correspondence.destination.id;
            this.title = "Edit Assignment";
            const dialogRef = this.correspondenceAssignmentService.openAssignmentDialog(
              this.title,
              department_id,
              this.departmentItems,
              assignment,
              this.assignedUserDepartment,
              type
            );
            
            const sub = dialogRef.componentInstance.onAssignmentSave
                .subscribe(
                  (data) => {
                    if(data) {
                      data.loading = true;
                      let to_user_id = data.form.value.assignedTo;
                      let create = this.correspondenceAssignmentService
                          .assignToOtherUser(to_user_id, assignment, this.direction, this.request_type)
                            .pipe(first())
                            .subscribe(
                              (response) => {
                                if(response.success) {
                                  this.message.title = this.title;
                                  this.message.content = [response.message];
                                  this.message.type = "success";
                                  const infoDialogRef = this.infoDialogService.openInfoDialog(
                                    this.message
                                  );
                                  infoDialogRef.afterClosed().subscribe(() => {
                                    this.correspondenceAssignments = this.correspondenceAssignments.filter(
                                      (item) => item.id !== assignment.id
                                    );
                                    this.correspondenceAssignment = this.assignCorrespondenceAssignmentValue(response.data);                            
                                    this.correspondenceAssignments.push(
                                      this.correspondenceAssignment
                                    );
                                    this.resultsLength = this.resultsLength;
                                    this.dataSource = new MatTableDataSource(
                                      this.correspondenceAssignments
                                    );
                                    this.dataSource.paginator = this.paginator;
                                    this.dataSource.sort = this.sort;
                                    this.table.renderRows();
                                    data.close();
                                  });
                                } else {
                                  this.message.title = "Edit Assigned User";
                                  this.message.type = "error";
                                  this.message.content = response.errors;
                                  const infoDialogRef = this.infoDialogService.openInfoDialog(
                                    this.message
                                  );
                                  infoDialogRef.afterClosed().subscribe(() => {
                                    data.loading = false;
                                    if (response.errors[0] == "Signature has expired") {
                                      this.authenticationService.logout_offline();
                                      this.router.navigate(["/login"]);
                                    }
                                  });
                                }
                              },
                              (error) => {
                                data.loading = false;
                                this.infoDialogService.openInfoDialog(error);
                              }
                            );
                      this.subManager.add(create);
                    }
                  }          
                );
            dialogRef.afterClosed().subscribe(() => {
              sub.unsubscribe();
            });
          } else {
            this.message.title = "Get Assigned User Department";
            this.message.type = "error";
            this.message.content = response.errors;
            const infoDialogRef = this.infoDialogService.openInfoDialog(
              this.message
            );
            infoDialogRef.afterClosed().subscribe(() => {
              if (response.errors[0] == "Signature has expired") {
                this.authenticationService.logout_offline();
                this.router.navigate(["/login"]);
              }
            });
          }
        },
        (error) => {
          this.infoDialogService.openInfoDialog(error);
        }
      );
    this.subManager.add(getUserDepartments);
  }

  editAssignedCorrespondence(assignment, type) {
    this.getAssignedUsersDepartment(assignment, type);
  }

  previewAssignment() {
    this.title = "Preview Assignment";
    const dialogRef = this.correspondenceAssignmentService.openAssignmentPreviewDialog(
      this.title,
      this.correspondenceAssignments.sort((a,b) => (a.order < b.order ? -1 : 1))
    );
  }

  previewAllAssignmentInfo(assignment) {
    this.title = "Preview Current";
    const dialogRef = this.correspondenceAssignmentService.openCurrentAssignmentPreviewDialog(
      this.title,
      assignment
    );
  }

  acceptOrRejectAssignedCorrespondence(assignment) {
    this.title = "Accept or Reject Assignment";
    const dialogRef = this.correspondenceAssignmentService.openCurrentAssignmentPreviewDialog(
      this.title,
      assignment
    );

    const sub = dialogRef.componentInstance.onAssignmentSave
        .subscribe(
          (data) => {
            if(data) {
              data.loading = true;
              let status = data.form.value.acceptReject;
              let remark = data.form.value.rejectionRemark;
              let receivedDate = this.datepipe.transform(new Date(), 'dd-MM-yyyy, h:mm:ss a');
              let create = this.correspondenceAssignmentService
                  .receiveAssignment(status, receivedDate, remark, assignment, this.direction, this.request_type)
                    .pipe(first())
                    .subscribe(
                      (response) => {
                        if(response.success) {
                          this.message.title = this.title;
                          this.message.content = [response.message];
                          this.message.type = "success";
                          const infoDialogRef = this.infoDialogService.openInfoDialog(
                            this.message
                          );
                          infoDialogRef.afterClosed().subscribe(() => {
                            this.accRejAssigned = true;
                            this.correspondenceAssignments = this.correspondenceAssignments.filter(
                              (item) => item.id !== assignment.id
                            );
                            this.correspondenceAssignment = this.assignCorrespondenceAssignmentValue(response.data);                            
                            this.correspondenceAssignments.push(
                              this.correspondenceAssignment
                            );
                            this.resultsLength = this.resultsLength;
                            this.dataSource = new MatTableDataSource(
                              this.correspondenceAssignments
                            );
                            this.dataSource.paginator = this.paginator;
                            this.dataSource.sort = this.sort;
                            this.table.renderRows();
                            data.close();
                          });
                        } else {
                          this.message.title = this.title;
                          this.message.type = "error";
                          this.message.content = response.errors;
                          const infoDialogRef = this.infoDialogService.openInfoDialog(
                            this.message
                          );
                          infoDialogRef.afterClosed().subscribe(() => {
                            data.loading = false;
                            if (response.errors[0] == "Signature has expired") {
                              this.authenticationService.logout_offline();
                              this.router.navigate(["/login"]);
                            }
                          });
                        }
                      },
                      (error) => {
                        data.loading = false;
                        this.infoDialogService.openInfoDialog(error);
                      }
                    );
              this.subManager.add(create);
            }
          }          
        );
    dialogRef.afterClosed().subscribe(() => {
      sub.unsubscribe();
    });
  }

  deleteAssignedCorrespondence(assignment) {    
  }  
}
