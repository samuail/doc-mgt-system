import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignCorrespondenceComponent } from './assign-correspondence.component';

describe('AssignCorrespondenceComponent', () => {
  let component: AssignCorrespondenceComponent;
  let fixture: ComponentFixture<AssignCorrespondenceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssignCorrespondenceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignCorrespondenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
