import { Component, OnInit, Inject, EventEmitter, OnDestroy } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Subscription } from "rxjs";
import { Router } from "@angular/router";

import { DepartmentService } from "src/app/_services/department.service";
import { AuthenticationService } from "src/app/_services/authentication.service";
import { InfoDialogService } from "src/app/_services/info-dialog.service";


import { DepartmentItem } from "src/app/_models/department-item";
import { Department } from "src/app/_models/department";
import { DepartmentRole } from "src/app/_models/department-role";
import { CorrespondenceCarbonCopy } from 'src/app/_models/correspondence-carbon-copy';
@Component({
  selector: 'app-cc-dialog',
  templateUrl: './cc-dialog.component.html',
  styleUrls: ['./cc-dialog.component.css']
})
export class CcDialogComponent implements OnInit, OnDestroy {
  subManager = new Subscription();
  
  title: string;
  loading = false;
  departmentLoading = false;
  
  ccForm: FormGroup;

  isCenter: boolean = false;
  isCollege: boolean = false;
  isOutgoingInternal: boolean = true;

  departmentItems: DepartmentItem[] = [];
  collegeDepartmentItems: DepartmentItem[] = [];

  department: Department = new Department();
  departments: Department[] = [];
  subDepartment: Department = new Department();
  subDepartments: Department[] = [];

  userDepartment: DepartmentRole = new DepartmentRole();
  correspondenceCarbonCopy: CorrespondenceCarbonCopy;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<CcDialogComponent>,
    private departmentService: DepartmentService,
    private router: Router,
    private authenticationService: AuthenticationService,
    public infoDialogService: InfoDialogService) {
    
    this.title = data.title;
    this.departmentItems = data.departmentItems;
    this.correspondenceCarbonCopy = data.correspondenceCc;
    
    this.authenticationService.userDepartment.subscribe(
      (userDepartment) => (this.userDepartment = userDepartment)
    );
  }

  ngOnInit() {
    if (this.title == "Add Cc") {
      this.ccForm = this.fb.group({
        from0: ["", [Validators.required]],
        from1: ["", [Validators.required]],
        from: ["", [Validators.required]]
      });
    } else if (this.title == "Edit Cc"){
      var from_id = this.correspondenceCarbonCopy.destination.id;
      var from0_id;
      var from1_id;

      for (var level1 of this.departmentItems) {
        for (var level2 of level1["children"]) {
          for (var level3 of level2["children"]) {
            if (level3.id == from_id) {
              from0_id = level1.id;
              from1_id = level2.id;
            }
          }
        }
      }

      this.onCollegeSelect(from0_id);
      this.onDepartmentSelect(from1_id);

      this.ccForm = this.fb.group({
        from0: [from0_id, [Validators.required]],
        from1: [from1_id, [Validators.required]],
        from: [from_id, [Validators.required]]
      });
    }
  }

  ngOnDestroy() {
    this.subManager.unsubscribe();
  }

  onCorrespondenceCcSave = new EventEmitter();

  save() {
    if (this.ccForm.invalid) {
      return;
    }
    this.onCorrespondenceCcSave.emit(this);
  }

  close() {
    this.dialogRef.close();
  }

  onCollegeSelect(deptId) {
    this.departments = [];
    this.collegeDepartmentItems = [];
    if (this.departmentItems != null) {
      let college = this.departmentItems.filter( (dept) => dept.id == deptId )[0];
      if (college["description"] == "Main Campus"){
        this.isCenter = true;
        this.isCollege = false;
        this.isOutgoingInternal = false;
      } else {
        this.isCenter = false;
        this.isCollege = true;
        this.isOutgoingInternal = false;
      }
      this.collegeDepartmentItems = college["children"];
      for (var value of this.collegeDepartmentItems) {
        this.department = {
          id: value["id"],
          code: value["code"],
          description: value["description"],
          department_type: value["department_type"],
        };
        this.departments.push(this.department);
      }
    }    
  }

  onDepartmentSelect(deptId) {
    this.subDepartments = [];
    if (this.collegeDepartmentItems.length > 0) {
      let department = this.collegeDepartmentItems.filter( (dept) => dept.id == deptId )[0]["children"];
      department = department.filter((dept) => dept.id != this.userDepartment.department_id)
      for (var value of department) {
        this.subDepartment = {
          id: value["id"],
          code: value["code"],
          description: value["description"],
          department_type: value["department_type"],
        };
        this.subDepartments.push(this.subDepartment);
      }
    }
  }

  departmentsLevel1(level3_id) {
    var level1_name = "";
    for (var level1 of this.departmentItems) {
      for (var level2 of level1["children"]) {
        for (var level3 of level2["children"]) {
          if (level3.id == level3_id) {
            level1_name = level1.description;
          }
        }
      }
    }
    return level1_name;
  }

  departmentsLevel2(level3_id) {
    var level2_name = "";
    for (var level1 of this.departmentItems) {
      for (var level2 of level1["children"]) {
        for (var level3 of level2["children"]) {
          if (level3.id == level3_id) {
            level2_name = level2.description;
          }
        }
      }
    }
    return level2_name;
  }

}
