import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocCcComponent } from './doc-cc.component';

describe('DocCcComponent', () => {
  let component: DocCcComponent;
  let fixture: ComponentFixture<DocCcComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocCcComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocCcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
