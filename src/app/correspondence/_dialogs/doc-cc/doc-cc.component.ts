import { Component, OnInit, Inject, ViewChild, OnDestroy } from '@angular/core';
import { MAT_DIALOG_DATA } from "@angular/material/dialog";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { MatTableDataSource, MatTable } from "@angular/material/table";
import { first } from "rxjs/operators";
import { Subscription } from "rxjs";
import { Router } from "@angular/router";

import { CorrespondenceCcService } from "src/app/_services/correspondence-cc.service";
import { AuthenticationService } from "src/app/_services/authentication.service";
import { InfoDialogService } from "src/app/_services/info-dialog.service";

import { Message } from "src/app/_models/_helpers/message";
import { DepartmentItem } from "src/app/_models/department-item";
import { ExternalIncomingCorrespondence } from "src/app/_models/external-incoming-correspondence";
import { InternalCorrespondence } from "src/app/_models/internal-correspondence";
import { ExternalOutgoingCorrespondence } from "src/app/_models/external-outgoing-correspondence";
import { CorrespondenceCarbonCopy } from "src/app/_models/correspondence-carbon-copy";

@Component({
  selector: 'app-doc-cc',
  templateUrl: './doc-cc.component.html',
  styleUrls: ['./doc-cc.component.css']
})
export class DocCcComponent implements OnInit, OnDestroy {
  subManager = new Subscription();

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatTable) table: MatTable<any>;

  title: string;
  direction: string;
  request_type: string;
  correspondence: any;
  message: Message = new Message(); 

  loading: boolean = false;
  resultsLength: number = 0;  

  uploadActions: string[] = [];

  canAddCc: boolean = true;
  canEditCc: boolean = false;
  canDeleteCc: boolean = false;
  
  departmentItems: DepartmentItem[] = [];
  correspondenceCarbonCopy: CorrespondenceCarbonCopy = new CorrespondenceCarbonCopy();
  correspondenceCarbonCopies: CorrespondenceCarbonCopy [] = [];

  docCcDataSource = new MatTableDataSource<CorrespondenceCarbonCopy>();
  displayedColumns = ["order", "centerCollege", "presidentDepartments", "offices", "operations"];

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
    private router: Router,
    private correspondenceCcService: CorrespondenceCcService,
    private authenticationService: AuthenticationService,   
    public infoDialogService: InfoDialogService) {
    this.canAddCc = data.canAdd;
    this.canEditCc = data.canEdit;
    this.canDeleteCc = data.canDelete;
    this.title = data.title;
    this.uploadActions = data.uploadActions;
    this.departmentItems = data.departmentItems;
    if (data.type == "Center" || data.type == "College" || data.type == "Outgoing Internal") {
      this.correspondence = new InternalCorrespondence();
      this.correspondence = data.correspondence;
      this.direction = "internal";
    } else if (data.direction == "incoming") {
      this.correspondence = new ExternalIncomingCorrespondence();
      this.correspondence = data.correspondence;
      this.direction = "external";
      this.request_type = data.direction;
    } else if (data.direction == "outgoing") {
      this.correspondence = new ExternalOutgoingCorrespondence();
      this.correspondence = data.correspondence;
      this.direction = "external";
      this.request_type = data.direction;
    }
  }

  ngOnInit() {
    this.initDocCc();
  }

  ngAfterViewInit() {
    this.docCcDataSource.paginator = this.paginator;
    this.docCcDataSource.sort = this.sort;
  }

  ngOnDestroy() {
    this.subManager.unsubscribe();
  }

  initDocCc(){
    this.loading = true;
    let getAll = this.correspondenceCcService
      .getAll(this.correspondence.id, this.direction, this.request_type)
      .pipe(first())
      .subscribe(
        (response) => {
          if(response.success) {
            for(var value of response.data) {
              this.correspondenceCarbonCopy = {
                id: value["id"],                              
                correspondence: value["correspondence"],
                destination: value["destination"]
              };                            
              this.correspondenceCarbonCopies.push(
                this.correspondenceCarbonCopy
              );              
            }
            this.loading = false;
            this.resultsLength = response.total;
            this.docCcDataSource.data = this.correspondenceCarbonCopies;
          } else {
            this.message.title = "Document Ccs";
            this.message.type = "error";
            this.message.content = response.errors;
            const infoDialogRef = this.infoDialogService.openInfoDialog(
              this.message
            );
            infoDialogRef.afterClosed().subscribe(() => {
              this.loading = false;
              if (response.errors[0] == "Signature has expired") {
                this.authenticationService.logout_offline();
                this.router.navigate(["/login"]);
              }
            });
          }
        },
        (error) => {
          this.loading = false;
          this.infoDialogService.openInfoDialog(error);
        }
      );
    this.subManager.add(getAll);
  }

  departmentsLevel1(level3_id) {
    var level1_name = "";
    for (var level1 of this.departmentItems) {
      for (var level2 of level1["children"]) {
        for (var level3 of level2["children"]) {
          if (level3.id == level3_id) {
            level1_name = level1.code;
          }
        }
      }
    }
    return level1_name;
  }

  departmentsLevel2(level3_id) {
    var level2_name = "";
    for (var level1 of this.departmentItems) {
      for (var level2 of level1["children"]) {
        for (var level3 of level2["children"]) {
          if (level3.id == level3_id) {
            level2_name = level2.code;
          }
        }
      }
    }
    return level2_name;
  }

  addCc() {
    this.title = "Add Cc";
    const dialogRef = this.correspondenceCcService.openCcDialog(
      this.title,
      this.departmentItems
    );  
    
    const sub = dialogRef.componentInstance.onCorrespondenceCcSave
      .subscribe(
        (data) => {
          if(data) {
            data.loading = true;
            let formData = data.ccForm.value;
            let create = this.correspondenceCcService
              .create(formData, this.correspondence.id,
                    this.direction, this.request_type)
                  .pipe(first())
                  .subscribe(
                    (response) => {
                      if(response.success){
                        this.message.title = this.title;
                        this.message.content = [response.message];
                        this.message.type = "success";
                        const infoDialogRef = this.infoDialogService.openInfoDialog(
                          this.message
                        );
                        infoDialogRef.afterClosed().subscribe(() => {
                          this.correspondenceCarbonCopy = {
                            id: response.data["id"],                              
                            correspondence: response.data["correspondence"],
                            destination: response.data["destination"]
                          };                            
                          this.correspondenceCarbonCopies.splice(
                            0, 0, this.correspondenceCarbonCopy
                          );
                          this.resultsLength = this.resultsLength + 1;
                          this.docCcDataSource = new MatTableDataSource(
                            this.correspondenceCarbonCopies
                          );
                          this.docCcDataSource.paginator = this.paginator;
                          this.docCcDataSource.sort = this.sort;
                          data.close();
                        });
                      } else {
                        this.message.title = this.title;
                        this.message.type = "error";
                        this.message.content = response.errors;
                        const infoDialogRef = this.infoDialogService.openInfoDialog(
                          this.message
                        );
                        infoDialogRef.afterClosed().subscribe(() => {
                          data.loading = false;
                          if (response.errors[0] == "Signature has expired") {
                            this.authenticationService.logout_offline();
                            this.router.navigate(["/login"]);
                          }
                        });
                      }
                    },
                    (error) => {
                      data.loading = false;
                      this.infoDialogService.openInfoDialog(error);
                    }
                  );
            this.subManager.add(create);
          }
        }
      );
  }

  editCc(correspondenceCc) {
    this.title = "Edit Cc";
    const dialogRef = this.correspondenceCcService.openCcDialog(
      this.title,  
      this.departmentItems,
      correspondenceCc
    );

    const sub = dialogRef.componentInstance.onCorrespondenceCcSave
      .subscribe(
        (data) => {
          if(data) {
            data.loading = true;
            let formData = data.ccForm.value;
            let update = this.correspondenceCcService
              .update(formData, correspondenceCc.id,
                this.direction, this.request_type)
                  .pipe(first())
                  .subscribe(
                    (response) => {
                      if(response.success){
                        this.message.title = this.title;
                        this.message.content = [response.message];
                        this.message.type = "success";
                        const infoDialogRef = this.infoDialogService.openInfoDialog(
                          this.message
                        );
                        infoDialogRef.afterClosed().subscribe(() => {
                          let index = this.correspondenceCarbonCopies.findIndex(x => x.id === correspondenceCc.id);
                          this.correspondenceCarbonCopy = {
                            id: response.data["id"],                              
                            correspondence: response.data["correspondence"],
                            destination: response.data["destination"]
                          };                            
                          this.correspondenceCarbonCopies.splice(
                            index, 1, this.correspondenceCarbonCopy
                          );
                          this.resultsLength = this.resultsLength;
                          this.docCcDataSource = new MatTableDataSource(
                            this.correspondenceCarbonCopies
                          );
                          this.docCcDataSource.paginator = this.paginator;
                          this.docCcDataSource.sort = this.sort;
                          this.table.renderRows();
                          data.close();
                        });
                      } else {
                        this.message.title = this.title;
                        this.message.type = "error";
                        this.message.content = response.errors;
                        const infoDialogRef = this.infoDialogService.openInfoDialog(
                          this.message
                        );
                        infoDialogRef.afterClosed().subscribe(() => {
                          data.loading = false;
                          if (response.errors[0] == "Signature has expired") {
                            this.authenticationService.logout_offline();
                            this.router.navigate(["/login"]);
                          }
                        });
                      }
                    },
                    (error) => {
                      data.loading = false;
                      this.infoDialogService.openInfoDialog(error);
                    }
                  );
            this.subManager.add(update);
          }
        }
      );
  }

  previewCc(correspondenceCc) {
    this.title = "Preview Cc";
    const dialogRef = this.correspondenceCcService.openCcDialog(
      this.title,  
      this.departmentItems,
      correspondenceCc
    );
  }

  deleteCc(correspondenceCc) {}

}
