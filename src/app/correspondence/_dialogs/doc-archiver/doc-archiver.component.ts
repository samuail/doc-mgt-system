import { Component, OnInit, Inject, OnDestroy, EventEmitter } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Subscription } from "rxjs";
import { TranslateService } from '@ngx-translate/core';

import { InfoDialogService } from "src/app/_services/info-dialog.service";
import { InternalCorrespondence } from "src/app/_models/internal-correspondence";
import { ExternalIncomingCorrespondence } from "src/app/_models/external-incoming-correspondence";

@Component({
  selector: 'app-doc-archiver',
  templateUrl: './doc-archiver.component.html',
  styleUrls: ['./doc-archiver.component.css']
})
export class DocArchiverComponent implements OnInit, OnDestroy {

  subManager = new Subscription();

  loading = false;
  title: string;
  direction: string;
  request_type: string;
  correspondence: any;

  archiverForm: FormGroup;

  clickIfRequiredLabel: string;
  clickIfOptionalLabel: string;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
              private fb: FormBuilder,
              private dialogRef: MatDialogRef<DocArchiverComponent>,
              public infoDialogService: InfoDialogService,
              private translate: TranslateService) {
    this.title = data.title;
    if (data.type == "Center" || data.type == "College" || data.type == "Outgoing Internal") {
      this.correspondence = new InternalCorrespondence();
      this.correspondence = data.correspondence;
      this.direction = "internal";
    } else if (data.direction == "incoming") {
      this.correspondence = new ExternalIncomingCorrespondence();
      this.correspondence = data.correspondence;
      this.direction = "external";
      this.request_type = data.direction;
    } 
  }

  ngOnInit() {
    this.translate.get('crMgt.archive.clickIfOptional').subscribe((res: string) => {
      this.clickIfOptionalLabel = res;        
    });

    this.translate.get('crMgt.archive.clickIfRequired').subscribe((res: string) => {
      this.clickIfRequiredLabel = res;        
    });

    this.archiverForm = this.fb.group({
      docReferenceNo: ["", Validators.required],
      requiredDocReferenceNo: [true, null],
      amount: ["", Validators.required],
      requiredAmount: [true, null],
      noDocuments: ["", Validators.required],
      remark: ["", Validators.required]    
    });
  }

  ngOnDestroy() {
    this.subManager.unsubscribe();
  }

  requiredOptionReferenceNo(event) {
    let option = event.checked;
    if (option) {
      this.archiverForm.get("docReferenceNo").setValidators([Validators.required]);
      this.archiverForm.get('docReferenceNo').updateValueAndValidity();
    } else {
      this.archiverForm.get("docReferenceNo").clearValidators();
      this.archiverForm.get('docReferenceNo').updateValueAndValidity();
    }
  }

  requiredOptionAmount(event) {
    let option = event.checked;
    if (option) {
      this.archiverForm.get("amount").setValidators([Validators.required]);
      this.archiverForm.get('amount').updateValueAndValidity();
    } else {
      this.archiverForm.get("amount").clearValidators();
      this.archiverForm.get('amount').updateValueAndValidity();
    }
  }

  isDocReferenceNoChecked(comp) {
    return comp.archiverForm.get("requiredDocReferenceNo").value;
  }

  isAmountChecked(comp) {
    return comp.archiverForm.get("requiredAmount").value;
  }

  onCorrespondenceSave = new EventEmitter();

  save() {
    if (this.archiverForm.invalid) {
      return;
    }
    this.onCorrespondenceSave.emit(this);
  }

  close() {
    this.dialogRef.close();
  }

}
