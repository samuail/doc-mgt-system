import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocArchiverComponent } from './doc-archiver.component';

describe('DocArchiverComponent', () => {
  let component: DocArchiverComponent;
  let fixture: ComponentFixture<DocArchiverComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocArchiverComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocArchiverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
