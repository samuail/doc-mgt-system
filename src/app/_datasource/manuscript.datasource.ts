import { CollectionViewer, DataSource } from "@angular/cdk/collections";
import { Router } from "@angular/router";
import { Observable, BehaviorSubject, of } from "rxjs";
import { catchError, finalize } from "rxjs/operators";

import { Manuscript } from "src/app/_models/manuscript";
import { Message } from "src/app/_models/_helpers/message";

import { ManuscriptService } from "src/app/_services/manuscript.service";
import { InfoDialogService } from "src/app/_services/info-dialog.service";
import { AuthenticationService } from "src/app/_services/authentication.service";

export class ManuscriptDataSource implements DataSource<Manuscript> {
    private internalSubject = new BehaviorSubject<Manuscript[]>([]);

    private loadingSubject = new BehaviorSubject<boolean>(false);
    public loading$ = this.loadingSubject.asObservable();

    private resultsLengthSubject = new BehaviorSubject<number>(0);
    public resultsLength$ = this.resultsLengthSubject.asObservable();

    message: Message = new Message();

    constructor(private manuscriptService: ManuscriptService,
        private router: Router,
        private infoDialogService: InfoDialogService,
        private authenticationService: AuthenticationService) {

    }

    loadManuscript(dataSize: string,
                filter: string,
                sortDirection: string,
                pageIndex: number,
                pageSize: number) {

        this.loadingSubject.next(true);

        this.manuscriptService.getManuscript(dataSize, filter, sortDirection,
            pageIndex, pageSize).pipe(
                catchError((error) => of(error)),
                finalize(() => this.loadingSubject.next(false))
            )
            .subscribe(response => {
                if (response.success) {
                    let manuscripts: Manuscript[] = [];
                    for (var value of response["data"]) {
                        manuscripts.push (this.manuscriptService.assignManuscriptValue(value));
                    }                
                    this.internalSubject.next(manuscripts);
                    this.resultsLengthSubject.next(response["total"]);
                } else {
                    this.message.title = "Load Data";
                    this.message.type = "error";
                    this.message.content = response.errors;
                    const infoDialogRef = this.infoDialogService.openInfoDialog(
                        this.message
                    );
                    infoDialogRef.afterClosed().subscribe(() => {                        
                        if (response.errors[0] == "Signature has expired") {
                            this.authenticationService.logout_offline();
                            this.router.navigate(["/login"]);
                        }
                    });
                }
            },
            error => {
                this.infoDialogService.openInfoDialog(error);
            });

    }

    connect(collectionViewer: CollectionViewer): Observable<Manuscript[]> {        
        return this.internalSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.internalSubject.complete();
        this.loadingSubject.complete();
    }

}