import { CollectionViewer, DataSource } from "@angular/cdk/collections";
import { Router } from "@angular/router";
import { Observable, BehaviorSubject, of } from "rxjs";
import { catchError, finalize } from "rxjs/operators";

import { ExternalIncomingCorrespondence } from "src/app/_models/external-incoming-correspondence";
import { Message } from "src/app/_models/_helpers/message";

import { CorrespondenceService } from "src/app/_services/correspondence.service";
import { InfoDialogService } from "src/app/_services/info-dialog.service";
import { AuthenticationService } from "src/app/_services/authentication.service";

export class ExternalIncomingsDataSource implements DataSource<ExternalIncomingCorrespondence> {
    private externalIncomingsSubject = new BehaviorSubject<ExternalIncomingCorrespondence[]>([]);

    private loadingSubject = new BehaviorSubject<boolean>(false);
    public loading$ = this.loadingSubject.asObservable();

    private resultsLengthSubject = new BehaviorSubject<number>(0);
    public resultsLength$ = this.resultsLengthSubject.asObservable();

    message: Message = new Message();

    constructor(private correspondenceService: CorrespondenceService,
        private router: Router,
        private infoDialogService: InfoDialogService,
        private authenticationService: AuthenticationService) {

    }

    loadExternalIncomings(dataSize: string,
                requestType: string,             
                filter: string,
                sortDirection: string,
                pageIndex: number,
                pageSize: number) {

        this.loadingSubject.next(true);

        this.correspondenceService.getExternal(dataSize, requestType, filter, sortDirection,
            pageIndex, pageSize).pipe(
                catchError((error) => of(error)),
                finalize(() => this.loadingSubject.next(false))
            )
            .subscribe(response => {
                if (response.success) {
                    let externalCorrespondences: ExternalIncomingCorrespondence[] = [];
                    for (var value of response["data"]) {
                        externalCorrespondences.push (this.correspondenceService.assignExternalIncomingCorrespondenceValue(value));
                    }                
                    this.externalIncomingsSubject.next(externalCorrespondences);
                    this.resultsLengthSubject.next(response["total"]);
                } else {
                    this.message.title = "Load Data";
                    this.message.type = "error";
                    this.message.content = response.errors;
                    const infoDialogRef = this.infoDialogService.openInfoDialog(
                        this.message
                    );
                    infoDialogRef.afterClosed().subscribe(() => {                        
                        if (response.errors[0] == "Signature has expired") {
                            this.authenticationService.logout_offline();
                            this.router.navigate(["/login"]);
                        }
                    });
                }
            },
            error => {
                this.infoDialogService.openInfoDialog(error);
            });

    }

    connect(collectionViewer: CollectionViewer): Observable<ExternalIncomingCorrespondence[]> {        
        return this.externalIncomingsSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.externalIncomingsSubject.complete();
        this.loadingSubject.complete();
    }

}