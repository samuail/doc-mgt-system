import { CollectionViewer, DataSource } from "@angular/cdk/collections";
import { Router } from "@angular/router";
import { Observable, BehaviorSubject, of } from "rxjs";
import { catchError, finalize } from "rxjs/operators";

import { InternalCorrespondence } from "src/app/_models/internal-correspondence";
import { Message } from "src/app/_models/_helpers/message";

import { CorrespondenceService } from "src/app/_services/correspondence.service";
import { InfoDialogService } from "src/app/_services/info-dialog.service";
import { AuthenticationService } from "src/app/_services/authentication.service";

export class InternalDataSource implements DataSource<InternalCorrespondence> {
    private internalSubject = new BehaviorSubject<InternalCorrespondence[]>([]);

    private loadingSubject = new BehaviorSubject<boolean>(false);
    public loading$ = this.loadingSubject.asObservable();

    private resultsLengthSubject = new BehaviorSubject<number>(0);
    public resultsLength$ = this.resultsLengthSubject.asObservable();

    message: Message = new Message();

    constructor(private correspondenceService: CorrespondenceService,
        private router: Router,
        private infoDialogService: InfoDialogService,
        private authenticationService: AuthenticationService) {

    }

    loadInternal(dataSize: string,
                requestType: string, 
                departmentType: string, 
                filter: string,
                sortDirection: string,
                pageIndex: number,
                pageSize: number) {

        this.loadingSubject.next(true);

        this.correspondenceService.getInternal(dataSize, requestType, departmentType, filter, sortDirection,
            pageIndex, pageSize).pipe(
                catchError((error) => of(error)),
                finalize(() => this.loadingSubject.next(false))
            )
            .subscribe(response => {
                if (response.success) {
                    let internalCorrespondences: InternalCorrespondence[] = [];
                    for (var value of response["data"]) {
                        internalCorrespondences.push (this.correspondenceService.assignCorrespondenceValue(value));
                    }                
                    this.internalSubject.next(internalCorrespondences);
                    this.resultsLengthSubject.next(response["total"]);
                } else {
                    this.message.title = "Load Data";
                    this.message.type = "error";
                    this.message.content = response.errors;
                    const infoDialogRef = this.infoDialogService.openInfoDialog(
                        this.message
                    );
                    infoDialogRef.afterClosed().subscribe(() => {                        
                        if (response.errors[0] == "Signature has expired") {
                            this.authenticationService.logout_offline();
                            this.router.navigate(["/login"]);
                        }
                    });
                }
            },
            error => {
                this.infoDialogService.openInfoDialog(error);
            });

    }

    connect(collectionViewer: CollectionViewer): Observable<InternalCorrespondence[]> {        
        return this.internalSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.internalSubject.complete();
        this.loadingSubject.complete();
    }

}