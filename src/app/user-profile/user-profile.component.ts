import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from "@angular/router";
import { first } from 'rxjs/operators';
import { Subscription } from "rxjs";

import { AuthenticationService } from '../_services/authentication.service';
import { UserService } from '../_services/user.service';
import { InfoDialogService } from "src/app/_services/info-dialog.service";
import { User } from '../_models/user';
import { Message } from "src/app/_models/_helpers/message";


@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})

export class UserProfileComponent implements OnInit, OnDestroy {
	subManager = new Subscription();

	title: string;
	currentUser: User;
	message: Message = new Message();

	constructor(private router: Router,
				private authenticationService: AuthenticationService,
				private userService: UserService,
				public infoDialogService: InfoDialogService) {

	}

	ngOnInit() {
		this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
	}

	ngOnDestroy() {
    	this.subManager.unsubscribe();
  	}

	editUser() {
		this.userService.openUserDialog('Edit User', this.currentUser);
	}

	changePassword() {
		this.title = 'Change Password';
		const dialogRef = this.userService.openUserDialog(this.title, this.currentUser);
		const sub = dialogRef.componentInstance.onUserSave.subscribe( (data) => {
				if(data) {
        			data.loading = true;
        			let changePassword = this.userService
						.changePassword(this.currentUser.id, data.form.value)
						.pipe(first())
						.subscribe(
							(response) => {
								if (response.success) {
									this.message.title = this.title;
									this.message.content = [response.message];
									this.message.type = "success";
									const infoDialogRef = this.infoDialogService.openInfoDialog(
										this.message
									);
									infoDialogRef.afterClosed().subscribe(() => {
										data.close();
									});                  
								} else {
									this.message.title = this.title;
									this.message.type = "error";
									this.message.content = response.errors;
									const infoDialogRef = this.infoDialogService.openInfoDialog(
										this.message
									);
									infoDialogRef.afterClosed().subscribe(() => {
										data.loading = false;
										if (response.errors[0] == "Signature has expired") {
										data.close();
										this.authenticationService.logout_offline();
										this.router.navigate(["/login"]);
										}
									});								
								}
							},
							(error) => {
								data.loading = false;
								this.infoDialogService.openInfoDialog(error);
							}
						);
					this.subManager.add(changePassword);
        		}
			}
		);

		dialogRef.afterClosed().subscribe(() => {
        	sub.unsubscribe();
    	});
	}

}
